       IDENTIFICATION DIVISION.
       PROGRAM-ID.    DFSIVFSR.
       AUTHOR.        IRIS  .

      *REMARKS.
      **--------------------------------------------------------------**
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1) GENERALIZED CICS ROUTINE TO MANAGE BMS MAPPING           **
      **     GENERATED FROM IMS/DC                                    **
      **  SEND/RECEIVE istructions for map: IVTCBF
      **
      **--------------------------------------------------------------**
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*
      
           COPY DFHAID.       
           COPY IRISCOMM.
           COPY IVTCBF.

       01 WMFS-TIME             PIC X(8).
       01 WMFS-DATE             PIC X(8).
       01 HALF-WORD             PIC 9(4) COMP.
       01 HALF-WORD-X REDEFINES HALF-WORD PIC X(2).
       01 HALF-WORD-R           PIC 9(4) COMP.
       01 CX-ATTRIB             PIC X.
       01 FLG-CURS-OK           PIC X   VALUE 'N'.
       01 FLG-CURS              PIC X   VALUE 'N'.
       01 RESPONSEX             PIC S9(8) COMP.
       01 WMFS-PFK              PIC X(2).
       01 WS-MAPNAME            PIC X(08). 
       01 WS-HOLD-AREAS.
          05 WS-ABSTIME         PIC S9(16) COMP.
          05 WS-SYSDATE         PIC X(08)  VALUE SPACES.
          05 WS-SYSTIME         PIC X(08)  VALUE SPACES.
          05 WS-EIBDATE         PIC 9(05)  VALUE ZERO.
          05 WS-RESP            PIC S9(08) COMP VALUE ZERO.
        01 WS-SEND-AREA.
      **********************************************************
      *     TYPE: IMS/DC          MFS name = DFSIVF34         
      **********************************************************
      **********************************************************
      *                   C O B O L   S T R U C T U R E        *
      **********************************************************
         05 IVTCBF-OUT                         PIC X(93).
         05 IVTCB-OUT REDEFINES IVTCBF-OUT.
            10 FILLER                          PIC X(4).
            10 O-MSG                           PIC X(40).
            10 O-CMD                           PIC X(8).
            10 O-NAME1                         PIC X(10).
            10 O-NAME2                         PIC X(10).
            10 O-EXT                           PIC X(10).
            10 O-ZIP                           PIC X(7).
            10 O-SEGNO                         PIC X(4).
         05 IVTCBMO2-OUT REDEFINES IVTCBF-OUT.
            10 FILLER                          PIC X(4).
            10 O-MSG                           PIC X(40).
            10 O-CMD                           PIC X(8).
            10 O-NAME1                         PIC X(10).
            10 O-NAME2                         PIC X(10).
            10 O-EXT                           PIC X(10).
            10 O-ZIP                           PIC X(7).
            10 O-SEGNO                         PIC X(4).

        01 WS-RECEIVE-AREA.
      **********************************************************
      *     TYPE: IMS/DC          MFS name = DFSIVF34         
      **********************************************************
      **********************************************************
      *                   C O B O L   S T R U C T U R E        *
      **********************************************************
         05 IN-LL                           PIC S9(4) COMP.
         05 FILLER                          PIC X(2).
         05 FILLER                          PIC X(10)   
              VALUE 'IVTCB     '.
         05 I-CMD                           PIC X(8).
         05 I-NAME1                         PIC X(10).
         05 I-NAME2                         PIC X(10).
         05 I-EXT                           PIC X(10).
         05 I-ZIP                           PIC X(7).

       
       01 IRIS-WORK-AREA-PTR  USAGE POINTER. 

       LINKAGE SECTION.

       01 DFHCOMMAREA          PIC X(42000).
      *
      *    IRIS PCB AREA
      *
           COPY IRIPCBDC.
      *
      *    IRIS MESSAGE AREA
      *
        01 IRIS-MESSAGE-AREA PIC X(42000).
      *
      *    IRIS GLOBAL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='DFSIVFSR'==.
      
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHEIBLK
                                DFHCOMMAREA
                                IRIS-DC-PCB 
                                IRIS-MESSAGE-AREA.
       MAIN-START.
           SET IRIS-WORK-AREA-PTR TO ADDRESS OF DFHCOMMAREA
           SET ADDRESS OF IRIS-WORK-AREA TO IRIS-WORK-AREA-PTR
      *    
           IF IRIS-FUNC-ISRT
              EVALUATE IRIS-MODNAME
                 WHEN 'IVTCB'
                    MOVE 'IVTCBF' TO WS-MAPNAME
                 WHEN 'IVTCBMO2'
                    MOVE 'IVTCBF' TO WS-MAPNAME
                 WHEN OTHER
                    MOVE 'SP' TO DC-PCB-STATUS-CODE
              END-EVALUATE

              PERFORM SEND-MAP-IVTCBF
                 THRU SEND-MAP-IVTCBF-EX
           ELSE
              PERFORM RECEIVE-MAP-IVTCBF
                 THRU RECEIVE-MAP-IVTCBF-EX
           END-IF
           GO TO MAIN-END.

       MAIN-END.
           GOBACK.
      *----------------------------------------------------------------*
      *            SPLIT THE FIELD CONTAINING THE ATTRIBUTE            *
      *----------------------------------------------------------------*
       SPLIT-HWRD.
           IF HALF-WORD  >= 49152
              SUBTRACT 49152 FROM HALF-WORD
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           MOVE LOW-VALUE                 TO CX-ATTRIB.
           MOVE HALF-WORD                 TO HALF-WORD-R.
       SPLIT-HWRD-EX.
           EXIT.
      *----------------------------------------------------------------*
      *      FIND THE CORRESPONDANCE BETWEEN IMS/CICS ATTRIBUTES       *
      *----------------------------------------------------------------*
       FND-ATTRIB.
           IF HALF-WORD-R   =   196
              MOVE '<'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   197
              MOVE '('                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   200
              MOVE 'H'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   201
              MOVE 'I'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   208
              MOVE 'M'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   209
              MOVE 'J'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   212
              MOVE '*'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   213
              MOVE ')'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   216
              MOVE 'Q'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   217
              MOVE 'R'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   224
              MOVE '-'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   225
              MOVE '/'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   228
              MOVE '%'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   229
              MOVE '_'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   232
              MOVE 'Y'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   233
              MOVE 'Z'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   241
              MOVE '/'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   248
              MOVE 'Z'                    TO CX-ATTRIB
           END-IF
           .
       FND-ATTRIB-EX.
           EXIT.
       NEG-ATTRIB.
           IF HALF-WORD-R   =   -16191
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           IF HALF-WORD-R   =   -16183
              MOVE 'I'                    TO CX-ATTRIB
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           .
       NEG-ATTRIB-EX.
           EXIT.
       ANALYZE-PFK.
           MOVE LOW-VALUE TO IRIS-PFKEY

           IF EIBAID = DFHENTER
              MOVE SPACE TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF1
              MOVE '01' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF2
              MOVE '02' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF3
              MOVE '03' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF4
              MOVE '04' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF5
              MOVE '05' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF6
              MOVE '06' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF7
              MOVE '07' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF8
              MOVE '08' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF9
              MOVE '09' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF10
              MOVE '10' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF11
              MOVE '11' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF12
              MOVE '12' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF13
              MOVE '13' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF14
              MOVE '14' TO IRIS-PFKEY
              MOVE LOW-VALUE 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF15
              MOVE '15' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF16
              MOVE '16' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF17
              MOVE '17' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF18
              MOVE '18' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF19
              MOVE '19' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF20
              MOVE '20' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF21
              MOVE '21' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF22
              MOVE '22' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF23
              MOVE '23' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF24
              MOVE '24' TO IRIS-PFKEY
              MOVE LOW-VALUE
                TO WMFS-PFK
           END-IF
           . 
       EX-ANALYZE-PFK.
           EXIT.
      *----------------------------------------------------------------*
      *  Operation  : SEND                                             *
      *  On Map     : IVTCBF                                           *
      *----------------------------------------------------------------*
       SEND-MAP-IVTCBF.

           MOVE 'N' TO FLG-CURS-OK.
           PERFORM GET-TMDTMFS THRU EX-GET-TMDTMFS.
           MOVE IRIS-MESSAGE-AREA(1:LENGTH OF WS-SEND-AREA)
             TO      WS-SEND-AREA(1:LENGTH OF WS-SEND-AREA)
           PERFORM MOVE-TO-IVTCBF THRU EX-MOVE-TO-IVTCBF.
      *--------------------------------------------- 
      *---------------EXECUTE-SEND-MAP--------------- 
      *--------------------------------------------- 
           IF FLG-CURS-OK = 'N'
              MOVE -1 TO CMDL
           END-IF
           EXEC CICS SEND MAP('IVTCBF')
                          MAPSET('IVTCBF')
                          ERASE CURSOR
                          RESP  (RESPONSEX)
           END-EXEC.

           MOVE SPACES TO DC-PCB-STATUS-CODE.

           IF RESPONSEX = DFHRESP(INVREQ)
              MOVE 'ZZ' TO DC-PCB-STATUS-CODE
           END-IF.

           IF RESPONSEX = DFHRESP(INVMPSZ)
              MOVE 'ZZ' TO DC-PCB-STATUS-CODE
           END-IF.

       SEND-MAP-IVTCBF-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  Operation  : RECEIVE                                          *
      *  On Map     : IVTCBF                                           *
      *----------------------------------------------------------------*
       RECEIVE-MAP-IVTCBF.

      *   -------------------
      *   EXECUTE RECEIVE MAP
      *   -------------------

           EXEC CICS RECEIVE MAP('IVTCBF')
                             MAPSET('IVTCBF')
                             RESP(RESPONSEX)
           END-EXEC.

           MOVE SPACES TO DC-PCB-STATUS-CODE.

           IF RESPONSEX = DFHRESP(INVREQ)
              MOVE 'ZZ' TO DC-PCB-STATUS-CODE
              GO TO RECEIVE-MAP-IVTCBF-EX
           END-IF.

           IF RESPONSEX = DFHRESP(INVMPSZ)
              MOVE 'ZZ' TO DC-PCB-STATUS-CODE
              GO TO RECEIVE-MAP-IVTCBF-EX
           END-IF.

      *   THE NEXT STATEMENTS WILL BE REALLY ACTIVATED ONLY IN CASE OF
      *   CONVERSATIONAL APPROACH
           IF EIBAID = DFHCLEAR
              GO TO RECEIVE-MAP-IVTCBF-EX
           END-IF.

           PERFORM ANALYZE-PFK THRU EX-ANALYZE-PFK
           PERFORM MOVE-FROM-IVTCBF THRU EX-MOVE-FROM-IVTCBF
           COMPUTE IN-LL = LENGTH OF WS-RECEIVE-AREA
           MOVE   WS-RECEIVE-AREA(1:IN-LL OF WS-RECEIVE-AREA)
             TO IRIS-MESSAGE-AREA(1:IN-LL OF WS-RECEIVE-AREA)
           .

       RECEIVE-MAP-IVTCBF-EX.
           EXIT.
           
	       
      **********************************************************
      *     TYPE: IMS/DC          MFS name = DFSIVF34         
      **********************************************************
      **********************************************************
      *                   C O B O L   S T R U C T U R E        *
      **********************************************************
       MOVE-FROM-IVTCBF.
            MOVE CMDI OF IVTCBFI TO I-CMD OF WS-RECEIVE-AREA
            MOVE NAME1I OF IVTCBFI 
               TO I-NAME1 OF WS-RECEIVE-AREA
            MOVE NAME2I OF IVTCBFI 
               TO I-NAME2 OF WS-RECEIVE-AREA
            MOVE EXTI OF IVTCBFI TO I-EXT OF WS-RECEIVE-AREA
            MOVE ZIPI OF IVTCBFI TO I-ZIP OF WS-RECEIVE-AREA
            .
       EX-MOVE-FROM-IVTCBF.
            EXIT.
      **********************************************************
      *     TYPE: IMS/DC          MFS name = DFSIVF34         
      **********************************************************
      **********************************************************
      *                   C O B O L   S T R U C T U R E        *
      **********************************************************
       MOVE-TO-IVTCBF.
           EVALUATE IRIS-MODNAME
               WHEN 'IVTCB'
                  MOVE LOW-VALUE TO IVTCBFO
                  MOVE O-MSG OF IVTCB-OUT TO MSGO OF IVTCBFO
                  MOVE O-CMD OF IVTCB-OUT TO CMDO OF IVTCBFO
                  MOVE O-NAME1 OF IVTCB-OUT TO NAME1O OF IVTCBFO
                  MOVE O-NAME2 OF IVTCB-OUT TO NAME2O OF IVTCBFO
                  MOVE O-EXT OF IVTCB-OUT TO EXTO OF IVTCBFO
                  MOVE O-ZIP OF IVTCB-OUT TO ZIPO OF IVTCBFO
                  MOVE O-SEGNO OF IVTCB-OUT TO SEGNOO OF IVTCBFO
                  MOVE WMFS-DATE TO SDATEO OF IVTCBFO
               WHEN 'IVTCBMO2'
                  MOVE LOW-VALUE TO IVTCBFO
                  MOVE O-MSG OF IVTCBMO2-OUT TO MSGO OF IVTCBFO
                  MOVE O-CMD OF IVTCBMO2-OUT TO CMDO OF IVTCBFO
                  MOVE O-NAME1 OF IVTCBMO2-OUT TO NAME1O OF IVTCBFO
                  MOVE O-NAME2 OF IVTCBMO2-OUT TO NAME2O OF IVTCBFO
                  MOVE O-EXT OF IVTCBMO2-OUT TO EXTO OF IVTCBFO
                  MOVE O-ZIP OF IVTCBMO2-OUT TO ZIPO OF IVTCBFO
                  MOVE O-SEGNO OF IVTCBMO2-OUT TO SEGNOO OF IVTCBFO
                  MOVE WMFS-DATE TO SDATEO OF IVTCBFO
           END-EVALUATE.
       EX-MOVE-TO-IVTCBF.
           EXIT.

           COPY IRISDCDT.

