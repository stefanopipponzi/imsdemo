       IDENTIFICATION DIVISION.
       PROGRAM-ID.    DCRCVE.
       AUTHOR.        IRIS  .

      *REMARKS.
      **--------------------------------------------------------------**
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **     GENERALIZED CICS ROUTINE TO MANAGE "RECEIVE MAP"         **
      **     GENERATED FROM IMS/DC                                    **
      **                                                              **
      **
      **  #CODE#
      **
      **--------------------------------------------------------------**
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*

           COPY IRISCOMM.

       01  RECEIVE-AREA           PIC X(2000).
       01  RESPONSEX              PIC S9(8) COMP.
       01  WMFS-PFK               PIC X(#LENPFK#).
       
       01  AREA-LNK-PASS.
           COPY LNKDCRT.        
       
           COPY DFHAID.
 
       LINKAGE SECTION.

          01 DFHCOMMAREA              PIC X(42000).
      *
      *    IRIS PCB AREA
      *
           COPY IRIPCBDC.

        01 IRIS-MESSAGE-AREA            PIC X(4096).
      *
      *    IRIS GLOBAL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='DCRCVE'==.
      
      *----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHEIBLK
                                DFHCOMMAREA
                                IRIS-DC-PCB 
                                IRIS-MESSAGE-AREA.

       MAIN-START.
        
           MOVE DFHCOMMAREA TO AREA-LNK-PASS.          
           
      *    #OPE#
           EVALUATE LNKDC-MAPSETNAME
               WHEN 'IVTCBF'
                  CALL 'DFSIVFR' USING DFHEIBLK AREA-LNK-PASS
               WHEN OTHER
                  MOVE '  ' TO DC-PCB-STATUS-CODE
            END-EVALUATE
           
           
           MOVE AREA-LNK-PASS TO DFHCOMMAREA.

       MAIN-END.
           GOBACK.
