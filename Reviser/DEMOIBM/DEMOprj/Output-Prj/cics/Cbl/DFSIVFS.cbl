       IDENTIFICATION DIVISION.
       PROGRAM-ID.    DFSIVFS.
       AUTHOR.        IRIS  .

      *REMARKS.
      **--------------------------------------------------------------**
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1) GENERALIZED CICS ROUTINE TO MANAGE "SEND MAP"            **
      **     GENERATED FROM IMS/DC                                    **
      **                                                              **
      **  #CODE#

      **  SEND istruction for map: IVTCBF
      **
      **--------------------------------------------------------------**
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*
      
           COPY DFHAID.       
           COPY IVTCBF.


       01  WK-MAPNAME            PIC X(8).
       01  WK-INTOAREA           PIC X(9).
       01  WK-FROMAREA           PIC X(9).
       01  WMFS-TIME             PIC X(8).
       01  WMFS-DATE             PIC X(8).
       01 HALF-WORD              PIC 9(4) COMP.
       01 HALF-WORD-X REDEFINES HALF-WORD PIC X(2).
       01 HALF-WORD-R            PIC 9(4) COMP.
       01 CX-ATTRIB              PIC X.
       01 FLG-CURS-OK            PIC X   VALUE 'N'.
       01 FLG-CURS               PIC X   VALUE 'N'.
       01 RESPONSEX              PIC S9(8) COMP.
       01  WS-HOLD-AREAS.
           05  WS-ABSTIME         PIC S9(16) COMP.
           05  WS-SYSDATE         PIC X(08)  VALUE SPACES.
           05  WS-SYSTIME         PIC X(08)  VALUE SPACES.
           05  WS-EIBDATE         PIC 9(05)  VALUE ZERO.
           05  WS-RESP            PIC S9(08) COMP VALUE ZERO.
       01  TSOQ-QUEUE.
          05  TSOQ-TERM           PIC X(4).
          05  FILLER              PIC X(4)  VALUE 'OPER'.
       01  TSOQ-LEN               PIC S9(4) COMP  VALUE +2030.
       01  TSOQ-ITEM.
          05  TSOQ-AREA           PIC X(2030).

       01  BASE-PAGE               PIC S9(8) COMP.   
       01  LEN-PAGE                PIC S9(8) COMP.          
       
       01 IRIS-WORK-AREA-PTR  USAGE POINTER. 

       LINKAGE SECTION.

       01 DFHCOMMAREA          PIC X(42000).
      *
      *    IRIS PCB AREA
      *
           COPY IRIPCBDC.
      *
      *    IRIS GLOBAL AREA
      *
        01 IRIS-MESSAGE-AREA.
           COPY DFSIVFSO.
.
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='DFSIVFS'==.
      
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHEIBLK
                                DFHCOMMAREA
                                IRIS-DC-PCB 
                                IRIS-MESSAGE-AREA.

       MAIN-START.
           SET IRIS-WORK-AREA-PTR TO ADDRESS OF DFHCOMMAREA
           SET ADDRESS OF IRIS-WORK-AREA TO IRIS-WORK-AREA-PTR

      *    #OPE#
            EVALUATE IRIS-MODNAME
               WHEN 'IVTCB'
                  MOVE 'IVTCBF' TO WK-MAPNAME
               WHEN 'IVTCBMO2'
                  MOVE 'IVTCBF' TO WK-MAPNAME
               WHEN OTHER
                  MOVE 'SP' TO DC-PCB-STATUS-CODE
            END-EVALUATE 

            PERFORM SEND-MAP-IVTCBF
               THRU SEND-MAP-IVTCBF-EX
            GO TO MAIN-END.

            
       MAIN-END.
           GOBACK.
      
      *----------------------------------------------------------------*
      *            SPLIT THE FIELD CONTAINING THE ATTRIBUTE            *
      *----------------------------------------------------------------*
       SPLIT-HWRD.

           IF HALF-WORD  >= 49152
              SUBTRACT 49152 FROM HALF-WORD
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           MOVE LOW-VALUE                 TO CX-ATTRIB.
           MOVE HALF-WORD                 TO HALF-WORD-R.

       SPLIT-HWRD-EX.
           EXIT.
      *----------------------------------------------------------------*
      *      FIND THE CORRESPONDANCE BETWEEN IMS/CICS ATTRIBUTES       *
      *----------------------------------------------------------------*
       FND-ATTRIB.

           IF HALF-WORD-R   =   196
              MOVE '<'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   197
              MOVE '('                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   200
              MOVE 'H'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   201
              MOVE 'I'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   208
              MOVE 'M'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   209
              MOVE 'J'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   212
              MOVE '*'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   213
              MOVE ')'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   216
              MOVE 'Q'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   217
              MOVE 'R'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   224
              MOVE '-'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   225
              MOVE '/'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   228
              MOVE '%'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   229
              MOVE '_'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   232
              MOVE 'Y'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   233
              MOVE 'Z'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   241
              MOVE '/'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   248
              MOVE 'Z'                    TO CX-ATTRIB
           END-IF
           .

       FND-ATTRIB-EX.
           EXIT.
       
       NEG-ATTRIB.
           IF HALF-WORD-R   =   -16191
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           IF HALF-WORD-R   =   -16183
              MOVE 'I'                    TO CX-ATTRIB
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           .
       NEG-ATTRIB-EX.
           EXIT.
       
       
      *----------------------------------------------------------------*
      *  Operation  : SEND                                             *
      *  On Map     : IVTCBF                                           *
      *----------------------------------------------------------------*
       SEND-MAP-IVTCBF.

           MOVE 'N' TO FLG-CURS-OK.
           PERFORM GET-TMDTMFS THRU EX-GET-TMDTMFS.
           PERFORM MOVE-TO-IVTCBF THRU EX-MOVE-TO-IVTCBF.
      *--------------------------------------------- 
      *---------------EXECUTE-SEND-MAP--------------- 
      *--------------------------------------------- 
           IF FLG-CURS-OK = 'N'
              MOVE -1 TO CMDL
           END-IF
           EXEC CICS SEND MAP('IVTCBF')
                          MAPSET('IVTCBF')
                          ERASE CURSOR
                          RESP  (RESPONSEX)
           END-EXEC.

           MOVE SPACES TO DC-PCB-STATUS-CODE.

           IF RESPONSEX = DFHRESP(INVREQ)
              MOVE 'ZZ' TO DC-PCB-STATUS-CODE
           END-IF.

           IF RESPONSEX = DFHRESP(INVMPSZ)
              MOVE 'ZZ' TO DC-PCB-STATUS-CODE
           END-IF.

       SEND-MAP-IVTCBF-EX.
           EXIT.



       


          COPY DFSIVFMO.

          COPY IRISDCDT.


           
