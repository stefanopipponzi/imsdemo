      $SET FCDREG
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    #NOME_RTE#.
       AUTHOR.        #LABEL_AUTHOR#.
       DATE-WRITTEN.  #DATA_CREAZ#.
       DATE-COMPILED. #DATA_COM#.
       
      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   22 11 2006        (RELEASE 1.0)   **
      **                                                              **
      **  EXECUTED BY           :   Development and support group     **
      **                            Mainframe Affinity                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1)Generalized BATCH Routine for GSAM TO SEQ                 **
      **                                                              **
      **  STANDARD TEMPLATE SOURCE                                    **
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            ** 
      **                                                              **
      **--------------------------------------------------------------**
      ** 
      **  #CODE#   
      **
      **--------------------------------------------------------------**
 
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT FILE-GSAM ASSIGN TO #DB_DLI#
              FILE STATUS IS WK-STATUS.
       DATA DIVISION.
       FILE SECTION.
       FD FILE-GSAM.
       01 REC-GSAM PIC X(32767).

      /----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.
           
       01 START-WS PIC X(70) 
              VALUE '*** START WORKING STORAGE #NOME_RTE# ***'.

           COPY ERRVSMRT.

       01  WK-STATUS          PIC X(2).
       01  FILE-STATUS REDEFINES WK-STATUS.
           05 STATUS-KEY-1                               PIC X.
           05 STATUS-KEY-2                               PIC X.
           05 STATUS-KEY-2-BINARY REDEFINES STATUS-KEY-2 PIC 99 COMP-X.
       01  WK-IND             PIC S9(8) COMP.
       01  WINDICE            PIC S9(8) COMP.
       01  WK-SEGMENT         PIC X(8).
       01  WK-ISTR            PIC X(4).
       01  WK-REC-COUNT       PIC S9(8) COMP VALUE ZERO.

       01  WK-LENGTH          PIC X(8) VALUE SPACES.
       01  WK-LENGTH-X        PIC X(8) VALUE ZERO.
       01  WK-LENGTH-9 REDEFINES WK-LENGTH-X PIC 9(8).
       01  WK-COUNT           PIC S9(8) COMP VALUE ZERO.
       01  WK-COUNT-XRST      PIC S9(8) COMP VALUE ZERO.
       
       01  WK-DBD-LENGTH      PIC 9(8) VALUE #SEG-LEN#.
       
       LINKAGE SECTION.

       01  AREA-CUSTDB2.
           COPY LNKDBRT.

           COPY FCDGSAM.
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-CUSTDB2.

       1000-MAIN-START SECTION.
      *----------------------------------------------------------------*
           PERFORM 4000-INIT-AREAS.
      
      *    COMMAND RECOGNITION AND
      *    RELATIVE SECTION ACTIVATION

      *    #OPE#    
 
           MOVE 'NOOP' TO LNKDB-CODE-ERROR.
           PERFORM 8000-ROUT-ERROR.
           PERFORM 3000-MAIN-END.
                
       2000-MAIN-001 SECTION.
           PERFORM 5000-PREPARE-EXIT.

      *----------------------------------------------------------------*

       3000-MAIN-END SECTION.
           PERFORM 7000-RETURN.

      *----------------------------------------------------------------*
      *             A R E A S   I N I T I A L I Z A T I O N
      *----------------------------------------------------------------*
       4000-INIT-AREAS SECTION.
           
           MOVE SPACES TO LNKDB-ABEND-RTCODE.
           MOVE SPACES TO WK-STATUS.

           IF LNKDB-STATE NOT GREATER SPACES
              INITIALIZE LNKDB-ACTIVE
              MOVE 'A' TO LNKDB-STATE
              MOVE ZERO TO LNKDB-NUMSTACK
           END-IF.

      *----------------------------------------------------------------*
      *             P R E P A R E   E X I T   A R E A S
      *            S E T   T H E   R E T U R N   C O D E
      *----------------------------------------------------------------*
       5000-PREPARE-EXIT SECTION.
           MOVE 1 TO WK-IND 
           PERFORM UNTIL WK-IND GREATER MAX-FILRESP
               IF WK-STATUS = ERRFIL-CODRESP(WK-IND) 
                  MOVE ERRFIL-CODDLI(WK-IND) TO LNKDB-PCBSTAT
                  MOVE 2000 TO WK-IND
               ELSE 
                 IF ERRFIL-CODRESP(WK-IND) = '99'                  
                    MOVE ERRFIL-CODDLI(WK-IND) TO LNKDB-PCBSTAT
                    MOVE 2000 TO WK-IND
                 END-IF
               END-IF                 
               ADD 1 TO WK-IND
           END-PERFORM

           IF LNKDB-PCBSTAT = 'XI'
              DISPLAY '************************************************'
              DISPLAY '***            P2NVNAGS                      ***'
              DISPLAY '***      I-TER IMS CONVERSION:               ***'
              DISPLAY '***        NOT MANAGED FILE STATUS           ***'
              DISPLAY '***                                          ***'
              DISPLAY '***    FILE STATUS 1 = ' STATUS-KEY-1
                                              '                     ***'  
              IF STATUS-KEY-1 = '9'
                 DISPLAY '***    FILE STATUS 2 = ' STATUS-KEY-2-BINARY
                                                '                   ***'
              ELSE                                   
                 DISPLAY '***    FILE STATUS 2 = ' STATUS-KEY-2
                                              '                     ***'
              END-IF 
              DISPLAY '***                                          ***'
              DISPLAY '***    INSTRUCTION = ' WK-ISTR
                                               '                    ***'
              DISPLAY '***                                          ***'
              DISPLAY '************************************************'
           END-IF

           MOVE '#DB_DLI#' TO LNKDB-PCBSEGNM.
           
           MOVE SPACES TO LNKDB-FDBKEY.
           
      *----------------------------------------------------------------*
      *             R E T U R N   T O   C A L L E R
      *----------------------------------------------------------------*
       
       7000-RETURN SECTION.
           GOBACK.
           
      *----------------------------------------------------------------*
      *             E R R O R S   S E C T I O N
      *----------------------------------------------------------------*

       8000-ROUT-ERROR SECTION.
           IF LNKDB-CODE-ERROR = 'NOOP'
              DISPLAY '*** FORCED ABEND ***'
              DISPLAY 'OPERATION: ' LNKDB-OPERATION
              DISPLAY 'CALLER: ' LNKDB-CALLER
              CALL 'NOOP'
           END-IF.
   
           IF LNKDB-CODE-ERROR = 'DLSA'
                   MOVE MSGDB-DLSA TO LNKDB-RTIOC
                   MOVE ACTION-DLSA TO ACTION-ERRORE
           ELSE
                   MOVE MSGDB-NOER TO LNKDB-RTIOC 
                   MOVE CODDB-NOER TO LNKDB-CODE-ERROR 
                   MOVE ACTION-NOER TO ACTION-ERRORE
           END-IF.       
            
           DISPLAY ' NAME-RTE ERROR: ' LNKDB-CODE-ERROR.
           IF ACTION-ERRORE = 1
              MOVE 'ADL0' TO LNKDB-ABEND-RTCODE
           ELSE   
              MOVE 'ADL1' TO LNKDB-CODE-ERROR
           END-IF.

           PERFORM 7000-RETURN.

      *------------------------------------------------------------* 
      *                      PCB MANAGING                          *
      *------------------------------------------------------------* 

       9000-MANAGE-PCB SECTION. 
           MOVE ZERO TO WK-IND
           MOVE 1    TO LNKDB-NUMSTACK

           PERFORM UNTIL WK-IND > 49
             ADD 1 TO WK-IND             
             IF LNKDB-NUMPCB  = LNKDB-STKNUMPCB(WK-IND) OR
                 ZEROES       = LNKDB-STKNUMPCB(WK-IND)
                 MOVE WK-IND TO LNKDB-NUMSTACK
                 MOVE 50 TO WK-IND
             END-IF
           END-PERFORM.

           MOVE LNKDB-NUMPCB TO LNKDB-STKNUMPCB(LNKDB-NUMSTACK).     
           
      *------------------------------------------------------------*
      *                   RESTORE LAST READ KEY                    *
      *------------------------------------------------------------*
       10000-REST-FDBKEY SECTION.
       
      *#REST_FDBKEY#    
       
      *------------------------------------------------------------*
      *                     SAVE LAST READ KEY                     *
      *------------------------------------------------------------*
       11000-STORE-FDBKEY SECTION.
       
      *#SET_FDBKEY# 