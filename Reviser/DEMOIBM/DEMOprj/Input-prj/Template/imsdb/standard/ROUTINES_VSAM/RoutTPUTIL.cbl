       IDENTIFICATION DIVISION.
       PROGRAM-ID.    #NOME_RTE#.
       AUTHOR.        #LABEL_AUTHOR#.
       DATE-WRITTEN.  #DATA_CREAZ#.
       DATE-COMPILED. #DATA_COM#.
       
      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   22 07 2005        (RELEASE 3.0)   **
      **                                                              **
      **  EXECUTED BY           :   Development and support group     **
      **                            Mainframe Affinity                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  CICS Routine for UTILITY commands                           **
      **                                                              **
      **  DB2 VERSION                                                 ** 
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            ** 
      **                                                              **
      **--------------------------------------------------------------**
      ** 
      **  #CODE#   
      **
      **--------------------------------------------------------------**
 
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      /----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.
       
       01 START-WS PIC X(70) 
              VALUE '*** START WORKING STORAGE #NOME_RTE# ***'.
              
           COPY ERRVSMRT.

       01  WK-NAMETABLE       PIC X(16).
       01  WK-SELTABLE        PIC X(16).
       01  WK-ISTR            PIC X(4).
       01  WK-SSA.
           03 WK-KEYNAME      PIC X(030).
           03 WK-KEYOPER      PIC X(002).
           03 WK-KEYVALUE     PIC X(100).

       01  WK-IND            PIC S9(8) COMP.
       01  WINDICE           PIC S9(8) COMP.

       01  WK-OPERATION      PIC X(4)  VALUE SPACES.
       01  WK-PSBNAME        PIC X(8)  VALUE SPACES.       
       01  WK-CHKPID         PIC X(8)  VALUE SPACES.
       01  WK-NUMPCB         PIC S9(4) COMP VALUE ZEROES.

       01  WK-DATA-AREA1     PIC X(100)    VALUE SPACES.
 
       LINKAGE SECTION.

       01  AREA-CUSTDB2.
           COPY LNKDBRT.
           
         
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-CUSTDB2.

       MAIN-START.
      *----------------------------------------------------------------*
           PERFORM 4000-INIT-AREAS.
      
      *    COMMAND RECOGNITION AND
      *    RELATIVE SECTION ACTIVATION

      *    #OPE#    
 
           MOVE 'NOOP' TO LNKDB-CODE-ERROR.
           PERFORM 8000-ROUT-ERROR.
           PERFORM 3000-MAIN-END.
                
       2000-MAIN-001 SECTION.
           PERFORM 5000-PREPARE-EXIT.

      *----------------------------------------------------------------*

       3000-MAIN-END SECTION.
           PERFORM 7000-RETURN.

      *----------------------------------------------------------------*
      *             A R E A S   I N I T I A L I Z A T I O N
      *----------------------------------------------------------------*
       4000-INIT-AREAS SECTION.
           
           MOVE SPACES TO LNKDB-ABEND-RTCODE.
           MOVE ZERO   TO WK-VSAMCODE.
         
           INITIALIZE LNKDB-ACTIVE.    

           EXIT.

      *----------------------------------------------------------------*
      *             P R E P A R E   E X I T   A R E A S
      *            S E T   T H E   R E T U R N   C O D E
      *----------------------------------------------------------------*
       5000-PREPARE-EXIT SECTION.
           
           PERFORM 6000-SET-LASTOP.

           EXIT.
      *------------------------------------------------------------*
      *  BACKUP ROUTINE OF LAST OPER IN LINKAGE AREA
      *------------------------------------------------------------*
       6000-SET-LASTOP SECTION.

           MOVE WK-ISTR TO LNKDB-STKLASTOP(LNKDB-NUMSTACK).

           EXIT.
      *----------------------------------------------------------------*
      *             R E T U R N   T O   C A L L E R
      *----------------------------------------------------------------*
       
       7000-RETURN SECTION.
           GOBACK.

      *----------------------------------------------------------------*
      *             E R R O R S   S E C T I O N
      *----------------------------------------------------------------*

       8000-ROUT-ERROR SECTION.
           IF LNKDB-CODE-ERROR = 'NOOP'
              DISPLAY '*** FORCED ABEND ***'
              DISPLAY 'OPERATION: ' LNKDB-OPERATION
              DISPLAY 'CALLER: ' LNKDB-CALLER
              CALL 'NOOP'
           END-IF.
   
           MOVE MSGDB-NOER TO LNKDB-RTIOC 
           MOVE CODDB-NOER TO LNKDB-CODE-ERROR 
           MOVE ACTION-NOER TO ACTION-ERRORE. 
             
           DISPLAY ' NAME-RTE ERROR: ' LNKDB-CODE-ERROR.
           IF ACTION-ERRORE = 1
              MOVE 'ADL0' TO LNKDB-ABEND-RTCODE
           ELSE   
              MOVE 'ADL1' TO LNKDB-CODE-ERROR
           END-IF.

           PERFORM 7000-RETURN.

           EXIT.      