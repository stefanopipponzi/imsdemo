       IDENTIFICATION DIVISION.
     /*TITOLO= Generalized CICS Routine for GNP and GHNP     */       
       PROGRAM-ID.    #NOME_RTE#.
       AUTHOR.        #LABEL_AUTHOR#.
       DATE-WRITTEN.  #DATA_CREAZ#.
       DATE-COMPILED. #DATA_COM#.
       
      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   22 07 2005        (RELEASE 3.0)   **
      **                                                              **
      **  EXECUTED BY           :   Development and support group     **
      **                            Mainframe Affinity                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1)Generalized CICS Routine for GNP and GHNP                 **
      **                                                              **
      **  STANDARD TEMPLATE SOURCE                                    **
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            ** 
      **                                                              **
      **--------------------------------------------------------------**
      ** 
      **  #CODE#   
      **
      **--------------------------------------------------------------**
 
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      /----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.
           
       01 START-WS PIC X(70) 
              VALUE '*** START WORKING STORAGE #NOME_RTE# ***'.
           
           COPY ERRDBRT.

       01  WK-GNQUAL                  PIC X    VALUE 'S'.
       
       01  WK-NAMETABLE       PIC X(18).
       01  WK-SEGMENT         PIC X(8).
       01  WK-ISTR            PIC X(4).
       01  WK-TABLE-SQL-ERR   PIC X(18).
       01  WK-LABEL-ERR       PIC X(16).

       01  WK-DATA-AREA  PIC X(27955).

       01  WK-KEYSEQ               PIC S9(4) COMP.
       01  WK-KEYSEQ1      PIC S9(4) COMP VALUE ZEROES.
       01  WK-KEYSEQ2      PIC S9(4) COMP VALUE ZEROES.
       01  WK-KEYSEQ3      PIC S9(4) COMP VALUE ZEROES.
       
       01  WK-SSA.
           03 WK-KEYNAME      PIC X(030).
           03 WK-KEYOPER      PIC X(002).
           03 WK-KEYVALUE     PIC X(100).

           EXEC SQL INCLUDE SQLCA END-EXEC.
 
      *#COPY_WK_DB2#    
 
       01  WK-SQLCODE             PIC S9(8) COMP.
       01  WK-IND                 PIC S9(8) COMP.
       01  WK-IND2                PIC S9(8) COMP.
       01  WINDICE                PIC S9(8) COMP.
       01  WK-IND-ERR             PIC S9(8) COMP.

           EXEC SQL INCLUDE WKDATERT END-EXEC.
       
       LINKAGE SECTION.
       01  DFHCOMMAREA PIC X(30355).
       01  AREA-CUSTDB2 REDEFINES DFHCOMMAREA.
           COPY LNKDBRT.

      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA.


       1000-MAIN-START SECTION.
      *----------------------------------------------------------------*
           PERFORM 4000-INIT-AREAS.
      
      *    COMMAND RECOGNITION AND
      *    RELATIVE SECTION ACTIVATION

      *    #OPE#    
 
           MOVE 'NOOP' TO LNKDB-CODE-ERROR
           PERFORM 8000-ROUT-ERROR
           PERFORM 3000-MAIN-END.
                
       2000-MAIN-001 SECTION.
           PERFORM 5000-PREPARE-EXIT.

      *----------------------------------------------------------------*

       3000-MAIN-END SECTION.
           PERFORM 7000-RETURN.

      *----------------------------------------------------------------*
      *             A R E A S   I N I T I A L I Z A T I O N
      *----------------------------------------------------------------*
       4000-INIT-AREAS SECTION.
           
           MOVE SPACES TO LNKDB-ABEND-RTCODE.
           MOVE ZERO TO WK-SQLCODE.
           MOVE 'S' TO WK-GNQUAL

           IF LNKDB-STATE NOT GREATER SPACES
              INITIALIZE LNKDB-ACTIVE
              MOVE 'A' TO LNKDB-STATE
              MOVE ZERO TO LNKDB-NUMSTACK
           END-IF.

      *----------------------------------------------------------------*
      *             P R E P A R E   E X I T   A R E A S
      *            S E T   T H E   R E T U R N   C O D E
      *----------------------------------------------------------------*
       5000-PREPARE-EXIT SECTION.
           MOVE 1 TO WK-IND 
           PERFORM UNTIL WK-IND GREATER MAX-SQLRESP
               IF WK-SQLCODE = ERRDB-CODRESP(WK-IND) 
                  MOVE ERRDB-CODDLI(WK-IND) TO LNKDB-PCBSTAT
                  MOVE WK-IND TO WK-IND-ERR
                  MOVE 2000 TO WK-IND
               ELSE 
                 IF ERRDB-CODRESP(WK-IND) = -999999                  
                    MOVE ERRDB-CODDLI(WK-IND) TO LNKDB-PCBSTAT
                    MOVE WK-IND TO WK-IND-ERR
                    MOVE 2000 TO WK-IND
                 END-IF 
               END-IF                 
               ADD 1 TO WK-IND
           END-PERFORM

           MOVE WK-SEGMENT TO LNKDB-PCBSEGNM
           
           EVALUATE LNKDB-PCBSTAT
               WHEN 'GE'
                   IF WK-ISTR = 'GN' AND
                      WK-GNQUAL = 'N'
                         MOVE 'GB' TO LNKDB-PCBSTAT
                   END-IF
           END-EVALUATE
           
           IF LNKDB-PCBSTAT NOT = '  '
              MOVE SPACES TO LNKDB-FDBKEY
           END-IF
           
           PERFORM 6000-SET-LASTOP.

      *------------------------------------------------------------*
      *  BACKUP ROUTINE OF LAST OPER IN LINKAGE AREA
      *------------------------------------------------------------*
       6000-SET-LASTOP SECTION.
           IF WK-ISTR = 'GU' OR 'ISRT'
              MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)
           END-IF
           IF WK-ISTR = 'GU' OR 'ISRT' OR 'GN' OR 'GNP'
              MOVE ZERO TO WK-IND
              PERFORM UNTIL WK-IND > 49
                 ADD 1 TO WK-IND
                 IF LNKDB-NUMPCB = LNKDB-STKNUMPCB(WK-IND)
                 AND LNKDB-STKLEV(LNKDB-NUMSTACK) <
                                      LNKDB-STKLEV(WK-IND)
                     MOVE SPACES TO LNKDB-STKCURSOR(WK-IND)
                     MOVE SPACES TO LNKDB-STKLASTOP(WK-IND)
                     MOVE SPACES TO LNKDB-STKSSA(WK-IND)
                 END-IF
              END-PERFORM
           END-IF

           MOVE WK-ISTR TO LNKDB-STKLASTOP(LNKDB-NUMSTACK).

      *----------------------------------------------------------------*
      *             R E T U R N   T O   C A L L E R
      *----------------------------------------------------------------*
       
       7000-RETURN SECTION.
           GOBACK.
           
      *----------------------------------------------------------------*
      *             E R R O R S   S E C T I O N
      *----------------------------------------------------------------*

       8000-ROUT-ERROR SECTION.
           IF LNKDB-CODE-ERROR = 'NOOP'
              EXEC CICS ABEND ABCODE('NOOP') END-EXEC
                   MOVE MSGDB-NOOP TO LNKDB-RTIOC
                   MOVE ACTION-NOOP TO ACTION-ERRORE
           END-IF
   
           IF LNKDB-CODE-ERROR = 'DLSA'
                   MOVE MSGDB-DLSA TO LNKDB-RTIOC
                   MOVE ACTION-DLSA TO ACTION-ERRORE
           ELSE
                   MOVE MSGDB-NOER TO LNKDB-RTIOC 
                   MOVE CODDB-NOER TO LNKDB-CODE-ERROR 
                   MOVE ACTION-NOER TO ACTION-ERRORE
           END-IF.       
             
       ROUT-ERR01.
      *    DISPLAY ' NAME-RTE ERROR: ' LNKDB-CODE-ERROR  
           IF ACTION-ERRORE = 1
              MOVE 'ADL0' TO LNKDB-ABEND-RTCODE
           ELSE   
              MOVE 'ADL1' TO LNKDB-CODE-ERROR
           END-IF.

           PERFORM 7000-RETURN.

      *------------------------------------------------------------* 
      *                      PCB MANAGING                          *
      *------------------------------------------------------------* 

       9000-MANAGE-PCB SECTION. 
           MOVE ZERO TO WK-IND
           MOVE 1    TO LNKDB-NUMSTACK

           PERFORM UNTIL WK-IND > 49
             ADD 1 TO WK-IND             
             IF LNKDB-NUMPCB = LNKDB-STKNUMPCB(WK-IND) AND
                 WK-NAMETABLE = LNKDB-STKNAME(WK-IND) OR
                 ZEROES       = LNKDB-STKNUMPCB(WK-IND)
                 MOVE WK-IND TO LNKDB-NUMSTACK
                 MOVE 50 TO WK-IND
             END-IF
           END-PERFORM

           MOVE LNKDB-NUMPCB TO LNKDB-STKNUMPCB(LNKDB-NUMSTACK).     
           MOVE WK-NAMETABLE TO LNKDB-STKNAME(LNKDB-NUMSTACK). 
           
      *------------------------------------------------------------*
      *  AREAS TRANSFER ROUTINES
      *------------------------------------------------------------*
       
      *#COPY_PD_DB2#    

           EXEC SQL INCLUDE PDDATERT END-EXEC.
      
      *------------------------------------------------------------*
      *                   RESTORE LAST READ KEY                    *
      *------------------------------------------------------------*
       10000-REST-FDBKEY SECTION.
       
      *#REST_FDBKEY#    
       
      *------------------------------------------------------------*
      *                     SAVE LAST READ KEY                     *
      *------------------------------------------------------------*
       11000-STORE-FDBKEY SECTION.
       
      *#SET_FDBKEY# 
      
