       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.

      *REMARKS.
      **--------------------------------------------------------------**
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1) GENERALIZED CICS ROUTINE TO MANAGE "SEND MAP"            **
      **     GENERATED FROM IMS/DC                                    **
      **
      **  #CODE#
      **
      **--------------------------------------------------------------**
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*
      
           COPY DFHAID.       
           COPY #CICS-MAP#.

       01  WMFS-TIME              PIC X(8).
       01  WMFS-DATE              PIC X(8).
       01 HALF-WORD              PIC S9(4) COMP.
       01 HALF-WORD-R            PIC S9(4) COMP.
       01 CX-ATTRIB              PIC X.
       01 FLG-CURS               PIC X   VALUE 'N'.
       01 RESPONSE               PIC S9(8) COMP.
       01  WS-HOLD-AREAS.
           05  WS-ABSTIME         PIC S9(16) COMP.
           05  WS-SYSDATE         PIC X(08)  VALUE SPACES.
           05  WS-SYSTIME         PIC X(08)  VALUE SPACES.
           05  WS-EIBDATE         PIC 9(05)  VALUE ZERO.
           05  WS-RESP            PIC S9(08) COMP VALUE ZERO.
       01  TSOQ-QUEUE.
          05  TSOQ-TERM           PIC X(4).
          05  FILLER              PIC X(4)  VALUE 'OPER'.
       01  TSOQ-LEN               PIC S9(4) COMP  VALUE +2030.
       01  TSOQ-ITEM.
          05  TSOQ-AREA           PIC X(2030).
       
       LINKAGE SECTION.

       01  DFHCOMMAREA.
           COPY LNKDCRT.
           COPY LNKDC15.
      
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA.

       MAIN-START.

             IF LNK15-FLAG-SEND  =  '1'
               MOVE EIBTRMID     TO  TSOQ-TERM
               MOVE DFHCOMMAREA  TO  TSOQ-ITEM
               PERFORM WRITE-TS
                  THRU WRITE-TS-EX
               GO TO MAIN-END.

      *    #OPE#
            
       MAIN-END.
           GOBACK.
      
      *----------------------------------------------------------------*
      *            SPLIT THE FIELD CONTAINING THE ATTRIBUTE            *
      *----------------------------------------------------------------*
       SPLIT-HWRD.

           IF HALF-WORD  GREATER   49152
              SUBTRACT 49152 FROM HALF-WORD
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           MOVE LOW-VALUE                 TO CX-ATTRIB.
           MOVE HALF-WORD                 TO HALF-WORD-R.

       SPLIT-HWRD-EX.
           EXIT.
      *----------------------------------------------------------------*
      *      WRITE THE TS QUEUE                                        *
      *----------------------------------------------------------------*
       WRITE-TS.
           MOVE 1           TO  I.
           EXEC CICS WRITEQ TS QUEUE(TSOQ-QUEUE)
                               FROM(TSOQ-ITEM)
                               LENGTH(TSOQ-LEN)
                               ITEM(I)
                               RESP(RESPONSE)
           END-EXEC.
           IF RESPONSE = DFHRESP(QIDERR)
              MOVE 'ZZ' TO LNKDC-RETCODE
              GO TO MAIN-END
           END-IF.
           IF RESPONSE = DFHRESP(ITEMERR)
              MOVE 'ZZ' TO LNKDC-RETCODE
              GO TO MAIN-END
           END-IF.
           IF RESPONSE = DFHRESP(INVREQ)
              MOVE 'ZZ' TO LNKDC-RETCODE
              GO TO MAIN-END
           END-IF.
       WRITE-TS-EX.
           EXIT.
      *----------------------------------------------------------------*
      *      FIND THE CORRESPONDANCE BETWEEN IMS/CICS ATTRIBUTES       *
      *----------------------------------------------------------------*
       FND-ATTRIB.

           IF HALF-WORD-R   =   196
              MOVE '<'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   197
              MOVE '('                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   200
              MOVE 'H'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   201
              MOVE 'I'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   208
              MOVE 'M'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   209
              MOVE 'J'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   212
              MOVE '*'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   213
              MOVE ')'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   216
              MOVE 'Q'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   217
              MOVE 'R'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   224
              MOVE '-'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   225
              MOVE '/'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   228
              MOVE '%'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   229
              MOVE '_'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   232
              MOVE 'Y'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   233
              MOVE 'Z'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   241
              MOVE '/'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   248
              MOVE 'Z'                    TO CX-ATTRIB
           END-IF
           .

       FND-ATTRIB-EX.
           EXIT.
       
              NEG-ATTRIB.
           IF HALF-WORD-R   =   -16191
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           IF HALF-WORD-R   =   -16183
              MOVE 'I'                    TO CX-ATTRIB
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           .
       NEG-ATTRIB-EX.
           EXIT.
       
       *#BEGINFIELD#

       #FIELD#.


          #COPYMOVE#.

          COPY GETDTTI.


           
