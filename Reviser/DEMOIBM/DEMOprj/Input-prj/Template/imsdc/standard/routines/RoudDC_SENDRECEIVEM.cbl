       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.

      *REMARKS.
      **--------------------------------------------------------------**
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1) GENERALIZED CICS ROUTINE TO MANAGE BMS MAPPING           **
      **     GENERATED FROM IMS/DC                                    **
      **  SEND/RECEIVE istructions for map: #CICS-MAP#
      **
      **--------------------------------------------------------------**
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*
      
           COPY DFHAID.       
           COPY IRISCOMM.
           COPY #CICS-MAP#.

       01 WMFS-TIME             PIC X(8).
       01 WMFS-DATE             PIC X(8).
       01 HALF-WORD             PIC 9(4) COMP.
       01 HALF-WORD-X REDEFINES HALF-WORD PIC X(2).
       01 HALF-WORD-R           PIC 9(4) COMP.
       01 CX-ATTRIB             PIC X.
       01 FLG-CURS-OK           PIC X   VALUE 'N'.
       01 FLG-CURS              PIC X   VALUE 'N'.
       01 RESPONSEX             PIC S9(8) COMP.
       01 WMFS-PFK              PIC X(#LENPFK#).
       01 WS-MAPNAME            PIC X(08). 
       01 WS-HOLD-AREAS.
          05 WS-ABSTIME         PIC S9(16) COMP.
          05 WS-SYSDATE         PIC X(08)  VALUE SPACES.
          05 WS-SYSTIME         PIC X(08)  VALUE SPACES.
          05 WS-EIBDATE         PIC 9(05)  VALUE ZERO.
          05 WS-RESP            PIC S9(08) COMP VALUE ZERO.
        01 WS-SEND-AREA.
#SEND-AREAS#
        01 WS-RECEIVE-AREA.
#RECEIVE-AREAS#
       
       01 IRIS-WORK-AREA-PTR  USAGE POINTER. 

       LINKAGE SECTION.

       01 DFHCOMMAREA          PIC X(42000).
      *
      *    IRIS PCB AREA
      *
           COPY IRIPCBDC.
      *
      *    IRIS MESSAGE AREA
      *
        01 IRIS-MESSAGE-AREA PIC X(42000).
      *
      *    IRIS GLOBAL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='NOME-RTE'==.
      
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHEIBLK
                                DFHCOMMAREA
                                IRIS-DC-PCB 
                                IRIS-MESSAGE-AREA.
       MAIN-START.
           SET IRIS-WORK-AREA-PTR TO ADDRESS OF DFHCOMMAREA
           SET ADDRESS OF IRIS-WORK-AREA TO IRIS-WORK-AREA-PTR
      *    #OPE#
       MAIN-END.
           GOBACK.
      *----------------------------------------------------------------*
      *            SPLIT THE FIELD CONTAINING THE ATTRIBUTE            *
      *----------------------------------------------------------------*
       SPLIT-HWRD.
           IF HALF-WORD  >= 49152
              SUBTRACT 49152 FROM HALF-WORD
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           MOVE LOW-VALUE                 TO CX-ATTRIB.
           MOVE HALF-WORD                 TO HALF-WORD-R.
       SPLIT-HWRD-EX.
           EXIT.
      *----------------------------------------------------------------*
      *      FIND THE CORRESPONDANCE BETWEEN IMS/CICS ATTRIBUTES       *
      *----------------------------------------------------------------*
       FND-ATTRIB.
           IF HALF-WORD-R   =   196
              MOVE '<'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   197
              MOVE '('                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   200
              MOVE 'H'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   201
              MOVE 'I'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   208
              MOVE 'M'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   209
              MOVE 'J'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   212
              MOVE '*'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   213
              MOVE ')'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   216
              MOVE 'Q'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   217
              MOVE 'R'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   224
              MOVE '-'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   225
              MOVE '/'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   228
              MOVE '%'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   229
              MOVE '_'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   232
              MOVE 'Y'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   233
              MOVE 'Z'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   241
              MOVE '/'                    TO CX-ATTRIB
           END-IF
           IF HALF-WORD-R   =   248
              MOVE 'Z'                    TO CX-ATTRIB
           END-IF
           .
       FND-ATTRIB-EX.
           EXIT.
       NEG-ATTRIB.
           IF HALF-WORD-R   =   -16191
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           IF HALF-WORD-R   =   -16183
              MOVE 'I'                    TO CX-ATTRIB
              MOVE 'Y'                    TO FLG-CURS
           END-IF
           .
       NEG-ATTRIB-EX.
           EXIT.
       ANALYZE-PFK.
           MOVE LOW-VALUE TO IRIS-PFKEY

           IF EIBAID = DFHENTER
              MOVE SPACE TO IRIS-PFKEY
              MOVE #dfvalue#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF1
              MOVE '01' TO IRIS-PFKEY
              MOVE #value1# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF2
              MOVE '02' TO IRIS-PFKEY
              MOVE #value2# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF3
              MOVE '03' TO IRIS-PFKEY
              MOVE #value3# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF4
              MOVE '04' TO IRIS-PFKEY
              MOVE #value4# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF5
              MOVE '05' TO IRIS-PFKEY
              MOVE #value5# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF6
              MOVE '06' TO IRIS-PFKEY
              MOVE #value6# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF7
              MOVE '07' TO IRIS-PFKEY
              MOVE #value7# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF8
              MOVE '08' TO IRIS-PFKEY
              MOVE #value8# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF9
              MOVE '09' TO IRIS-PFKEY
              MOVE #value9# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF10
              MOVE '10' TO IRIS-PFKEY
              MOVE #value10# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF11
              MOVE '11' TO IRIS-PFKEY
              MOVE #value11# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF12
              MOVE '12' TO IRIS-PFKEY
              MOVE #value12# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF13
              MOVE '13' TO IRIS-PFKEY
              MOVE #value13# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF14
              MOVE '14' TO IRIS-PFKEY
              MOVE #value14# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF15
              MOVE '15' TO IRIS-PFKEY
              MOVE #value15#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF16
              MOVE '16' TO IRIS-PFKEY
              MOVE #value16#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF17
              MOVE '17' TO IRIS-PFKEY
              MOVE #value17#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF18
              MOVE '18' TO IRIS-PFKEY
              MOVE #value18#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF19
              MOVE '19' TO IRIS-PFKEY
              MOVE #value19#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF20
              MOVE '20' TO IRIS-PFKEY
              MOVE #value20#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF21
              MOVE '21' TO IRIS-PFKEY
              MOVE #value21#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF22
              MOVE '22' TO IRIS-PFKEY
              MOVE #value22#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF23
              MOVE '23' TO IRIS-PFKEY
              MOVE #value23#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF24
              MOVE '24' TO IRIS-PFKEY
              MOVE #value24#
                TO WMFS-PFK
           END-IF
           . 
       EX-ANALYZE-PFK.
           EXIT.
#SEND-PARA#
#RECEIVE-PARA#           
	       #FIELD#.
#COPYMOVE#.
           COPY IRISDCDT.
