       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.
       DATE-WRITTEN.  DATA-CREAZ.
       DATE-COMPILED. DATA-COM.

      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   06 04 2005        (RELEASE 2.1)   **
      **                                                              **
      **  EXECUTED BY           :   Development and support group     **
      **                            Mainframe Affinity                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **                                                              **
      **  IMS/DC VERSION                                              ** 
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            **
      **                                                              **
      **--------------------------------------------------------------**
      **
      **  #CODE#
      **
      **--------------------------------------------------------------**

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      /----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

           COPY ERRDBRT.

       01  WK-NUMPCB         PIC S9(4) COMP VALUE ZEROES.
       01  WK-DATA-AREA1     PIC X(12000)   VALUE SPACES.
       01  WK-AREALENGTH1    PIC S9(4) COMP VALUE ZEROES.
       01  WK-KEYVALUE1      PIC X(100)     VALUE SPACES.
       01  WK-KEYLENGTH1     PIC S9(4) COMP VALUE ZEROES.
       01  WK-SEGNAME1       PIC X(8)       VALUE SPACES.
       01  WK-DATA-AREA2     PIC X(12000)   VALUE SPACES.
       01  WK-AREALENGTH2    PIC S9(4) COMP VALUE ZEROES.
       01  WK-KEYVALUE2      PIC X(100)     VALUE SPACES.
       01  WK-KEYLENGTH2     PIC S9(4) COMP VALUE ZEROES.
       01  WK-SEGNAME2       PIC X(8)       VALUE SPACES.
       01  WK-DATA-AREA3     PIC X(12000)   VALUE SPACES.
       01  WK-AREALENGTH3    PIC S9(4) COMP VALUE ZEROES.
       01  WK-KEYVALUE3      PIC X(100)     VALUE SPACES.
       01  WK-KEYLENGTH3     PIC S9(4) COMP VALUE ZEROES.
       01  WK-SEGNAME3       PIC X(8)       VALUE SPACES.
       01  WK-DATA-AREA4     PIC X(12000)   VALUE SPACES.
       01  WK-AREALENGTH4    PIC S9(4) COMP VALUE ZEROES.
       01  WK-KEYVALUE4      PIC X(100)     VALUE SPACES.
       01  WK-KEYLENGTH4     PIC S9(4) COMP VALUE ZEROES.
       01  WK-SEGNAME4       PIC X(8)       VALUE SPACES.
       01  WK-DATA-AREA5     PIC X(12000)   VALUE SPACES.
       01  WK-AREALENGTH5    PIC S9(4) COMP VALUE ZEROES.
       01  WK-KEYVALUE5      PIC X(100)     VALUE SPACES.
       01  WK-KEYLENGTH5     PIC S9(4) COMP VALUE ZEROES.
       01  WK-SEGNAME5       PIC X(8)       VALUE SPACES.
       01  WK-FDBKEY         PIC X(100)     VALUE SPACES.

       LINKAGE SECTION.

       01  DFHCOMMAREA.
           COPY LNKDBRT.
           COPY DWPARM.

      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA DWPARM.


       MAIN-START.
      *----------------------------------------------------------------*
           EXEC CICS PUSH HANDLE END-EXEC.
           EXEC CICS HANDLE ABEND LABEL (ROUT-ABEND) END-EXEC.
           
           PERFORM INIT-AREAS THRU INIT-AREAS-EX.

      *    COMMAND RECOGNITION AND
      *    RELATIVE SECTION ACTIVATION

      *    #OPE#

           MOVE 'NOOP' TO LNKDB-CODE-ERROR
           PERFORM ROUT-ERROR THRU ROUT-ERROR-EX
           GO TO MAIN-END.

       MAIN-001.
           PERFORM PREPARE-EXIT THRU PREPARE-EXIT-EX.

      *----------------------------------------------------------------*

       MAIN-END.
           PERFORM Z999-RETURN THRU Z999-RETURN-EX.
           GOBACK.

      *----------------------------------------------------------------*
      *             A R E A S   I N I T I A L I Z A T I O N
      *----------------------------------------------------------------*
       INIT-AREAS.

           MOVE SPACES TO LNKDB-ABEND-RTCODE.
           IF LNKDB-STATE GREATER SPACES
              GO TO INIT-AREAS-EX
           END-IF.

           MOVE 'A' TO LNKDB-STATE.

           MOVE ZERO TO LNKDB-NUMSTACK.
           PERFORM UNTIL LNKDB-NUMSTACK > 49
               ADD 1 TO LNKDB-NUMSTACK
               MOVE SPACES    TO LNKDB-STKNAME(LNKDB-NUMSTACK)
               MOVE ZERO      TO LNKDB-STKTBNUM(LNKDB-NUMSTACK)
               MOVE ZERO      TO LNKDB-STKNUMPCB(LNKDB-NUMSTACK)
               MOVE SPACES    TO LNKDB-STKLASTOP(LNKDB-NUMSTACK)
               MOVE SPACES    TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)
               MOVE LOW-VALUE TO LNKDB-STKFDBKEY(LNKDB-NUMSTACK)
               MOVE ZERO      TO LNKDB-STKKEYLEN(LNKDB-NUMSTACK)
           END-PERFORM
           MOVE ZERO TO LNKDB-NUMSTACK.

       INIT-AREAS-EX.
           EXIT.

      *----------------------------------------------------------------*
      *             P R E P A R E   E X I T   A R E A S
      *            S E T   T H E   R E T U R N   C O D E
      *----------------------------------------------------------------*
       PREPARE-EXIT.
           MOVE DIBVER   TO LNKDB-DIBVER.
           MOVE DIBSTAT  TO LNKDB-DIBSTAT.
           MOVE DIBSEGM  TO LNKDB-DIBSEGM.
           MOVE DIBFIL01 TO LNKDB-DIBFIL01.
           MOVE DIBFIL02 TO LNKDB-DIBFIL02.
           MOVE DIBSEGLV TO LNKDB-DIBSEGLV.
           MOVE DIBKFBL  TO LNKDB-DIBKFBL.
           MOVE DIBDBDNM TO LNKDB-DIBDBDNM.
           MOVE DIBDBORG TO LNKDB-DIBDBORG.
           MOVE DIBFIL03 TO LNKDB-DIBFIL03.
       PREPARE-EXIT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *             R E T U R N   T O   C A L L E R
      *----------------------------------------------------------------*
       Z999-RETURN.
           EXEC CICS POP HANDLE END-EXEC.
           GOBACK.
       Z999-RETURN-EX.
           EXIT.

      *
      *----------------------------------------------------------------*
      *             E R R O R S   S E C T I O N
      *----------------------------------------------------------------*

       ROUT-ERROR.
           IF LNKDB-CODE-ERROR = 'NOOP'
                   MOVE MSGDB-NOOP TO LNKDB-RTIOC
                   MOVE ACTION-NOOP TO ACTION-ERRORE
                   GO TO ROUT-ERR01
           END-IF

           IF LNKDB-CODE-ERROR = 'DLSA'
                   MOVE MSGDB-DLSA TO LNKDB-RTIOC
                   MOVE ACTION-DLSA TO ACTION-ERRORE

                   GO TO ROUT-ERR01
           END-IF

           MOVE MSGDB-NOER TO LNKDB-RTIOC
           MOVE CODDB-NOER TO LNKDB-CODE-ERROR
           MOVE ACTION-NOER TO ACTION-ERRORE.

       ROUT-ERR01.

           IF ACTION-ERRORE = 1
              MOVE 'ADL0' TO LNKDB-ABEND-RTCODE
           END-IF.
       ROUT-ERROR-EX.
           EXIT.

       ROUT-ABEND.
           MOVE 'ADL1' TO LNKDB-ABEND-RTCODE.
           PERFORM Z999-RETURN THRU Z999-RETURN-EX.
       ROUT-ABEND-EX.
           EXIT.

