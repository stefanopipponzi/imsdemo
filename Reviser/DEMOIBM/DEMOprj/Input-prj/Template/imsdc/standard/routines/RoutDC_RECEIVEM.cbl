       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.

      *REMARKS.
      **--------------------------------------------------------------**
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **     GENERALIZED CICS ROUTINE TO MANAGE "RECEIVE MAP"         **
      **     GENERATED FROM IMS/DC                                    **
      **                                                              **
      **  #CODE#
      **
      **--------------------------------------------------------------**
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*

           COPY IRISCOMM.
           COPY #CICS-MAP#.

       01  RECEIVE-AREA           PIC X(2000).
       01  RESPONSEX              PIC S9(8) COMP.
       01  WMFS-PFK               PIC X(#LENPFK#).
       
       01 IRIS-WS-MESSAGE-AREA.
          COPY #FORMAT-AREAS#.
       
           COPY DFHAID.

       01  BASE-PAGE               PIC S9(8) COMP.   
       01  LEN-PAGE                PIC S9(8) COMP.  

       
       LINKAGE SECTION.

          01 DFHCOMMAREA              PIC X(42000).
      *
      *    IRIS PCB AREA
      *
           COPY IRIPCBDC.

        01 IRIS-MESSAGE-AREA            PIC X(4096).
      *
      *    IRIS GLOBAL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='NOME-RTE'==.
      
      *----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHEIBLK
                                DFHCOMMAREA
                                IRIS-DC-PCB 
                                IRIS-MESSAGE-AREA.

       MAIN-START.
      
      *    #OPE#

            

       MAIN-END.
           GOBACK.
           
      
      *    #OPEREC#           
      
       ANALYZE-PFK.
           MOVE LOW-VALUE TO IRIS-PFKEY

           IF EIBAID = DFHENTER
              MOVE SPACE TO IRIS-PFKEY
              MOVE #dfvalue#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF1
              MOVE '01' TO IRIS-PFKEY
              MOVE #value1# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF2
              MOVE '02' TO IRIS-PFKEY
              MOVE #value2# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF3
              MOVE '03' TO IRIS-PFKEY
              MOVE #value3# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF4
              MOVE '04' TO IRIS-PFKEY
              MOVE #value4# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF5
              MOVE '05' TO IRIS-PFKEY
              MOVE #value5# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF6
              MOVE '06' TO IRIS-PFKEY
              MOVE #value6# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF7
              MOVE '07' TO IRIS-PFKEY
              MOVE #value7# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF8
              MOVE '08' TO IRIS-PFKEY
              MOVE #value8# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF9
              MOVE '09' TO IRIS-PFKEY
              MOVE #value9# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF10
              MOVE '10' TO IRIS-PFKEY
              MOVE #value10# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF11
              MOVE '11' TO IRIS-PFKEY
              MOVE #value11# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF12
              MOVE '12' TO IRIS-PFKEY
              MOVE #value12# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF13
              MOVE '13' TO IRIS-PFKEY
              MOVE #value13# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF14
              MOVE '14' TO IRIS-PFKEY
              MOVE #value14# 
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF15
              MOVE '15' TO IRIS-PFKEY
              MOVE #value15#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF16
              MOVE '16' TO IRIS-PFKEY
              MOVE #value16#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF17
              MOVE '17' TO IRIS-PFKEY
              MOVE #value17#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF18
              MOVE '18' TO IRIS-PFKEY
              MOVE #value18#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF19
              MOVE '19' TO IRIS-PFKEY
              MOVE #value19#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF20
              MOVE '20' TO IRIS-PFKEY
              MOVE #value20#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF21
              MOVE '21' TO IRIS-PFKEY
              MOVE #value21#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF22
              MOVE '22' TO IRIS-PFKEY
              MOVE #value22#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF23
              MOVE '23' TO IRIS-PFKEY
              MOVE #value23#
                TO WMFS-PFK
           END-IF
           IF EIBAID = DFHPF24
              MOVE '24' TO IRIS-PFKEY
              MOVE #value24#
                TO WMFS-PFK
           END-IF
           . 
       EX-ANALYZE-PFK.
           EXIT.
