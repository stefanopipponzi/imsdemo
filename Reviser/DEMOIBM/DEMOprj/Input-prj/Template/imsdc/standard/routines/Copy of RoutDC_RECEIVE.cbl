       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.
       DATE-WRITTEN.  DATA-CREAZ.
       DATE-COMPILED. DATA-COM.

      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   DATA-CREAZ        (RELEASE 1.1A ) **
      **                                                              **
      **  EXECUTED BY           :   DEVELOPMENT AND SUPPORT GROUP     **
      **                            MAINFRAME AFFINITY                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1) GENERALIZED CICS ROUTINE TO MANAGE "RECEIVE MAP"         **
      **     GENERATED FROM IMS/DC                                    **
      **                                                              **
      **  STANDARD TEMPLATE SOURCE                                    **
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            **
      **                                                              **
      **--------------------------------------------------------------**
      **
      **  #CODE#
      **
      **--------------------------------------------------------------**
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      *----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *--------------------------------------------------*
      *  MANAGEMENT PARAMETERS FOR TREATMENT ROUTINES    *
      *  SPECIFICALLY FIELDS IMS->CICS AND VICEVERSA     *
      *--------------------------------------------------*

       COPY #CICS-MAP#.

       01  RECEIVE-AREA           PIC X(2000).
       01  RESPONSE               PIC S9(8) COMP.
       
       #FORMAT-AREAS#.
       
           COPY DFHAID.
       
       LINKAGE SECTION.

       01  DFHCOMMAREA.
           COPY LNKDCRT.
      
      *----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA.

       MAIN-START.
      
      *    #OPE#

           MOVE SPACES TO LNKDC-RETCODE.
      

       MAIN-END.
           GOBACK.
           
      
      *    #OPEREC#           
      
   