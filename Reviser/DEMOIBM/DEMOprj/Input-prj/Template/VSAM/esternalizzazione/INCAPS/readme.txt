I template principali sono:

COMMONINIT
MAINCOMB 
MAINCALL

Su tutte le righe viene inserito il commento impostato da programma.
L'inserimento del commento non modifica l'indentazione originale

I tag non sostituiti fanno si che la riga venga ignorata al momento dell'inserimento nel programma originale
I tag da sostituire con variabili sono quelli che contengono parole in  maiuscolo (senza carattere %)
I tag in minuscolo indicano che la riga verr� ignorata o utilizzata 
I tag tipo <nomevar%....  sono:

1) %IND sono di indentazione (- verso sinistra  + verso destra)
2) nomevar%istruzione1/istruzione2 indicano due possibili alternative di identificatore
