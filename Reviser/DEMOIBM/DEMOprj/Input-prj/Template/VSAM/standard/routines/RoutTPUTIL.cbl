       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.
       DATE-WRITTEN.  DATA-CREAZ.
       DATE-COMPILED. DATA-COM.
       
      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   22 07 2005        (RELEASE 3.0)   **
      **                                                              **
      **  EXECUTED BY           :   Development and support group     **
      **                            Mainframe Affinity                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  CICS Routine for UTILITY commands                           **
      **                                                              **
      **  DB2 VERSION                                                 ** 
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            ** 
      **                                                              **
      **--------------------------------------------------------------**
      ** 
      **  #CODE#   

      *  PCB.000020 - T2UTILPCB.
      *                  
      **
      **--------------------------------------------------------------**
 
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      /----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.
       
           COPY ERRDBRT.

       01  WK-NAMETABLE       PIC X(16).
       01  WK-SELTABLE        PIC X(16).
       01  WK-ISTR            PIC X(4).

           EXEC SQL INCLUDE SQLCA END-EXEC.
 

       01  WK-SQLCODE        PIC S9(8) COMP.
       01  WK-IND            PIC S9(8) COMP.
       01  WINDICE           PIC S9(8) COMP.

       01  WK-OPERATION      PIC X(4)  VALUE SPACES.
       01  WK-PSBNAME        PIC X(8)  VALUE SPACES.       
       01  WK-CHKPID         PIC X(8)  VALUE SPACES.
       01  WK-NUMPCB         PIC S9(4) COMP VALUE ZEROES.

       01  WK-DATA-AREA1     PIC X(100)    VALUE SPACES.
 
           EXEC SQL INCLUDE WKDATERT END-EXEC.

       LINKAGE SECTION.

       01  DFHCOMMAREA.
           COPY LNKDBRT.
           COPY DLIUIB.
           
       01  PCB-AREA.
         05 DBD-NAME       PIC X(8).                                          
         05 SEG-LEVEL      PIC XX.                                            
         05 ST-CODE        PIC XX.                                            
         05 PROC-OPT       PIC X(4).                                           
         05 R-DLI          PIC S9(5) COMP.                                   
         05 SEG-NAME       PIC X(8).                                          
         05 L-KEY          PIC S9(5) COMP.                            
         05 SEN-SEG        PIC S9(5) COMP.                            
         05 KEFEDBAK       PIC X(100).                               
         
      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA.

       MAIN-START.
      *----------------------------------------------------------------*
           PERFORM INIT-AREAS THRU EX-INIT-AREAS.      
      
      *    COMMAND RECOGNITION AND
      *    RELATIVE SECTION ACTIVATION

      *    #OPE#    
            IF LNKDB-OPERATION = 'PCB.000020' 
               PERFORM ROUT-PCB000020
                  THRU ROUT-PCB000020-EX
               GO TO MAIN-001.
 
           MOVE 'NOOP' TO LNKDB-CODE-ERROR
           PERFORM ROUT-ERROR THRU EX-ROUT-ERROR
           GO TO MAIN-END.
                
       MAIN-001.
           PERFORM PREPARE-EXIT THRU EX-PREPARE-EXIT.

      *----------------------------------------------------------------*

       MAIN-END.
           PERFORM Z999-RETURN THRU Z999-EX-RETURN.
           GOBACK.

      *----------------------------------------------------------------*
      *             A R E A S   I N I T I A L I Z A T I O N
      *----------------------------------------------------------------*
       INIT-AREAS.
           
           MOVE SPACES TO LNKDB-ABEND-RTCODE.
           MOVE ZERO TO WK-SQLCODE.

           IF LNKDB-STATE GREATER SPACES
              GO TO EX-INIT-AREAS
           END-IF.
         
           INITIALIZE LNKDB-ACTIVE.    

           MOVE 'A' TO LNKDB-STATE.
           
           MOVE ZERO TO LNKDB-NUMSTACK.
       EX-INIT-AREAS.
           EXIT.

      *----------------------------------------------------------------*
      *             P R E P A R E   E X I T   A R E A S
      *            S E T   T H E   R E T U R N   C O D E
      *----------------------------------------------------------------*
       PREPARE-EXIT.
           MOVE 1 TO WK-IND 
           PERFORM UNTIL WK-IND GREATER MAX-SQLRESP
               IF WK-SQLCODE = ERRDB-CODRESP(WK-IND) 
                  MOVE ERRDB-CODDLI(WK-IND)  TO LNKDB-PCBSTAT
                  MOVE 2000 TO WK-IND
               ELSE 
                 IF ERRDB-CODRESP(WK-IND) = -999999                  
                    MOVE ERRDB-CODDLI(WK-IND)  TO LNKDB-PCBSTAT
                    MOVE 2000 TO WK-IND
                 END-IF 
               END-IF                 
               ADD 1 TO WK-IND
           END-PERFORM

           MOVE WK-NAMETABLE TO LNKDB-PCBSEGNM
           
           IF LNKDB-PCBSTAT NOT = '  '
              MOVE SPACES TO LNKDB-FDBKEY
           ELSE
              MOVE LNKDB-STKFDBKEY(LNKDB-NUMSTACK) TO LNKDB-FDBKEY
           END-IF
           
           PERFORM SET-LASTOP THRU EX-SET-LASTOP.
       EX-PREPARE-EXIT.
           EXIT.

      *------------------------------------------------------------*
      *  BACKUP ROUTINE OF LAST OPER IN LINKAGE AREA
      *------------------------------------------------------------*
       SET-LASTOP.

           MOVE WK-ISTR TO LNKDB-STKLASTOP(LNKDB-NUMSTACK).

       EX-SET-LASTOP.
           EXIT.
            
      *----------------------------------------------------------------*
      *             R E T U R N   T O   C A L L E R
      *----------------------------------------------------------------*
       Z999-RETURN.
            GOBACK.
       Z999-EX-RETURN.
           EXIT.

      *----------------------------------------------------------------*
      *             E R R O R S   S E C T I O N
      *----------------------------------------------------------------*

       ROUT-ERROR.
           IF LNKDB-CODE-ERROR = 'NOOP'
                   MOVE MSGDB-NOOP TO LNKDB-RTIOC
                   MOVE ACTION-NOOP TO ACTION-ERRORE
                   GO TO ROUT-ERR01
           END-IF
   
           IF LNKDB-CODE-ERROR = 'DLSA'
                   MOVE MSGDB-DLSA TO LNKDB-RTIOC
                   MOVE ACTION-DLSA TO ACTION-ERRORE

                   GO TO ROUT-ERR01
           END-IF       
   
           MOVE MSGDB-NOER TO LNKDB-RTIOC 
           MOVE CODDB-NOER TO LNKDB-CODE-ERROR 
           MOVE ACTION-NOER TO ACTION-ERRORE. 
             
       ROUT-ERR01.
           
           IF ACTION-ERRORE = 1
              MOVE 'ADL0' TO LNKDB-ABEND-RTCODE
           END-IF.
       EX-ROUT-ERROR.
           EXIT.

       ROUT-ABEND.
           MOVE 'ADL1' TO LNKDB-ABEND-RTCODE.
           PERFORM Z999-RETURN THRU Z999-EX-RETURN.
       EX-ROUT-ABEND.
           EXIT.
      

      *------------------------------------------------------------*
      *  Call   : PCB.000020  
      *  Coding : T2UTILPCB.
      *                  
      *------------------------------------------------------------*
       ROUT-PCB000020.
           MOVE LNKDB-OPERATION(1:4) TO WK-OPERATION.
           INSPECT WK-OPERATION REPLACING ALL '.' BY SPACE.
           MOVE LNKDB-PSBNAME TO WK-PSBNAME.
      
           CALL 'CBLTDLI' USING WK-OPERATION
                                WK-PSBNAME
                                ADDRESS OF DLIUIB.
      
           SET LNKDB-PTRPCB  TO ADDRESS OF DLIUIB.
      
           INITIALIZE LNKDB-ACTIVE.
      
       ROUT-PCB000020-EX.
           EXIT.

