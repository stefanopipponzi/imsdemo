       IDENTIFICATION DIVISION.
       PROGRAM-ID.    NOME-RTE.
       AUTHOR.        AUTHOR-RTE.
       DATE-WRITTEN.  DATA-CREAZ.
       DATE-COMPILED. DATA-COM.

      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   06 04 2005        (RELEASE 2.1)   **
      **                                                              **
      **  EXECUTED BY           :   Development and support group     **
      **                            Mainframe Affinity                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  CICS Routine for GN and GHN commands                        **
      **                                                              **
      **  DLI VERSION                                                 ** 
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            **
      **                                                              **
      **--------------------------------------------------------------**
      **
      **  #CODE#
      **
      **--------------------------------------------------------------**

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       DATA DIVISION.

      /----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.

           COPY ERRDBRT.

       01  WK-OPERATION      PIC X(4)        VALUE SPACES.
       01  WK-DATA-AREA1     PIC X(12000)    VALUE SPACES.
       01  WK-SSA-AREA1      PIC X(400)      VALUE SPACES.
       01  WK-DATA-AREA2     PIC X(12000)    VALUE SPACES.
       01  WK-SSA-AREA2      PIC X(400)      VALUE SPACES.
       01  WK-DATA-AREA3     PIC X(12000)    VALUE SPACES.
       01  WK-SSA-AREA3      PIC X(400)      VALUE SPACES.
       01  WK-DATA-AREA4     PIC X(12000)    VALUE SPACES.
       01  WK-SSA-AREA4      PIC X(400)      VALUE SPACES.
       01  WK-DATA-AREA5     PIC X(12000)    VALUE SPACES.
       01  WK-SSA-AREA5      PIC X(400)      VALUE SPACES.

       LINKAGE SECTION.

       01  DFHCOMMAREA.
           COPY LNKDBRT.
       01  PCB-AREA.
           05 DBD-NAME       PIC X(8).                                  00000700
           05 SEG-LEVEL      PIC XX.                                    00000800
           05 ST-CODE        PIC XX.                                    00000900
           05 PROC-OPT       PIC X(4).                                  00001000
           05 R-DLI          PIC S9(5) COMP.                            00001100
           05 SEG-NAME       PIC X(8).                                  00001200
           05 L-KEY          PIC S9(5) COMP.                            00001300
           05 SEN-SEG        PIC S9(5) COMP.                            00001400
           05 KEFEDBAK       PIC X(100).                                00001500

      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA.


       MAIN-START.
      *----------------------------------------------------------------*
           PERFORM INIT-AREAS THRU INIT-AREAS-EX.

      *    COMMAND RECOGNITION AND
      *    RELATIVE SECTION ACTIVATION

      *    #OPE#

           MOVE 'NOOP' TO LNKDB-CODE-ERROR
           EXEC CICS ABEND ABCODE('NOOP') END-EXEC
           PERFORM ROUT-ERROR THRU ROUT-ERROR-EX
           GO TO MAIN-END.

       MAIN-001.
           PERFORM PREPARE-EXIT THRU PREPARE-EXIT-EX.

      *----------------------------------------------------------------*

       MAIN-END.
           PERFORM Z999-RETURN THRU Z999-RETURN-EX.
           GOBACK.

      *----------------------------------------------------------------*
      *             A R E A S   I N I T I A L I Z A T I O N
      *----------------------------------------------------------------*
       INIT-AREAS.

           SET ADDRESS OF PCB-AREA TO LNKDB-PTRPCB

           MOVE SPACES TO LNKDB-ABEND-RTCODE.
           IF LNKDB-STATE GREATER SPACES
              GO TO INIT-AREAS-EX
           END-IF.

           INITIALIZE LNKDB-ACTIVE.

           MOVE 'A' TO LNKDB-STATE.

           MOVE ZERO TO LNKDB-NUMSTACK.

       INIT-AREAS-EX.
           EXIT.

      *----------------------------------------------------------------*
      *             P R E P A R E   E X I T   A R E A S
      *            S E T   T H E   R E T U R N   C O D E
      *----------------------------------------------------------------*
       PREPARE-EXIT.
           MOVE PCB-AREA TO LNKDB-DLZPCB.
       PREPARE-EXIT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *             R E T U R N   T O   C A L L E R
      *----------------------------------------------------------------*
       Z999-RETURN.
           GOBACK.
       Z999-RETURN-EX.
           EXIT.

      *
      *----------------------------------------------------------------*
      *             E R R O R S   S E C T I O N
      *----------------------------------------------------------------*

       ROUT-ERROR.
           IF LNKDB-CODE-ERROR = 'NOOP'
                   MOVE MSGDB-NOOP TO LNKDB-RTIOC
                   MOVE ACTION-NOOP TO ACTION-ERRORE
                   GO TO ROUT-ERR01
           END-IF

           IF LNKDB-CODE-ERROR = 'DLSA'
                   MOVE MSGDB-DLSA TO LNKDB-RTIOC
                   MOVE ACTION-DLSA TO ACTION-ERRORE

                   GO TO ROUT-ERR01
           END-IF

           MOVE MSGDB-NOER TO LNKDB-RTIOC
           MOVE CODDB-NOER TO LNKDB-CODE-ERROR
           MOVE ACTION-NOER TO ACTION-ERRORE.

       ROUT-ERR01.

           IF ACTION-ERRORE = 1
              MOVE 'ADL0' TO LNKDB-ABEND-RTCODE
           END-IF.
       ROUT-ERROR-EX.
           EXIT.

       ROUT-ABEND.
           MOVE 'ADL1' TO LNKDB-ABEND-RTCODE.
           PERFORM Z999-RETURN THRU Z999-RETURN-EX.
       ROUT-ABEND-EX.
           EXIT.

