       IDENTIFICATION DIVISION.
       PROGRAM-ID.    #NOME_RTE#.
       AUTHOR.        #LABEL_AUTHOR#.
       DATE-WRITTEN.  #DATA_CREAZ#.
       DATE-COMPILED. #DATA_COM#.
       
      *REMARKS.
      **--------------------------------------------------------------**
      **  LAST MODIFICATION DATE:   22 07 2005        (RELEASE 3.0)   **
      **                                                              **
      **  EXECUTED BY           :   Development and support group     **
      **                            Mainframe Affinity                **
      **                                                              **
      **          DESCRIPTION                                         **
      **          -----------                                         **
      **  1)Generalized CICS Routine for ISRT, REPL and DLET          **
      **                                                              **
      **  STANDARD TEMPLATE SOURCE                                    **
      **                                                              **
      **--------------------------------------------------------------**
      **                                                              **
      **         CODED INSTRUCTIONS INTERNAL TO THE MODULE            ** 
      **                                                              **
      **--------------------------------------------------------------**
      ** 
      **  #CODE#   
      **
      **--------------------------------------------------------------**
 
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      /----------------------------------------------------------------*
      *           W O R K I N G     S T O R A G E                      *
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.
           
       01 INIZIO-WS PIC X(70) 
              VALUE '*** INIZIO WORKING STORAGE #NOME_RTE# ***'.
           
           COPY ERRDBRT.

       01  WK-NAMETABLE       PIC X(16).
       01  WK-SEGMENT         PIC X(8).
       01  WK-ISTR            PIC X(4).

           EXEC SQL INCLUDE SQLCA END-EXEC.
 
      *#COPY_WK_DB2#    
 
       01  WK-SQLCODE             PIC S9(8) COMP.
       01  WK-IND                 PIC S9(8) COMP.
       01  WINDICE                PIC S9(8) COMP.
       
       01  WK-SSA.
           03 WK-KEYNAME      PIC X(030).
           03 WK-KEYOPER      PIC X(002).
           03 WK-KEYVALUE     PIC X(100).

           EXEC SQL INCLUDE WKDATERT END-EXEC.

       LINKAGE SECTION.

       01  DFHCOMMAREA.
           COPY LNKDBRT.
    

      /----------------------------------------------------------------*
      *          P R O C E D U R E     D I V I S I O N                 *
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING DFHCOMMAREA.


       MAIN-START.
      *----------------------------------------------------------------*
           PERFORM INIT-AREAS THRU INIT-AREAS-EX.      
      
      *    COMMAND RECOGNITION AND
      *    RELATIVE SECTION ACTIVATION

      *    #OPE#    
 
           MOVE 'NOOP' TO LNKDB-CODE-ERROR
           PERFORM ROUT-ERROR THRU ROUT-ERROR-EX
           GO TO MAIN-END.
                
       MAIN-001.
           PERFORM PREPARE-EXIT THRU PREPARE-EXIT-EX.

      *----------------------------------------------------------------*

       MAIN-END.
           PERFORM Z999-RETURN THRU Z999-RETURN-EX.
           GOBACK.

      *----------------------------------------------------------------*
      *             A R E A S   I N I T I A L I Z A T I O N
      *----------------------------------------------------------------*
       INIT-AREAS.
           
           MOVE SPACES TO LNKDB-ABEND-RTCODE.
           MOVE ZERO TO WK-SQLCODE.

           EXEC CICS ASKTIME  ABSTIME(DATETIME)            
           END-EXEC.                                       
                                                           
           EXEC CICS FORMATTIME ABSTIME(DATETIME)          
                     YYMMDD            (WK-SYSTEM-DATE)            
                     TIME              (WK-SYSTEM-TIME)            
           END-EXEC.                                       
                                                           
           EXEC CICS ASSIGN USERID(WK-USERID)   
           END-EXEC.

      *    MOVE LNKDB-CALLER  TO  WK-PGMCREAZ.

           IF LNKDB-STATE GREATER SPACES
              GO TO INIT-AREAS-EX
           END-IF.
         
           INITIALIZE LNKDB-ACTIVE.    

           MOVE 'A' TO LNKDB-STATE.
           
           MOVE ZERO TO LNKDB-NUMSTACK.
       INIT-AREAS-EX.
           EXIT.

      *----------------------------------------------------------------*
      *             P R E P A R E   E X I T   A R E A S
      *            S E T   T H E   R E T U R N   C O D E
      *----------------------------------------------------------------*
       PREPARE-EXIT.
           MOVE 1 TO WK-IND 
           PERFORM UNTIL WK-IND GREATER MAX-SQLRESP
               IF WK-SQLCODE = ERRDB-CODRESP(WK-IND) 
                  MOVE ERRDB-CODDLI(WK-IND)  TO LNKDB-PCBSTAT
                  MOVE 2000 TO WK-IND
               ELSE 
                 IF ERRDB-CODRESP(WK-IND) = -999999                  
                    MOVE ERRDB-CODDLI(WK-IND)  TO LNKDB-PCBSTAT
                    MOVE 2000 TO WK-IND
                 END-IF 
               END-IF                 
               ADD 1 TO WK-IND
           END-PERFORM

           MOVE WK-SEGMENT TO LNKDB-PCBSEGNM
           
           IF LNKDB-PCBSTAT NOT = '  '
              MOVE SPACES TO LNKDB-FDBKEY
           END-IF
           
           PERFORM SET-LASTOP THRU SET-LASTOP-EX.
       PREPARE-EXIT-EX.
           EXIT.
           
      *------------------------------------------------------------*
      *  DELETE CHILD ROUTINE
      *------------------------------------------------------------*
       DEL-CHILD.
           MOVE ZERO TO SQLCODE.
      *#DEL-CHILD#
       
       DEL-CHILD-EX.
           EXIT.
          
      *------------------------------------------------------------*
      *  BACKUP ROUTINE OF LAST OPER IN LINKAGE AREA
      *------------------------------------------------------------*
       SET-LASTOP.
           IF WK-ISTR = 'GU' OR 'ISRT'
              MOVE SPACES TO LNKDB-STKCURSOR(LNKDB-NUMSTACK)
           END-IF
           IF WK-ISTR = 'GU' OR 'ISRT' OR 'GN' OR 'GNP'
              MOVE ZERO TO WK-IND
              PERFORM UNTIL WK-IND > 49
                 ADD 1 TO WK-IND
                 IF LNKDB-NUMPCB = LNKDB-STKNUMPCB(WK-IND)
                 AND LNKDB-STKLEV(LNKDB-NUMSTACK) <
                                      LNKDB-STKLEV(WK-IND)
                     MOVE SPACES TO LNKDB-STKCURSOR(WK-IND)
                     MOVE SPACES TO LNKDB-STKLASTOP(WK-IND)
                     MOVE SPACES TO LNKDB-STKSSA(WK-IND)
                 END-IF
              END-PERFORM
           END-IF

           MOVE WK-ISTR TO LNKDB-STKLASTOP(LNKDB-NUMSTACK).

       SET-LASTOP-EX.
           EXIT.
            
      *----------------------------------------------------------------*
      *             R E T U R N   T O   C A L L E R
      *----------------------------------------------------------------*
       Z999-RETURN.
            GOBACK.
       Z999-RETURN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *             E R R O R S   S E C T I O N
      *----------------------------------------------------------------*

       ROUT-ERROR.
           IF LNKDB-CODE-ERROR = 'NOOP'
              EXEC CICS ABEND ABCODE('NOOP') END-EXEC
                   MOVE MSGDB-NOOP TO LNKDB-RTIOC
                   MOVE ACTION-NOOP TO ACTION-ERRORE
                   GO TO ROUT-ERR01
           END-IF
   
           IF LNKDB-CODE-ERROR = 'DLSA'
                   MOVE MSGDB-DLSA TO LNKDB-RTIOC
                   MOVE ACTION-DLSA TO ACTION-ERRORE

                   GO TO ROUT-ERR01
           END-IF       
   
           MOVE MSGDB-NOER TO LNKDB-RTIOC 
           MOVE CODDB-NOER TO LNKDB-CODE-ERROR 
           MOVE ACTION-NOER TO ACTION-ERRORE. 
             
       ROUT-ERR01.
           
           IF ACTION-ERRORE = 1
              MOVE 'ADL0' TO LNKDB-ABEND-RTCODE
           END-IF.
       ROUT-ERROR-EX.
           EXIT.

       ROUT-ABEND.
           MOVE 'ADL1' TO LNKDB-CODE-ERROR.
           PERFORM Z999-RETURN THRU Z999-RETURN-EX.
       ROUT-ABEND-EX.
           EXIT.

      *------------------------------------------------------------* 
      *                      PCB MANAGING                          *
      *------------------------------------------------------------* 

       MANAGE-PCB. 
           MOVE ZERO TO WK-IND
           MOVE 1    TO LNKDB-NUMSTACK

           PERFORM UNTIL WK-IND > 49
             ADD 1 TO WK-IND             
             IF LNKDB-NUMPCB = LNKDB-STKNUMPCB(WK-IND) AND
                 WK-NAMETABLE = LNKDB-STKNAME(WK-IND) OR
                 ZEROES       = LNKDB-STKNUMPCB(WK-IND)
                 MOVE WK-IND TO LNKDB-NUMSTACK
                 MOVE 50 TO WK-IND
             END-IF
           END-PERFORM

           MOVE LNKDB-NUMPCB TO LNKDB-STKNUMPCB(LNKDB-NUMSTACK).     
           MOVE WK-NAMETABLE TO LNKDB-STKNAME(LNKDB-NUMSTACK). 
           
       MANAGE-PCB-EX. 
           EXIT. 


      *------------------------------------------------------------*
      *                  AREAS TRANSFER ROUTINES                   *
      *------------------------------------------------------------*
       
      *#COPY_PD_DB2#    
      
      *------------------------------------------------------------*
      *                   RESTORE LAST READ KEY                    *
      *------------------------------------------------------------*
       REST-FDBKEY.
       
      *#REST_FDBKEY#    
       
       REST-FDBKEY-EX.
       
           EXIT.
           
      *------------------------------------------------------------*
      *                     SAVE LAST READ KEY                     *
      *------------------------------------------------------------*
       STORE-FDBKEY.
       
      *#SET_FDBKEY# 
      
       STORE-FDBKEY-EX.   
       
           EXIT.
           
           EXEC SQL INCLUDE PDDATERT END-EXEC.
