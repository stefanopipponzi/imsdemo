      *----------------------------------------------------------------*
      *  LAST MODIFICATION DATE:   2007 02 25         ( RELEASE 1.0 )  *
      *                                                                *
      *  EXECUTED BY           :   Development and support group       *
      *                            Mainframe Affinity                  *
      *                                                                *
      *----------------------------------------------------------------*
      *                                                                *
      *                 INTERFACE AREA FOR MICROFOCUS FCD              *
      *             USED IN THE SEQUENTIAL ACCESS FROM GSAM            *
      *                                                                *
      *----------------------------------------------------------------*
       01  FCDGSAM.
          05  FCD-FILE-STATUS.
               10  FCD-STATUS-KEY-1 PIC X.
               10  FCD-STATUS-KEY-2 PIC X.
               10  FCD-BINARY       REDEFINES FCD-STATUS-KEY-2
                                    PIC 99 COMP-X.
           05  FILLER               PIC X(3).
           05  FCD-ORGANIZATION     PIC 9(2) COMP-X.
           05  FCD-ACCESS-MODE      PIC 9(2) COMP-X.
           05  FCD-OPEN-MODE        PIC 9(2) COMP-X.
           05  FILLER               PIC X(3).
           05  FCD-NAME-LENGTH      PIC 9(4) COMP-X.
           05  FCD-RELADDR-BIG      PIC X(8) COMP-X.
           05  FILLER               PIC X.
           05  FCD-TRANS-LOG        PIC 9(2) COMP-X.
           05  FILLER               PIC X(1).
           05  FCD-LOCK-MODE        PIC 9(2) COMP-X.
           05  FCD-OTHER-FLAGS      PIC 9(2) COMP-X.
           05  FILLER               PIC X(2).
           05  FCD-HANDLE           USAGE POINTER.
           05  FCD-PERCENT          PIC 9(2) COMP-X.
           05  FCD-STATUS-TYPE      PIC 9(2) COMP-X.
           05  FCD-FILE-FORMAT      PIC 9(2) COMP-X.
           05  FILLER               PIC X(3).
           05  FCD-MAX-REC-LENGTH   PIC 9(4) COMP-X.
           05  FILLER               PIC X(3).
           05  FCD-RELATIVE-KEY     PIC 9(9) COMP-X.
           05  FCD-RECORDING-MODE   PIC 9(2) COMP-X.
           05  FCD-CURRENT-REC-LEN  PIC 9(4) COMP-X.
           05  FCD-MIN-REC-LENGTH   PIC 9(4) COMP-X.
           05  FCD-KEY-ID           PIC 9(4) COMP-X.
           05  FCD-LINE-COUNT       REDEFINES FCD-KEY-ID
                                    PIC 9(4) COMP-X.
           05  FCD-KEY-LENGTH       PIC 9(4) COMP-X.
           05  FCD-RECORD-ADDRESS   USAGE POINTER.
           05  FCD-FILENAME-ADDRESS USAGE POINTER.
           05  FCD-KEY-DEF-ADDRESS  USAGE POINTER.
           05  FCD-COL-SEQ-ADDRESS  USAGE POINTER.
           05  FCD-RELADDR-OFFSET   PIC 9(9) COMP-X.
           05  FCD-RELADDR          REDEFINES FCD-RELADDR-OFFSET
                                    PIC 9(9) COMP-X.
           05  FILLER               PIC X(2).
           05  FCD-DATA-COMPRESS    PIC 9(2) COMP-X.
           05  FCD-SESSION-ID       PIC 9(9) COMP-X.
           05  FCD-FS-FILE-ID       PIC 9(4) COMP-X.
           05  FCD-MAX-REL-KEY      PIC 9(9) COMP-X.
           05  FCD-FLAGS-1          PIC 99   COMP-X.
           05  FCD-BLOCKING         PIC 99   COMP-X.
           05  FCD-LOCKTYPES        PIC 99   COMP-X.
           05  FCD-FS-FLAGS         PIC 99   COMP-X.
           05  FCD-CONFIG-FLAGS     PIC 99   COMP-X.
           05  FCD-MISC-FLAGS       PIC 99   COMP-X.
           05  FCD-CONFIG-FLAGS2    PIC 99   COMP-X.
           05  FCD-IDXCACHE-SIZE    PIC 99   COMP-X.
           05  FCD-IDXCACHE-BUFFS   PIC 99   COMP-X.
           05  FILLER               PIC X(2).

      *---------   END COPY -------------------------------------------*
