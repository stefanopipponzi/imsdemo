      *****************************************************************

      *  COMODI PER LA SEGNALAZIONE DEGLI ERRORI: VERSIONE COBOL/CICS *

      *****************************************************************

      *                                                               *

      *            A N A L I S I - E R R O R I - C I C S              *

      *            -------------------------------------              *

      *                                                               *

      *    QUEST'AREA VIENE UTILIZZATA PER DETERMINARE IL TIPO DI     *

      *    ERRORE CHE SI E' VERIFICATO IN SEGUITO ALL'ESECUZIONE      *

      *    DI UN COMANDO CICS.                                        *

      *                                                               *

      *            - PER UTILIZZARE QUEST'AREA DI ANALISI -           *

      *                                                               *

      *    IL PRIMO BYTE DELL'EIBRCODE CONTIENE IL VALORE NUMERICO    *

      *    CHE INDICA LO STATO DI ESECUZIONE DEL COMANDO.             *

      *    SI MUOVA L'EIBRCODE AL CAMPO EURA-EIBRCODE QUI SOTTO.      *

      *    POI SI UTILIZZINO I NOMI DI LIVELLO 88 PER DETERMINARE     *

      *    L'ERRORE.                                                  *

      *                                                               *

      *****************************************************************

       01 ITERCICS-ERROR-ANALYSIS.

          03 ITERCICS-WORK-AREA.

             05 ITERCICS-FILLER             PIC X(1)  VALUE LOW-VALUES.

             05 ITERCICS-EIBRCODE           PIC X(6).

          03 ITERCICS-ANALYZE-AREA     REDEFINES ITERCICS-WORK-AREA.

             05 ITERCICS-RETURN-CODE               PIC S9(4) COMP.

                88 ITERCICS-RETURN-NORMAL            VALUE +0.

                88 ITERCICS-RETURN-QZERO             VALUE +1.

                88 ITERCICS-RETURN-ITEMERR           VALUE +1.

                88 ITERCICS-RETURN-DSIDERR           VALUE +1.

                88 ITERCICS-RETURN-PGMIDERR          VALUE +1.

                88 ITERCICS-RETURN-INVREQ            VALUE +1 +2 +8 +32

                                                           +224 +255.

                88 ITERCICS-RETURN-QIDERR            VALUE +2.

                88 ITERCICS-RETURN-ILLOGIC           VALUE +2.

                88 ITERCICS-RETURN-MAPFAIL           VALUE +4.

                88 ITERCICS-RETURN-EOF               VALUE +4 +193.

                88 ITERCICS-RETURN-IOERR             VALUE +4 +7 +128.

                88 ITERCICS-RETURN-NOTOPEN           VALUE +5 +8 +12.

                88 ITERCICS-RETURN-LENGERR           VALUE +6 +225.

                88 ITERCICS-RETURN-INVMPSZ           VALUE +8.

                88 ITERCICS-RETURN-NOSPACE           VALUE +8 +16 +131.

                88 ITERCICS-RETURN-ENDFILE           VALUE +15.

                88 ITERCICS-RETURN-TRANSIDERR        VALUE +17.

                88 ITERCICS-RETURN-TERMIDERR         VALUE +18 +230.

                88 ITERCICS-RETURN-ENQBUSY           VALUE +32.

                88 ITERCICS-RETURN-NOTFND            VALUE +129.

                88 ITERCICS-RETURN-DUPREC            VALUE +130.

                88 ITERCICS-RETURN-DUPKEY            VALUE +132.

                88 ITERCICS-RETURN-QBUSY             VALUE +192.

                88 ITERCICS-RETURN-SYSIDERR          VALUE +208.

                88 ITERCICS-RETURN-ISCINVREQ         VALUE +209.

                88 ITERCICS-RETURN-NOTAUTH           VALUE +214.

             05 ITERCICS-FILLER                    PIC  X(5).

