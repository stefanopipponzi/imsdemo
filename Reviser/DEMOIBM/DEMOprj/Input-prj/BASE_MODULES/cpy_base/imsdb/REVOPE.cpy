       01  REVOPER.
           03  REVEQ1 PIC XX VALUE 'EQ'.
           03  REVEQ2 PIC XX VALUE '= '.
           03  REVEQ3 PIC XX VALUE ' ='.
           03  REVGT1 PIC XX VALUE 'GT'.
           03  REVGT2 PIC XX VALUE '> '.
           03  REVGT3 PIC XX VALUE ' >'.
           03  REVGE1 PIC XX VALUE 'GE'.
           03  REVGE2 PIC XX VALUE '>='.
           03  REVGE3 PIC XX VALUE '=>'.
           03  REVLT1 PIC XX VALUE 'LT'.
           03  REVLT2 PIC XX VALUE '< '.
           03  REVLT3 PIC XX VALUE ' <'.
           03  REVLE1 PIC XX VALUE 'LE'.
           03  REVLE2 PIC XX VALUE '<='.
           03  REVLE3 PIC XX VALUE '=<'.
           03  REVNE1 PIC XX VALUE 'NE'.
           03  REVNE2 PIC XX VALUE '�='.
           03  REVNE3 PIC XX VALUE '=�'.
           
       01  REVCOUNT PIC S9(4) COMP VALUE ZERO.    

       01  REVLEN PIC S9(4) COMP VALUE ZERO.    
