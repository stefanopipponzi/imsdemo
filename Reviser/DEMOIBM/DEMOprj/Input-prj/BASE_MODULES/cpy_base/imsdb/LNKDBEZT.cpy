MACRO
*----------------------------------------------------------------*
*  LAST MODIFICATION DATE:   DATA-MODIF         ( RELEASE 1.0 )  *
*                                                                *
*  EXECUTED BY           :   Development and support group       *
*                            Mainframe Affinity                  *
*                                                                *
*----------------------------------------------------------------*
*                                                                *
*               USER PROGRAM INTERFACE AREA                      *
*             FOR GENERALIZED ROUTINES OF I-O IMS                *
*             EASYTRIEVE VERSION                                 *
*----------------------------------------------------------------*
LNKDB-AREA W 20355 A
LNKDB-CALLER LNKDB-AREA +3 8 A
LNKDB-COMMAND LNKDB-AREA +10 12060 A
LNKDB-OPERATION LNKDB-AREA +10 10 A
LNKDB-NUMPCB LNKDB-AREA +24 2 B
LNKDB-SEGLEVEL1 LNKDB-AREA +50 2 A
LNKDB-DATA-AREA1 LNKDB-AREA +52 2000 A
LNKDB-SSA-AREA1 LNKDB-AREA +2054 100 A
LNKDB-KEYVALUE11 LNKDB-AREA +2162 60 A
LNKDB-KEYVALUE12 LNKDB-AREA +2237 60 A
LNKDB-KEYVALUE13 LNKDB-AREA +2312 60 A
LNKDB-SEGLEVEL2 LNKDB-AREA +2462 2 A
LNKDB-DATA-AREA2 LNKDB-AREA +2464 2000 A
LNKDB-SSA-AREA2 LNKDB-AREA +4466 100 A
LNKDB-KEYVALUE21 LNKDB-AREA +4574 60 A
LNKDB-KEYVALUE22 LNKDB-AREA +4649 60 A
LNKDB-KEYVALUE23 LNKDB-AREA +4724 60 A
LNKDB-SEGLEVEL3 LNKDB-AREA +4874 2 A
LNKDB-DATA-AREA3 LNKDB-AREA +4877 2000 A
LNKDB-SSA-AREA3 LNKDB-AREA +6878 100 A
LNKDB-KEYVALUE31 LNKDB-AREA +6986 60 A
LNKDB-KEYVALUE32 LNKDB-AREA +7061 60 A
LNKDB-KEYVALUE33 LNKDB-AREA +7136 60 A
LNKDB-PCBSTAT LNKDB-AREA +20165 2 A

*---------   END INCLUDE ----------------------------------------*