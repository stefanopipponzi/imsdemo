      *----------------------------------------------------------------*
      *  LAST MODIFICATION DATE:   DATA-MODIF         ( RELEASE 2.0 )  *
      *                                                                *
      *  EXECUTED BY           :   Development and support group       *
      *                            Mainframe Affinity                  *
      *                                                                *
      *----------------------------------------------------------------*
      *                                                                *
      *                 GENERIC MESSAGES AREA                          *
      *                                                                *
      *----------------------------------------------------------------*
       01  ACTION-ERRORE            PIC 9.
       01  ERRDBRT-AREA.
           02       ERRDB-NOOP.
             05     CODDB-NOER.
               10   FILLER          PIC X(2) VALUE 'XX'.
               10   FILLER          PIC X(6).
             05     MSGDB-NOER      PIC X(50) VALUE
               'GENERIC ERROR                                     '.
             05     ACTION-NOER     PIC 9  VALUE 0.

             05     CODDB-NOOP.
               10   FILLER          PIC X(2) VALUE 'XX'.
               10   FILLER          PIC X(6).
             05     MSGDB-NOOP      PIC X(50) VALUE
               'OPERATION NOT DECODED                             '.
             05     ACTION-NOOP     PIC 9  VALUE 1.

             05     CODDB-DLSA.
               10   FILLER          PIC X(2) VALUE 'XX'.
               10   FILLER          PIC X(6).
             05     MSGDB-DLSA      PIC X(50) VALUE
               'SSA NOT BUILT CORRECTLY                           '.
             05     ACTION-DLSA     PIC 9  VALUE 1.

      *----------------------------------------------------------------*
      *
      *             SQLCODE CONVERSION TABLE
      *
      *----------------------------------------------------------------*
       01  ERRDB-SQLRESP.
           02 ERRDB-00.
              05 FILLER             PIC S9(8) COMP  VALUE ZERO.
              05 FILLER             PIC X(36) VALUE
                 'NORMAL '.
              05 FILLER             PIC X(2)  VALUE '  '.

           02 ERRDB-100.
              05 FILLER             PIC S9(8) COMP  VALUE +100.
              05 FILLER             PIC X(36) VALUE
                 'ROW NOT FOUND'.
              05 FILLER             PIC X(2)  VALUE 'GE'.

           02 ERRDB-180.
              05 FILLER             PIC S9(8) COMP  VALUE -180.
              05 FILLER             PIC X(36) VALUE
                 'FORMAT DATE-TIME INVALID'.
              05 FILLER             PIC X(2)  VALUE 'XD'.

           02 ERRDB-501.
              05 FILLER             PIC S9(8) COMP  VALUE -501.
              05 FILLER             PIC X(36) VALUE
                 'FETCH/CLOSE CURSOR NOT OPEN'.
              05 FILLER             PIC X(2)  VALUE 'GE'.

           02 ERRDB-803.
              05 FILLER             PIC S9(8) COMP  VALUE -803.
              05 FILLER             PIC X(36) VALUE
                 'DUPLICATE KEY'.
              05 FILLER             PIC X(2)  VALUE 'II'.

          02 ERRDB-IT-805.
              05 FILLER             PIC S9(8) COMP    VALUE -805.
              05 FILLER             PIC X(36) VALUE
                 'BIND ERROR'.
              05 FILLER             PIC X(2)  VALUE 'BD'.

           02 ERRDB-922.
              05 FILLER             PIC S9(8) COMP  VALUE -922.
              05 FILLER             PIC X(36) VALUE
                 'NOT AUTHORIZED '.
              05 FILLER             PIC X(2)  VALUE 'TH'.

          02 ERRDB-IT-GENERIC.
              05 FILLER             PIC S9(8) COMP    VALUE -111101.
              05 FILLER             PIC X(36) VALUE
                 'GENERIC'.
              05 FILLER             PIC X(2)  VALUE 'AJ'.

          02 ERRDB-IT-NOTFOUND.
              05 FILLER             PIC S9(8) COMP    VALUE -111102.
              05 FILLER             PIC X(36) VALUE
                 'NOT FOUND'.
              05 FILLER             PIC X(2)  VALUE 'GE'.

          02 ERRDB-IT-NOTFOUND.
              05 FILLER             PIC S9(8) COMP    VALUE -111103.
              05 FILLER             PIC X(36) VALUE
                 'REC ALREADY EXISTS'.
              05 FILLER             PIC X(2)  VALUE 'II'.

          02 ERRDB-IT-NOTFOUND.
              05 FILLER             PIC S9(8) COMP    VALUE -111104.
              05 FILLER             PIC X(36) VALUE
                 'PARENT CHANGED'.
              05 FILLER             PIC X(2)  VALUE 'GA'.

          02 ERRDB-IT-NOTFOUND.
              05 FILLER             PIC S9(8) COMP    VALUE -111105.
              05 FILLER             PIC X(36) VALUE
                 'BROTHER CHANGED'.
              05 FILLER             PIC X(2)  VALUE 'GK'.

          02 ERRDB-IT-NOTFOUND.
              05 FILLER             PIC S9(8) COMP    VALUE -111106.
              05 FILLER             PIC X(36) VALUE
                 'END OF FILE'.
              05 FILLER             PIC X(2)  VALUE 'GB'.

      *----- if you need to add other managed codes, leave
      *----- the 'XI' code as the last one in the table!!!!!

          02 ERRDB-IT-NOTFOUNDERR.
              05 FILLER             PIC S9(8) COMP    VALUE -999999.
              05 FILLER             PIC X(36) VALUE
                 'NOT FOUND ERROR'.
              05 FILLER             PIC X(2)  VALUE 'XI'.

         01  TABERRDB-SQLRESP REDEFINES ERRDB-SQLRESP.
           02 ELERRDB-SQLRESP  OCCURS 14 TIMES.
              05 ERRDB-CODRESP      PIC S9(8) COMP.
              05 ERRDB-MSGRESP      PIC X(36).
              05 ERRDB-CODDLI       PIC X(2).

         01   MAX-SQLRESP           PIC 9(8) VALUE 14.
      *----------------------------------------------------------------*
      *
      *             FILE STATUS CONVERSION TABLE
      *
      *----------------------------------------------------------------*
       01  ERRFIL-RESP.
           02 ERRFIL-00.
              05 FILLER             PIC X(2)  VALUE '00'.
              05 FILLER             PIC X(36) VALUE
                 'NORMAL '.
              05 FILLER             PIC X(2)  VALUE '  '.

           02 ERRFIL-10.
              05 FILLER             PIC X(2)  VALUE '10'.
              05 FILLER             PIC X(36) VALUE
                 'RECORD NOT FOUND'.
              05 FILLER             PIC X(2)  VALUE 'GB'.

      *----- if you need to add other managed codes, leave
      *----- the 'XI' code as the last one in the table!!!!!

           02 ERRFIL-99-NOTFOUNDERR.
              05 FILLER             PIC X(2)  VALUE '99'.
              05 FILLER             PIC X(36) VALUE
                 'NOT FOUND ERROR'.
              05 FILLER             PIC X(2)  VALUE 'XI'.

         01  TABERRFIL-RESP REDEFINES ERRFIL-RESP.
           02 ELERRDB-SQLRESP  OCCURS 3 TIMES.
              05 ERRFIL-CODRESP     PIC X(2).
              05 ERRFIL-MSGRESP     PIC X(36).
              05 ERRFIL-CODDLI      PIC X(2).

         01   MAX-FILRESP           PIC 9(8) VALUE 3.

                                                                        