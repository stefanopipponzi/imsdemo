      *-----------------------------------------------------------------*
      *                 GENERIC MESSAGES AREA                           *
      *-----------------------------------------------------------------*
       01  ACTION-ERRORE            PIC 9.
       01  ERRDBRT-AREA.
           02       ERRDB-NOOP.
             05     CODDB-NOER.
               10   FILLER          PIC X(2) VALUE 'XX'.
               10   FILLER          PIC X(6).
             05     MSGDB-NOER      PIC X(50) VALUE
               'GENERIC ERROR                                     '.
             05     ACTION-NOER     PIC 9  VALUE 0.

             05     CODDB-NOOP.
               10   FILLER          PIC X(2) VALUE 'XX'.
               10   FILLER          PIC X(6).
             05     MSGDB-NOOP      PIC X(50) VALUE
               'OPERATION NOT DECODED                             '.
             05     ACTION-NOOP     PIC 9  VALUE 1.

             05     CODDB-DLSA.
               10   FILLER          PIC X(2) VALUE 'XX'.
               10   FILLER          PIC X(6).
             05     MSGDB-DLSA      PIC X(50) VALUE
               'SSA NOT BUILT CORRECTLY                           '.
             05     ACTION-DLSA     PIC 9  VALUE 1.
      *-----------------------------------------------------------------*
      *             VSAMCODE CONVERSION TABLE
      *-----------------------------------------------------------------*
       01  ERRDB-VSAMRESP.
           02 ERRDB-00.
              05 FILLER             PIC X(2)  VALUE '00'.
              05 FILLER             PIC X(36) VALUE
                 'KEY FOUND '.
              05 FILLER             PIC X(2)  VALUE '  '.
           02 ERRDB-04.
              05 FILLER             PIC X(2)  VALUE '04'.
              05 FILLER             PIC X(36) VALUE
                 'RECORD KEY LENGTH '.
              05 FILLER             PIC X(2)  VALUE '  '.
           02 ERRDB-10.
              05 FILLER             PIC X(2)  VALUE '10'.
              05 FILLER             PIC X(36) VALUE
                 'END OF FILE'.
              05 FILLER             PIC X(2)  VALUE 'GE'.
           02 ERRDB-22.
              05 FILLER             PIC X(2)  VALUE '22'.
              05 FILLER             PIC X(36) VALUE
                 'DUPLICATE KEY'.
              05 FILLER             PIC X(2)  VALUE 'II'.
           02 ERRDB-23.
              05 FILLER             PIC X(2)  VALUE '23'.
              05 FILLER             PIC X(36) VALUE
                 'KEY NOT FOUND'.
              05 FILLER             PIC X(2)  VALUE 'GE'.
           02 ERRDB-34.
              05 FILLER             PIC X(2)  VALUE '34'.
              05 FILLER             PIC X(36) VALUE
                 'NO SPACE - BOUNDARY VIOLATION'.
              05 FILLER             PIC X(2)  VALUE 'GE'.
           02 ERRDB-35.
              05 FILLER             PIC X(2)  VALUE '35'.
              05 FILLER             PIC X(36) VALUE
                 'OPEN FILE THAT IS NOT PRESENT'.
              05 FILLER             PIC X(2)  VALUE 'GE'.
      *----- if you need to add other managed codes, leave
      *----- the 'XI' code as the last one in the table!!!!!
           02 ERRDB-IT-NOTFOUNDERR.
              05 FILLER             PIC X(2)  VALUE '99'.
              05 FILLER             PIC X(36) VALUE
                 'NOT FOUND ERROR'.
              05 FILLER             PIC X(2)  VALUE 'XI'.

       01  TABERRDB-VSAMRESP REDEFINES ERRDB-VSAMRESP.
           02 ELERRDB-VSAMRESP  OCCURS 8 TIMES.
              05 ERRDB-CODRESP      PIC X(2).
              05 ERRDB-MSGRESP      PIC X(36).
              05 ERRDB-CODDLI       PIC X(2).

       01  MAX-VSAMRESP            PIC 9(8) VALUE 8.
      *-----------------------------------------------------------------*
      *             FILE STATUS CONVERSION TABLE
      *-----------------------------------------------------------------*
       01  ERRFIL-RESP.
           02 ERRFIL-00.
              05 FILLER             PIC X(2)  VALUE '00'.
              05 FILLER             PIC X(36) VALUE
                 'NORMAL '.
              05 FILLER             PIC X(2)  VALUE '  '.

           02 ERRFIL-10.
              05 FILLER             PIC X(2)  VALUE '10'.
              05 FILLER             PIC X(36) VALUE
                 'RECORD NOT FOUND'.
              05 FILLER             PIC X(2)  VALUE 'GB'.
      *if you need to add other managed codes, leave
      *the 'XI' code as the last one in the table!
           02 ERRFIL-99-NOTFOUNDERR.
              05 FILLER             PIC X(2)  VALUE '99'.
              05 FILLER             PIC X(36) VALUE
                 'NOT FOUND ERROR'.
              05 FILLER             PIC X(2)  VALUE 'XI'.

       01  TABERRFIL-RESP REDEFINES ERRFIL-RESP.
           02 ELERRDB-VSAMRESP  OCCURS 3 TIMES.
              05 ERRFIL-CODRESP     PIC X(2).
              05 ERRFIL-MSGRESP     PIC X(36).
              05 ERRFIL-CODDLI      PIC X(2).

       01  MAX-FILRESP             PIC 9(8)   VALUE 3.
      *-----------------------------------------------------------------*
      *                 FILE STATUS USED BY ROUTINES                    *
      *-----------------------------------------------------------------*
       01  WK-VSAMCODE             PIC X(2).
           88 VSC-OK                          VALUES '00'.
           88 VSC-NOT-FOUND                   VALUES '23'.
           88 VSC-ALREADY-OPEN                VALUES '41'.
           88 VSC-EOF                	      VALUES '10'.

       01  Z-VSAMCODE              PIC ---------9.
      *
