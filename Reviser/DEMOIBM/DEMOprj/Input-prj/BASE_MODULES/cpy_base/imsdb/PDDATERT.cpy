       9999-SYSTEM-DATE SECTION.
           ACCEPT WK-SYSTEM-TIME     FROM TIME.
           ACCEPT WK-SYSTEM-DATE     FROM DATE.   

       CONTR-DATE SECTION.
           MOVE 'N' TO WK-DATE-ERROR.
           PERFORM CHECK-DATE THRU CHECK-DATE-EX
           IF WK-DATE-ERROR NOT = 'Y'
              MOVE CORRESPONDING WK-DATE-DLI-Z8 TO
                    WK-DATE-DB2
           END-IF.

       CHECK-DATE.
           IF WK-MM OF WK-DATE-DLI-Z8 < 01 OR
              WK-MM OF WK-DATE-DLI-Z8 > 12
              MOVE 'Y'             TO WK-DATE-ERROR
              GO TO CHECK-DATE-EX
           END-IF

           IF WK-MM OF WK-DATE-DLI-Z8 = 02
              IF WK-AA OF WK-DATE-DLI-Z8 = 00
                 DIVIDE 4 INTO WK-SS OF WK-DATE-DLI-Z8 GIVING
                               WK-RISULTATO REMAINDER WK-RESTO
                 IF WK-RESTO = ZEROES
                    MOVE 29 TO WK-GIORNI(2)
                 END-IF
              ELSE
                 DIVIDE 4 INTO WK-AA OF WK-DATE-DLI-Z8 GIVING
                               WK-RISULTATO REMAINDER WK-RESTO
                 IF WK-RESTO = ZEROES
                    MOVE 29 TO WK-GIORNI(2)
                 END-IF
              END-IF
           END-IF.

           IF WK-GG OF WK-DATE-DLI-Z8 < 01 OR
              WK-GG OF WK-DATE-DLI-Z8 > 
              WK-GIORNI (WK-MM OF WK-DATE-DLI-Z8)
              MOVE 'Y'             TO WK-DATE-ERROR
              GO TO CHECK-DATE-EX
           END-IF.
       CHECK-DATE-EX.
           EXIT.
