      *--------------------------------------------------------------*
      *  LAST MODIFICATION DATE:   DATA-MODIF         ( RELEASE 2.0 )*
      *                                                              *
      *  EXECUTED BY           :   Development and support group     *
      *                            Mainframe Affinity                *
      *                                                              *
      *--------------------------------------------------------------*
      *                                                              *
      *     VARIABLE DECLARATION FOR                                 *
      *                  GENERALIZED ROUTINES OF I-O IMS             *
      *                                                              *
      *--------------------------------------------------------------*

      *---------   ROUTINES PHASE 1.0   -----------------------------*
       01  DCRCVE                PIC X(8)  VALUE 'DCRCVE  '.
       01  DCSEND                PIC X(8)  VALUE 'DCSEND  '.

      *---------   ROUTINES PHASE 1.5   -----------------------------*
       01  DCCHKP                PIC X(8)  VALUE 'DCCHKP  '.
       01  DCCHNG                PIC X(8)  VALUE 'DCCHNG  '.
       01  DCCLTS                PIC X(8)  VALUE 'DCCLTS  '.
       01  DCPURG                PIC X(8)  VALUE 'DCPURG  '.
       01  DCROLB                PIC X(8)  VALUE 'DCROLB  '.
       01  DCROLL                PIC X(8)  VALUE 'DCROLL  '.
       01  DCXRST                PIC X(8)  VALUE 'DCXRST  '.
       01  DCICMD                PIC X(8)  VALUE 'DCICMD  '.
       01  DCINQY                PIC X(8)  VALUE 'DCINQY  '.
       01  DCAUTH                PIC X(8)  VALUE 'DCAUTH  '.

      *---------   ROUTINES PHASE 2.0   -----------------------------*
       01  DCPAGE                PIC X(8)  VALUE 'DCPAGE  '.
       01  DCRTTR                PIC X(8)  VALUE 'DCRTTR  '.

      *---------   END COPY -----------------------------------------*
