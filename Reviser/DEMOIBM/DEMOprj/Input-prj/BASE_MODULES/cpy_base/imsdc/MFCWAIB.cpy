Y2KY2K*============================================================
Y2KY2K*   MAKE Y2K COMPLIANT - ERROR HANDLING
Y2KY2K*============================================================
      * AIB INTERFACE - WORKING STORAGE (NAMES FROM IMS MANUALS)
      *============================================================
       01  INQY                             PIC  X(04)
                                                 VALUE 'INQY'.
      *
       01  AIB.
           05  AIBID                        PIC  X(08)
                                                 VALUE 'DFSAIB  '.
           05  AIBLEN                       PIC S9(08)     COMP.
           05  AIBSFUNC                     PIC  X(08).
               88  AIBSFUNC-NULL                 VALUE '        '.
               88  AIBSFUNC-DBQUERY              VALUE 'DBQUERY '.
               88  AIBSFUNC-FIND                 VALUE 'FIND    '.
               88  AIBSFUNC-ENVIRON              VALUE 'ENVIRON '.
               88  AIBSFUNC-PROGRAM              VALUE 'PROGRAM '.
           05  AIBRSNM1                     PIC  X(08).
               88  AIBRSNM1-IOPCB                VALUE 'IOPCB   '.
           05  FILLER                       PIC  X(16).
           05  AIBOALEN                     PIC S9(08)     COMP.
           05  AIBOAUSE                     PIC S9(08)     COMP.
           05  FILLER                       PIC  X(12).
           05  AIBRETRN                     PIC S9(08)     COMP.
           05  AIBREASN                     PIC S9(08)     COMP.
           05  AIBRSA1                      PIC S9(08)     COMP.
           05  FILLER                       PIC  X(48).
      *
       01  AIB-OUT-AREA                     PIC  X(150).
      *
       01  AIB-OUT-ENVIRON  REDEFINES  AIB-OUT-AREA.
           05  AIB-OUT-IMS-ID               PIC  X(08).
           05  AIB-OUT-IMS-REL-LVL          PIC S9(08)     COMP.
           05  AIB-OUT-CTL-REG-TYP          PIC  X(08).
               88  AIB-OUT-CTL-REG-TYP-BATCH          VALUE 'BATCH   '.
               88  AIB-OUT-CTL-REG-TYP-DB             VALUE 'DB      '.
               88  AIB-OUT-CTL-REG-TYP-TM             VALUE 'TM      '.
               88  AIB-OUT-CTL-REG-TYP-DBDC           VALUE 'DB/DC   '.
           05  AIB-OUT-APL-REG-TYP          PIC  X(08).
               88  AIB-OUT-APL-REG-TYP-BATCH          VALUE 'BATCH   '.
               88  AIB-OUT-APL-REG-TYP-BMP            VALUE 'BMP     '.
               88  AIB-OUT-APL-REG-TYP-DRA            VALUE 'DRA     '.
               88  AIB-OUT-APL-REG-TYP-IFP            VALUE 'IFP     '.
               88  AIB-OUT-APL-REG-TYP-MPP            VALUE 'MPP     '.
           05  AIB-OUT-REG-ID               PIC S9(08)     COMP.
           05  AIB-OUT-APL-PGM-NAME         PIC  X(08).
           05  AIB-OUT-PSB-NAME             PIC  X(08).
           05  AIB-OUT-TRANS-NAME           PIC  X(08).
           05  AIB-OUT-USER-ID              PIC  X(08).
           05  AIB-OUT-GRP-NAME             PIC  X(08).
           05  AIB-OUT-GRP-STATUS-A         PIC  X(04).
           05  AIB-OUT-GRP-STATUS-B         PIC  X(04).
           05  AIB-OUT-ADR-RCV-TKN          PIC S9(04)     COMP.
           05  AIB-OUT-RCV-TKN              PIC  X(16).
           05  AIB-OUT-ADR-APL-PRM          PIC S9(04)     COMP.
           05  AIB-OUT-APL-PRM              PIC  X(32).
