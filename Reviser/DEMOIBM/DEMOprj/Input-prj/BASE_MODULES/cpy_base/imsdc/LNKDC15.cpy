      **--------------------------------------------------------------**
      **                  COPY       L N K D C 1 5                    **
      **             CHECKPOINT / RESTART    MANAGEMENT               **
      **                      LENGTH   46038                          **
      **--------------------------------------------------------------**

          05  LNK15-CKPTID.
             10  LNK15-SUBSID          PIC X(20).
             10  LNK15-JNUM            PIC X(8).
             10  LNK15-JNAM            PIC X(8).
          05  LNK15-SAVAREA.
             10  LNK15-ITEM   OCCURS 7.
                15  LNK15-LEN             PIC S9(4) COMP.
                15  LNK15-AREA            PIC X(1998).
      *  FIELD TO STORE THE "LNKDB-ACTIVE" AREA
             10  LNK15-FBK-AREA           PIC X(32000).
          05  LNK15-RETCODE            PIC X(2).
