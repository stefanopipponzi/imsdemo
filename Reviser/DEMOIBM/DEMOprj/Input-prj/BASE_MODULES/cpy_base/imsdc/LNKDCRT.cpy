      **--------------------------------------------------------------**
      **                                                              **
      **                                                              **
      **                                                              **
      **--------------------------------------------------------------**
          05  LNKDC-CALLER          PIC X(8).
          05  LNKDC-OPERATION       PIC X(4).
          05  LNKDC-MAPNAME         PIC X(8).
          05  LNKDC-INTOAREA        PIC X(9).
          05  LNKDC-MAPSETNAME      PIC X(8).
          05  LNKDC-MSGNAME         PIC X(8).
          05  LNKDC-RETCODE         PIC X(2).
          05  LNKDC-STCODE          PIC X(2).
          05  LNKDC-CHNGDEST        PIC X(8).
          05  LNKDC-MFSMAP-AREA     PIC X(2000).
