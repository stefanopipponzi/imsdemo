      *--------------------------------------------------------------*
      * TABLE  REVCKPT                                        *
      *  LAST MODIFICATION DATE:   DATA-MODIF  	      ( RELEASE 1.0 )*
      *                                                              *
      *  EXECUTED BY           :   I-TER SpA                         *
      *--------------------------------------------------------------*
SET DEFINE OFF;
       CREATE TABLE REVCKPT        
         (
           CKPT_TAB_KEY          CHAR (36)     NOT NULL,
           CKPT_TAB_SAVAREA      VARCHAR2 (46000)  NOT NULL
         )
       TABLESPACE DATA_TS  ; 
       
       
