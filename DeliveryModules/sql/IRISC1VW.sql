-- -----------------------------------------------------------------------
-- 
--      Licensed Materials - Property of Modern Systems, INC.
--                                                           
--      (C) Copyright Modern Systems 2018
-- 
-- -----------------------------------------------------------------------
-- Product: IRIS-DB - v. 5.5.0
-- -----------------------------------------------------------------------
 
-- -----------------------------------------------------------------------
-- CREATE TABLE : IRIS_PSB_CHECKPOINT
-- -----------------------------------------------------------------------
 
CREATE TABLE IRISC1VW (
  REGION_ID        CHAR(8)        NOT NULL,
  PSB_NAME         CHAR(8)        NOT NULL,
  CHECKPOINT_ID    CHAR(14)        NOT NULL,
  CHECKPOINT_TIME  CHAR(16)       NOT NULL,
  VARS_COUNT       SMALLINT       NOT NULL,
--  VARS_AREA        VARCHAR(32704) NOT NULL,
--  VARS_AREA        VARCHAR(4000)  NOT NULL,
  VARS_AREA        VARCHAR(3900)  NOT NULL,
  PRIMARY KEY(
   REGION_ID,
   PSB_NAME,
   CHECKPOINT_ID
--   CHECKPOINT_TIME,
--   VARS_COUNT
  )
)
