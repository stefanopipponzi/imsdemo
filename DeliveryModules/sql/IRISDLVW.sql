-- -----------------------------------------------------------------------
-- 
--      Licensed Materials - Property of Modern Systems, INC.
--                                                           
--      (C) Copyright Modern Systems 2018
-- 
-- -----------------------------------------------------------------------
-- Product: IRIS-DB - v. 5.5.0
-- -----------------------------------------------------------------------
 
-- -----------------------------------------------------------------------
-- CREATE TABLE : IRIS_DUAL_LOOKUP
-- -----------------------------------------------------------------------
 
CREATE TABLE PROD.IRISDLVW (
  DBD_NAME      CHAR(3)  NOT NULL,
  DBD_STATUS    CHAR(1)  NOT NULL,
  PRIMARY KEY(
   DBD_NAME
  )
)
;