      *   MapSet Name   IVTCBF
      *   Date Created  02/14/2020
      *   Time Created  11:56:06

      *  Input Data For Map IVTCBF
         01 IVTCBFI.
            03 FILLER                         PIC X(12).
            03 SDATEL                         PIC S9(4) COMP.
            03 SDATEF                         PIC X.
            03 FILLER REDEFINES SDATEF.
               05 SDATEA                         PIC X.
            03 SDATEI                         PIC X(8).
            03 CMDL                           PIC S9(4) COMP.
            03 CMDF                           PIC X.
            03 FILLER REDEFINES CMDF.
               05 CMDA                           PIC X.
            03 CMDI                           PIC X(8).
            03 NAME1L                         PIC S9(4) COMP.
            03 NAME1F                         PIC X.
            03 FILLER REDEFINES NAME1F.
               05 NAME1A                         PIC X.
            03 NAME1I                         PIC X(10).
            03 NAME2L                         PIC S9(4) COMP.
            03 NAME2F                         PIC X.
            03 FILLER REDEFINES NAME2F.
               05 NAME2A                         PIC X.
            03 NAME2I                         PIC X(10).
            03 EXTL                           PIC S9(4) COMP.
            03 EXTF                           PIC X.
            03 FILLER REDEFINES EXTF.
               05 EXTA                           PIC X.
            03 EXTI                           PIC X(10).
            03 ZIPL                           PIC S9(4) COMP.
            03 ZIPF                           PIC X.
            03 FILLER REDEFINES ZIPF.
               05 ZIPA                           PIC X.
            03 ZIPI                           PIC X(7).
            03 MSGL                           PIC S9(4) COMP.
            03 MSGF                           PIC X.
            03 FILLER REDEFINES MSGF.
               05 MSGA                           PIC X.
            03 MSGI                           PIC X(40).
            03 SEGNOL                         PIC S9(4) COMP.
            03 SEGNOF                         PIC X.
            03 FILLER REDEFINES SEGNOF.
               05 SEGNOA                         PIC X.
            03 SEGNOI                         PIC X(4).
            03 SYSMSGAL                       PIC S9(4) COMP.
            03 SYSMSGAF                       PIC X.
            03 FILLER REDEFINES SYSMSGAF.
               05 SYSMSGAA                       PIC X.
            03 SYSMSGAI                       PIC X(79).

      *  Output Data For Map IVTCBF
         01 IVTCBFO REDEFINES IVTCBFI.
            03 FILLER                         PIC X(12).
            03 FILLER                         PIC X(3).
            03 SDATEO                         PIC X(8).
            03 FILLER                         PIC X(3).
            03 CMDO                           PIC X(8).
            03 FILLER                         PIC X(3).
            03 NAME1O                         PIC X(10).
            03 FILLER                         PIC X(3).
            03 NAME2O                         PIC X(10).
            03 FILLER                         PIC X(3).
            03 EXTO                           PIC X(10).
            03 FILLER                         PIC X(3).
            03 ZIPO                           PIC X(7).
            03 FILLER                         PIC X(3).
            03 MSGO                           PIC X(40).
            03 FILLER                         PIC X(3).
            03 SEGNOO                         PIC X(4).
            03 FILLER                         PIC X(3).
            03 SYSMSGAO                       PIC X(79).

