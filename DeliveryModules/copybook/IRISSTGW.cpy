      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2019
      ******************************************************************
      *
      * Description: SEGMENTS DICTIONARY FOR DBD: II6DAP
      *
      ******************************************************************
      *
      *
      *
       01 IRIS-STAGE-TABLE.
         03 FILLER                 PIC S9(4) COMP    VALUE 3.
         03 IRIS-STAGE-TABLE-A001.
           04 IRIS-STAGE-MAPPING.
            05 IRIS-IMS-TRAN         PIC X(8) VALUE '/FOR    '.
            05 IRIS-CICS-TRAN        PIC X(4) VALUE '/FOR'.
            05 IRIS-PROGRAM          PIC X(8) VALUE 'IRISROUT'.
            05 IRIS-SPA              PIC S9(4) COMP VALUE 0.
            05 IRIS-TRAN-TYPE        PIC X(1)  VALUE 'T'.
         03 IRIS-STAGE-TABLE-A002.
           04 IRIS-STAGE-MAPPING.
            05 IRIS-IMS-TRAN         PIC X(8) VALUE 'IVTCB   '.
            05 IRIS-CICS-TRAN        PIC X(4) VALUE 'IVTC'.
            05 IRIS-PROGRAM          PIC X(8) VALUE 'DFSIVA34'.
            05 IRIS-SPA              PIC S9(4) COMP VALUE 0.
            05 IRIS-TRAN-TYPE        PIC X(1)  VALUE 'T'.
         03 IRIS-STAGE-TABLE-A003.
           04 IRIS-STAGE-MAPPING.
            05 IRIS-IMS-TRAN         PIC X(8) VALUE '/SIGNOFF'.
            05 IRIS-CICS-TRAN        PIC X(4) VALUE '/SIG'.
            05 IRIS-PROGRAM          PIC X(8) VALUE 'IRISROUT'.
            05 IRIS-SPA              PIC S9(4) COMP VALUE 0.
            05 IRIS-TRAN-TYPE        PIC X(1)  VALUE 'T'.
       01 IRIS-STAGE-TABLE-RED REDEFINES IRIS-STAGE-TABLE.
         03 STAGE-COUNT              PIC S9(4) COMP.
         03 IRIS-STAGE-TABLE-LAYOUT  OCCURS 3.
            05 IRIS-IMS-TRAN-S         PIC X(8).
            05 IRIS-CICS-TRAN-S        PIC X(4).
            05 IRIS-PROGRAM-S          PIC X(8).
            05 IRIS-SPA-S              PIC S9(4) COMP.
            05 IRIS-TRAN-TYPE-S        PIC X(1).
       01 IRIS-STAGE-ITEM-FOUND.
            05 IRIS-IMS-TRAN-F         PIC X(8).
            05 IRIS-CICS-TRAN-F        PIC X(4).
            05 IRIS-PROGRAM-F          PIC X(8).
            05 IRIS-SPA-F              PIC S9(4) COMP.
            05 IRIS-TRAN-TYPE-F        PIC X(1).

