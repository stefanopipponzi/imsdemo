       IRIS-SEARCH-MESSAGE.       
           MOVE SPACE TO IRIS-MFS-ITEM-FOUND
           INSPECT IRIS-SEARCH-KEY REPLACING ALL LOW-VALUE BY SPACE
           SET IRIS-IMS-NOT-FOUND TO TRUE
           PERFORM VARYING IRIS-IDX FROM 1 BY 1 UNTIL
                                   IRIS-IDX > MFS-COUNT
             IF IRIS-MSG-NAME-S(IRIS-IDX) = IRIS-SEARCH-KEY                   
               MOVE IRIS-MFS-TABLE-LAYOUT(IRIS-IDX) 
                                     TO IRIS-MFS-ITEM-FOUND
               MOVE MFS-COUNT TO IRIS-IDX
               SET IRIS-IMS-FOUND TO TRUE
             END-IF             
           END-PERFORM.
       IRIS-SEARCH-MESSAGE-EX.
           EXIT.
       IRIS-SEARCH-FORMAT.       
           MOVE SPACE TO IRIS-MFS-ITEM-FOUND
           SET IRIS-IMS-NOT-FOUND TO TRUE
           INSPECT IRIS-SEARCH-KEY REPLACING ALL LOW-VALUE BY SPACE
           PERFORM VARYING IRIS-IDX FROM 1 BY 1 UNTIL
                                   IRIS-IDX > MFS-COUNT
             IF IRIS-FMT-NAME-S(IRIS-IDX) = IRIS-SEARCH-KEY                  
               MOVE IRIS-MFS-TABLE-LAYOUT(IRIS-IDX) 
                                     TO IRIS-MFS-ITEM-FOUND
               MOVE MFS-COUNT TO IRIS-IDX
               SET IRIS-IMS-FOUND TO TRUE
             END-IF            
           END-PERFORM.
       IRIS-SEARCH-FORMAT-EX.
           EXIT.
       IRIS-SEARCH-ROUTINE.       
           MOVE SPACE TO IRIS-MFS-ITEM-FOUND
           SET IRIS-IMS-NOT-FOUND TO TRUE
           INSPECT IRIS-SEARCH-KEY REPLACING ALL LOW-VALUE BY SPACE
           PERFORM VARYING IRIS-IDX FROM 1 BY 1 UNTIL
                                   IRIS-IDX > MFS-COUNT
             IF IRIS-FMT-NAME-S(IRIS-IDX) = IRIS-SEARCH-KEY                  
               MOVE IRIS-MFS-TABLE-LAYOUT(IRIS-IDX) 
                                     TO IRIS-MFS-ITEM-FOUND
               MOVE MFS-COUNT TO IRIS-IDX
               SET IRIS-IMS-FOUND TO TRUE
             END-IF            
           END-PERFORM.
       IRIS-SEARCH-ROUTINE-EX.
           EXIT.