      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: SEGMENTS DICTIONARY FOR DBD: IVPDB2
      *
      ******************************************************************
      *
      *   MEMORY-DBD-SEGMENTS
      *
      *
       01 MEM-DBD-SEGMS-IVPDB2.
         03 FILLER                 PIC S9(4) COMP    VALUE 1.
         03 MEM-DBD-SEGM-A1111111-01.
           05 DBD-NAME             PIC X(8)          VALUE 'IVPDB2  '.
           05 SEGMENT-NAME         PIC X(8)          VALUE 'A1111111'.
           05 PROGRESSIVE          PIC S9(4) COMP    VALUE 1.
           05 SEGMENT-LEVEL        PIC S9(4) COMP    VALUE 0.
           05 PARENT-SEGMENT-NAME  PIC X(8)          VALUE SPACE.
           05 PARENT-TYPE          PIC S9(9) COMP    VALUE 0.
           05 LPARENT-SEGMENT-NAME PIC X(8)          VALUE SPACE.
           05 LPARENT-TYPE         PIC S9(9) COMP    VALUE 0.
           05 LPARENT-DBD-NAME     PIC X(8)          VALUE SPACE.
           05 BYTES-MIN            PIC S9(9) COMP    VALUE 0.
           05 BYTES-MAX            PIC S9(9) COMP    VALUE 40.
           05 FREQUENCE            PIC S9(9) COMP    VALUE 0.
           05 POINTER-HIER         PIC X             VALUE '0'.
           05 POINTER-HIERBWD      PIC X             VALUE '0'.
           05 POINTER-TWIN         PIC X             VALUE '0'.
           05 POINTER-TWINBWD      PIC X             VALUE '0'.
           05 POINTER-NOTWIN       PIC X             VALUE '0'.
           05 POINTER-LTWIN        PIC X             VALUE '0'.
           05 POINTER-LTWINBWD     PIC X             VALUE '0'.
           05 POINTER-PAIRED       PIC X             VALUE '0'.
           05 POINTER-LPARNT       PIC X             VALUE '0'.
           05 POINTER-CTR          PIC X             VALUE '0'.
           05 RULE-INSERTION       PIC S9(9) COMP    VALUE 1017.
           05 RULE-DELETION        PIC S9(9) COMP    VALUE 1017.
           05 RULE-REPLACEMENT     PIC S9(9) COMP    VALUE 1017.
           05 RULE-POSITION        PIC S9(9) COMP    VALUE 1019.
           05 SOURCE-SEGMENT-NAME  PIC X(8)          VALUE SPACE.
           05 SOURCE-TYPE          PIC S9(9) COMP    VALUE 0.
           05 SOURCE-DBD-NAME      PIC X(8)          VALUE SPACE.
           05 LSOURCE-SEGMENT-NAME PIC X(8)          VALUE SPACE.
           05 LSOURCE-TYPE         PIC S9(9) COMP    VALUE 0.
           05 LSOURCE-DBD-NAME     PIC X(8)          VALUE SPACE.
           05 ROUTINE-NAME         PIC X(8)          VALUE SPACE.
           05 ROUTINE-TYPE         PIC S9(9) COMP    VALUE 0.
           05 ROUTINE-VALUES       PIC X(250)        VALUE SPACE.
       01 MEM-DBD-SEGMS-IVPDB2-RED REDEFINES MEM-DBD-SEGMS-IVPDB2.
         03 SEGMENTS-COUNT         PIC S9(4) COMP.
         03 TAB-MEM-DBD-SEGMS-IVPDB2 PIC X(392) OCCURS 1.
