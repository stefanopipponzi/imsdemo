      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.5.0
      ******************************************************************
      * Date: 2018-10-05 07:03:36.069
      ******************************************************************
      *
      * Description: DCLGEN FOR TABLE: IRIS_PCB_CHECKPOINT
      *
      ******************************************************************

      ******************************************************************
      * COBOL DCLGEN FOR PSB CHECKPOINT TABLE
      ******************************************************************

       01 IRISC1VW.
         03 REGION-ID                         PIC X(8).
         03 PSB-NAME                          PIC X(8).
         03 CHECKPOINT-ID                     PIC X(14).
         03 CHECKPOINT-TIME                   PIC X(16).
         03 VARS-COUNT                        PIC S9(4) COMP-5.
         03 VARS-AREA.
            49 VARS-AREA-LEN                  PIC S9(4) COMP-5.
*******     49 VARS-AREA-TEXT                 PIC X(32704).
*******     49 VARS-AREA-TEXT                 PIC X(4000).
            49 VARS-AREA-TEXT                 PIC X(3900).