      *---------------------------------------------*
      *
      * IMS / DC   TO   MTP (CICS)  - MFS 2 BMS
      *                            (COPY CONTR FIELD)
      *--------------------------------------------*
      * GET SYSTEM TIME AND DATE
      *--------------------------------------------*
       GET-TMDTMFS.
           EXEC CICS ASKTIME
                     ABSTIME(WS-ABSTIME)
                     RESP(WS-RESP)
           END-EXEC.

           EXEC CICS FORMATTIME
                     ABSTIME(WS-ABSTIME)
                     MMDDYY(WS-SYSDATE)
                     DATESEP('/')
                     TIME(WS-SYSTIME)
                     TIMESEP(':')
                     RESP(WS-RESP)
           END-EXEC.

           MOVE WS-SYSDATE TO WMFS-DATE.
           MOVE WS-SYSTIME TO WMFS-TIME.
       EX-GET-TMDTMFS.
           EXIT.
