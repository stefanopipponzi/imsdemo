      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * IRIS-DC - V. 5.1
      ******************************************************************
      *                                                                *
       01 IRIS-IMSTASK.
         03 IRIS-CALLER-TRANCODE PIC X(8).
         03 IRIS-CALLED-TRANCODE PIC X(4).
         03 IRIS-TASK-ID         PIC S9(4) COMP.    
         03 IRIS-TASK-MODNAME    PIC X(8).
         03 IRIS-CALLER-IMS-TRAN PIC X(8).
         03 IRIS-CALLED-IMS-TRAN PIC X(8).
         03 IRIS-ENQUEUE         PIC X(20).
         03 IRIS-CALLER-EIBTASKN PIC 9(8).
       01 IRIS-RESPONSE         PIC S9(8) COMP VALUE 0.
       01 IRIS-RESP             PIC S9(8) COMP VALUE 0.
       01 IRIS-WS-TASK-ITEM     PIC S9(4) COMP VALUE 0.
       01 IRIS-WS-CALLER-TRANCODE   PIC X(8).
       01 IRIS-WS-TRAN          PIC X(4).
       01 IRIS-IDX                    PIC S9(4) COMP. 
       01  IRIS-MFS-FLAG              PIC X(1) VALUE 'N'.
             88 IRIS-IMS-FOUND       VALUE 'Y'.
             88 IRIS-IMS-NOT-FOUND   VALUE 'N'.
       01  IRIS-SEARCH-KEY            PIC X(8).
       01  FILLER REDEFINES IRIS-SEARCH-KEY.
          03 IRIS-SEARCH-KEY4         PIC X(4).
          03 FILLER                   PIC X(4).
