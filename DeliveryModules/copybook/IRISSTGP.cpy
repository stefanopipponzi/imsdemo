       IRIS-MATCH-CICS-TRAN.       
           MOVE SPACE TO IRIS-STAGE-ITEM-FOUND
           SET IRIS-IMS-NOT-FOUND TO TRUE
           INSPECT IRIS-SEARCH-KEY REPLACING ALL LOW-VALUE BY SPACE
           PERFORM VARYING IRIS-IDX FROM 1 BY 1 UNTIL
                                   IRIS-IDX > STAGE-COUNT   
             IF IRIS-CICS-TRAN-S(IRIS-IDX) = IRIS-SEARCH-KEY4                  
               MOVE IRIS-STAGE-TABLE-LAYOUT(IRIS-IDX) 
                                     TO IRIS-STAGE-ITEM-FOUND
               MOVE STAGE-COUNT TO IRIS-IDX
               SET IRIS-IMS-FOUND TO TRUE
             END-IF             
           END-PERFORM.
       IRIS-MATCH-CICS-TRAN-EX.
           EXIT.
       IRIS-MATCH-IMS-TRAN.       
           MOVE SPACE TO IRIS-STAGE-ITEM-FOUND
           SET IRIS-IMS-NOT-FOUND TO TRUE
           INSPECT IRIS-SEARCH-KEY REPLACING ALL LOW-VALUE BY SPACE
           IF IRIS-SEARCH-KEY NOT = SPACE 
             AND IRIS-SEARCH-KEY NOT = LOW-VALUES           
             PERFORM VARYING IRIS-IDX FROM 1 BY 1 UNTIL
                                     IRIS-IDX > STAGE-COUNT   
               IF IRIS-IMS-TRAN-S(IRIS-IDX) = IRIS-SEARCH-KEY              
                 MOVE IRIS-STAGE-TABLE-LAYOUT(IRIS-IDX) 
                                       TO IRIS-STAGE-ITEM-FOUND
                 MOVE STAGE-COUNT TO IRIS-IDX
                 SET IRIS-IMS-FOUND TO TRUE
               END-IF             
             END-PERFORM
           END-IF.
       IRIS-MATCH-IMS-TRAN-EX.
           EXIT.
       IRIS-MATCH-PROGRAM.       
           MOVE SPACE TO IRIS-STAGE-ITEM-FOUND
           SET IRIS-IMS-NOT-FOUND TO TRUE
           INSPECT IRIS-SEARCH-KEY REPLACING ALL LOW-VALUE BY SPACE
           PERFORM VARYING IRIS-IDX FROM 1 BY 1 UNTIL
                                   IRIS-IDX > STAGE-COUNT   
             IF IRIS-PROGRAM-S(IRIS-IDX) = IRIS-SEARCH-KEY                  
               MOVE IRIS-STAGE-TABLE-LAYOUT(IRIS-IDX) 
                                     TO IRIS-STAGE-ITEM-FOUND
               MOVE STAGE-COUNT TO IRIS-IDX
               SET IRIS-IMS-FOUND TO TRUE
             END-IF             
           END-PERFORM.
       IRIS-MATCH-PROGRAM-EX.
           EXIT.
