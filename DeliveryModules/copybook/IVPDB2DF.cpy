      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: FIELDS DICTIONARY FOR DBD: IVPDB2
      *
      ******************************************************************
      *
      *   MEMORY-SEGMENT-FIELDS
      *
      *
       01 MEM-DBD-FLDS-IVPDB2.
         03 FILLER                     PIC S9(4) COMP  VALUE 1.
         03 TAB-MEM-SEGM-FLDS-IVPDB2.
           05 MEM-SEGM-FLDS-A1111111.
             07 MEM-SEGM-FLD-A1111111-01.
               09 DBD-NAME             PIC X(8)        VALUE 'IVPDB2  '.
               09 SEGMENT-NAME         PIC X(8)        VALUE 'A1111111'.
               09 FIELD-NAME           PIC X(8)        VALUE 'A1111111'.
               09 FIELD-SEQUENCE       PIC X           VALUE '1'.
               09 FIELD-SX             PIC X           VALUE '0'.
               09 FIELD-CK             PIC X           VALUE '0'.
               09 FIELD-BYTES          PIC S9(9) COMP  VALUE 10.
               09 FIELD-START          PIC S9(9) COMP  VALUE 1.
               09 FIELD-DATA-TYPE      PIC S9(9) COMP  VALUE 1028.
               09 FIELD-IORTN-LEN      PIC S9(4) COMP  VALUE 12.
               09 FIELD-IORTN-NAME     PIC X(32)
                          VALUE 'KEY_A1111111                  '.
               09 FIELD-IORTN-TYPE     PIC S9(9) COMP  VALUE 0.
               09 FIELD-IORTN-DECIMALS PIC S9(9) COMP  VALUE 0.
       01 FILLER REDEFINES MEM-DBD-FLDS-IVPDB2.
         03 MEM-DBD-FLDS-IVPDB2-CNT    PIC S9(4) COMP.
         03 TAB-MEM-FIELDS-IVPDB2      PIC X(83) OCCURS 1.
