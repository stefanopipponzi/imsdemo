      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2019
      ******************************************************************
      *
      * Description: SEGMENTS DICTIONARY FOR DBD: II6DAP
      *
      ******************************************************************
      *
      *   MEMORY-DBD-SEGMENTS
      *
      *
       01 IRIS-MFS-TABLE.
         03 FILLER                 PIC S9(4) COMP    VALUE 4.
         03 IRIS-MFS-TABLE-A01.
           05 IRIS-MFS-MAPPING.
            15 IRIS-FMT-NAME          PIC X(8) VALUE 'IVTCB   '.
            15 IRIS-MSG-NAME          PIC X(8) VALUE 'IVTCB   '.
            15 IRIS-MSG-NEXT          PIC X(8) VALUE 'IVTCBMI1'.
            15 IRIS-MSG-TYPE          PIC X(1) VALUE 'O'.
            15 IRIS-ROUTINE           PIC X(8) VALUE 'DFSIVFS '.
         03 IRIS-MFS-TABLE-A02.
           05 IRIS-MFS-MAPPING.
            15 IRIS-FMT-NAME          PIC X(8) VALUE 'IVTCB   '.
            15 IRIS-MSG-NAME          PIC X(8) VALUE 'IVTCBMO2'.
            15 IRIS-MSG-NEXT          PIC X(8) VALUE 'IVTCBMI2'.
            15 IRIS-MSG-TYPE          PIC X(1) VALUE 'O'.
            15 IRIS-ROUTINE           PIC X(8) VALUE 'DFSIVFS '.
         03 IRIS-MFS-TABLE-A05.
           05 IRIS-MFS-MAPPING.
            15 IRIS-FMT-NAME          PIC X(8) VALUE 'IVTCB   '.
            15 IRIS-MSG-NAME          PIC X(8) VALUE 'IVTCBMI1'.
            15 IRIS-MSG-NEXT          PIC X(8) VALUE 'IVTCBMO2'.
            15 IRIS-MSG-TYPE          PIC X(1) VALUE 'I'.
            15 IRIS-ROUTINE           PIC X(8) VALUE 'DFSIVFR '.
         03 IRIS-MFS-TABLE-A06.
           05 IRIS-MFS-MAPPING.
            15 IRIS-FMT-NAME          PIC X(8) VALUE 'IVTCB   '.
            15 IRIS-MSG-NAME          PIC X(8) VALUE 'IVTCBMI2'.
            15 IRIS-MSG-NEXT          PIC X(8) VALUE 'IVTCBMO2'.
            15 IRIS-MSG-TYPE          PIC X(1) VALUE 'I'.
            15 IRIS-ROUTINE           PIC X(8) VALUE 'DFSIVFR '.
       01 IRIS-MFS-TABLE-RED REDEFINES IRIS-MFS-TABLE.
         03 MFS-COUNT              PIC S9(4) COMP.
         03 IRIS-MFS-TABLE-LAYOUT  OCCURS 4.
            05 IRIS-FMT-NAME-S         PIC X(8).
            05 IRIS-MSG-NAME-S         PIC X(8).
            05 IRIS-MSG-NEXT-F         PIC X(8).
            05 IRIS-MSG-TYPE-S         PIC X(1).
            05 IRIS-ROUTINE-S          PIC X(8).       
       01 IRIS-MFS-ITEM-FOUND.
           05 IRIS-FMT-NAME-F         PIC X(8).
           05 IRIS-MSG-NAME-F         PIC X(8).
           05 IRIS-MSG-NEXT-F         PIC X(8).
           05 IRIS-MSG-TYPE-F         PIC X(1).
           05 IRIS-ROUTINE-F          PIC X(8). 