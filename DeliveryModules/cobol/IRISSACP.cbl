CBL TRUNC(BIN)
      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: SSA RUNTIME ANALYZER
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IRISSACP.
      *
       ENVIRONMENT DIVISION.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
           EXEC SQL INCLUDE SQLCA END-EXEC.
       01 IRIS-CURRENT-TIMESTAMP PIC X(26) VALUE SPACES.
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
      *
      *    DATA DICTIONARY CONSTANTS
      *
           COPY IRISSQLC.
      *
      *    VARIABLES COBOL DEPENDENT
      *
       01  CP-PTR-NULL                     POINTER    VALUE NULL.
      *
      *    DATABASE CONSTANTS
      *
      *
      * WORKING STORAGE COMMON VARIABLES
      *
      *    SSA OFFSET
       01 WS-SSA-OFFSET                    PIC S9(4) COMP VALUE 0.
      *    BOOLEAN SSA STATUS
       01 WS-SSA-BOOLEAN-STATUS            PIC 9.
         88 WS-SSA-IS-NOT-BOOLEAN          VALUE 0.
         88 WS-SSA-IS-BOOLEAN              VALUE 1.
      *    SSA BOOLEAN OPERATOR
       01 WS-BOOLEAN-SSA-OPERATOR          PIC X.
         88 WS-SSA-OPERATOR-NONE           VALUE ' '.
         88 WS-SSA-OPERATOR-PARENTHESIS    VALUE ')'.
         88 WS-SSA-OPERATOR-OR             VALUE '+' '|'.
         88 WS-SSA-OPERATOR-AND            VALUE '*' '&'.
      *    USED WHEN IN A BOOLEAN CONDITION A FIELD WITHOUT KEY IS
      *    INVOLVED, 'AND ...AUTO_KEY' FIELD HAS BEEN ADDED TO THE
      *    CONDITION AND ONE MORE PARENTHESIS GROUP IS NEEDED
      * 01 WS-BOOLEAN-CLOSE-PARENTHESIS     PIC S9(9) COMP.
      *    WORKING SSA SEGMENT INDEX
       01 WS-SSA-SEGMENT-INDEX             PIC S9(9) COMP.
      *  IS ANY OF THE SSAS CHANGED?
       01 WS-SSA-STATUS                    PIC 9.
         88 SSAS-NOT-CHANGED               VALUE 0.
         88 SSAS-CHANGED                   VALUE 1.
      *  IS A GET NEXT COMMAND?
       01 WS-COMMAND                       PIC 9.
         88 CMD-NOT-NEXT                   VALUE 0.
         88 CMD-NEXT                       VALUE 1.
      *  IS ANY OF THE SSAS CHANGED?
       01 WS-SSA-MISSING                   PIC 9.
         88 SSA-NOT-ADDED                  VALUE 0.
         88 SSA-ADDED                      VALUE 1.
      *    LAST INDEX INDICATOR
       01 WS-LAST-INDEX-STATUS             PIC 9.
         88 WS-LAST-INDEX-ISNOT-VALID      VALUE 0.
         88 WS-LAST-INDEX-IS-VALID         VALUE 1.
      *    PARENTAGE INDICATOR
       01 WS-LEVEL-PARENTAGE-STATUS        PIC 9.
         88 LEVEL-ISNOT-PARENTAGE          VALUE 0.
         88 LEVEL-IS-PARENTAGE             VALUE 1.
      *    CONDITION SIGN INDICATOR
       01 WS-CONDITION-SIGN-STATUS         PIC 9.
         88 WS-CONDITION-HAS-NOT-GREATER   VALUE 0.
         88 WS-CONDITION-HAS-GREATER       VALUE 1.
      *    LSOURCE INDICATOR
       01 WS-LSOURCE-FLAG                  PIC 9.
         88 WS-HAS-NOT-LSOURCE             VALUE 0.
         88 WS-HAS-LSOURCE                 VALUE 1.
      *    PERFORM CYCLE INDICATOR
       01 WS-SSA-FOUND-STATUS              PIC 9.
         88 WS-SSA-NOT-FOUND               VALUE 0.
         88 WS-SSA-FOUND                   VALUE 1.
      *    KEY INDICATOR
       01 WS-KEY-STATUS                    PIC 9.
         88 WS-KEY-IS-NOT-CHANGED          VALUE 0.
         88 WS-KEY-IS-CHANGED              VALUE 1.
IRISST*    FIRST/NEXT PASS ON MULTI PARENT
IRISST*    Applied for now only to a specific combination
IRISST 01 WS-MULTI-PARENT                  PIC 9.
IRISST   88 WS-FIRST-STEP-MULTI            VALUE 0.
IRISST   88 WS-NOT-FIRST-STEP-MULTI        VALUE 1.
      *    BACKSLASH TO ADD INDICATOR
       01 WS-BACKSLASH-STATUS              PIC 9.
         88 WS-DONT-ADD-BACKSLASH          VALUE 0.
         88 WS-ADD-BACKSLASH               VALUE 1.
       01 WS-CONDITION-STATUS              PIC 9.
         88 WS-CONDITION-NOT-SET           VALUE 0.
         88 WS-CONDITION-SET               VALUE 1.
       01 WS-FIELD-TYPE-ZONED              PIC 9(5).
       01 WS-FIELD-BRIDGE-TYPE-ZONED       PIC 9(5).
       01 WS-FIELD-START-ZONED             PIC 9(5).
       01 WS-FIELD-BYTES-ZONED             PIC 9(5).
       01 WS-FIELD-DECIMALS-ZONED          PIC 9(5).
       01 WS-FIELD-IORTN-ZONED             PIC X(32).
       01 WS-FIELD-TYPE                    PIC S9(9) COMP.
      *    VALUES INTO DATA DICTIONARY
         88 WS-FIELD-TYPE-NONE             VALUE 0.
         88 WS-FIELD-TYPE-HEX              VALUE 1026.
         88 WS-FIELD-TYPE-PACKED           VALUE 1027.
         88 WS-FIELD-TYPE-ALPHA            VALUE 1028.
         88 WS-FIELD-TYPE-F-WORD           VALUE 1029.
         88 WS-FIELD-TYPE-H-WORD           VALUE 1030.
         88 WS-FIELD-TYPE-NUMERIC-ZONED    VALUE 1031.
      *    VALUES CHANGED AT RUN-TIME
      *    'I'
         88 WS-FIELD-TYPE-INDEX            VALUE 91000.
      *    'X'
         88 WS-FIELD-TYPE-XDFLD            VALUE 91001.
      *    'Z' EDITED
         88 WS-FIELD-TYPE-EDITED           VALUE 91002.
      *    'c' EBCDIC VALUE
         88 WS-FIELD-TYPE-EBCDIC           VALUE 91003.
      *    WORKING INDEXES AND COMMON VARIABLES
       01 WS-FIELD-DECIMALS                PIC S9(9) COMP.
       01 WS-XDFLD-NAME                    PIC X(17).
       01 WS-I1                            PIC S9(9) COMP.
       01 WS-I2                            PIC S9(9) COMP.
       01 WS-O1                            PIC S9(9) COMP.
       01 WS-O2                            PIC S9(9) COMP.
       01 WS-INDEX                         PIC S9(9) COMP.
       01 WS-INDEX-1                       PIC S9(9) COMP.
       01 WS-INDEX-2                       PIC S9(9) COMP.
       01 WS-INDEX-3                       PIC S9(9) COMP.
      *01 WS-ZONED-NUM-1                   PIC 9.
      *01 WS-ZONED-NUM-2                   PIC 9(2).
       01 WS-ZONED-NUM-4                   PIC 9(4).
      *01 WS-ZONED-NUM-4                   PIC 9(5).
       01 WS-ZONED-NUM-5                   PIC 9(5).
      *01 WS-SAVE-SEGMENT-INDEX            PIC S9(9) COMP.
      *01 WS-SAVE-PARENT-INDEX             PIC S9(9) COMP.
       01 WS-SAVE-BUFFER-LENGTH            PIC S9(9) COMP.
       01 WS-SAVE-LAST-SSA-INDEX           PIC S9(9) COMP.
       01 WS-SAVE-NEXT-SSA-INDEX           PIC S9(9) COMP.
       01 WS-SAVE-QUAL-SSA-INDEX           PIC S9(9) COMP.
       01 WS-SAVE-LSOURCE-INDEX            PIC S9(9) COMP.
       01 WS-CURR-PHYSICAL-LVL             PIC S9(4) COMP.
       01 WS-PARE-PHYSICAL-LVL             PIC S9(4) COMP.
       01 WS-CURR-PHYSICAL-NAME            PIC X(8).
       01 WS-PARE-PHYSICAL-NAME            PIC X(8).
       01 WS-TABLE-INDEX                   PIC S9(9) COMP.
       01 WS-SEGMENT-NAME                  PIC X(8).
       01 WS-SOURCE-SEGMENT                PIC X(8).
      *01 WS-INDEX-PREFIX                  PIC X(8).
      *01 WS-CURRENT-PREFIX                PIC X(9).
       01 WS-INDEX-PREFIX                  PIC X(32) VALUE SPACES.
       01 WS-CURRENT-PREFIX                PIC X(32) VALUE SPACES.
       01 WS-SEGMENT-CONDITION             PIC X(2).
       01 WS-PARENT-SEGMENT                PIC X(8).
       01 WS-SSA-PTR                       USAGE POINTER.
       01 WS-QUALIFIED-SSA-PTR             USAGE POINTER.
       01 WS-LVL-CCODES                    PIC X(32) VALUE SPACES.
       01 WS-IO-RTN.
         03 WS-IO-RTN-DBDNAME              PIC X(6).
         03 FILLER                         PIC X(2)  VALUE 'CP'.
       01 WS-DUMMY-PCB.
         03 WS-DUMMY-PCB-HEADER         PIC X(36).
         03 FILLER REDEFINES WS-DUMMY-PCB-HEADER.
           05 WS-DUMMY-DBD-NAME         PIC X(8).
           05 WS-DUMMY-SEG-LVL          PIC 9(2).
           05 WS-DUMMY-STS-CODE         PIC X(2).
           05 WS-DUMMY-PROC-OPT         PIC X(4).
           05 WS-DUMMY-PCB-RESERVED     PIC S9(9)  COMP.
           05 FILLER REDEFINES WS-DUMMY-PCB-RESERVED PIC X(4).
             88 WS-DUMMY-PCB-IRIS-EYE   VALUE 'IRIS'.
         03 WS-DUMMY-FB-KEY             PIC X(1587).
         03 WS-DUMMY-FB-RNTM            PIC X(377).
         03 WS-DUMMY-CNTL-FIXED         PIC X(1000).
         03 WS-DUMMY-CNTL-VAR           PIC X(32000).
      *
       01 FILLER.
         03 FILLER OCCURS 16.
           05 WS-DUMMY-SSA                 PIC X(1024).
      *
       01 FILLER                           PIC 9.
         88 SEGMENT-WITHOUT-C-CODE-UV      VALUE 0.
         88 SEGMENT-WITH-C-CODE-UV         VALUE 1.
       01 WS-SSA-IDX                       PIC S9(9) COMP.
       01 WS-SSA-LEN                       PIC S9(9) COMP.
       01 WS-SSA-SEGMENT-NAME              PIC X(8).
       01 WS-SSA-BUFFER.
         03 WS-SSA-BUFFER-LEN              PIC S9(4) COMP.
         03 WS-SSA-BUFFER-TXT              PIC X(4096).
       01 WS-SSA-KEY.
         03 WS-SSA-KEY-LEN                 PIC S9(4) COMP.
         03 WS-SSA-KEY-TXT                 PIC X(32000).
       01 WS-MEM-SSA-FUNCID                PIC S9(9) COMP.
       01 WS-FIELD-START                   PIC S9(9) COMP.
       01 WS-FIELD-BYTES                   PIC S9(9) COMP.
       01 WS-COM-FIELD-BYTES               PIC S9(9) COMP.
       01 WS-SSA-KEYS-LENGTH               PIC S9(9) COMP.
       01 WS-ST-COUNT                      PIC S9(4) COMP.
       01 WS-SSA-KEYS-START                PIC S9(9) COMP.
       01 WS-SSA-VALUE.
         02 WS-SSA-VALUE-LENGTH            PIC S9(4) COMP.
         02 WS-SSA-VALUE-TEXT              PIC X(256).
IRISST 01 WS-SSA-KEYVALUE.
IRISST   02 WS-SSA-KEYVALUE-LEN            PIC S9(4) COMP.
IRISST   02 WS-SSA-KEYVALUE-TXT            PIC X(256).
IRISST 01 WS-SSA-KEYVALUE-BOOL             PIC S9(9) COMP.
IRISST 01 WS-IS-SSA                        PIC 9.
IRISST   88 WS-IS-NOT-SSA                  VALUE 0.
IRISST   88 WS-IS-ACTUAL-SSA               VALUE 1.
       01 WS-FIELD-NAME                    PIC X(8).
       01 WS-CONDITION                     PIC X(2).
       01 WS-FIELD-IORTN.
         49 WS-FIELD-IORTN-LEN             PIC S9(4) COMP.
         49 WS-FIELD-IORTN-TXT             PIC X(32).
       01 WS-ROOT-SEGMENT-ZONED            PIC 9(2).
       01 WS-PHYS-ROOT-SEGMENT-ZONED       PIC 9(2).
       01 WS-FIELD-BRIDGE-TYPE             PIC S9(9) COMP.
      *    FORMAT NUMERIC FIELD INTO CONDITIONS
       01 WS-EDITED-MASK                   PIC  ------------------9.
       01 WS-EDITED-MASK-D1                PIC ------------------.9.
       01 WS-EDITED-MASK-D2                PIC -----------------.99.
       01 WS-EDITED-MASK-D3                PIC ----------------.999.
       01 WS-EDITED-MASK-D4                PIC ---------------.9999.
       01 WS-EDITED-MASK-D5                PIC --------------.99999.
       01 WS-EDITED-MASK-D6                PIC -------------.999999.
       01 WS-EDITED-MASK-D7                PIC ------------.9999999.
       01 WS-EDITED-MASK-D8                PIC -----------.99999999.
       01 WS-EDITED-MASK-D9                PIC ----------.999999999.
       01 WS-EDITED-MASK-D10               PIC ---------.9999999999.
       01 WS-EDITED-MASK-D11               PIC --------.99999999999.
       01 WS-EDITED-MASK-D12               PIC -------.999999999999.
       01 WS-EDITED-MASK-D13               PIC ------.9999999999999.
       01 WS-EDITED-MASK-D14               PIC -----.99999999999999.
       01 WS-EDITED-MASK-D15               PIC ----.999999999999999.
       01 WS-EDITED-MASK-D16               PIC ---.9999999999999999.
       01 WS-EDITED-MASK-D17               PIC --.99999999999999999.
       01 WS-EDITED-MASK-D18               PIC -.999999999999999999.
       01 WS-PACKED-MASK-RED               PIC X(10).
       01 WS-PACKED-MASK     REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(18) COMP-3.
       01 WS-PACKED-MASK-D1  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(17)V9 COMP-3.
       01 WS-PACKED-MASK-D2  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(16)V9(2) COMP-3.
       01 WS-PACKED-MASK-D3  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(15)V9(3) COMP-3.
       01 WS-PACKED-MASK-D4  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(14)V9(4) COMP-3.
       01 WS-PACKED-MASK-D5  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(13)V9(5) COMP-3.
       01 WS-PACKED-MASK-D6  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(12)V9(6) COMP-3.
       01 WS-PACKED-MASK-D7  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(11)V9(7) COMP-3.
       01 WS-PACKED-MASK-D8  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(10)V9(8) COMP-3.
       01 WS-PACKED-MASK-D9  REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(9)V9(9) COMP-3.
       01 WS-PACKED-MASK-D10 REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(8)V9(10) COMP-3.
       01 WS-PACKED-MASK-D11 REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(7)V9(11) COMP-3.
       01 WS-PACKED-MASK-D12 REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(6)V9(12) COMP-3.
       01 WS-PACKED-MASK-D13 REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(5)V9(13) COMP-3.
       01 WS-PACKED-MASK-D14 REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(4)V9(14) COMP-3.
       01 WS-PACKED-MASK-D15 REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(3)V9(15) COMP-3.
       01 WS-PACKED-MASK-D16 REDEFINES WS-PACKED-MASK-RED
                                           PIC S9(2)V9(16) COMP-3.
       01 WS-PACKED-MASK-D17 REDEFINES WS-PACKED-MASK-RED
                                           PIC S9V9(17) COMP-3.
       01 WS-PACKED-MASK-D18 REDEFINES WS-PACKED-MASK-RED
                                           PIC SV9(18) COMP-3.
       01 WS-BINARY-MASK                   PIC S9(9) COMP-4.
       01 WS-BINARY-MASK-RED REDEFINES WS-BINARY-MASK PIC X(4).
       01 FILLER                           PIC X.
         88 IS-NOT-READING-WITH-XDFLD      VALUE '0'.
         88 IS-READING-WITH-XDFLD          VALUE '1'.
       01 FILLER                           PIC X.
         88 IS-NOT-USING-INDEX-DBD         VALUE '0'.
         88 IS-USING-INDEX-DBD             VALUE '1'.
       01 FILLER                           PIC X.
         88 IS-INDEX-COND-NOT-ADDED        VALUE '0'.
         88 IS-INDEX-COND-ADDED            VALUE '1'.
      *
      *    CONSTANTS
      *
      *    SSA FIXED PART LENGTH
       01 SSA-FIX-L                        PIC S9(4) COMP VALUE +12.
      *    SLASH CHARACTER
       01 SLASH                            PIC X VALUE '\'.
      *
      *    SSA PARSER WORKING FIELDS
      *
           COPY IRISWKSS.
      *
      *  STRUCTURES TO REPRESENT TP AND DB PCB
      *
      *    COPY IRISISEG.
      *
       LINKAGE SECTION.
      *
      *    CONTROL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IRISPSSA'==.
      *
      *    IRIS PCB AREA
      *
           COPY IRISPCB.
       01  IRIS-PCB-DB-RED REDEFINES IRIS-DB-PCB.
         05  IRIS-PCB-HEADER               PIC X(36).
         05  IRIS-PCB-FEEDBACK             PIC X(1587).
      * 377 Bytes
           COPY IRISISEG REPLACING ==01 MEMORY-PCB-AREA.==
                                BY ==05 MEMORY-PCB-AREA.==.

           COPY IRISASSA.
      *
      * COMMON VARIABLES TO BE ADDRESSED AT RUN-TIME
      *
      *
      *  SENSEG REPRESENTATION
      *
           COPY IRISSSEG.
      *
      *  SEGMENT REPRESENTATION
      *
           COPY IRISSEGM.
      *
      *  DBD FIELD REPRESENTATION
      *
           COPY IRISFLD.
      *
      * INFO STATISTICS
      *
           COPY IRISRNTM.
      *
      *    IRIS PCB POINTERS
      *
           COPY IRISPTRS.
      *
      * COMMON SSA WORKING AREA
      *
       01 LK-QUALIFIED-AREA                PIC X(64).
       01 LK-COM-SSA                       PIC X(32000).
       01 LK-COM-SSA-2                     PIC X(32000).
       01 LK-SSA-AREA.
         03 LK-SSA-SEGMENT-NAME            PIC X(8).
         03 LK-SSA-COMMAND-PREFIX          PIC X.
           88 LK-SSA-COMMAND-PREFIX-NONE   VALUE ' '.
           88 LK-SSA-COMMAND-PREFIX-QUAL   VALUE '('.
           88 LK-SSA-COMMAND-PREFIX-CODE   VALUE '*'.
         03 LK-SSA-COMMAND-CODE            PIC X.
      *
      *    LIST OF THE SUPPORTED COMMAND CODES
      *
           88 IS-SUPPORTED-COMMAND-CODE    VALUE ' ' 'F' 'P' 'L'
                                                 'D' 'U' 'V'
                                                 '-' '(' 'T'.
      *    NO COMMAND CODE
           88 LK-SSA-COMMAND-CODE-NONE     VALUE ' '.
      *    USE THE CONCATENATED KEY OF A SEGMENT TO IDENTIFY THE SEGMENT
           88 LK-SSA-COMMAND-CODE-C        VALUE 'C'.
      *    RETRIEVE OR INSERT A SEQUENCE OF SEGMENTS IN A HIERARCHIC
      *    PATH USING ONLY ONE CALL, INSTEAD OF HAVING TO USE A SEPARATE
      *    CALL FOR EACH SEGMENT (PATH CALL).
           88 LK-SSA-COMMAND-CODE-D        VALUE 'D'
      *    HRDB RESERVED SAME AS 'D' BUT THE BRIDGE PROGRAM WON'T EMIT
      *    AN ERROR MESSAGE IN CASE OF NOT FOUND
                                                 'T'.
      *    BACK UP TO THE FIRST OCCURRENCE OF A SEGMENT UNDER ITS PARENT
      *    WHEN SEARCHING FOR A PARTICULAR SEGMENT OCCURRENCE.
      *    DISREGARDED FOR A ROOT SEGMENT.
           88 LK-SSA-COMMAND-CODE-F        VALUE 'F'.
      *    RETRIEVE THE LAST OCCURRENCE OF A SEGMENT UNDER ITS PARENT
           88 LK-SSA-COMMAND-CODE-L        VALUE 'L'.
      *    MOVE A SUBSET POINTER TO THE NEXT SEGMENT OCCURRENCE AFTER
      *    YOUR CURRENT POSITION. (USED WITH DEDBS ONLY.)
           88 LK-SSA-COMMAND-CODE-M        VALUE 'M'.
      *    DESIGNATE SEGMENTS THAT YOU DO NOT WANT REPLACED WHEN
      *    REPLACING SEGMENTS AFTER A GET HOLD CALL. USUALLY USED WHEN
      *    REPLACING A PATH OF SEGMENTS.
           88 LK-SSA-COMMAND-CODE-N        VALUE 'N'.
      *    SET PARENTAGE AT A HIGHER LEVEL THAN WHAT IT USUALLY IS
      *    (THE LOWEST SSA LEVEL OF THE CALL).
           88 LK-SSA-COMMAND-CODE-P        VALUE 'P'.
      *    RESERVE A SEGMENT SO THAT OTHER PROGRAMS WILL NOT BE ABLE TO
      *    UPDATE IT UNTIL YOU HAVE FINISHED PROCESSING AND UPDATING IT.
           88 LK-SSA-COMMAND-CODE-Q        VALUE 'Q'.
      *    RETRIEVE THE FIRST SEGMENT OCCURRENCE IN A SUBSET.
      *    (USED WITH DEDBS ONLY.)
           88 LK-SSA-COMMAND-CODE-R        VALUE 'R'.
      *    UNCONDITIONALLY SET A SUBSET POINTER TO THE CURRENT POSITION.
      *    (USED WITH DEDBS ONLY.)
           88 LK-SSA-COMMAND-CODE-S        VALUE 'S'.
      *    LIMIT THE SEARCH FOR A SEGMENT TO THE DEPENDENTS OF THE
      *    SEGMENT OCCURRENCE ON WHICH POSITION IS ESTABLISHED.
           88 LK-SSA-COMMAND-CODE-U        VALUE 'U'.
      *    USE THE CURRENT POSITION AT THIS HIERARCHIC LEVEL AND ABOVE
      *    AS QUALIFICATION FOR THE SEGMENT.
           88 LK-SSA-COMMAND-CODE-V        VALUE 'V'.
      *    SET A SUBSET POINTER TO YOUR CURRENT POSITION, IF THE SUBSET
      *    POINTER IS NOT ALREADY SET. (USED WITH DEDBS ONLY.)
           88 LK-SSA-COMMAND-CODE-W        VALUE 'W'.
      *    SET A SUBSET POINTER TO ZERO, SO IT CAN BE REUSED.
      *    (USED WITH DEDBS ONLY.)
           88 LK-SSA-COMMAND-CODE-Z        VALUE 'Z'.
      *    ENABLES TO RESERVE ONE OR MORE POSITION IN SSA IN WHICH THE
      *    PROGRAM CAN STORE COMMAND CODES DURING THE EXCUTION.
           88 LK-SSA-COMMAND-CODE-NULL     VALUE '-'.
      *    END COMMAND VALUES
           88 LK-SSA-COMMAND-CODE-CLOSE    VALUE ' ' '('.
      *
       01 LK-MEM-SSA-KEYS.
         03 LK-MEM-SSA-COUNT               PIC S9(4) COMP.
         03 FILLER OCCURS 1 TO 255 DEPENDING ON LK-MEM-SSA-COUNT.
           05 LK-MEM-SSA-FUNCID            PIC S9(9) COMP.
           05 LK-MEM-SSA-LEN               PIC S9(4) COMP.
SANTIX*    05 LK-MEM-SSA-TXT               PIC X(240).
SANTIX     05 LK-MEM-SSA-TXT               PIC X(600).
      *
       01 LK-SSA-01                        PIC X(512).
       01 LK-SSA-02                        PIC X(512).
       01 LK-SSA-03                        PIC X(512).
       01 LK-SSA-04                        PIC X(512).
       01 LK-SSA-05                        PIC X(512).
       01 LK-SSA-06                        PIC X(512).
       01 LK-SSA-07                        PIC X(512).
       01 LK-SSA-08                        PIC X(512).
       01 LK-SSA-09                        PIC X(512).
       01 LK-SSA-10                        PIC X(512).
       01 LK-SSA-11                        PIC X(512).
       01 LK-SSA-12                        PIC X(512).
       01 LK-SSA-13                        PIC X(512).
       01 LK-SSA-14                        PIC X(512).
       01 LK-SSA-15                        PIC X(512).
       01 LK-SSA-16                        PIC X(512).
      *
       PROCEDURE DIVISION USING IRIS-WORK-AREA
                                IRIS-DB-PCB
                                IRIS-SSA-AREA
                                LK-MEM-SSA-KEYS
                                LK-SSA-01 LK-SSA-02 LK-SSA-03 LK-SSA-04
                                LK-SSA-05 LK-SSA-06 LK-SSA-07 LK-SSA-08
                                LK-SSA-09 LK-SSA-10 LK-SSA-11 LK-SSA-12
                                LK-SSA-13 LK-SSA-14 LK-SSA-15 LK-SSA-16.
      *
       MAIN-PROGRAM.
      *
      *    INITIALIZE VALARIABLES
      *
           IF IRIS-TRACE-DEBUG
             EXEC SQL
              SET :IRIS-CURRENT-TIMESTAMP = CURRENT_TIMESTAMP
             END-EXEC
           END-IF
           PERFORM INIT-VARIABLES THRU INIT-VARIABLES-EX
      *
      *    INITIALIZE AND PARSE THE SSAS
      *
           PERFORM INIT-SSAS THRU INIT-SSAS-EX
           PERFORM PARSE-SSAS THRU PARSE-SSAS-EX
      *
      *    IF NO SSA HAS BEEN USED GO BACK,
      *     OTHERWISE STORE THE LAST SEGMENT NAME
      *
           IF ST-COUNT = ZERO
             IF IRIS-FUNC-REPL OR IRIS-FUNC-DLET
              PERFORM BUILD-SQL-QUERY THRU BUILD-SQL-QUERY
DBG   *       DISPLAY 'SSA_KEY=(' WS-SSA-KEY-TXT(1:WS-SSA-KEY-LEN) ')'
              STRING DB-PCB-SEGMENT-NAME DELIMITED BY SIZE
              ':'  DELIMITED BY SIZE
              INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
DBG   *       DISPLAY 'SSA_KEY=(' WS-SSA-KEY-TXT(1:WS-SSA-KEY-LEN) ')'
              PERFORM SEARCH-FOR-CUSTOM THRU SEARCH-FOR-CUSTOM-EX
              IF WS-MEM-SSA-FUNCID > ZERO
                MOVE WS-MEM-SSA-FUNCID TO IRIS-SSA-CUSTOM-FUNC-ID
DBG   *         DISPLAY 'FUNC_ID=(' IRIS-SSA-CUSTOM-FUNC-ID ')'
                SET SSA-USER-CUSTOM(1) TO TRUE
              END-IF
             ELSE
              MOVE SPACE TO WS-SSA-SEGMENT-NAME
                           IRIS-SSA-SEGMENT
             END-IF
             GO TO GO-BACK

           ELSE
             MOVE STE-SEGM-NAME(ST-COUNT) TO WS-SSA-SEGMENT-NAME
                                             IRIS-SSA-SEGMENT
           END-IF
      *
      *    RETRIEVE THE MEMORY INDEX OF THE SEGMENT
      *
           SET IRISGSEG-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                  MEMORY-PCB-AREA
                                  WS-SSA-SEGMENT-NAME
                                  WS-SSA-SEGMENT-INDEX
      *
           IF NOT IRIS-NO-ERROR
             GO TO GO-BACK
           END-IF
           
SANTIX     IF IRIS-DYNSSA-PARSEONLY
      *
              PERFORM BUILD-SQL-QUERY THRU BUILD-SQL-QUERY
      *
              PERFORM SEARCH-FOR-CUSTOM THRU SEARCH-FOR-CUSTOM-EX
      *
              IF WS-MEM-SSA-FUNCID > ZERO
                MOVE WS-MEM-SSA-FUNCID TO IRIS-SSA-CUSTOM-FUNC-ID
                                          RUN-LAST-DYN-SSA-FUNCID
DBG   *         DISPLAY 'FUNC_ID=(' IRIS-SSA-CUSTOM-FUNC-ID ')'
                SET SSA-USER-CUSTOM(1) TO TRUE
SANTIX*         MOVE IRIS-SSA-CUSTOM-FUNC-ID TO RUN-LAST-DYN-SSA-FUNCID
              END-IF
              GOBACK
           END-IF
      *
      *    HANDLE SPLECIAL CASES WITH JUST ONE UNQUALIFIED SSA
      *
           IF IRIS-FUNC-ISRT
             CONTINUE
           ELSE
             PERFORM ONLY-ONE-UNQUALIFIED THRU ONLY-ONE-UNQUALIFIED-EX
           END-IF
      *
      *    CONTINUE WITH THE REGULAR HANDLING
      *
      *    IF LAST-SEGMENT-INDEX < 0
      *      SET WS-LAST-INDEX-IS-VALID TO TRUE
      *    ELSE
      *      SET WS-LAST-INDEX-ISNOT-VALID TO TRUE
      *    END-IF
      *    MOVE WS-SSA-SEGMENT-INDEX TO WS-SAVE-SEGMENT-INDEX
      *
      *    BUILD THE SQL CONDITION FOR EACH LEVEL
      *
           SET IS-NOT-LAST-SSA-KEY TO TRUE
           EVALUATE TRUE
           WHEN IRIS-FUNC-GU
           WHEN IRIS-FUNC-GHU
             SET CMD-NOT-NEXT TO TRUE
      *
             PERFORM BUILD-SQL-QUERY THRU BUILD-SQL-QUERY
      *
             PERFORM SEARCH-FOR-CUSTOM THRU SEARCH-FOR-CUSTOM-EX
      *
             IF WS-MEM-SSA-FUNCID > ZERO
               MOVE WS-MEM-SSA-FUNCID TO IRIS-SSA-CUSTOM-FUNC-ID
SANTIX                                   RUN-LAST-DYN-SSA-FUNCID
DBG   *        DISPLAY 'FUNC_ID=(' IRIS-SSA-CUSTOM-FUNC-ID ')'
               SET SSA-USER-CUSTOM(1) TO TRUE
SANTIX*        MOVE IRIS-SSA-CUSTOM-FUNC-ID TO RUN-LAST-DYN-SSA-FUNCID
             ELSE
               SUBTRACT 1 FROM WS-SSA-BUFFER-LEN
               MOVE 1 TO IRIS-SSA-CMDS-COUNT
               MOVE WS-SSA-BUFFER-TXT(1:WS-SSA-BUFFER-LEN)
                        TO IRIS-SSA-SQL-TXT(1)(1:WS-SSA-BUFFER-LEN)
               MOVE WS-SSA-BUFFER-LEN TO IRIS-SSA-SQL-LEN(1)
               SET SSA-SELECT-DYNAMIC(1) TO TRUE
             END-IF
      *
           WHEN IRIS-FUNC-GN
           WHEN IRIS-FUNC-GHN

             IF SSAS-CHANGED
             OR STE-QUAL(ST-COUNT) NOT = CP-PTR-NULL
               SET CMD-NEXT TO TRUE
               IF SSAS-NOT-CHANGED
                 SET IS-LAST-SSA-KEY TO TRUE
               END-IF
               PERFORM BUILD-SQL-QUERY THRU BUILD-SQL-QUERY
               PERFORM SEARCH-FOR-CUSTOM THRU SEARCH-FOR-CUSTOM-EX
               IF WS-MEM-SSA-FUNCID > ZERO 
                 OR ( RUN-LAST-DYN-SSA-FUNCID > ZERO 
                      AND SSAS-NOT-CHANGED)
                 IF WS-MEM-SSA-FUNCID > ZERO 
                   MOVE WS-MEM-SSA-FUNCID TO IRIS-SSA-CUSTOM-FUNC-ID
                                             RUN-LAST-DYN-SSA-FUNCID
                 ELSE
                   MOVE RUN-LAST-DYN-SSA-FUNCID TO 
                                             IRIS-SSA-CUSTOM-FUNC-ID
                 END-IF
DBG   *        DISPLAY 'FUNC_ID=(' IRIS-SSA-CUSTOM-FUNC-ID ')'
                 SET SSA-USER-CUSTOM(1) TO TRUE               
               ELSE
                 SUBTRACT 1 FROM WS-SSA-BUFFER-LEN
                 MOVE 1 TO IRIS-SSA-CMDS-COUNT
                 MOVE WS-SSA-BUFFER-TXT(1:WS-SSA-BUFFER-LEN)
                        TO IRIS-SSA-SQL-TXT(1)(1:WS-SSA-BUFFER-LEN)
                 MOVE WS-SSA-BUFFER-LEN TO IRIS-SSA-SQL-LEN(1)
                 SET SSA-SELECT-DYNAMIC(1) TO TRUE
                 MOVE ZERO TO RUN-LAST-DYN-SSA-FUNCID
               END-IF
             ELSE
               IF RUN-LAST-DYN-SSA-FUNCID > ZERO
                 PERFORM BUILD-SQL-QUERY THRU BUILD-SQL-QUERY
                 MOVE RUN-LAST-DYN-SSA-FUNCID TO 
                                            IRIS-SSA-CUSTOM-FUNC-ID
                 SET SSA-USER-CUSTOM(1) TO TRUE
               ELSE
                SET SSA-SELECT-NEXT(1) TO TRUE
               END-IF
             END-IF
           WHEN IRIS-FUNC-GNP
           WHEN IRIS-FUNC-GHNP
             IF SSAS-CHANGED
             OR STE-QUAL(ST-COUNT) NOT = CP-PTR-NULL
               SET CMD-NEXT TO TRUE
               IF SSAS-NOT-CHANGED
                 SET IS-LAST-SSA-KEY TO TRUE
               END-IF
               PERFORM BUILD-SQL-QUERY THRU BUILD-SQL-QUERY
               PERFORM SEARCH-FOR-CUSTOM THRU SEARCH-FOR-CUSTOM-EX
               IF WS-MEM-SSA-FUNCID > ZERO
                 MOVE WS-MEM-SSA-FUNCID TO IRIS-SSA-CUSTOM-FUNC-ID
DBG   *        DISPLAY 'FUNC_ID=(' IRIS-SSA-CUSTOM-FUNC-ID ')'
                 SET SSA-USER-CUSTOM(1) TO TRUE
               ELSE
                 SUBTRACT 1 FROM WS-SSA-BUFFER-LEN
                 MOVE 1 TO IRIS-SSA-CMDS-COUNT
                 MOVE WS-SSA-BUFFER-TXT(1:WS-SSA-BUFFER-LEN)
                        TO IRIS-SSA-SQL-TXT(1)(1:WS-SSA-BUFFER-LEN)
                 MOVE WS-SSA-BUFFER-LEN TO IRIS-SSA-SQL-LEN(1)
                 SET SSA-SELECT-DYNAMIC(1) TO TRUE
               END-IF
             ELSE
               SET SSA-SELECT-NEXT(1) TO TRUE
             END-IF
           WHEN IRIS-FUNC-ISRT
             SET CMD-NOT-NEXT TO TRUE

             PERFORM BUILD-SQL-QUERY THRU BUILD-SQL-QUERY

             PERFORM SEARCH-FOR-CUSTOM THRU SEARCH-FOR-CUSTOM-EX

             IF WS-MEM-SSA-FUNCID > ZERO
               MOVE WS-MEM-SSA-FUNCID TO IRIS-SSA-CUSTOM-FUNC-ID
DBG   *        DISPLAY 'FUNC_ID=(' IRIS-SSA-CUSTOM-FUNC-ID ')'
               SET SSA-USER-CUSTOM(1) TO TRUE
             ELSE
               MOVE ZERO TO IRIS-MSG-LEN
               STRING '<IRISTRACE> - '
               '(IRISPSSA) UNSUPPORTED FUNCTION (' IRIS-IMS-FUNCTION ')'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRIS-MSG-LEVEL-ERROR TO TRUE
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               SET IRIS-ERR-UNSUPPORTED-FUNC TO TRUE
               GO TO GO-BACK
             END-IF

           WHEN OTHER
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - '
             '(IRISPSSA) UNSUPPORTED FUNCTION (' IRIS-IMS-FUNCTION ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-ERROR TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             SET IRIS-ERR-UNSUPPORTED-FUNC TO TRUE
             GO TO GO-BACK
           END-EVALUATE
      *
      *    VERIFY PARENTAGE CONSTRAINTS
      *
      *<FIXME> IS THIS STILL NEEDED?
      *    PERFORM VERIFY-PARENTAGE THRU VERIFY-PARENTAGE-EX
           .
      *
       MAIN-PROGRAM-EX.
      *
           GOBACK.
      *
      ******************************************************************
      * INIZIALIZE VARIABLES
      ******************************************************************
      *
       INIT-VARIABLES SECTION.
      *
           MOVE ZERO TO IRIS-SSA-CUSTOM-FUNC-ID
           MOVE SPACE TO IRIS-SSA-FUNCTION(1)
           MOVE ZERO TO WS-SSA-OFFSET ST-SHIFT
                        IRIS-SSA-CMDS-COUNT IRIS-SSA-COUNT
           SET WS-DONT-ADD-BACKSLASH TO TRUE
           SET WS-SSA-IS-NOT-BOOLEAN TO TRUE
           SET WS-SSA-OPERATOR-NONE TO TRUE
IRISST     SET WS-FIRST-STEP-MULTI TO TRUE
           SET ADDRESS OF RUN-CONTROL-AREA TO DT-INFO-PTR
           SET ADDRESS OF MEMORY-PCB-SENSEG TO DT-PSB-SENSEG-PTR
           SET ADDRESS OF MEMORY-DBD-SEGMENTS TO DT-DBD-SEGM-PTR
           SET ADDRESS OF MEMORY-DBD-FIELDS TO DT-DBD-FIELD-PTR
      *    RESET SSAS POINTERS
           IF IRIS-FUNC-GU OR IRIS-FUNC-GHU
             PERFORM VARYING WS-INDEX FROM 1 BY 1
             UNTIL WS-INDEX > DT-PHYS-NUM-SENSEG
               MOVE ZERO TO RUN-USED-SSA-LEN(WS-INDEX)
             END-PERFORM
             MOVE ZERO TO RUN-LAST-DYN-SSA-FUNCID
           END-IF
           SET IS-NOT-READING-WITH-XDFLD TO TRUE.
      *
       INIT-VARIABLES-EX.
           EXIT.
      *
      ******************************************************************
      * INIZIALIZE SSAS
      ******************************************************************
      *
       INIT-SSAS SECTION.
      *
           MOVE 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-01
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-02
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-03
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-04
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-05
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-06
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-07
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-08
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-09
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-10
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-11
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-12
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-13
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-14
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-15
           ELSE
             GO TO INIT-SSAS-END
           END-IF
           ADD 1 TO WS-INDEX
           IF WS-INDEX <= IRIS-SSAS-NUM
             SET STE-POINTER(WS-INDEX) TO ADDRESS OF LK-SSA-16
           END-IF.
      *
       INIT-SSAS-END.
      *
           COMPUTE ST-COUNT = WS-INDEX - 1.
      *
       INIT-SSAS-EX.
           EXIT.
      *
      ******************************************************************
      *    PARSE THE SSA CONTENT
      ******************************************************************
      *
       PARSE-SSAS SECTION.
      *
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSSA:PARSE-SSAS START) '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             MOVE ST-COUNT TO WS-ZONED-NUM-5
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSSA:PARSE-SSAS ST-COUNT=('
             WS-ZONED-NUM-5 ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           PERFORM PARSE-PHY-SSAS THRU PARSE-PHY-SSAS-EX

      *
      *    CHECK IF ALL THE SSAS FOR ALL THE PARENTS ARE PRESENT
      *    OTHERWISE KEEP ADDING ALL OF THEM, REUSING A PREVIOUS
      *    INSTANCE IS PRESENT OR ADDING UNQUALIFIED ONES
           MOVE ST-COUNT TO WS-ST-COUNT
           SET SSA-ADDED TO TRUE
           PERFORM UNTIL SSA-NOT-ADDED
             SET SSA-NOT-ADDED TO TRUE
             PERFORM WITH TEST BEFORE
             VARYING WS-INDEX-1 FROM ST-COUNT BY -1
             UNTIL WS-INDEX-1 <= 0 OR SSA-ADDED
               MOVE STE-K(WS-INDEX-1) TO WS-SSA-IDX
               MOVE PS-PARENT-SEGMENT-NAME(WS-SSA-IDX)
                                                  TO WS-PARENT-SEGMENT
      *        NO PARENT IT'S THE ROOT SEGMENT IN THE SENSEG
               IF WS-PARENT-SEGMENT = SPACE
      *        THERE IS A SEQUENCE ERROR EMIT AN ERROR
                 IF WS-INDEX-1 NOT = 1
                   MOVE ZERO TO IRIS-MSG-LEN
                   STRING '<IRISTRACE> - '
                   '(IRISPSSA) SENSEG SEQUENCE, THE SEGMENT IN THE '
                   'FIRST SENSEG MUST BE A ROOT'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRIS-MSG-LEVEL-ERROR TO TRUE
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                   SET IRIS-ERR-WRONG-SENSEG TO TRUE
                   GO TO GO-BACK
                 END-IF
               ELSE
                 IF WS-INDEX-1 = 1
                   SET SSA-ADDED TO TRUE
                 ELSE
      *            THE FATHER IS MISSING IN THE SSAS PASSED
      *            INSERT HERE A DUMMY SSA
                   IF STE-SEGM-NAME(WS-INDEX-1 - 1) NOT =
                                                     WS-PARENT-SEGMENT
                     SET SSA-ADDED TO TRUE
                   END-IF
                 END-IF
                 IF SSA-ADDED
                   MOVE ST-ELEMENT(WS-INDEX-1) TO
                                            ST-ELEMENT(WS-INDEX-1 + 1)
      *            RESET THE NEW ELEMENT
                   ADD 1 TO ST-COUNT
                   SET STE-POINTER(WS-INDEX-1)
                                TO ADDRESS OF WS-DUMMY-SSA(WS-INDEX-1)
                   MOVE WS-PARENT-SEGMENT TO STE-SEGM-NAME(WS-INDEX-1)
                   MOVE ZERO TO STE-K(WS-INDEX-1)
      *
      *      SET TO STE-K THE INDEX TO THE SESENSEG AND RCA TABLES
      *      RUN-SEGMENT-DETAIL AND ARRAY-PCB-SENSEG HAVE THE SAME
      *      INDEX, ARE PARALLEL MEMORY TABLES
                   MOVE ZERO TO STE-K(WS-INDEX-1)
                   PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
                   UNTIL WS-INDEX-2 > DT-PHYS-NUM-SENSEG
                   OR STE-K(WS-INDEX-1) > 0
                     IF PS-SEGMENT-NAME(WS-INDEX-2) = WS-PARENT-SEGMENT
                       MOVE WS-INDEX-2 TO STE-K(WS-INDEX-1)
                     END-IF
                   END-PERFORM
      *
                   IF STE-K(WS-INDEX-1) = ZERO
                     MOVE ZERO TO IRIS-MSG-LEN
                     STRING '<IRISTRACE> - '
                     '(IRISPSSA) CANNOT RETRIEVE INTERNAL SENSEG '
                     'POINTER' MESSAGE-END DELIMITED BY SIZE
                     INTO IRIS-MSG-TXT
                     SET IRIS-MSG-LEVEL-ERROR TO TRUE
                     SET IRISTRAC-RTN TO TRUE
                     CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                     SET IRIS-ERR-WRONG-SENSEG-PTR TO TRUE
                     GO TO GO-BACK
                   END-IF
      *
                   MOVE STE-K(WS-INDEX-1) TO WS-SSA-IDX
      *
                   IF RUN-USED-SSA-LEN(WS-SSA-IDX) > 0
                     MOVE RUN-USED-SSA-LEN(WS-SSA-IDX) TO WS-SSA-LEN
                     MOVE RUN-USED-SSA(WS-SSA-IDX)(1:WS-SSA-LEN)
                                           TO WS-DUMMY-SSA(WS-INDEX-1)
                   ELSE
                     MOVE SPACE TO WS-DUMMY-SSA(WS-INDEX-1)(1:9)
                     MOVE WS-PARENT-SEGMENT
                                      TO WS-DUMMY-SSA(WS-INDEX-1)(1:8)
                   END-IF
                 END-IF
               END-IF
             END-PERFORM
           END-PERFORM
      *
      *    THERE ARE SSAS ADDED REDO THE PYSICAL PARSER
      *
           IF ST-COUNT > WS-ST-COUNT
             PERFORM PARSE-PHY-SSAS THRU PARSE-PHY-SSAS-EX
           END-IF
      *
      *    CHECK IF THE SSAS CHANGED FROM THE PREVIOUS ACCESS
      *    USING THE SAME PCB
           SET SSAS-NOT-CHANGED TO TRUE
           PERFORM VARYING WS-INDEX-1 FROM 1 BY 1
           UNTIL WS-INDEX-1 > ST-COUNT
             MOVE STE-SEGM-NAME(WS-INDEX-1) TO WS-SSA-SEGMENT-NAME
             SET IRISGSEG-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                    MEMORY-PCB-AREA
                                    WS-SSA-SEGMENT-NAME
                                    WS-SSA-SEGMENT-INDEX
      *
             IF NOT IRIS-NO-ERROR
               GO TO GO-BACK
             END-IF
      *
      * SET UP THE COMMAND CODE FOR THE IO ROUTINE
      *
             COMPUTE WS-INDEX-2 =
                          DS-SEGMENT-LEVEL(WS-SSA-SEGMENT-INDEX) + 1
             IF STEC-C(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-CONCAT-KEY(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-D(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-PATHCALL(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-F(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-FIRST(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-L(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-LAST(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-M(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-PTRNEXT(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-N(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-NOREPL(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-P(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-PARENTAGE(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-Q(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-HOLDSEG(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-R(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-FIRSTSEG(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-S(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-PTRCURR(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-U(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-LIMSRCH(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-V(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-HIERQUAL(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-W(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-PTRCURR2(WS-INDEX-2) TO TRUE
             END-IF
             IF STEC-Z(WS-INDEX-1) NOT = SPACE
               SET IRIS-CODE-PTRRES(WS-INDEX-2) TO TRUE
             END-IF
      *

      *
      *      DISPLAY 'DS-SEGMENT-NAME='
      *                         DS-SEGMENT-NAME(WS-SSA-SEGMENT-INDEX)
      *      SET TO STE-K THE INDEX TO THE SESENSEG AND RCA TABLES
      *      RUN-SEGMENT-DETAIL AND ARRAY-PCB-SENSEG HAVE THE SAME
      *      INDEX, ARE PARALLEL MEMORY TABLES
      *      PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
      *      UNTIL WS-INDEX-2 > DT-PHYS-NUM-SENSEG
      *      OR STE-K(WS-INDEX-1) > 0
      *        IF PS-SEGMENT-NAME(WS-INDEX-2) = WS-SSA-SEGMENT-NAME
      *          MOVE WS-INDEX-2 TO STE-K(WS-INDEX-1)
      *        END-IF
      *      END-PERFORM
      *
      *      IF STE-K(WS-INDEX-1) = ZERO
      *        MOVE ZERO TO IRIS-MSG-LEN
      *        STRING
      *        '(IRISPSSA) CANNOT RETRIEVE INTERNAL SENSEG POINTER'
      *        MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
      *        SET IRIS-MSG-LEVEL-ERROR TO TRUE
      *        SET IRISTRAC-RTN TO TRUE
      *        CALL IRIS-WS-RTN USING IRIS-WORK-AREA
      *        SET IRIS-ERR-WRONG-SENSEG-PTR TO TRUE
      *        GO TO GO-BACK
      *      END-IF
      *
             MOVE STE-K(WS-INDEX-1) TO WS-SSA-IDX
      *
             SET ADDRESS OF LK-SSA-AREA TO STE-INIT-PTR(WS-INDEX-1)
             IF STE-QUAL(WS-INDEX-1) = CP-PTR-NULL
               MOVE 9 TO WS-SSA-LEN
             ELSE
               COMPUTE WS-SSA-LEN = LENGTH OF RUN-USED-SSA(WS-SSA-IDX)
               MOVE WS-SSA-LEN TO WS-INDEX-3
               PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
               UNTIL WS-INDEX-2 > WS-SSA-LEN
               OR WS-SSA-LEN < WS-INDEX-3
                 IF LK-SSA-AREA(WS-INDEX-2:1) = ')'
                   MOVE WS-INDEX-2 TO WS-SSA-LEN
                 END-IF
               END-PERFORM
             END-IF
             IF RUN-USED-SSA-LEN(WS-SSA-IDX) > 0
             AND RUN-USED-SSA(WS-SSA-IDX)(1:WS-SSA-LEN) =
                                             LK-SSA-AREA(1:WS-SSA-LEN)
               SET RUN-USED-KEY-NOT-CHANGED(WS-SSA-IDX) TO TRUE
             ELSE
               SET RUN-USED-KEY-CHANGED(WS-SSA-IDX) TO TRUE
               SET SSAS-CHANGED TO TRUE
             END-IF
             MOVE LK-SSA-AREA(1:WS-SSA-LEN)
                             TO RUN-USED-SSA(WS-SSA-IDX)(1:WS-SSA-LEN)
                                IRIS-SSA-TXT(WS-INDEX-1)(1:WS-SSA-LEN)
             MOVE WS-SSA-LEN TO RUN-USED-SSA-LEN(WS-SSA-IDX)
                                IRIS-SSA-LEN(WS-INDEX-1)
           END-PERFORM
      *
           MOVE ST-COUNT TO IRIS-SSA-COUNT
      *
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSSA:PARSE-SSAS END) '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
      *
       PARSE-SSAS-EX.
           EXIT.
      *
      ******************************************************************
      *    PARSE PHYSICALLY THE SSAS CONTENT
      ******************************************************************
      *
       PARSE-PHY-SSAS SECTION.
      *
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSSA:PARSE-PHY-SSAS START) '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             MOVE ST-COUNT TO WS-ZONED-NUM-5
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSSA:PARSE-PHY-SSAS ST-COUNT=('
             WS-ZONED-NUM-5 ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           PERFORM VARYING WS-INDEX-1 FROM 1 BY 1
           UNTIL WS-INDEX-1 > ST-COUNT
             SET ADDRESS OF LK-SSA-AREA TO STE-POINTER(WS-INDEX-1)
             SET STE-INIT-PTR(WS-INDEX-1) TO STE-POINTER(WS-INDEX-1)
             MOVE LK-SSA-SEGMENT-NAME TO STE-SEGM-NAME(WS-INDEX-1)
             MOVE 0 TO STE-K(WS-INDEX-1)
             EVALUATE TRUE
      *
      *      NO CHARACTER, IT IS AN UNQUALIFIED SSA
             WHEN LK-SSA-COMMAND-PREFIX-NONE
               MOVE SPACE TO STE-COMMAND-CODES(WS-INDEX-1)
               SET STE-QUAL(WS-INDEX-1) TO CP-PTR-NULL
      *
      *      IT IS A QUALIFIED SSA
             WHEN LK-SSA-COMMAND-PREFIX-QUAL
               MOVE SPACE TO STE-COMMAND-CODES(WS-INDEX-1)
               SET STE-QUAL(WS-INDEX-1) TO STE-POINTER(WS-INDEX-1)
               ADD 8 TO STE-QUAL-R(WS-INDEX-1)
      *
      *      IT IS A COMMAND CODE
             WHEN LK-SSA-COMMAND-PREFIX-CODE
               SET STE-QUAL(WS-INDEX-1) TO STE-POINTER(WS-INDEX-1)
               MOVE SPACE TO STE-COMMAND-CODES(WS-INDEX-1)
      *
      *        STORE ALL THE COMMAND CODES
               PERFORM UNTIL LK-SSA-COMMAND-CODE-CLOSE
      *
                 EVALUATE TRUE
                 WHEN LK-SSA-COMMAND-CODE-C
                   MOVE LK-SSA-COMMAND-CODE TO STEC-C(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-D
                   MOVE LK-SSA-COMMAND-CODE TO STEC-D(WS-INDEX-1)
      *FIXME: DOUBLE-CHECK
      *            SET IS-USING-PATH-CALL TO TRUE
                 WHEN LK-SSA-COMMAND-CODE-F
                   MOVE LK-SSA-COMMAND-CODE TO STEC-F(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-L
                   MOVE LK-SSA-COMMAND-CODE TO STEC-L(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-M
                   MOVE LK-SSA-COMMAND-CODE TO STEC-M(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-N
                   MOVE LK-SSA-COMMAND-CODE TO STEC-N(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-P
                   MOVE LK-SSA-COMMAND-CODE TO STEC-P(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-Q
                   MOVE LK-SSA-COMMAND-CODE TO STEC-Q(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-R
                   MOVE LK-SSA-COMMAND-CODE TO STEC-R(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-S
                   MOVE LK-SSA-COMMAND-CODE TO STEC-S(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-U
                   MOVE LK-SSA-COMMAND-CODE TO STEC-U(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-V
                   MOVE LK-SSA-COMMAND-CODE TO STEC-V(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-W
                   MOVE LK-SSA-COMMAND-CODE TO STEC-W(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-Z
                   MOVE LK-SSA-COMMAND-CODE TO STEC-Z(WS-INDEX-1)
                 WHEN LK-SSA-COMMAND-CODE-NULL
                   MOVE LK-SSA-COMMAND-CODE TO STEC-NULL(WS-INDEX-1)
                 END-EVALUATE

      *
      *           IT IS AN UNSUPPORTED COMMAND-CODE
      *
                 IF NOT IS-SUPPORTED-COMMAND-CODE
                   MOVE ZERO TO IRIS-MSG-LEN
                   STRING
                   '<IRISTRACE> - (IRISPSSA:PARSE-SSAS) UNSUPPORTED '
                   'COMMAND CODE (' LK-SSA-COMMAND-CODE ')'
                   DELIMITED BY SIZE MESSAGE-END DELIMITED BY SIZE
                   INTO IRIS-MSG-TXT
                   SET IRIS-MSG-LEVEL-ERROR TO TRUE
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                   MOVE ZERO TO IRIS-MSG-LEN
                   SET IRIS-ERR-UNSUPPORTED-C-COMMAND TO TRUE
                   SET DB-STATUS-INTERNAL-NOT-HANDLED TO TRUE
      *            MOVE IRIS-MESSAGE-ID TO RETURN-CODE
                   GO TO GO-BACK
                 END-IF
      *
                 ADD 1 TO STE-QUAL-R(WS-INDEX-1)
      *
                 ADD 1 TO STE-POINTER-R(WS-INDEX-1)
                 SET ADDRESS OF LK-SSA-AREA TO STE-POINTER(WS-INDEX-1)
               END-PERFORM
      *
               IF LK-SSA-COMMAND-CODE-NONE
                 SET STE-QUAL(WS-INDEX-1) TO CP-PTR-NULL
               ELSE
                 ADD 9 TO STE-QUAL-R(WS-INDEX-1)
               END-IF
      *
             END-EVALUATE
      *
      *      SET TO STE-K THE INDEX TO THE SESENSEG AND RCA TABLES
      *      RUN-SEGMENT-DETAIL AND ARRAY-PCB-SENSEG HAVE THE SAME
      *      INDEX, ARE PARALLEL MEMORY TABLES
             MOVE STE-SEGM-NAME(WS-INDEX-1) TO WS-SSA-SEGMENT-NAME
             MOVE ZERO TO STE-K(WS-INDEX-1)
             PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
             UNTIL WS-INDEX-2 > DT-PHYS-NUM-SENSEG
             OR STE-K(WS-INDEX-1) > 0
               IF PS-SEGMENT-NAME(WS-INDEX-2) = WS-SSA-SEGMENT-NAME
                 MOVE WS-INDEX-2 TO STE-K(WS-INDEX-1)
               END-IF
             END-PERFORM
      *
             IF STE-K(WS-INDEX-1) = ZERO
               MOVE ZERO TO IRIS-MSG-LEN
               STRING '<IRISTRACE> - '
               '(IRISPSSA) CANNOT RETRIEVE INTERNAL SENSEG POINTER'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRIS-MSG-LEVEL-ERROR TO TRUE
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               SET IRIS-ERR-WRONG-SENSEG-PTR TO TRUE
               GO TO GO-BACK
             END-IF
      *
             IF IRIS-TRACE-DEBUG
               MOVE ZERO TO IRIS-MSG-LEN
               MOVE WS-INDEX-1 TO WS-ZONED-NUM-4
               STRING '<IRISTRACE> - (IRISPSSA:PARSE-SSAS)' NL
                   ' POSITION   =(' WS-ZONED-NUM-4 ')' NL
                   ' SEGMENT    =(' STE-SEGM-NAME(WS-INDEX-1) ')' NL
                   ' COM CODE   =(' STE-COMMAND-CODES(WS-INDEX-1) ')'
               MESSAGE-END DELIMITED BY SIZE
               INTO IRIS-MSG-TXT
               SET IRIS-MSG-LEVEL-INFO TO TRUE
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               IF STE-QUAL(WS-INDEX-1) NOT = CP-PTR-NULL
                 MOVE ZERO TO IRIS-MSG-LEN
                 SET ADDRESS OF LK-QUALIFIED-AREA
                                             TO STE-QUAL(WS-INDEX-1)
                 STRING '<IRISTRACE> - (IRISPSSA:PARSE-SSAS) '
                 'SSA AREA=(' LK-QUALIFIED-AREA(1:20) ') '
                 DELIMITED BY SIZE MESSAGE-END DELIMITED BY SIZE
                 INTO IRIS-MSG-TXT
                 SET IRIS-MSG-LEVEL-INFO TO TRUE
                 SET IRISTRAC-RTN TO TRUE
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               END-IF
             END-IF
           END-PERFORM
      *
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSSA:PARSE-PHY-SSAS END) '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
      *
       PARSE-PHY-SSAS-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE TYPICAL CASES OF UNQUALIFIED SSA
      ******************************************************************
      *
       ONLY-ONE-UNQUALIFIED SECTION.
      *<FIXME> REVIEW LOGIC
      *    HANDLE ONLY ONE UNQUALIFIED SSA
           IF ST-COUNT = 1
           AND WS-SSA-SEGMENT-INDEX = DT-ROOT-SEGM
           AND STE-QUAL(ST-COUNT) = CP-PTR-NULL
             MOVE 0 TO WS-SSA-BUFFER-LEN
             MOVE DT-ROOT-SEGM TO STE-K(ST-COUNT)
             PERFORM TRACE-UNQUAL THRU TRACE-UNQUAL-EX
             GO TO GO-BACK
           END-IF
      *
      *    HANDLE ONLY ONE UNQUALIFIED SSA WITH PARENTAGE
           IF STE-QUAL(ST-COUNT) = CP-PTR-NULL
           AND PARENT-SEGMENT-INDEX > 0
      *    <FIXME> DOES THIS INDEX MAKE SENSE HERE? I GUESS NO
           AND RUN-PARENT-SEGMENT-INDEX(WS-SSA-SEGMENT-INDEX)
                                               = PARENT-SEGMENT-INDEX
             MOVE DS-SEGMENT-LEVEL(WS-SSA-SEGMENT-INDEX)
                                                TO WS-CURR-PHYSICAL-LVL
             MOVE DS-SEGMENT-NAME(WS-SSA-SEGMENT-INDEX)
                                                TO WS-CURR-PHYSICAL-NAME
             MOVE DS-SEGMENT-LEVEL(PARENT-SEGMENT-INDEX)
                                                TO WS-PARE-PHYSICAL-LVL
             MOVE DS-SEGMENT-NAME(PARENT-SEGMENT-INDEX)
                                                TO WS-PARE-PHYSICAL-NAME
             IF WS-CURR-PHYSICAL-LVL < WS-PARE-PHYSICAL-LVL
               MOVE ZERO TO IRIS-MSG-LEN
               STRING '<IRISTRACE> - (IRISPSSA)' NL
                      ' ERROR      =(UNSUPPORTED REVERSE ACCESS)' NL
                      ' LOGIC CHILD=(' WS-CURR-PHYSICAL-NAME ')' NL
                      ' LOGIC PAREN=(' WS-PARE-PHYSICAL-NAME ')'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRIS-MSG-LEVEL-ERROR TO TRUE
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               SET IRIS-ERR-SSA-UNHANDLED-ACCESS TO TRUE
             ELSE
               MOVE 0 TO WS-SSA-BUFFER-LEN
               PERFORM TRACE-UNQUAL THRU TRACE-UNQUAL-EX
               GO TO GO-BACK
             END-IF
           END-IF.
      *
       ONLY-ONE-UNQUALIFIED-EX.
           EXIT.
      *
      ******************************************************************
      * BUILD THE SQL QUERY
      ******************************************************************
      *
       BUILD-SQL-QUERY SECTION.
      *
           MOVE 1 TO WS-SSA-BUFFER-LEN
                     WS-SSA-KEY-LEN
           MOVE 0 TO WS-SAVE-BUFFER-LENGTH
      *
           EVALUATE TRUE
           WHEN IRIS-FUNC-GU
           WHEN IRIS-FUNC-GHU
           WHEN IRIS-FUNC-GN
           WHEN IRIS-FUNC-GHN
             STRING 'G:' DELIMITED BY SIZE
             INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
           WHEN IRIS-FUNC-GNP
           WHEN IRIS-FUNC-GHNP
             STRING 'GP:' DELIMITED BY SIZE
             INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
           WHEN IRIS-FUNC-ISRT
             STRING 'I:' DELIMITED BY SIZE
             INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
           WHEN IRIS-FUNC-DLET
             STRING 'D:' DELIMITED BY SIZE
             INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
           WHEN IRIS-FUNC-REPL
             STRING 'R:' DELIMITED BY SIZE
             INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
           WHEN OTHER
             STRING '?:' DELIMITED BY SIZE
             INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
           END-EVALUATE
      *
           SET LEVEL-ISNOT-PARENTAGE TO TRUE
           SET WS-CONDITION-HAS-NOT-GREATER TO TRUE
           MOVE 0 TO WS-SAVE-LAST-SSA-INDEX
      *
      *    PREPARE THE LEVELS CONDITIONS
      *
           PERFORM VARYING WS-SSA-IDX FROM 1 BY 1
           UNTIL WS-SSA-IDX > ST-COUNT
      *      FROM SECOND LAP
             IF WS-SSA-BUFFER-LEN > 1
               STRING ' AND ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               MOVE WS-SSA-BUFFER-LEN TO WS-SAVE-BUFFER-LENGTH
             END-IF
      *
IRISST       MOVE 1 TO WS-SSA-KEYVALUE-BOOL
IRISST       SET WS-IS-ACTUAL-SSA TO TRUE
      *      ANALYZE THE CURRENT LEVEL
             PERFORM ANALYZE-LEVEL THRU ANALYZE-LEVEL-EX

             IF WS-SSA-BUFFER-LEN = WS-SAVE-BUFFER-LENGTH
      *        NOTHING HAS BEEN ADDED SUBTRACT THE PART OF ' AND '
               SUBTRACT 5 FROM WS-SSA-BUFFER-LEN
             END-IF
      *
           END-PERFORM.
      *
       BUILD-SQL-QUERY-EX.
           EXIT.
      *
      ******************************************************************
      * VERIFY IF THERE ARE PARENTAGE CONTRAINTS
      ******************************************************************
      *
      *VERIFY-PARENTAGE SECTION.
DBG   *    DISPLAY '++ IRISPSSA, CHECK-PARENTAGE 1' UPON SYSERR
DBG   *    DISPLAY '++ IRISPSSA, WS-LEVEL-IS-PARENTAGE='
DBG   *                          WS-LEVEL-IS-PARENTAGE UPON SYSERR
DBG   *    DISPLAY '++ IRISPSSA, DT-ROOT-SEGM='
DBG   *                          DT-ROOT-SEGM UPON SYSERR
DBG   *    DISPLAY '++ IRISPSSA, DT-PHYS-ROOT-SEGM='
DBG   *                          DT-PHYS-ROOT-SEGM UPON SYSERR
      *    IF WS-LEVEL-IS-PARENTAGE
      *    AND DT-ROOT-SEGM NOT = DT-PHYS-ROOT-SEGM
DBG   *    DISPLAY '++ IRISPSSA, CHECK-PARENTAGE 2' UPON SYSERR
      *      MOVE DT-ROOT-SEGM TO WS-ROOT-SEGMENT-ZONED
      *      MOVE DT-PHYS-ROOT-SEGM TO WS-PHYS-ROOT-SEGMENT-ZONED
      *      IF IRIS-TRACE-DEBUG
      *         MOVE ZERO TO IRIS-MSG-LEN
      *         STRING
      *         '(IRISPSSA) PARENTAGE=(' WS-LEVEL-PARENTAGE-STATUS ')'NL
      *         '        ROOT=(' WS-ROOT-SEGMENT-ZONED ')' NL
      *         '        PHYS-ROOT=(' WS-PHYS-ROOT-SEGMENT-ZONED ')'
      *         MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
      *         SET IRIS-MSG-LEVEL-DEBUG TO TRUE
      *         SET IRISTRAC-RTN TO TRUE
      *         CALL IRIS-WS-RTN USING IRIS-WORK-AREA
      *      END-IF
      *      MOVE DT-PHYS-ROOT-SEGM TO WS-SAVE-SEGMENT-INDEX
      *      PERFORM WITH TEST BEFORE
      *      UNTIL WS-SAVE-SEGMENT-INDEX = DT-ROOT-SEGM
DBG   *    DISPLAY '++ IRISPSSA, CHECK-PARENTAGE 4' UPON SYSERR
      *        STRING ' AND ' DELIMITED BY SIZE
      *        INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *        MOVE DS-SEGMENT-NAME(WS-SAVE-SEGMENT-INDEX)
      *                                             TO WS-INDEX-PREFIX
      *        MOVE WS-SAVE-SEGMENT-INDEX TO WS-SAVE-LSOURCE-INDEX
      *        SET WS-HAS-LSOURCE TO TRUE
      *        PERFORM LAST-KEY-COND-EQUAL THRU LAST-KEY-COND-EQUAL-EX
      *        MOVE RUN-PARENT-SEGMENT-INDEX(WS-SAVE-SEGMENT-INDEX)
      *                                       TO WS-SAVE-SEGMENT-INDEX
      *      END-PERFORM
      *    END-IF.
      *VERIFY-PARENTAGE-EX.
      *    EXIT.
      *
      ******************************************************************
      * ANALYZE THE CURRENT LEVEL OF THE SSA
      ******************************************************************
      *
       ANALYZE-LEVEL SECTION.
      *
IRIS       SET IS-NOT-USING-INDEX-DBD TO TRUE
           SET IS-INDEX-COND-NOT-ADDED TO TRUE
           IF DT-PROC-SEQ NOT = SPACE
IRISST     AND DT-PROC-SEQ = IRIS-SSA-PROCSEQ
             SET IS-USING-INDEX-DBD TO TRUE
           END-IF
      *
           SET ADDRESS OF LK-COM-SSA TO STE-QUAL(WS-SSA-IDX)
           SET WS-QUALIFIED-SSA-PTR TO STE-QUAL(WS-SSA-IDX)
           SET SEGMENT-WITHOUT-C-CODE-UV TO TRUE
           IF STEC-U(WS-SSA-IDX) NOT = SPACE
           OR STEC-V(WS-SSA-IDX) NOT = SPACE
             SET SEGMENT-WITH-C-CODE-UV TO TRUE
           END-IF
           MOVE STE-SEGM-NAME(WS-SSA-IDX) TO WS-SEGMENT-NAME
           SET IRISGSEG-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                  MEMORY-PCB-AREA
                                  WS-SEGMENT-NAME
                                  WS-TABLE-INDEX
           IF NOT IRIS-NO-ERROR
             GO TO GO-BACK
           END-IF
      *
           STRING WS-SEGMENT-NAME DELIMITED BY SPACE
           ':' DELIMITED BY SIZE
           INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
           MOVE SPACE TO WS-LVL-CCODES
           MOVE 0 TO WS-INDEX-1
           IF STEC-C(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-C(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-D(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-D(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-F(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-F(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-L(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-L(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-M(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-M(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-N(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-N(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-P(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-P(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-Q(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-Q(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-R(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-R(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-S(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-S(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-U(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-U(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-V(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-V(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF STEC-W(WS-SSA-IDX) NOT = SPACE
             ADD 1 TO WS-INDEX-1
             MOVE STEC-W(WS-SSA-IDX) TO WS-LVL-CCODES(WS-INDEX-1:1)
           END-IF
           IF WS-LVL-CCODES NOT = SPACE
             STRING WS-LVL-CCODES DELIMITED BY SPACE
             ':' DELIMITED BY SIZE
             INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
           END-IF
      *
           MOVE DS-SOURCE-SEGMENT-NAME(WS-TABLE-INDEX)
                                                TO WS-SOURCE-SEGMENT
           IF WS-SOURCE-SEGMENT = SPACE
             MOVE WS-SEGMENT-NAME TO WS-SOURCE-SEGMENT
           END-IF
      *
           IF WS-SSA-IDX = ST-COUNT
             MOVE KEY-FIELD-PREFIX TO WS-INDEX-PREFIX
             MOVE SPACE TO WS-CURRENT-PREFIX
           ELSE
      *      MOVE WS-SOURCE-SEGMENT TO WS-INDEX-PREFIX
      *      STRING WS-SOURCE-SEGMENT DELIMITED BY SPACE
      *      '_' DELIMITED BY SIZE INTO WS-CURRENT-PREFIX
             STRING WS-SSA-SEGMENT-NAME DELIMITED BY SPACE
             '_' DELIMITED BY SIZE
             KEY-FIELD-PREFIX DELIMITED BY SPACE
             INTO WS-INDEX-PREFIX
             STRING WS-SSA-SEGMENT-NAME DELIMITED BY SPACE
             '_' DELIMITED BY SIZE INTO WS-CURRENT-PREFIX
           END-IF
      *
      *    IF WS-SSA-IDX = ST-COUNT
      *    OR WS-CONDITION-HAS-GREATER
      *      MOVE IMS-CONDITION-GREATER TO WS-SEGMENT-CONDITION
      *    ELSE
      *      MOVE IMS-CONDITION-GREATER-EQUAL TO WS-SEGMENT-CONDITION
      *    END-IF
      *
           IF LEVEL-IS-PARENTAGE
IRIS       OR (CMD-NEXT
IRIS           AND RUN-USED-KEY-NOT-CHANGED(WS-SSA-IDX)
IRIS           AND WS-SSA-IDX < ST-COUNT)
IRIS       OR ((IRIS-FUNC-GNP OR IRIS-FUNC-GHNP)
IRIS           AND WS-SSA-IDX < ST-COUNT)
DBG   *      DISPLAY 'IRISPSSA, STEP LEVEL-IS-PARENTAGE'
      *      IF THE LEVEL HAS A PARENTAGE TAKE THE KEY ALREADY DONE
             PERFORM LAST-KEY-COND-EQUAL THRU LAST-KEY-COND-EQUAL-EX
           ELSE
             IF WS-QUALIFIED-SSA-PTR NOT = CP-PTR-NULL
DBG   *      DISPLAY 'IRISPSSA, STEP QUALIFIED'
               EVALUATE LK-COM-SSA(1:1)
               WHEN '('
DBG   *          DISPLAY 'IRISPSSA, STEP QUALIFIED ('
      *          HANDLE THE QUALIFIED SSA
                 PERFORM QUALIFIED THRU QUALIFIED-EX
               END-EVALUATE
             ELSE
DBG   *        DISPLAY 'IRISPSSA, STEP UNQUALIFIED'
      *        HANDLE THE UNQUALIFIED SSA
      *        IT IS USING A SECONDARY KEY ALL THE UNQUALIFIED SEGMENTS
      *        ARE SKIPPED OTHERWISE KEYS CAN JUMP
               IF IS-READING-WITH-XDFLD
DBG   *          DISPLAY 'IRISPSSA, STEP READ XFLD'
                 PERFORM TRACE-UNQ-SKIPPED THRU TRACE-UNQ-SKIPPED-EX
               ELSE
DBG   *          DISPLAY 'IRISPSSA, STEP READ UNQUAL'
SANTIX*  When a command code V has been set at level 1 and the segment
SANTIX*  accessed is deeper than its child, we don't behave correctly
SANITX*  it is test case 5 of the regression test,
      *  the pattern has to be changed
SANITX*  with a GU with parentage and GN without any SSA
SANTIX*          IF SEGMENT-WITH-C-CODE-UV
SANTIX*          AND RUN-USED-KEY-CHANGED(ST-COUNT)
SANTIX*            PERFORM TRACE-UNQUAL-SSA-SKIPPED
SANTIX*          ELSE
                   PERFORM UNQUAL THRU UNQUAL-EX
SANTIX*          END-IF
               END-IF
             END-IF
           END-IF.
      *
       ANALYZE-LEVEL-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE A QUALIFIED SSA
      ******************************************************************
      *
       QUALIFIED SECTION.
           MOVE LK-COM-SSA(WS-SSA-OFFSET + 2:8) TO WS-FIELD-NAME
           MOVE LK-COM-SSA(WS-SSA-OFFSET + 10:2) TO WS-CONDITION
           MOVE WS-CONDITION TO IRIS-IMS-CONDITION
           EVALUATE TRUE
           WHEN IRIS-COND-GREATER
             MOVE IRIS-GT2 TO WS-CONDITION
           WHEN IRIS-COND-GREATER-EQUAL
             MOVE IRIS-GE2 TO WS-CONDITION
           WHEN IRIS-COND-EQUAL
             MOVE IRIS-EQ2 TO WS-CONDITION
           WHEN IRIS-COND-NOT-EQUAL
             MOVE IRIS-NE4 TO WS-CONDITION
           WHEN IRIS-COND-LESS
             MOVE IRIS-LT2 TO WS-CONDITION
           WHEN IRIS-COND-LESS-EQUAL
             MOVE IRIS-LE2 TO WS-CONDITION
           END-EVALUATE
      *
           STRING WS-FIELD-NAME DELIMITED BY SPACE
           ':' DELIMITED BY SIZE
           WS-CONDITION DELIMITED BY SPACE
           ':' DELIMITED BY SIZE
           INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
      *
      *    RETRIEVE THE FIELD INFORMATION
           SET IRISGFLD-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                  MEMORY-PCB-AREA
                                  WS-SEGMENT-NAME
                                  WS-FIELD-NAME
                                  WS-FIELD-START
                                  WS-FIELD-BYTES
                                  WS-FIELD-TYPE
                                  WS-FIELD-BRIDGE-TYPE
                                  WS-FIELD-IORTN
                                  WS-FIELD-DECIMALS
IRIS  *                           WS-FIELD-EXTRA-INFO
IRIS  *   IF FIELD-REDEFINED-CHAR
IRIS  *      SET WS-FIELD-TYPE-ALPHA TO TRUE
DBG   *      DISPLAY 'DBG>>SSA, FIELD=' WS-FIELD-NAME
DBG   *      ', FORCING CHAR REDEFINE!!!' UPON SYSERR
IRIS  *    END-IF
DBG   *    IF WS-FIELD-NAME = 'CPCXKEY'
DBG   *      DISPLAY 'SSA, FORCING CPCXKEY TO XDFLD' UPON SYSERR
DBG   *      SET WS-FIELD-TYPE-XDFLD TO TRUE
DBG   *    END-IF
           IF IRIS-TRACE-DEBUG
             MOVE WS-FIELD-TYPE TO WS-FIELD-TYPE-ZONED
             MOVE WS-FIELD-START TO WS-FIELD-START-ZONED
             MOVE WS-FIELD-BYTES TO WS-FIELD-BYTES-ZONED
             MOVE WS-FIELD-DECIMALS TO WS-FIELD-DECIMALS-ZONED
             MOVE WS-FIELD-BRIDGE-TYPE TO WS-FIELD-BRIDGE-TYPE-ZONED
             IF WS-FIELD-IORTN-LEN > ZERO
               MOVE WS-FIELD-IORTN-TXT(1:WS-FIELD-IORTN-LEN)
                                             TO WS-FIELD-IORTN-ZONED
             ELSE
               MOVE ALL '-' TO WS-FIELD-IORTN-ZONED
             END-IF
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSSA) -- FIELD INFO --' NL
                    ' NAME       =(' WS-FIELD-NAME ')' NL
                    ' START      =(' WS-FIELD-START-ZONED ')' NL
                    ' BYTES      =(' WS-FIELD-BYTES-ZONED ')' NL
                    ' DECIMALS   =(' WS-FIELD-DECIMALS-ZONED ')' NL
                    ' TYPE       =(' WS-FIELD-TYPE-ZONED ')' NL
                    ' IO RTN TYPE=(' WS-FIELD-BRIDGE-TYPE-ZONED ')' NL
                    ' IO RTN NAME =(' WS-FIELD-IORTN-ZONED ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF NOT IRIS-NO-ERROR
             GO TO GO-BACK
           END-IF
      *
           COMPUTE WS-I1 = WS-SSA-OFFSET + WS-FIELD-BYTES + SSA-FIX-L
DBG   *    DISPLAY '##WS-I1= ' WS-I1 UPON SYSERR
DBG   *    DISPLAY '##LK-COM-SSA(WS-I1:1)= ' LK-COM-SSA(WS-I1:1)
           IF LK-COM-SSA(WS-I1:1) NOT = ')'
             SET WS-SSA-IS-BOOLEAN TO TRUE
             COMPUTE WS-I1 = WS-SSA-OFFSET + WS-FIELD-BYTES + SSA-FIX-L
             MOVE LK-COM-SSA(WS-I1:1) TO WS-BOOLEAN-SSA-OPERATOR
           ELSE
              SET WS-SSA-OPERATOR-PARENTHESIS TO TRUE
           END-IF
      *
           PERFORM TRACE-QUAL THRU TRACE-QUAL-EX
      *
           SET WS-CONDITION-NOT-SET TO TRUE
           IF WS-FIELD-TYPE-XDFLD
           OR WS-FIELD-TYPE-INDEX
DBG   *      DISPLAY 'XDFLD' UPON SYSERR
             SET IS-READING-WITH-XDFLD TO TRUE
             PERFORM XDFLD THRU XDFLD-EX
           ELSE
             IF WS-SSA-IDX = ST-COUNT
DBG   *        DISPLAY 'LAST-QUAL' UPON SYSERR
               PERFORM LAST-QUAL THRU LAST-QUAL-EX
IRIS           IF IS-LAST-SSA-KEY
IRIS           AND RUN-USED-KEY-NOT-CHANGED(WS-SSA-IDX)
IRIS             IF NOT WS-FIELD-TYPE-XDFLD
                 AND (IS-NOT-USING-INDEX-DBD
                 OR (IS-USING-INDEX-DBD AND NOT IS-INDEX-COND-ADDED))
IRIS               STRING ' AND ' DELIMITED BY SIZE
IRIS               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRIS               PERFORM LAST-KEY THRU LAST-KEY-EX
IRIS             END-IF
IRIS           END-IF
             ELSE
DBG   *        DISPLAY 'QUAL' UPON SYSERR
               PERFORM QUAL THRU QUAL-EX
      * Bug, if it is an intermediate level with a qualified
      * value, setting WS-CONDITION-SET TO TRUE prevents to add
      * again a condition for '=' to the latest read
      *        SET WS-CONDITION-SET TO TRUE
             END-IF
           END-IF
      *
           IF IS-LAST-SSA-KEY
           AND RUN-USED-KEY-CHANGED(WS-SSA-IDX)
           AND WS-CONDITION-NOT-SET
DBG   *      DISPLAY 'QUALIFIED 1' UPON SYSERR
             IF NOT WS-FIELD-TYPE-XDFLD
DBG   *      DISPLAY 'QUALIFIED 2' UPON SYSERR
               STRING ' AND ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               PERFORM LAST-KEY THRU LAST-KEY-EX
             END-IF
           ELSE
             IF (RUN-SEQUENCE-MULTI(WS-SSA-IDX)
                 OR RUN-SEQUENCE-NONE(WS-SSA-IDX))
             AND DS-RULE-POSITION-FIRST(WS-SSA-IDX)
             AND WS-SSA-IDX = ST-COUNT
             AND RUN-SEQUENCE-NAME(WS-SSA-IDX) =
                                                WS-FIELD-NAME
               STRING ' AND ' DELIMITED BY SIZE
               WS-INDEX-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
               '_PREV_KEY = 0' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             END-IF
           END-IF
      *
           IF WS-SSA-IS-BOOLEAN
             IF WS-SSA-OPERATOR-PARENTHESIS
IRISST         IF WS-NOT-FIRST-STEP-MULTI
IRISST           STRING ')))' DELIMITED BY SIZE
IRISST           INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST         ELSE
                 STRING '))' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               END-IF
               MOVE ZERO TO WS-SSA-OFFSET
               SET WS-SSA-IS-NOT-BOOLEAN TO TRUE
             ELSE
               IF WS-SSA-OPERATOR-AND
      *
                 STRING 'AND:' DELIMITED BY SIZE
                 INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
      *
IRISST           IF WS-FIRST-STEP-MULTI
                   STRING ') AND (' DELIMITED BY SIZE
                   INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST           ELSE
IRISST             STRING ' AND ' DELIMITED BY SIZE
IRISST             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST           END-IF
               ELSE
                 IF WS-SSA-OPERATOR-OR
      *
                   STRING 'OR:' DELIMITED BY SIZE
                   INTO WS-SSA-KEY-TXT POINTER WS-SSA-KEY-LEN
      *
IRISST             IF WS-FIRST-STEP-MULTI
                     STRING ') OR (' DELIMITED BY SIZE
                     INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST             ELSE
IRISST              STRING ' OR ' DELIMITED BY SIZE
IRISST              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST             END-IF
                 ELSE
                   PERFORM SEND-BOOLEAN-SSA-MISMATCH
                 END-IF
               END-IF
IRISST         ADD 1 TO WS-SSA-KEYVALUE-BOOL
               COMPUTE WS-SSA-OFFSET = WS-SSA-OFFSET + 11
                                                 + WS-FIELD-BYTES
               GO TO QUALIFIED
             END-IF
           END-IF.
       QUALIFIED-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE THE XDFLD SSA
      ******************************************************************
      *
       XDFLD SECTION.
      *
           MOVE SPACE TO WS-XDFLD-NAME
      *
      *    IT IS ACCESSING THE SEGMENT INDEX USING THE DBD INDEX
      *    DIRECTLY, NO PROCOPT, THE REAL NAME HAS BEEN CONCATENATED
      *    DURING IN THE PSB ALLOCATION
           IF DT-PROC-SEQ = SPACE
             MOVE WS-FIELD-IORTN-TXT(1:WS-FIELD-IORTN-LEN)
                                               TO WS-XDFLD-NAME
           ELSE
             STRING DT-PROC-SEQ DELIMITED BY SPACE
             '_' DELIMITED BY SIZE
             WS-FIELD-NAME DELIMITED BY SPACE
             INTO WS-XDFLD-NAME
           END-IF
      *
           IF WS-SSA-IS-BOOLEAN AND WS-SSA-OFFSET = ZERO
              STRING '((' DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           END-IF
      *
IRIS  *    IF WS-SAVE-SEGMENT-INDEX = WS-SSA-SEGMENT-INDEX
           IF RUN-PHYSICAL-INDEX(WS-SSA-IDX) = WS-SSA-SEGMENT-INDEX
             MOVE WS-CONDITION TO IRIS-IMS-CONDITION
             IF IRIS-COND-EQUAL
               STRING WS-XDFLD-NAME DELIMITED BY SPACE
               ' LIKE ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
      *        ANALYZE THE FIELD TYPE
               PERFORM ANALYZE-FIELD-TYPE-LIKE
             ELSE
               STRING WS-XDFLD-NAME DELIMITED BY SPACE
               ' ' WS-CONDITION ' ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               COMPUTE WS-I1 = WS-SSA-OFFSET + SSA-FIX-L
               MOVE LK-COM-SSA(WS-I1:WS-FIELD-BYTES)
                              TO WS-SSA-VALUE-TEXT(1:WS-FIELD-BYTES)
               COMPUTE WS-I1 = WS-FIELD-BYTES + 1
               MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
               IF WS-FIELD-START > 1
                 COMPUTE WS-SSA-VALUE-LENGTH = WS-SSA-VALUE-LENGTH +
                                               WS-FIELD-START - 1
                 IF IRIS-COND-GREATER-EQUAL
                   MOVE LOW-VALUE
                         TO WS-SSA-VALUE-TEXT(WS-I1:WS-FIELD-START - 1)
                 ELSE
                   MOVE HIGH-VALUE
                         TO WS-SSA-VALUE-TEXT(WS-I1:WS-FIELD-START - 1)
                 END-IF
               END-IF
               PERFORM ANALYZE-SSA-VALUE
               SET WS-CONDITION-SET TO TRUE
             END-IF
           ELSE
             IF RUN-SEQUENCE-UNIQUE(WS-SSA-IDX)
IRIS         OR RUN-SEQUENCE-MULTI(WS-SSA-IDX)
               IF RUN-PHYSICAL-INDEX(WS-SSA-IDX) NOT = DT-PHYS-ROOT-SEGM
                 STRING '(' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               END-IF
      *
               STRING WS-INDEX-PREFIX DELIMITED BY SPACE
               '_' DELIMITED BY SIZE
               RUN-SEQUENCE-NAME(WS-SSA-IDX)
               DELIMITED BY SPACE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               IF RUN-PHYSICAL-INDEX(WS-SSA-IDX) NOT = DT-PHYS-ROOT-SEGM
                 MOVE RUN-PHYSICAL-PARENT-INDEX(WS-SSA-IDX)
                                                      TO WS-INDEX-2
                 PERFORM UNTIL WS-INDEX-2 = 0
                   IF RUN-SEQUENCE-UNIQUE(WS-INDEX-2)
                   OR RUN-SEQUENCE-MULTI(WS-INDEX-2)
                     STRING ', ' DELIMITED BY SIZE
                     DS-SEGMENT-NAME(WS-INDEX-2) DELIMITED BY SPACE
                     '_' DELIMITED BY SIZE
                     RUN-SEQUENCE-NAME(WS-INDEX-2) DELIMITED BY SPACE
                     INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
                   END-IF
                   MOVE RUN-PHYSICAL-PARENT-INDEX(WS-INDEX-2)
                                                      TO WS-INDEX-2
                 END-PERFORM
               END-IF
      *
               IF RUN-PHYSICAL-INDEX(WS-SSA-IDX) NOT = DT-PHYS-ROOT-SEGM
                 STRING ')' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               END-IF
      *
               STRING ' = (SELECT ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               STRING KEY-FIELD-PREFIX '_' DELIMITED BY SIZE
               RUN-SEQUENCE-NAME(WS-SSA-IDX)
               DELIMITED BY SPACE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               IF WS-SSA-IDX NOT = DT-PHYS-ROOT-SEGM
                 MOVE RUN-PHYSICAL-PARENT-INDEX(WS-SSA-IDX)
                                                       TO WS-INDEX-2
                 PERFORM UNTIL WS-INDEX-2 = 0
                   IF RUN-SEQUENCE-UNIQUE(WS-INDEX-2)
                   OR RUN-SEQUENCE-MULTI(WS-INDEX-2)
                     STRING ', ' DELIMITED BY SIZE
                     DS-SEGMENT-NAME(WS-INDEX-2) DELIMITED BY SPACE
                     '_' DELIMITED BY SIZE
                     RUN-SEQUENCE-NAME(WS-INDEX-2) DELIMITED BY SPACE
                     INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
                   END-IF
                   MOVE RUN-PHYSICAL-PARENT-INDEX(WS-INDEX-2)
                                                        TO WS-INDEX-2
                 END-PERFORM
               END-IF
      *
               STRING ' FROM ' DELIMITED BY SIZE
      *        DT-DBD-NAME DELIMITED BY SPACE
      *        '_' DELIMITED BY SIZE
               WS-SOURCE-SEGMENT(1:6) 'VW' DELIMITED BY SIZE
               ' WHERE ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               STRING WS-XDFLD-NAME DELIMITED BY SPACE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               STRING ' = (SELECT MIN (' DELIMITED BY SIZE
               WS-XDFLD-NAME DELIMITED BY SPACE
               ')' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               STRING ' FROM ' DELIMITED BY SIZE
      *        DT-DBD-NAME DELIMITED BY SPACE
      *        '_' DELIMITED BY SIZE
               WS-SOURCE-SEGMENT(1:6) 'VW' DELIMITED BY SIZE
               ' WHERE ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN

               MOVE WS-CONDITION TO IRIS-IMS-CONDITION
               IF IRIS-COND-EQUAL
                 STRING WS-XDFLD-NAME DELIMITED BY SPACE
                 ' LIKE ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
      *          ANALYZE THE FIELD TYPE
                 PERFORM ANALYZE-FIELD-TYPE-LIKE
               ELSE
                 STRING WS-XDFLD-NAME DELIMITED BY SPACE
                 ' ' WS-CONDITION ' ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
                 COMPUTE WS-I1 = WS-SSA-OFFSET + SSA-FIX-L
                 MOVE LK-COM-SSA(WS-I1:WS-FIELD-BYTES)
                                TO WS-SSA-VALUE-TEXT (1:WS-FIELD-BYTES)
                 COMPUTE WS-I1 = WS-FIELD-BYTES + 1
                 MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
                 IF WS-FIELD-START > 1
                   COMPUTE WS-SSA-VALUE-LENGTH = WS-SSA-VALUE-LENGTH +
                                               WS-FIELD-START - 1
                   IF IRIS-COND-GREATER-EQUAL
                     MOVE LOW-VALUE
                          TO WS-SSA-VALUE-TEXT(WS-I1:WS-FIELD-START - 1)
                   ELSE
                     MOVE HIGH-VALUE
                          TO WS-SSA-VALUE-TEXT(WS-I1:WS-FIELD-START - 1)
                   END-IF
                 END-IF
                 PERFORM ANALYZE-SSA-VALUE
                 SET WS-CONDITION-SET TO TRUE
               END-IF
               STRING '))' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             END-IF
           END-IF
      *
           IF IS-LAST-SSA-KEY
           AND RUN-USED-KEY-CHANGED(WS-SSA-IDX)
             STRING ' AND ' DELIMITED BY SIZE
             WS-XDFLD-NAME DELIMITED BY SPACE
             ' ' WS-SEGMENT-CONDITION ' ' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             COMPUTE WS-O1 = WS-FIELD-BYTES + WS-FIELD-START - 1
             MOVE DT-LAST-KEY-SEC(1:WS-O1) TO WS-SSA-VALUE-TEXT
             MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
             ADD WS-FIELD-START TO WS-SSA-VALUE-LENGTH
IRISST       SET WS-IS-NOT-SSA TO TRUE
             PERFORM ANALYZE-SSA-VALUE
             SET WS-CONDITION-SET TO TRUE
           END-IF
      *
           IF IS-LAST-SSA-KEY
           AND WS-SSA-IDX = ST-COUNT
           AND RUN-USED-KEY-CHANGED(WS-SSA-IDX)
           AND WS-CONDITION-NOT-SET
             STRING ' AND ' DELIMITED BY SIZE
             WS-XDFLD-NAME DELIMITED BY SPACE
             ' ' WS-SEGMENT-CONDITION ' ' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
             MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
             IF WS-FIELD-START > 1
               COMPUTE WS-SSA-VALUE-LENGTH = WS-FIELD-BYTES
                                           + WS-FIELD-START - 1
             END-IF
             MOVE DT-LAST-KEY-SEC(1:WS-SSA-VALUE-LENGTH)
                                           TO WS-SSA-VALUE-TEXT
             PERFORM ANALYZE-SSA-VALUE
           END-IF.
      *
IRISST     IF IS-LAST-SSA-KEY
IRISST     AND RUN-USED-KEY-NOT-CHANGED(WS-SSA-IDX)
IRISST     AND (IRIS-FUNC-GN OR IRIS-FUNC-GHN
IRISST      OR IRIS-FUNC-GNP OR IRIS-FUNC-GHNP)
IRISST        COMPUTE WS-COM-FIELD-BYTES =
IRISST              WS-FIELD-BYTES + 10
IRISST       STRING ' AND ' DELIMITED BY SIZE
IRISST       WS-XDFLD-NAME DELIMITED BY SPACE
IRISST       ' > ''' RUN-USED-KEY-SECONDARY(1:WS-COM-FIELD-BYTES)
IRISST       '''' DELIMITED BY SIZE
IRISST       INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST     END-IF.
      *
       XDFLD-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE THE UNQUALIFIED SSA
      ******************************************************************
      *
       UNQUAL SECTION.
      *
           PERFORM TRACE-UNQUAL THRU TRACE-UNQUAL-EX
      *
           IF IS-LAST-SSA-KEY
           AND RUN-USED-KEY-CHANGED(WS-SSA-IDX)
             PERFORM LAST-KEY THRU LAST-KEY-EX
           ELSE
             IF (RUN-SEQUENCE-MULTI(WS-SSA-IDX)
                 OR RUN-SEQUENCE-NONE(WS-SSA-IDX))
             AND DS-RULE-POSITION-FIRST(WS-SSA-IDX)
             AND WS-SSA-IDX = ST-COUNT
               STRING
               WS-INDEX-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
               '_PREV_KEY = 0' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             END-IF
           END-IF.
       UNQUAL-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE THE LAST QUALIFIED SSA
      ******************************************************************
      *
       LAST-QUAL SECTION.
      *
           IF WS-SSA-IS-BOOLEAN AND WS-SSA-OFFSET = ZERO
              STRING '((' DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           END-IF
      *
      *    CHECK IF THE BRIDGE NAME HAS BEEN STORED
           IF WS-FIELD-IORTN-LEN > 0
DBG   *      DISPLAY 'LAST-QUAL POS 1'
             STRING
             WS-FIELD-IORTN-TXT(1:WS-FIELD-IORTN-LEN)
             DELIMITED BY SIZE ' ' WS-CONDITION ' ' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
      *      ANALYZE THE FIELD TYPE
             PERFORM ANALYZE-FIELD-TYPE
      *
      *      IF THE FIELD HAS NO INDEX ON THE DATABASE SEND A WARNING
      *      MESSAGE
             MOVE WS-FIELD-BRIDGE-TYPE TO BRIDGES-INTERNAL-TYPES
             IF FLD-TYPE-STANDARD
               PERFORM SEND-MISSING-INDEX-WARNING
             END-IF
           ELSE
             IF NOT WS-FIELD-TYPE-INDEX
DBG   *        DISPLAY 'LAST-QUAL POS 2'
               STRING WS-INDEX-PREFIX DELIMITED BY SPACE
               '_' DELIMITED BY SIZE
               WS-FIELD-NAME DELIMITED BY SPACE
               ' ' WS-CONDITION ' ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
      *        ANALYZE THE FIELD TYPE
               PERFORM ANALYZE-FIELD-TYPE
             ELSE
               STRING '_________________ ' DELIMITED BY SIZE
               WS-CONDITION ' ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               SET WS-FIELD-TYPE-ALPHA TO TRUE
      *
      *        ANALYZE THE FIELD TYPE
               PERFORM ANALYZE-FIELD-TYPE
             END-IF
           END-IF.
      *
       LAST-QUAL-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE THE NOT LAST QUALIFIED SSA
      ******************************************************************
      *
       QUAL SECTION.
      *
           IF WS-SSA-IS-BOOLEAN AND WS-SSA-OFFSET = ZERO
             STRING '((' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           END-IF
      *
           MOVE WS-FIELD-BRIDGE-TYPE TO BRIDGES-INTERNAL-TYPES
      *    It is trying to use a field of an upper level which
      *    is not in key, i.e. case P1FWWB:SHIPROOT using WBDTE
      *    IF FLD-TYPE-STANDARD
      *
      *      IF IRIS-TRACE-DEBUG
      *        MOVE ZERO TO IRIS-MSG-LEN
      *        STRING
      *        '(IRISPSSA) DBD     =(' DT-DBD-NAME ')' NL
      *        '           SEGMENT =(' WS-SOURCE-SEGMENT ') ' NL
      *        '           FIELD   =(' WS-FIELD-NAME ') ' NL
      *        '           ERROR   =(UNSUPPORTED ACCESS OF A CHILD '
      *        'USING PARENT FIELDS NOT PART OF ANY KEY) ' NL
      *        MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
      *        SET IRIS-MSG-LEVEL-ERROR TO TRUE
      *        CALL UITRACE USING IRIS-WORK-AREA
      *        SET IRISTRAC-RTN TO TRUE
      *        CALL IRIS-WS-RTN USING IRIS-WORK-AREA
      *      END-IF
      *
      *      SET IRIS-ERR-SSA-UNHANDLED-ACCESS TO TRUE
      *
      *      GO TO GO-BACK
      *    END-IF
      *
IRISST*  Moved below as not to do in specific cases, for now managed
IRISST*  only one
IRISST*    STRING WS-INDEX-PREFIX DELIMITED BY SPACE
IRISST*    '_' DELIMITED BY SIZE
IRISST*    INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
           EVALUATE TRUE
           WHEN RUN-SEQUENCE-UNIQUE(WS-SSA-IDX)
IRISST       STRING WS-INDEX-PREFIX DELIMITED BY SPACE
IRISST       '_' DELIMITED BY SIZE
IRISST       INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             IF RUN-SEQUENCE-NAME(WS-SSA-IDX) = WS-FIELD-NAME
               STRING WS-FIELD-NAME DELIMITED BY SPACE
               ' ' WS-CONDITION ' ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
      *        ANALYZE THE FIELD TYPE
               PERFORM ANALYZE-FIELD-TYPE
             ELSE
               STRING RUN-SEQUENCE-NAME(WS-SSA-IDX)
               DELIMITED BY SPACE
               ' = ANY (SELECT ' KEY-FIELD-PREFIX '_' DELIMITED BY SIZE
               RUN-SEQUENCE-NAME(WS-SSA-IDX)
               DELIMITED BY SPACE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
      *    CHECK IF THE BRIDGE NAME HAS BEEN STORED
               IF WS-FIELD-IORTN-LEN > 0
                 STRING ' FROM ' DELIMITED BY SIZE
      *          DT-DBD-NAME DELIMITED BY SPACE
      *          '_' DELIMITED BY SIZE
                 WS-SOURCE-SEGMENT(1:6) 'VW' DELIMITED BY SIZE
                 ' WHERE ' DELIMITED BY SIZE
                 WS-FIELD-IORTN-TXT(1:WS-FIELD-IORTN-LEN)
                 DELIMITED BY SIZE
                 ' ' WS-CONDITION ' ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               ELSE
                 STRING ' FROM ' DELIMITED BY SIZE
      *          DT-DBD-NAME DELIMITED BY SPACE
      *          '_' DELIMITED BY SIZE
                 WS-SOURCE-SEGMENT(1:6) 'VW' DELIMITED BY SIZE
                 ' WHERE ' DELIMITED BY SIZE
                 KEY-FIELD-PREFIX DELIMITED BY SIZE
                 '_' DELIMITED BY SIZE
                 WS-FIELD-NAME DELIMITED BY SPACE
                 ' ' WS-CONDITION ' ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               END-IF
      *
      *        ANALYZE THE FIELD TYPE
               PERFORM ANALYZE-FIELD-TYPE
               STRING ')' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             END-IF
      *
           WHEN RUN-SEQUENCE-MULTI(WS-SSA-IDX)
IRISST       IF WS-SSA-IDX = ST-COUNT
IRISST       OR WS-FIRST-STEP-MULTI
IRISST         STRING WS-INDEX-PREFIX DELIMITED BY SPACE
IRISST         '_' DELIMITED BY SIZE
IRISST         INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST       ELSE
IRISST         STRING KEY-FIELD-PREFIX DELIMITED BY SPACE
IRISST         '_' DELIMITED BY SIZE
IRISST         INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST       END-IF
             STRING WS-FIELD-NAME DELIMITED BY SPACE
             ' ' WS-CONDITION ' ' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
      *      ANALYZE THE FIELD TYPE
             PERFORM ANALYZE-FIELD-TYPE
      *
IRISST       IF WS-SSA-IDX = ST-COUNT
IRISST       OR WS-FIRST-STEP-MULTI
IRISST         SET WS-NOT-FIRST-STEP-MULTI TO TRUE
      * Bug, first positioning on a child segment of a parent
      * having MULTIPLE key, was doing a simple (and wrong)
      * PARENT_AUTO_KEY = 0
               IF NOT RUN-USED-KEY-CHANGED(ST-COUNT)
               OR CMD-NOT-NEXT
                 STRING ' AND ' DELIMITED BY SIZE
IRISST*          WS-INDEX-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
IRISST           WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
                 '_AUTO_KEY IN '
                 ' (SELECT ' DELIMITED BY SIZE
IRISST*          WS-INDEX-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
IRISST           WS-SOURCE-SEGMENT DELIMITED BY SPACE
                 '_AUTO_KEY FROM ' DELIMITED BY SIZE
      *          DT-DBD-NAME DELIMITED BY SPACE
      *          '_' DELIMITED BY SIZE
                 WS-SOURCE-SEGMENT(1:6) 'VW' DELIMITED BY SIZE
                 ' WHERE ' DELIMITED BY SIZE
                 KEY-FIELD-PREFIX DELIMITED BY SPACE
                 '_' DELIMITED BY SIZE
                 WS-FIELD-NAME DELIMITED BY SPACE
                  ' = ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *          ANALYZE THE FIELD TYPE
                 PERFORM ANALYZE-FIELD-TYPE
IRISST           IF WS-SSA-IDX = ST-COUNT
                   STRING ' AND ' DELIMITED BY SIZE
IRISST*            WS-INDEX-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
IRISST             WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY
                   SPACE
                   '_PREV_KEY = 0)' DELIMITED BY SIZE
                   INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST           END-IF
               ELSE
                 MOVE RUN-USED-KEY-NUMERIC(WS-SSA-IDX)
                                               TO WS-EDITED-MASK
                 STRING ' AND ' DELIMITED BY SIZE
IRISST*          WS-INDEX-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
IRISST           WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
                 '_' WS-SOURCE-SEGMENT
                 '_AUTO_KEY ' WS-CONDITION ' '
                 WS-EDITED-MASK DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               END-IF
IRISST       ELSE
IRISST         IF WS-SSA-IDX = ST-COUNT
IRISST           STRING ')' DELIMITED BY SIZE
IRISST           INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
IRISST         END-IF
IRISST       END-IF
      *
           WHEN RUN-SEQUENCE-NONE(WS-SSA-IDX)
IRISST       STRING WS-INDEX-PREFIX DELIMITED BY SPACE
IRISST       '_' DELIMITED BY SIZE
IRISST       INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             STRING WS-SOURCE-SEGMENT '_'
             'AUTO_KEY' DELIMITED BY SIZE
             ' = ANY (SELECT ' WS-SOURCE-SEGMENT '_'
             'AUTO_KEY' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
             STRING ' FROM ' DELIMITED BY SIZE
      *      DT-DBD-NAME DELIMITED BY SPACE
      *      '_' DELIMITED BY SIZE
             WS-SOURCE-SEGMENT(1:6) 'VW' DELIMITED BY SIZE
             ' WHERE ' DELIMITED BY SIZE
      *      SS: HANDLED CASE WHERE THE FIELD IS NOT INDEXED
      *      KEY-FIELD-PREFIX DELIMITED BY SIZE
      *      '_' DELIMITED BY SIZE
      *      WS-FIELD-NAME DELIMITED BY SPACE
      *      ' ' WS-CONDITION ' ' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
      *      SS: HANDLED CASE WHERE THE FIELD IS NOT INDEXED
      *      CHECK IF THE BRIDGE NAME HAS BEEN STORED
             IF WS-FIELD-IORTN-LEN > 0
               STRING
               WS-FIELD-IORTN-TXT(1:WS-FIELD-IORTN-LEN)
               ' ' WS-CONDITION ' ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             ELSE
               STRING
               KEY-FIELD-PREFIX '_' DELIMITED BY SIZE
               WS-FIELD-NAME DELIMITED BY SPACE
               ' ' WS-CONDITION ' ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             END-IF
      *
      *      ANALYZE THE FIELD TYPE
             PERFORM ANALYZE-FIELD-TYPE
      *
             STRING ')' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
             IF FLD-TYPE-STANDARD
               PERFORM SEND-MISSING-INDEX-WARNING
             END-IF
           END-EVALUATE.
      *
       QUAL-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE THE LAST SSA WITH KEYS
      ******************************************************************
      *
       LAST-KEY SECTION.
      *
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSSA)' NL
                    ' PARENTAGE  =(' WS-LEVEL-PARENTAGE-STATUS ')' NL
                    ' PREFIX     =(' WS-INDEX-PREFIX ')' NL
                    ' CONDITION  =(' WS-SEGMENT-CONDITION ')' NL
                    ' SIGN STATUS=(' WS-CONDITION-SIGN-STATUS ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF WS-SSA-IDX = ST-COUNT
           OR WS-CONDITION-HAS-GREATER
             SET WS-CONDITION-HAS-NOT-GREATER TO TRUE
             PERFORM HANDLE-LAST-KEY-COND-GT
           ELSE
             IF WS-LAST-INDEX-IS-VALID
               PERFORM LAST-KEY-COND-EQUAL THRU LAST-KEY-COND-EQUAL-EX
             ELSE
               IF WS-SSA-BUFFER-LEN > 5
               AND WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN - 5:5) = ' AND '
                 SUBTRACT 5 FROM WS-SSA-BUFFER-LEN
               END-IF
             END-IF
           END-IF.
      *
       LAST-KEY-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE THE LAST SSA WITH KEYS AND CONDITION EQUAL
      ******************************************************************
      *
       LAST-KEY-COND-EQUAL SECTION.
      *
           EVALUATE TRUE
      *
           WHEN RUN-SEQUENCE-UNIQUE(WS-SSA-IDX)
             STRING WS-INDEX-PREFIX DELIMITED BY SPACE
             '_' DELIMITED BY SIZE
             RUN-SEQUENCE-NAME(WS-SSA-IDX) DELIMITED BY SPACE
             ' = ' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
             PERFORM ANALYZE-KEY-TYPE
      *
           WHEN RUN-SEQUENCE-MULTI(WS-SSA-IDX)
             STRING WS-INDEX-PREFIX DELIMITED BY SPACE
             '_' DELIMITED BY SIZE
             RUN-SEQUENCE-NAME(WS-SSA-IDX) DELIMITED BY SPACE
             ' = ' DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
             PERFORM ANALYZE-KEY-TYPE
      *
             IF WS-HAS-LSOURCE
               MOVE RUN-USED-KEY-PARENT-NUMERIC(WS-SAVE-LSOURCE-INDEX)
                                                     TO WS-EDITED-MASK
             ELSE
               MOVE RUN-USED-KEY-NUMERIC(WS-SSA-IDX)
                                                 TO WS-EDITED-MASK
             END-IF
      *
             STRING ' AND ' DELIMITED BY SIZE
      *      WS-INDEX-PREFIX DELIMITED BY SPACE
      *      '_' WS-SOURCE-SEGMENT
             WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
             '_AUTO_KEY = ' WS-EDITED-MASK DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
           WHEN RUN-SEQUENCE-NONE(WS-SSA-IDX)
             IF WS-HAS-LSOURCE
               MOVE RUN-USED-KEY-PARENT-NUMERIC(WS-SAVE-LSOURCE-INDEX)
                                                     TO WS-EDITED-MASK
             ELSE
               MOVE RUN-USED-KEY-NUMERIC(WS-SSA-IDX)
                                                 TO WS-EDITED-MASK
             END-IF
      *
      *      STRING WS-INDEX-PREFIX DELIMITED BY SPACE
      *      '_' WS-SOURCE-SEGMENT
             STRING
             WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
             '_AUTO_KEY = ' WS-EDITED-MASK DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
           END-EVALUATE.
      *
       LAST-KEY-COND-EQUAL-EX.
           EXIT.
      *
      ******************************************************************
      * HANDLE THE LAST SSA WITH KEYS AND CONDITION GREATER
      ******************************************************************
      *
       HANDLE-LAST-KEY-COND-GT SECTION.
      *
           EVALUATE TRUE
           WHEN IS-USING-INDEX-DBD
      *
             IF NOT IS-INDEX-COND-ADDED
               COMPUTE WS-COM-FIELD-BYTES =
                      DF-FIELD-BYTES(DT-SECONDARY-KEY) + 10
      *
               STRING DT-PROC-SEQ DELIMITED BY SPACE
               '_' DELIMITED BY SIZE
               DF-FIELD-NAME(DT-SECONDARY-KEY) DELIMITED BY SPACE
               ' > '''  RUN-USED-KEY-SECONDARY(1:WS-COM-FIELD-BYTES)
               ''' ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               SET IS-INDEX-COND-ADDED TO TRUE
           END-IF
      *
           WHEN RUN-SEQUENCE-UNIQUE(WS-SSA-IDX)
      *
             IF RUN-SEQUENCE-TYPE-INDEX(WS-SSA-IDX)
               STRING '_________________ > ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             ELSE
               STRING WS-INDEX-PREFIX DELIMITED BY SPACE
               '_' DELIMITED BY SIZE RUN-SEQUENCE-NAME (WS-SSA-IDX)
               DELIMITED BY SPACE ' > ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
             END-IF
      *
             PERFORM ANALYZE-KEY-TYPE
      *
           WHEN RUN-SEQUENCE-MULTI(WS-SSA-IDX)
             EVALUATE TRUE
             WHEN DS-RULE-POSITION-NONE(WS-SSA-IDX)
             WHEN DS-RULE-POSITION-LAST(WS-SSA-IDX)
             WHEN DS-RULE-POSITION-FIRST(WS-SSA-IDX)
      *
               STRING '((' DELIMITED BY SIZE
               WS-INDEX-PREFIX DELIMITED BY SPACE
               '_' DELIMITED BY SIZE RUN-SEQUENCE-NAME(WS-SSA-IDX)
               DELIMITED BY SPACE ' = ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               PERFORM ANALYZE-KEY-TYPE
      *
               MOVE RUN-USED-KEY-NUMERIC(WS-SSA-IDX)
                                                   TO WS-EDITED-MASK
      *
               STRING ' AND ' DELIMITED BY SIZE
               WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
               '_AUTO_KEY > ' WS-EDITED-MASK ') OR ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               STRING WS-INDEX-PREFIX DELIMITED BY SPACE
               '_' DELIMITED BY SIZE RUN-SEQUENCE-NAME(WS-SSA-IDX)
               DELIMITED BY SPACE ' > ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               PERFORM ANALYZE-KEY-TYPE
      *
               STRING ')' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
             WHEN DS-RULE-POSITION-HERE(WS-SSA-IDX)
      *
               IF RUN-USED-KEY-NUMERIC-NEXT(WS-SSA-IDX) NOT = 0
                 STRING '(' DELIMITED BY SIZE
                 WS-INDEX-PREFIX DELIMITED BY SPACE
                 '_' DELIMITED BY SIZE RUN-SEQUENCE-NAME(WS-SSA-IDX)
                 DELIMITED BY SPACE ' = ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
                 PERFORM ANALYZE-KEY-TYPE
      *
                 MOVE RUN-USED-KEY-NUMERIC-NEXT(WS-SSA-IDX)
                                                     TO WS-EDITED-MASK
                 STRING ' AND ' DELIMITED BY SIZE
                 WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
                 '_AUTO_KEY = ' WS-EDITED-MASK DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
                 STRING ' OR ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
                 STRING WS-INDEX-PREFIX DELIMITED BY SPACE
                 '_' DELIMITED BY SIZE RUN-SEQUENCE-NAME(WS-SSA-IDX)
                 DELIMITED BY SPACE ' > ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
                 PERFORM ANALYZE-KEY-TYPE
      *
                 STRING ' AND ' DELIMITED BY SIZE
                 WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
                 '_PREV_KEY = 0)' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
               ELSE
      *
                 STRING WS-INDEX-PREFIX DELIMITED BY SPACE
                 '_' DELIMITED BY SIZE
                 RUN-SEQUENCE-NAME(WS-SSA-IDX)
                 DELIMITED BY SPACE
                 ' > ' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
                 PERFORM ANALYZE-KEY-TYPE
      *
                 STRING ' AND ' DELIMITED BY SIZE
                 WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
                 '_PREV_KEY = 0' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
               END-IF
             END-EVALUATE
      *
           WHEN RUN-SEQUENCE-NONE(WS-SSA-IDX)
      *
             EVALUATE TRUE
             WHEN DS-RULE-POSITION-NONE(WS-SSA-IDX)
             WHEN DS-RULE-POSITION-LAST(WS-SSA-IDX)
             WHEN DS-RULE-POSITION-FIRST(WS-SSA-IDX)
               MOVE RUN-USED-KEY-NUMERIC(WS-SSA-IDX)
                                                      TO WS-EDITED-MASK
               STRING
               WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
               '_AUTO_KEY > ' WS-EDITED-MASK ' ' DELIMITED BY SIZE
               INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
             WHEN DS-RULE-POSITION-HERE(WS-SSA-IDX)
               IF RUN-USED-KEY-NUMERIC-NEXT(WS-SSA-IDX)
                                                                NOT = 0
               OR RUN-PARENT-SEGMENT-INDEX(WS-SSA-IDX) =
                                                  PARENT-SEGMENT-INDEX
                 MOVE RUN-USED-KEY-NUMERIC-NEXT(WS-SSA-IDX)
                                                     TO WS-EDITED-MASK
                 STRING
                 WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
                 '_AUTO_KEY = ' WS-EDITED-MASK DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
                 SET WS-CONDITION-HAS-NOT-GREATER TO TRUE
               ELSE
                 STRING
                 WS-CURRENT-PREFIX WS-SOURCE-SEGMENT DELIMITED BY SPACE
                 '_PREV_KEY = 0' DELIMITED BY SIZE
                 INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
                 SET WS-CONDITION-HAS-GREATER TO TRUE
               END-IF
             END-EVALUATE
      *
           END-EVALUATE.
      *
       HANDLE-LAST-KEY-COND-GT-EX.
           EXIT.
      *
      ******************************************************************
      * ANALYZE THE FIELD TYPE WITH A LIKE CLAUSE
      ******************************************************************
      *
       ANALYZE-FIELD-TYPE-LIKE SECTION.
           SET WS-ADD-BACKSLASH TO TRUE
      *
           PERFORM ANALYZE-FIELD-TYPE
      *
           SET WS-DONT-ADD-BACKSLASH TO TRUE

IRIS  *    MOVE '%' ESCAPE '\''
IRIS       MOVE '%''           '
            TO WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN - 1:SSA-FIX-L + 1)
IRIS  *    MOVE SLASH TO WS-SSA-BUFFER-TXT (WS-SSA-BUFFER-LEN + 10:1)
           ADD SSA-FIX-L TO WS-SSA-BUFFER-LEN.
      *
       ANALYZE-FIELD-TYPE-LIKE-EX.
           EXIT.
      *
      ******************************************************************
      * ANALYZE THE FIELD TYPE
      ******************************************************************
      *
       ANALYZE-FIELD-TYPE SECTION.
      *
           EVALUATE TRUE
           WHEN WS-FIELD-TYPE-ALPHA
             OR WS-FIELD-TYPE-EDITED
             OR WS-FIELD-TYPE-EBCDIC
             OR WS-FIELD-TYPE-HEX
             MOVE LK-COM-SSA(WS-SSA-OFFSET + SSA-FIX-L:WS-FIELD-BYTES)
                                                    TO WS-SSA-VALUE-TEXT
             MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
      *
             PERFORM ANALYZE-SSA-VALUE
           WHEN WS-FIELD-TYPE-NUMERIC-ZONED
             MOVE LK-COM-SSA(WS-SSA-OFFSET + SSA-FIX-L:WS-FIELD-BYTES)
                                                    TO WS-SSA-VALUE-TEXT
             MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
             PERFORM ANALYZE-SSA-NUM-ZONED-VALUE
      *
           WHEN WS-FIELD-TYPE-PACKED
             MOVE 0 TO WS-PACKED-MASK
             MOVE LK-COM-SSA(WS-SSA-OFFSET + SSA-FIX-L:WS-FIELD-BYTES)
               TO WS-PACKED-MASK-RED(11 - WS-FIELD-BYTES:WS-FIELD-BYTES)
             PERFORM FORMAT-PACKED-VALUE
      *      MOVE WS-PACKED-MASK TO WS-EDITED-MASK
      *      STRING WS-EDITED-MASK DELIMITED BY SIZE
      *      INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
           WHEN WS-FIELD-TYPE-F-WORD
             OR WS-FIELD-TYPE-H-WORD
             MOVE 0 TO WS-BINARY-MASK
             MOVE LK-COM-SSA(WS-SSA-OFFSET + SSA-FIX-L:WS-FIELD-BYTES)
                TO WS-BINARY-MASK-RED(5 - WS-FIELD-BYTES:WS-FIELD-BYTES)
             MOVE WS-BINARY-MASK TO WS-EDITED-MASK
             STRING WS-EDITED-MASK DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
           WHEN WS-FIELD-TYPE-XDFLD
             MOVE LK-COM-SSA(WS-SSA-OFFSET + SSA-FIX-L:WS-FIELD-BYTES)
                                                    TO WS-SSA-VALUE-TEXT
             MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
      *
             PERFORM ANALYZE-SSA-VALUE
      *
           END-EVALUATE.
      *
       ANALYZE-FIELD-TYPE-EX.
           EXIT.
      *
      ******************************************************************
      * ANALYZE THE KEY FIELD TYPE
      ******************************************************************
      *
       ANALYZE-KEY-TYPE SECTION.
      *
           MOVE WS-FIELD-BYTES TO WS-COM-FIELD-BYTES
           MOVE RUN-SEQUENCE-LENGTH(WS-SSA-IDX) TO WS-FIELD-BYTES
           MOVE RUN-SEQUENCE-DATA-TYPE(WS-SSA-IDX) TO WS-FIELD-TYPE
           EVALUATE TRUE
IRIS  *    WHEN WS-FIELD-TYPE-XDFLD
      *
      *      COMPUTE WS-I1 = WS-SAVE-LSOURCE-INDEX
      *      COMPUTE WS-I2 = WS-SSA-IDX
      *
      *      ADD THE LENGTH OF /SX PART FORCED WITH A PROGRESSIVE
      *      WS-FIELD-BYTES IS RESTORED AT THE END OF THIS SECTION
      *      ADD 10 TO WS-FIELD-BYTES
      *
      *      MOVE RUN-USED-KEY-SECONDARY(1:WS-FIELD-BYTES)
      *                                             TO WS-SSA-VALUE-TEXT
      *      MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
      *
      *      PERFORM ANALYZE-SSA-VALUE
      *
           WHEN WS-FIELD-TYPE-ALPHA
             OR WS-FIELD-TYPE-EDITED
             OR WS-FIELD-TYPE-INDEX
             OR WS-FIELD-TYPE-EBCDIC
             OR WS-FIELD-TYPE-HEX
      *
             COMPUTE WS-I1 = WS-SAVE-LSOURCE-INDEX
             COMPUTE WS-I2 = WS-SSA-IDX
      *
      *      ADD THE LENGTH OF /SX PART FORCED WITH A PROGRESSIVE
      *      WS-FIELD-BYTES IS RESTORED AT THE END OF THIS SECTION
             IF WS-FIELD-TYPE-INDEX
               ADD 10 TO WS-FIELD-BYTES
             END-IF
      *
             IF WS-HAS-LSOURCE
               MOVE RUN-USED-KEY-PARENT-ALPHA(WS-I1)(1:WS-FIELD-BYTES)
                                                    TO WS-SSA-VALUE-TEXT
             ELSE
               MOVE RUN-USED-KEY-ALPHA(WS-I2)(1:WS-FIELD-BYTES)
                                                    TO WS-SSA-VALUE-TEXT
             END-IF
             MOVE WS-FIELD-BYTES TO WS-SSA-VALUE-LENGTH
      *
             PERFORM ANALYZE-SSA-VALUE
      *
           WHEN RUN-SEQUENCE-TYPE-PACKED(WS-SSA-IDX)
      *
             MOVE 0 TO WS-PACKED-MASK
             COMPUTE WS-I1 = WS-SAVE-LSOURCE-INDEX
             COMPUTE WS-I2 = WS-SSA-IDX
             IF WS-HAS-LSOURCE
               MOVE RUN-USED-KEY-PARENT-ALPHA(WS-I1)(1:WS-FIELD-BYTES)
               TO WS-PACKED-MASK-RED(11 - WS-FIELD-BYTES:WS-FIELD-BYTES)
             ELSE
               MOVE RUN-USED-KEY-ALPHA(WS-I2)(1:WS-FIELD-BYTES)
               TO WS-PACKED-MASK-RED(11 - WS-FIELD-BYTES:WS-FIELD-BYTES)
             END-IF
             PERFORM FORMAT-PACKED-VALUE
      *      MOVE WS-PACKED-MASK TO WS-EDITED-MASK
      *      STRING WS-EDITED-MASK DELIMITED BY SIZE
      *      INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
           WHEN RUN-SEQUENCE-TYPE-H-WORD(WS-SSA-IDX)
      *
             OR RUN-SEQUENCE-TYPE-F-WORD(WS-SSA-IDX)
             MOVE 0 TO WS-BINARY-MASK
             COMPUTE WS-I1 = WS-SAVE-LSOURCE-INDEX
             COMPUTE WS-I2 = WS-SSA-IDX
             IF WS-HAS-LSOURCE
               MOVE RUN-USED-KEY-PARENT-ALPHA(WS-I1)(1:WS-FIELD-BYTES)
                TO WS-BINARY-MASK-RED(5 - WS-FIELD-BYTES:WS-FIELD-BYTES)
             ELSE
               MOVE RUN-USED-KEY-ALPHA(WS-I2)(1:WS-FIELD-BYTES) TO
                   WS-BINARY-MASK-RED(5 - WS-FIELD-BYTES:WS-FIELD-BYTES)
             END-IF
             MOVE WS-BINARY-MASK TO WS-EDITED-MASK
             STRING WS-EDITED-MASK DELIMITED BY SIZE
             INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
      *
           END-EVALUATE
           MOVE WS-COM-FIELD-BYTES TO WS-FIELD-BYTES.
      *
       ANALYZE-KEY-TYPE-EX.
           EXIT.
      *
      ******************************************************************
      * ANALYZE SSA ALPHA VALUE
      ******************************************************************
      *
       ANALYZE-SSA-VALUE SECTION.
      *
           MOVE '''' TO WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN:1)
           ADD 1 TO WS-SSA-BUFFER-LEN
           IF WS-SSA-VALUE-TEXT(WS-SSA-VALUE-LENGTH:1) NOT = '\'
             MOVE '\' TO SLASH
           ELSE
             MOVE '/' TO SLASH
           END-IF
      *
IRISST     MOVE 1 TO WS-SSA-KEYVALUE-LEN
           PERFORM VARYING WS-INDEX-3 FROM 1 BY 1
           UNTIL WS-INDEX-3 > WS-SSA-VALUE-LENGTH
             IF WS-SSA-VALUE-TEXT(WS-INDEX-3:1) = ''''
               MOVE '''' TO WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN:1)
IRISST                      WS-SSA-KEYVALUE-TXT(WS-SSA-KEYVALUE-LEN:1)
               ADD 1 TO WS-SSA-BUFFER-LEN
IRISST                  WS-SSA-KEYVALUE-LEN
             END-IF
      *
             IF WS-ADD-BACKSLASH
             AND (WS-SSA-VALUE-TEXT(WS-INDEX-3:1) = '%'
                  OR WS-SSA-VALUE-TEXT(WS-INDEX-3:1) = '_'
                  OR WS-SSA-VALUE-TEXT(WS-INDEX-3:1) = SLASH)
               MOVE SPACE TO WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN:1)
IRISST                       WS-SSA-KEYVALUE-TXT(WS-SSA-KEYVALUE-LEN:1)
               ADD 1 TO WS-SSA-BUFFER-LEN
IRISST                  WS-SSA-KEYVALUE-LEN
             END-IF
      *
             MOVE WS-SSA-VALUE-TEXT(WS-INDEX-3:1) TO
                             WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN:1)
IRISST                       WS-SSA-KEYVALUE-TXT(WS-SSA-KEYVALUE-LEN:1)
             ADD 1 TO WS-SSA-BUFFER-LEN
IRISST                WS-SSA-KEYVALUE-LEN
           END-PERFORM
      *
IRISST     IF WS-IS-ACTUAL-SSA
IRISST       SUBTRACT 1 FROM WS-SSA-KEYVALUE-LEN
IRISST       MOVE WS-SSA-KEYVALUE-TXT(1:WS-SSA-KEYVALUE-LEN)
IRISST         TO IRIS-KEYVALUE(WS-SSA-IDX, WS-SSA-KEYVALUE-BOOL)
IRISST     END-IF
           MOVE '''' TO WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN:1)
           ADD 1 TO WS-SSA-BUFFER-LEN.
      *
       ANALYZE-SSA-VALUE-EX.
           EXIT.
      *
      ******************************************************************
      * ANALYZE SSA ALPHA VALUE
      ******************************************************************
      *
       ANALYZE-SSA-NUM-ZONED-VALUE SECTION.
      *
           IF WS-SSA-VALUE-LENGTH = ZERO
           OR WS-SSA-VALUE-TEXT(1:WS-SSA-VALUE-LENGTH) = SPACE
           OR WS-SSA-VALUE-TEXT(1:WS-SSA-VALUE-LENGTH) = LOW-VALUE
             MOVE '0' TO WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN:1)
             ADD 1 TO WS-SSA-BUFFER-LEN
           ELSE
             PERFORM VARYING WS-INDEX-3 FROM 1 BY 1
             UNTIL WS-INDEX-3 > WS-SSA-VALUE-LENGTH
               MOVE WS-SSA-VALUE-TEXT(WS-INDEX-3:1) TO
                               WS-SSA-BUFFER-TXT(WS-SSA-BUFFER-LEN:1)
               ADD 1 TO WS-SSA-BUFFER-LEN
             END-PERFORM
           END-IF.
      *
       ANALYZE-SSA-NUM-ZONED-EX.
           EXIT.
      *
      ******************************************************************
      * FORMAT A PACKED VALUE FOR THE CONDITION
      ******************************************************************
      *
       FORMAT-PACKED-VALUE SECTION.
      *
           EVALUATE WS-FIELD-DECIMALS
           WHEN 0
              MOVE WS-PACKED-MASK TO WS-EDITED-MASK
              STRING WS-EDITED-MASK DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 1
              MOVE WS-PACKED-MASK-D1 TO WS-EDITED-MASK-D1
              STRING WS-EDITED-MASK-D1 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 2
              MOVE WS-PACKED-MASK-D2 TO WS-EDITED-MASK-D2
              STRING WS-EDITED-MASK-D2 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 3
              MOVE WS-PACKED-MASK-D3 TO WS-EDITED-MASK-D3
              STRING WS-EDITED-MASK-D3 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 4
              MOVE WS-PACKED-MASK-D4 TO WS-EDITED-MASK-D4
              STRING WS-EDITED-MASK-D4 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 5
              MOVE WS-PACKED-MASK-D5 TO WS-EDITED-MASK-D5
              STRING WS-EDITED-MASK-D5 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 6
              MOVE WS-PACKED-MASK-D6 TO WS-EDITED-MASK-D6
              STRING WS-EDITED-MASK-D6 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 7
              MOVE WS-PACKED-MASK-D7 TO WS-EDITED-MASK-D7
              STRING WS-EDITED-MASK-D7 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 8
              MOVE WS-PACKED-MASK-D8 TO WS-EDITED-MASK-D8
              STRING WS-EDITED-MASK-D8 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 9
              MOVE WS-PACKED-MASK-D9 TO WS-EDITED-MASK-D9
              STRING WS-EDITED-MASK-D9 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 10
              MOVE WS-PACKED-MASK-D10 TO WS-EDITED-MASK-D10
              STRING WS-EDITED-MASK-D10 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 11
              MOVE WS-PACKED-MASK-D11 TO WS-EDITED-MASK-D11
              STRING WS-EDITED-MASK-D11 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 12
              MOVE WS-PACKED-MASK-D12 TO WS-EDITED-MASK-D12
              STRING WS-EDITED-MASK-D12 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 13
              MOVE WS-PACKED-MASK-D13 TO WS-EDITED-MASK-D13
              STRING WS-EDITED-MASK-D13 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 14
              MOVE WS-PACKED-MASK-D14 TO WS-EDITED-MASK-D14
              STRING WS-EDITED-MASK-D14 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 15
              MOVE WS-PACKED-MASK-D15 TO WS-EDITED-MASK-D15
              STRING WS-EDITED-MASK-D15 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 16
              MOVE WS-PACKED-MASK-D16 TO WS-EDITED-MASK-D16
              STRING WS-EDITED-MASK-D16 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 17
              MOVE WS-PACKED-MASK-D17 TO WS-EDITED-MASK-D17
              STRING WS-EDITED-MASK-D17 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN 18
              MOVE WS-PACKED-MASK-D18 TO WS-EDITED-MASK-D18
              STRING WS-EDITED-MASK-D18 DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           WHEN OTHER
              MOVE WS-PACKED-MASK TO WS-EDITED-MASK
              STRING WS-EDITED-MASK DELIMITED BY SIZE
              INTO WS-SSA-BUFFER-TXT POINTER WS-SSA-BUFFER-LEN
           END-EVALUATE
      *
           .
      *
       FORMAT-PACKED-VALUE-EX.
           EXIT.
      *
      ******************************************************************
      * SEND A MISMATCH ERROR FOR A WRONG BOOLEAN SSA
      ******************************************************************
      *
       SEND-BOOLEAN-SSA-MISMATCH SECTION.
      *
           MOVE ZERO TO IRIS-MSG-LEN
           STRING '<IRISTRACE> - (IRISPSSA)' NL
                  ' ERROR      =(UNSUPPORTED BOOLEAN OPERATOR)' NL
                  ' DBD        =(' DT-DBD-NAME ')' NL
                  ' SEGMENT    =(' WS-SOURCE-SEGMENT ') ' NL
                  ' OPERATOR   =(' WS-BOOLEAN-SSA-OPERATOR ') '
           MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
           SET IRIS-MSG-LEVEL-ERROR TO TRUE
           SET IRISTRAC-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           SET IRIS-ERR-WRONG-BOOLEAN-OP TO TRUE
      *
           GO TO GO-BACK.
      *
       SEND-BOOLEAN-SSA-MISMATCH-EX.
           EXIT.
      *
      ******************************************************************
      * SEND A MISMATCH ERROR FOR A WRONG BOOLEAN SSA
      ******************************************************************
      *
       SEND-SSA-MISMATCH SECTION.
      *
           MOVE ZERO TO IRIS-MSG-LEN
           STRING '<IRISTRACE> - (IRISPSSA)' NL
                  ' ERROR      =(UNSUPPORTED SSA SYNTAX)' NL
                  ' DBD        =(' DT-DBD-NAME ')' NL
                  ' SEGMENT    =(' WS-SOURCE-SEGMENT ')'
           MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
           SET IRIS-MSG-LEVEL-ERROR TO TRUE
           SET IRISTRAC-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           SET IRIS-ERR-SSA-UNHANDLED-ACCESS TO TRUE
      *
           GO TO GO-BACK.
      *
       SEND-SSA-MISMATCH-EX.
           EXIT.
      *
      ******************************************************************
      * SEND A MISMATCH WARNING FOR A MISSING INDEX
      ******************************************************************
      *
       SEND-MISSING-INDEX-WARNING SECTION.
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             MOVE WS-FIELD-TYPE TO WS-FIELD-TYPE-ZONED
             STRING '<IRISTRACE> - (IRISPSSA)' NL
                    ' WARNING    =(SSA FIELD NOT INDEXED)' NL
                    ' DBD        =(' DT-DBD-NAME ')' NL
                    ' SEGMENT    =('  WS-SOURCE-SEGMENT ')' NL
                    ' FIELD NAME =('  WS-FIELD-NAME ')' NL
                    ' FIELD TYPE =(' WS-FIELD-TYPE-ZONED  ')' NL
                    ' SQL NAME   =('
                    WS-FIELD-IORTN-TXT(1:WS-FIELD-IORTN-LEN) ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-WARNING TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
      *
       SEND-MISSING-INDEX-WARNING-EX.
           EXIT.
      *
      ******************************************************************
      * TRACE AN UNQUALIFIED SSA
      ******************************************************************
      *
       TRACE-UNQUAL SECTION.
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - '
             '(IRISPSSA) LEVEL WITH UNQUALIFIED SSA'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
       TRACE-UNQUAL-EX.
           EXIT.
      *
      ******************************************************************
      * TRACE UNQUALIFIED SSA SKIPPED
      ******************************************************************
      *
       TRACE-UNQ-SKIPPED SECTION.
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - '
             '(IRISPSSA) LEVEL WITH UNQUALIFIED SSA, CONDITION SKIPPED'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
       TRACE-UNQ-SKIPPED-EX.
           EXIT.
      *
      ******************************************************************
      * TRACE QUALIFIED SSA
      ******************************************************************
      *
       TRACE-QUAL SECTION.
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - '
             '(IRISPSSA) LEVEL WITH QUALIFIED SSA =('
             LK-COM-SSA(WS-SSA-OFFSET + 1:WS-FIELD-BYTES + SSA-FIX-L)
             ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
       TRACE-QUAL-EX.
           EXIT.
      *
      ******************************************************************
      * TRACE SSA KEYS
      ******************************************************************
      *
       TRACE-WITH-KEYS SECTION.
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - '
             '(IRISPSSA) KEYS =(' LK-COM-SSA(1:2 + WS-SSA-KEYS-LENGTH)
             ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
       TRACE-WITH-KEYS-EX.
           EXIT.
      *
      ******************************************************************
      * SEARCH FOR CUSTOM
      ******************************************************************
      *
       SEARCH-FOR-CUSTOM SECTION.

             SUBTRACT 1 FROM WS-SSA-KEY-LEN
             IF WS-SSA-KEY-LEN > ZERO
DBG   *        DISPLAY 'SSA_KEY=(' WS-SSA-KEY-TXT(1:WS-SSA-KEY-LEN) ')'
               MOVE ZERO TO WS-MEM-SSA-FUNCID
               PERFORM VARYING WS-INDEX FROM 1 BY 1 UNTIL
                                       WS-INDEX > LK-MEM-SSA-COUNT
                 IF LK-MEM-SSA-LEN(WS-INDEX) = WS-SSA-KEY-LEN
DBG   *        DISPLAY
DBG   *        'SEARCH =(' LK-MEM-SSA-TXT(WS-INDEX)(1:WS-SSA-KEY-LEN) ')'
                   IF LK-MEM-SSA-TXT(WS-INDEX)(1:WS-SSA-KEY-LEN) =
                                  WS-SSA-KEY-TXT(1:WS-SSA-KEY-LEN)
                     MOVE LK-MEM-SSA-FUNCID(WS-INDEX)
                                             TO WS-MEM-SSA-FUNCID
                     MOVE LK-MEM-SSA-COUNT TO WS-INDEX
                   END-IF
                 END-IF
               END-PERFORM
             END-IF.
       SEARCH-FOR-CUSTOM-EX.
           EXIT.
      *
      *
      ******************************************************************
      * EXIT WITH ERROR
      ******************************************************************
      *
       GO-BACK SECTION.
      *
           GOBACK.
      *
       GO-BACK-EX.
           EXIT.
