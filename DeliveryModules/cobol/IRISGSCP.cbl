      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: RETRIEVES THE INDEX OF THE SEGMENT STORED INTO     
      *              MEMORY-DBD-SEGMENTS
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IRISGSCP.
      *
       ENVIRONMENT DIVISION.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
           EXEC SQL INCLUDE SQLCA END-EXEC.
       01 IRIS-CURRENT-TIMESTAMP PIC X(26) VALUE SPACES.
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
      *
      * WORKING STORAGE COMMON VARIABLES
      *
      * COMMON PERFORM INDEX
       01 WS-INDEX                PIC S9(9) COMP.

       LINKAGE SECTION.
      *
      *    CONTROL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='UIGETSEG'==.      
      *
      *  STRUCTURES TO REPRESENT TP AND DB PCB
      * 
           COPY IRISISEG.
      * 
       01 LK-SEGMENT-NAME         PIC X(8).
       01 LK-SEGMENT-INDEX        PIC S9(9) COMP.
      *
      *  SEGMENT REPRESENTATION
      * 
           COPY IRISSEGM.
      * 
       PROCEDURE DIVISION USING IRIS-WORK-AREA 
                                MEMORY-PCB-AREA 
                                LK-SEGMENT-NAME 
                                LK-SEGMENT-INDEX.
      *
       MAIN-PROGRAM.
      *
           SET ADDRESS OF MEMORY-DBD-SEGMENTS TO DT-DBD-SEGM-PTR
           IF IRIS-TRACE-DEBUG
             EXEC SQL
              SET :IRIS-CURRENT-TIMESTAMP = CURRENT_TIMESTAMP
             END-EXEC
           END-IF
           MOVE 0 TO LK-SEGMENT-INDEX
           PERFORM VARYING WS-INDEX FROM 1 BY 1 
           UNTIL WS-INDEX > DS-SEGMENTS-COUNT 
           OR LK-SEGMENT-NAME = DS-SEGMENT-NAME(WS-INDEX)
             CONTINUE
           END-PERFORM

           IF WS-INDEX <= DS-SEGMENTS-COUNT
             MOVE WS-INDEX TO LK-SEGMENT-INDEX
           ELSE
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (UIGETSEG)' NL
                    ' ERROR      =(SEGMENT NOT FOUND IN DBD)' NL
                    ' DBD        =(' DT-DBD-NAME ')' NL
                    ' SEGMENT    =(' LK-SEGMENT-NAME ')' NL
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT          
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             SET IRIS-ERR-SSA-SEGMENT-NOT-FOUND TO TRUE                 
             SET IRIS-MSG-LEVEL-ERROR TO TRUE 
           END-IF.
      *
       MAIN-PROGRAM-EX.
      *
           GOBACK.
