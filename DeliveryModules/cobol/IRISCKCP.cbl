CBL DYNAM,TRUNC(BIN),NOSQLCCSID
      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: DATABASE I/O ROUTINE FOR CHECKPOINT READ
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IRISCKCP.
      *
       ENVIRONMENT DIVISION.
      *
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT IRISCHKP ASSIGN TO IRISCHKP
              FILE STATUS IS WS-STATUS.
      *
       DATA DIVISION.
       FILE SECTION.
       FD IRISCHKP.
       01 REC-IRISCKCP.
          02 FILLER PIC X(80).
      *
       WORKING-STORAGE SECTION.
      *
           EXEC SQL INCLUDE SQLCA END-EXEC.
       01 IRIS-CURRENT-TIMESTAMP PIC X(26) VALUE SPACES.
      *
       01 WS-LAST-IORTN-SECTION  PIC X(30).
      *
       01  WS-STATUS             PIC X(2).
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
      *
       LINKAGE SECTION.
      *
      *    IRIS GLOBAL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IRISUTIL'==.
      *
       PROCEDURE DIVISION USING IRIS-WORK-AREA.
      *
       MAIN.
      *
           IF IRIS-TRACE-DEBUG
             EXEC SQL
              SET :IRIS-CURRENT-TIMESTAMP = CURRENT_TIMESTAMP
             END-EXEC
           END-IF
           IF IRIS-TRACE-FULL
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISCKCP:START' NL
                    ' CALLER PGM =(' IRIS-PROGRAM-NAME ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           PERFORM INIT-VARIABLES THRU INIT-VARIABLES-EX
      *
           PERFORM READ-CHKP-FILE THRU READ-CHKP-FILE-EX
      *
           PERFORM FINALIZE-VARIABLES THRU FINALIZE-VARIABLES-EX
      *
           IF IRIS-TRACE-FULL
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISCKCP:END' NL
                    ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                    ' SECTION    =(' WS-LAST-IORTN-SECTION ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
           END-IF.
      *
       MAIN-PROGRAM-EX.
      *
           GOBACK.
      *
      ******************************************************************
      *    INITIALIZE VARIABLES
      ******************************************************************
      *
       INIT-VARIABLES SECTION.
      *
           CONTINUE.
      *
       INIT-VARIABLES-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    FINALIZE VARIABLES
      ******************************************************************
      *
       FINALIZE-VARIABLES SECTION.
      *
           CONTINUE.
      *
       FINALIZE-VARIABLES-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    HANDLE PCB SCHEDULE FOR ONLINE
      ******************************************************************
      *
       READ-CHKP-FILE SECTION.
      *
           MOVE 'READ-CHKP-FILE' TO WS-LAST-IORTN-SECTION
      *
           STRING '<IRISTRACE> - IRISCKCP: XRST' NL
                  ' GETTING CHKP-ID FROM COLD PARAMETER'
           MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
           SET IRISTRAC-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           OPEN INPUT IRISCHKP
           IF WS-STATUS NOT = ZERO
             STRING '<IRISTRACE> - IRISCKCP: XRST' NL
                    ' CHKP-ID NOT FOUND ON FILE: NO RESTART'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             MOVE SPACES TO IRIS-GSAM-CHECKPOINT-ID
             GO TO READ-CHKP-FILE-EX
           END-IF
           READ IRISCHKP
           IF WS-STATUS NOT = ZERO
             STRING '<IRISTRACE> - IRISCKCP: XRST' NL
                    ' CHKP-ID NOT FOUND ON FILE: NO RESTART'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             CLOSE IRISCHKP
             MOVE SPACES TO IRIS-GSAM-CHECKPOINT-ID
             GO TO READ-CHKP-FILE-EX
           END-IF
           IF REC-IRISCKCP(6:8) = SPACES OR LOW-VALUES
             STRING '<IRISTRACE> - IRISUTCP: XRST' NL
                    ' CHKP-ID NOT FOUND ON FILE: NO RESTART'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             CLOSE IRISCHKP
             MOVE SPACES TO IRIS-GSAM-CHECKPOINT-ID
             GO TO READ-CHKP-FILE-EX
           END-IF
           MOVE REC-IRISCKCP(6:) TO IRIS-GSAM-CHECKPOINT-ID.
      *
       READ-CHKP-FILE-EX.
      *
           EXIT.
