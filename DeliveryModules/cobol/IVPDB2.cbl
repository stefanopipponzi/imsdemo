      *CBL DYNAM,TRUNC(BIN),NOSQLCCSID
      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: I/O ROUTINE FOR DBD: IVPDB2
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IVPDB2.
      *
       ENVIRONMENT DIVISION.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
           COPY IRISASSA.
      *
      *    DATABASE COMMON SQLCODES
      *
           COPY IRISSQLC.
      *
      *    STANDARD SQL INCLUDE
      *
           EXEC SQL INCLUDE SQLCA END-EXEC.
           EXEC SQL INCLUDE SQLDA END-EXEC.
      *
      *    WORKING STORAGE VARIABLES
      *
       01 WS-SSA-01                     PIC X(256).
       01 WS-SSA-02                     PIC X(256).
       01 WS-SSA-03                     PIC X(256).
       01 WS-SSA-04                     PIC X(256).
       01 WS-SSA-05                     PIC X(256).
       01 WS-SSA-06                     PIC X(256).
       01 WS-SSA-07                     PIC X(256).
       01 WS-SSA-08                     PIC X(256).
       01 WS-SSA-09                     PIC X(256).
       01 WS-SSA-10                     PIC X(256).
       01 WS-SSA-11                     PIC X(256).
       01 WS-SSA-12                     PIC X(256).
       01 WS-SSA-13                     PIC X(256).
       01 WS-SSA-14                     PIC X(256).
       01 WS-SSA-15                     PIC X(256).
       01 WS-SSA-16                     PIC X(256).
       01 WS-IMS-API                    PIC X(8).
       01 WS-PTR                        POINTER.
       01  WS-SQL-STM.
         49  WS-SQL-STM-LEN             PIC S9(4) COMP.
         49  WS-SQL-STM-TXT             PIC X(32000).
       01  SQL-ORDERBY-CLAUSE.
         49  SQL-ORDERBY-CLAUSE-LENGTH  PIC S9(4) COMP.
         49  SQL-ORDERBY-CLAUSE-TEXT    PIC X(32000).
       01 WS-DUAL-TRACE                 PIC 9 VALUE 0.
         88 DUAL-TRACE-ON               VALUE 0.
         88 DUAL-TRACE-OFF              VALUE 1.
      *
       01 WS-AUDIT-DUMMY                PIC X(25).
       01 WS-CREATION-USER.
         49 WS-CREATION-USER-LEN        PIC S9(4) COMP.
         49 WS-CREATION-USER-TXT        PIC X(25).
       01 WS-LAST-UPDATE-USER.
         49 WS-LAST-UPDATE-USER-LEN     PIC S9(4) COMP.
         49 WS-LAST-UPDATE-USER-TXT     PIC X(25).
       01 WS-CREATION-TRAN.
         49 WS-CREATION-TRAN-LEN        PIC S9(4) COMP.
         49 WS-CREATION-TRAN-TXT        PIC X(25).
       01 WS-LAST-UPDATE-TRAN.
         49 WS-LAST-UPDATE-TRAN-LEN     PIC S9(4) COMP.
         49 WS-LAST-UPDATE-TRAN-TXT     PIC X(25).
       01 WS-LOW-VALUES                 PIC X(256)     VALUE LOW-VALUE.
       01 WS-HIGH-VALUES                PIC X(256)     VALUE HIGH-VALUE.
       01 WS-IO-AREA                    PIC X(32000).
       01 WS-PATHCALL-AREA              PIC X(32000).
       01 WS-PATHCALL-LEN               PIC S9(4) COMP.
       01 WS-INIT-PATHCALL-LEN          PIC S9(4) COMP.
       01 WS-WHERE                      PIC X(4096).
       01 WS-WHERE-LEN                  PIC S9(4) COMP.
       01 WS-ORDERBY                    PIC X(256).
       01 WS-ORDERBY-LEN                PIC S9(4) COMP.
       01 WS-SAVE-ROUTINE-SQLCODE       PIC S9(9) COMP.
       01 WS-SQLCODE-N                  PIC S9(5).
       01 WS-SQLCODE-E                  PIC -ZZZZ9.
       01 WS-SAVE-ROUTINE-SQLCA         PIC X(1024).
       01 WS-MESSAGE-ID-EDITED          PIC ZZZZ9.
       01 WS-MESSAGE                    PIC X(32000).
       01 WS-ERROR-DESCRIPTION          PIC X(80).
       01 WS-ERROR-DESCRIPTION-LEN      PIC S9(4) COMP.
       01 WS-INDEX                      PIC S9(4) COMP.
       01 WS-LEN                        PIC S9(4) COMP.
       01 WS-IDX                        PIC 9(3).
       01 WS-FLAG                       PIC 9 VALUE 0.
         88 WS-NOT-FOUND                VALUE 0.
         88 WS-FOUND                    VALUE 1.
       01 WS-ZONED-FIELD-1              PIC 9.
       01 WS-ZONED-FIELD-2              PIC 9(2).
       01 WS-ZONED-FIELD-3              PIC 9(3).
       01 WS-ZONED-FIELD-4              PIC 9(4).
       01 WS-ZONED-FIELD-5              PIC 9(5).
       01 WS-ZONED-FIELD-6              PIC 9(6).
       01 WS-ZONED-FIELD-7              PIC 9(7).
       01 WS-ZONED-FIELD-8              PIC 9(8).
       01 WS-ZONED-FIELD-9              PIC 9(9).
       01 WS-ZONED-FIELD-10             PIC 9(10).
       01 WS-ZONED-FIELD-10-1           PIC 9(10).
       01 WS-ZONED-FIELD-10-2           PIC 9(10).
       01 WS-ZONED-FIELD-10-3           PIC 9(10).
       01 WS-ZONED-FIELD-10-4           PIC 9(10).
       01 WS-ZONED-FIELD-10-5           PIC 9(10).
       01 WS-ZONED-FIELD-10-6           PIC 9(10).
       01 WS-ZONED-FIELD-10-7           PIC 9(10).
       01 WS-ZONED-FIELD-10-8           PIC 9(10).
       01 WS-ZONED-FIELD-10-9           PIC 9(10).
       01 WS-ZONED-FIELD-10-10          PIC 9(10).
       01 WS-ZONED-FIELD-10-11          PIC 9(10).
       01 WS-ZONED-FIELD-10-12          PIC 9(10).
       01 WS-ZONED-FIELD-10-13          PIC 9(10).
       01 WS-ZONED-FIELD-10-14          PIC 9(10).
       01 WS-ZONED-FIELD-10-15          PIC 9(10).
       01 WS-ZONED-FIELD-10-16          PIC 9(10).
       01 WS-ZONED-FIELD-11             PIC 9(11).
       01 WS-ZONED-FIELD-12             PIC 9(12).
       01 WS-ZONED-FIELD-13             PIC 9(13).
       01 WS-ZONED-FIELD-14             PIC 9(14).
       01 WS-ZONED-FIELD-15             PIC 9(15).
       01 WS-ZONED-FIELD-16             PIC 9(16).
       01 WS-ZONED-FIELD-17             PIC 9(17).
       01 WS-ZONED-FIELD-18             PIC 9(18).
       01 WS-BINARY-FIELD               PIC S9(4) COMP.
       01 FILLER                        REDEFINES WS-BINARY-FIELD.
         03 WS-BINARY-FIELD-X           PIC X(2).
       01 WS-BINARY-BIG-END             PIC S9(4) COMP.
       01 WS-BINARY-BIG-END-RED         REDEFINES WS-BINARY-BIG-END.
         03 WS-BIG-END-BYTE-1           PIC X.
         03 WS-BIG-END-BYTE-2           PIC X.
       01 WS-BINARY-LIT-END             PIC S9(4) COMP.
       01 WS-BINARY-LIT-END-RED         REDEFINES WS-BINARY-LIT-END.
         03 WS-LIT-END-BYTE-1           PIC X.
         03 WS-LIT-END-BYTE-2           PIC X.
       01 WS-BINARY-BIG4-END            PIC S9(9) COMP.
       01 WS-BINARY-BIG4-END-RED        REDEFINES WS-BINARY-BIG4-END.
         03 WS-BIG4-END-BYTE-1          PIC X.
         03 WS-BIG4-END-BYTE-2          PIC X.
         03 WS-BIG4-END-BYTE-3          PIC X.
         03 WS-BIG4-END-BYTE-4          PIC X.
       01 WS-BINARY-LIT4-END            PIC S9(9) COMP.
       01 WS-BINARY-LIT4-END-RED        REDEFINES WS-BINARY-LIT4-END.
         03 WS-LIT4-END-BYTE-1          PIC X.
         03 WS-LIT4-END-BYTE-2          PIC X.
         03 WS-LIT4-END-BYTE-3          PIC X.
         03 WS-LIT4-END-BYTE-4          PIC X.
       01 WK-COMMAND-CODE               PIC S9(9) COMP.
         88 COMMAND-CODE-HERE           VALUE 0 1021.
         88 COMMAND-CODE-FIRST          VALUE 1020.
         88 COMMAND-CODE-LAST           VALUE 1019.
       01 FILLER                        PIC 9.
         88 HAS-NOT-PATHCALLS           VALUE 0.
         88 HAS-PATHCALLS               VALUE 1.
       01 FILLER                        PIC 9.
         88 HAS-PATHCALLS-ERROR         VALUE 0.
         88 HAS-PATHCALLS-NO-ERROR      VALUE 1.
       01 FILLER                        PIC 9.
         88 IS-NOT-PATHCALL-REVERSE     VALUE 0.
         88 IS-PATHCALL-REVERSE         VALUE 1.
       01 WS-PATHCALL-LEVEL             PIC S9(4) COMP.
       01 WS-COMMON-FLD                 PIC X(4608).
       01 FILLER REDEFINES WS-COMMON-FLD.
         03 WS-ZONED-FLD     OCCURS 256 PIC S9(18).
       01 FILLER REDEFINES WS-COMMON-FLD.
         03 WS-ZONED-FLD-CHR OCCURS 256 PIC X(18).
       01 FILLER REDEFINES WS-COMMON-FLD.
         03 WS-COMP-FLD-2    OCCURS 256 PIC S9(4) COMP.
       01 FILLER REDEFINES WS-COMMON-FLD.
         03 WS-COMP-FLD-4    OCCURS 256 PIC S9(9) COMP.
      *
       01 WS-COMMON-FLD-PACKED             PIC X(2560).
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-18-00 OCCURS 256 PIC S9(18)         COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-17-01 OCCURS 256 PIC S9(17)V9       COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-16-02 OCCURS 256 PIC S9(16)V9(02)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-15-03 OCCURS 256 PIC S9(15)V9(03)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-14-04 OCCURS 256 PIC S9(14)V9(04)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-13-05 OCCURS 256 PIC S9(13)V9(05)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-12-06 OCCURS 256 PIC S9(12)V9(06)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-11-07 OCCURS 256 PIC S9(11)V9(07)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-10-08 OCCURS 256 PIC S9(10)V9(08)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-09-09 OCCURS 256 PIC S9(09)V9(09)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-08-10 OCCURS 256 PIC S9(08)V9(10)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-07-11 OCCURS 256 PIC S9(07)V9(11)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-06-12 OCCURS 256 PIC S9(06)V9(12)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-05-13 OCCURS 256 PIC S9(05)V9(13)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-04-14 OCCURS 256 PIC S9(04)V9(14)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-03-15 OCCURS 256 PIC S9(03)V9(15)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-02-16 OCCURS 256 PIC S9(02)V9(16)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-01-17 OCCURS 256 PIC S9(01)V9(17)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-00-18 OCCURS 256 PIC SV9(18)        COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-18-00 OCCURS 256 PIC 9(18)       COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-17-01 OCCURS 256 PIC 9(17)V9     COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-16-02 OCCURS 256 PIC 9(16)V9(02) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-15-03 OCCURS 256 PIC 9(15)V9(03) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-14-04 OCCURS 256 PIC 9(14)V9(04) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-13-05 OCCURS 256 PIC 9(13)V9(05) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-12-06 OCCURS 256 PIC 9(12)V9(06) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-11-07 OCCURS 256 PIC 9(11)V9(07) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-10-08 OCCURS 256 PIC 9(10)V9(08) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-09-09 OCCURS 256 PIC 9(09)V9(09) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-08-10 OCCURS 256 PIC 9(08)V9(10) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-07-11 OCCURS 256 PIC 9(07)V9(11) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-06-12 OCCURS 256 PIC 9(06)V9(12) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-05-13 OCCURS 256 PIC 9(05)V9(13) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-04-14 OCCURS 256 PIC 9(04)V9(14) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-03-15 OCCURS 256 PIC 9(03)V9(15) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-02-16 OCCURS 256 PIC 9(02)V9(16) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-01-17 OCCURS 256 PIC 9(01)V9(17) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-00-18 OCCURS 256 PIC V9(18)      COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-CHR   OCCURS 256 PIC X(10).
      *
       01 WS-XZONED-18                  PIC X(18).
       01 WS-NUMZONED-18-00 REDEFINES WS-XZONED-18    PIC S9(18).
       01 WS-NUMZONED-17-01 REDEFINES WS-XZONED-18    PIC S9(17)V9.
       01 WS-NUMZONED-16-02 REDEFINES WS-XZONED-18    PIC S9(16)V9(02).
       01 WS-NUMZONED-15-03 REDEFINES WS-XZONED-18    PIC S9(15)V9(03).
       01 WS-NUMZONED-14-04 REDEFINES WS-XZONED-18    PIC S9(14)V9(04).
       01 WS-NUMZONED-13-05 REDEFINES WS-XZONED-18    PIC S9(13)V9(05).
       01 WS-NUMZONED-12-06 REDEFINES WS-XZONED-18    PIC S9(12)V9(06).
       01 WS-NUMZONED-11-07 REDEFINES WS-XZONED-18    PIC S9(11)V9(07).
       01 WS-NUMZONED-10-08 REDEFINES WS-XZONED-18    PIC S9(10)V9(08).
       01 WS-NUMZONED-09-09 REDEFINES WS-XZONED-18    PIC S9(09)V9(09).
       01 WS-NUMZONED-08-10 REDEFINES WS-XZONED-18    PIC S9(08)V9(10).
       01 WS-NUMZONED-07-11 REDEFINES WS-XZONED-18    PIC S9(07)V9(11).
       01 WS-NUMZONED-06-12 REDEFINES WS-XZONED-18    PIC S9(06)V9(12).
       01 WS-NUMZONED-05-13 REDEFINES WS-XZONED-18    PIC S9(05)V9(13).
       01 WS-NUMZONED-04-14 REDEFINES WS-XZONED-18    PIC S9(04)V9(14).
       01 WS-NUMZONED-03-15 REDEFINES WS-XZONED-18    PIC S9(03)V9(15).
       01 WS-NUMZONED-02-16 REDEFINES WS-XZONED-18    PIC S9(02)V9(16).
       01 WS-NUMZONED-01-17 REDEFINES WS-XZONED-18    PIC S9(01)V9(17).
       01 WS-NUMZONED-00-18 REDEFINES WS-XZONED-18    PIC SV9(18).
       01 WS-NUMZONED-NS-18-00 REDEFINES WS-XZONED-18 PIC 9(18).
       01 WS-NUMZONED-NS-17-01 REDEFINES WS-XZONED-18 PIC 9(17)V9.
       01 WS-NUMZONED-NS-16-02 REDEFINES WS-XZONED-18 PIC 9(16)V9(02).
       01 WS-NUMZONED-NS-15-03 REDEFINES WS-XZONED-18 PIC 9(15)V9(03).
       01 WS-NUMZONED-NS-14-04 REDEFINES WS-XZONED-18 PIC 9(14)V9(04).
       01 WS-NUMZONED-NS-13-05 REDEFINES WS-XZONED-18 PIC 9(13)V9(05).
       01 WS-NUMZONED-NS-12-06 REDEFINES WS-XZONED-18 PIC 9(12)V9(06).
       01 WS-NUMZONED-NS-11-07 REDEFINES WS-XZONED-18 PIC 9(11)V9(07).
       01 WS-NUMZONED-NS-10-08 REDEFINES WS-XZONED-18 PIC 9(10)V9(08).
       01 WS-NUMZONED-NS-09-09 REDEFINES WS-XZONED-18 PIC 9(09)V9(09).
       01 WS-NUMZONED-NS-08-10 REDEFINES WS-XZONED-18 PIC 9(08)V9(10).
       01 WS-NUMZONED-NS-07-11 REDEFINES WS-XZONED-18 PIC 9(07)V9(11).
       01 WS-NUMZONED-NS-06-12 REDEFINES WS-XZONED-18 PIC 9(06)V9(12).
       01 WS-NUMZONED-NS-05-13 REDEFINES WS-XZONED-18 PIC 9(05)V9(13).
       01 WS-NUMZONED-NS-04-14 REDEFINES WS-XZONED-18 PIC 9(04)V9(14).
       01 WS-NUMZONED-NS-03-15 REDEFINES WS-XZONED-18 PIC 9(03)V9(15).
       01 WS-NUMZONED-NS-02-16 REDEFINES WS-XZONED-18 PIC 9(02)V9(16).
       01 WS-NUMZONED-NS-01-17 REDEFINES WS-XZONED-18 PIC 9(01)V9(17).
       01 WS-NUMZONED-NS-00-18 REDEFINES WS-XZONED-18 PIC V9(18).
      *
      *    SEGMENTS CONCATENATED KEYS
      *
       01 WS-CK-POS                     PIC S9(4) COMP.
       01 WS-CK-LEN                     PIC S9(4) COMP.
       01 A1111111-CONCATENATED-KEY     PIC X(4096).
      *
      *    SEGMENTS INFO
      *
       01 A1111111-LEN                  PIC S9(9) COMP VALUE 40.
       01 A1111111-LVL                  PIC S9(4) COMP VALUE 1.
       01 WS-SEGMENTS-MAX-LVL           PIC S9(4) COMP VALUE 1.
       01 WS-SEGMENTS-AREA.
         03 A1111111-AREA               PIC X(40).
       01 WS-IMS-GU                     PIC X(4)  VALUE 'GU  '.
       01 WS-IMS-GN                     PIC X(4)  VALUE 'GN  '.
       01 WS-IMS-GNP                    PIC X(4)  VALUE 'GNP '.
       01 A1111111-SSA.
         03  FILLER                   PIC X(19)
             VALUE 'A1111111(A1111111='.
         03  A1111111-KEY                PIC X(10).
         03  FILLER                   PIC X(2)  VALUE ') '.
       01 A1111111-SSA-UNQ.
         03  FILLER                   PIC X(9)
             VALUE 'A1111111 '.
      *
      *    ACCESS INFO
      *
       01 WS-SEGMENT-NAME               PIC X(8)  VALUE SPACE.
       01 WS-SEGMENT-LEVEL              PIC 9(2)  VALUE ZERO.
       01 WS-SEGMENT-LEN                PIC S9(4) COMP VALUE ZERO.
       01 WS-FUNCTION-HOLD-CLAUSE       PIC 9     VALUE ZERO.
         88 COMMAND-WITHOUT-HOLD        VALUE 0.
         88 COMMAND-WITH-HOLD           VALUE 1.
       01 SQL-CONDITION-CLAUSE.
         05 SQL-CONDITION-CLAUSE-LENGTH PIC S9(4) COMP.
         05 SQL-CONDITION-CLAUSE-TEXT   PIC X(32000).
       01 SQL-JOIN-CLAUSE.
         05 SQL-JOIN-CLAUSE-LENGTH      PIC S9(4) COMP.
         05 SQL-JOIN-CLAUSE-TEXT        PIC X(32000).
      *
      *    STORE LAST VALID ACCESS INFO
      *
       01 WS-FB-KEY-LENGTH              PIC S9(9) COMP.
       01 WS-FB-KEY-AREA                PIC X(32000).
       01 WS-DBD-NAME                   PIC X(8)  VALUE 'IVPDB2  '.
       01 WS-LAST-SEGMENT-NAME          PIC X(8).
       01 WS-LAST-SEGMENT-LEVEL         PIC S9(9) COMP.
       01 WS-LAST-IORTN-SECTION         PIC X(30).
      * NEXT SEGMENT TO READ IN AN UNQUALIFIED GN
       01 WS-NEXT-SEGMENT-NAME          PIC X(8) VALUE SPACE.
       01 WS-NEXT-FUNCTION              PIC X(20) VALUE SPACE.
       01 FILLER                        PIC X VALUE SPACE.
         88 EXEC-DLI-ACCESS             VALUE '0'.
         88 SKIP-DLI-ACCESS             VALUE '1'.
       01 FILLER                        PIC X VALUE SPACE.
         88 EXEC-SEGMENT-ACCESS         VALUE '0'.
         88 SKIP-SEGMENT-ACCESS         VALUE '1'.
       01 FILLER                        PIC X VALUE SPACE.
         88 EXEC-SAVE-AREA              VALUE '0'.
         88 SKIP-SAVE-AREA              VALUE '1'.
       01 FILLER                        PIC X VALUE SPACE.
         88 EXEC-CHECK-MISMATCH         VALUE '0'.
         88 SKIP-CHECK-MISMATCH         VALUE '1'.
       01 FILLER                        PIC X VALUE SPACE.
         88 DEFAULT-DYNAMIC-ACCESS      VALUE '0'.
         88 CUSTOM-DYNAMIC-ACCESS       VALUE '1'.
       01 FILLER                        PIC X VALUE SPACE.
         88 CUSTOM-STATIC-ACCESS        VALUE '0'.
         88 CUSTOM-CURSOR-ACCESS        VALUE '1'.
       01 WS-DBD-STATUS                 PIC S9(4) COMP.
         88 WS-KEEP-AS-IS               VALUE 0.
         88 WS-REVERT-TO-SQL-ONLY       VALUE 1.
       01 WS-SSA-ALIAS.
         03 WS-FUNC-ALIAS               PIC S9(9) COMP.
         03 FILLER                      PIC 9.
           88 IS-NOT-ALIAS                    VALUE 0.
           88 IS-ALIAS                        VALUE 1.
      *
      *    DBD DATA DICTIONARY INCLUSION
      *
           COPY IVPDB2DS.
           COPY IVPDB2DF.
      *
      *    SSA ACCESS DICTIONARY
      *
       01 MEM-SSA-KEYS.
         03 FILLER                      PIC S9(4) COMP VALUE 4.
         03 FILLER                      PIC S9(9) COMP VALUE 5.
         03 FILLER                      PIC S9(4) COMP VALUE 11.
         03 FILLER                      PIC X(120)     VALUE
            'D:A1111111:'.
         03 FILLER                      PIC X(480)     VALUE SPACE.
         03 FILLER                      PIC S9(9) COMP VALUE 4.
         03 FILLER                      PIC S9(4) COMP VALUE 11.
         03 FILLER                      PIC X(120)     VALUE
            'R:A1111111:'.
         03 FILLER                      PIC X(480)     VALUE SPACE.
         03 FILLER                      PIC S9(9) COMP VALUE 1.
         03 FILLER                      PIC S9(4) COMP VALUE 11.
         03 FILLER                      PIC X(120)     VALUE
            'I:A1111111:'.
         03 FILLER                      PIC X(480)     VALUE SPACE.
         03 FILLER                      PIC S9(9) COMP VALUE 2.
         03 FILLER                      PIC S9(4) COMP VALUE 22.
         03 FILLER                      PIC X(120)     VALUE
            'G:A1111111:A1111111:=:'.
         03 FILLER                      PIC X(480)     VALUE SPACE.
       01 TAB-MEM-SSA-KEYS REDEFINES MEM-SSA-KEYS.
         03 TAB-MEM-SSA-COUNT           PIC S9(4) COMP.
         03 FILLER                      OCCURS 4.
           05 TAB-MEM-SSA-FUNCID        PIC S9(9) COMP.
           05 TAB-MEM-SSA-LEN           PIC S9(4) COMP.
           05 TAB-MEM-SSA-TXT           PIC X(600).
      *
      *    SEGMENTS DCLGENs INCLUSION
      *
           EXEC SQL INCLUDE A11111TB END-EXEC.
      *
       LINKAGE SECTION.
      *
      *    IRIS GLOBAL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IVPDB2'==.
      *
      *    IRIS PCB AREA
      *
           COPY IRISPCB.
      *
      *    PCB INFO FOR THE SEGMENT BEING ACCESSED
      *
       03 IO-RTN-USED-KEYS-PCB-AREA REDEFINES DB-RNT-CNTL-AREA.
         05 IO-RTN-FB-KEY-MAX-LENGTH             PIC S9(9) COMP.
         05 IO-RTN-USED-KEY-SECONDARY            PIC X(255).
         05 IO-RTN-USED-KEY-GNUNQ-NEXT-SEG       PIC X(8).
         05 IO-RTN-USED-KEY-GNUNQ-SAVE-POS       PIC X(512).
         05 IO-RTN-USED-KEY-GNUNQ-NEXT-ACT REDEFINES
            IO-RTN-USED-KEY-GNUNQ-SAVE-POS       PIC X(20).
           88 IO-RTN-USED-KEY-GNUNQ-SEEK
              VALUE 'SQL-SELECT-SEEK     '.
           88 IO-RTN-USED-KEY-GNUNQ-NEXT
              VALUE 'SQL-SELECT-NEXT     '.
         05 FILLER REDEFINES IO-RTN-USED-KEY-GNUNQ-SAVE-POS.
           07 A1111111-GNUNQ-AREA.
             09 FILLER                           PIC X.
               88 A1111111-GNUNQ-FIRST-VSTD      VALUE 'X'.
             09 A1111111-GNUNQ-SESS-AREA.
               11 FILLER                         PIC X.
                 88 A1111111-GNUNQ-VSTD          VALUE 'X'.
         05 RUN-DBD-STATUS                       PIC S9(4) COMP.
           88 DUAL-IMS-ONLY                      VALUE 0.
           88 DUAL-SQL-ONLY                      VALUE 1.
           88 DUAL-BOTH                          VALUE 2.
           88 DUAL-UPDATE-ONLY                   VALUE 3.
         05 RUN-IMS-DUAL-POINTER                 POINTER.
         05 RUN-PCB-INDEX                        PIC 9(3).
         05 RUN-IO-AREA-PTR                      USAGE POINTER.
         05 INIT-DBD-STATUS                      PIC S9(4) COMP.
           88 INIT-DUAL-IMS-ONLY                 VALUE 0.
           88 INIT-DUAL-SQL-ONLY                 VALUE 1.
           88 INIT-DUAL-BOTH                     VALUE 2.
           88 INIT-DUAL-UPDATE-ONLY              VALUE 3.
         05 FILLER                               PIC X(206).
         05 IO-RTN-USED-KEY-VALUE OCCURS 8.
           07 IO-RTN-USED-SSA-INFO               PIC X(1055).
           07 IO-RTN-USED-KEY-STATUS             PIC X.
             88 IO-RTN-USED-KEY-NOT-CHANGED      VALUE '0' X'00' ' '.
             88 IO-RTN-USED-KEY-CHANGED          VALUE '1'.
             88 IO-RTN-USED-KEY-IS-INDEX         VALUE '2'.
           07 IO-RTN-USED-SSA-KEYS.
             09 IO-RTN-USED-KEY-ALPHA            PIC X(256).
             09 IO-RTN-USED-KEY-PACKED REDEFINES IO-RTN-USED-KEY-ALPHA
                                                 PIC S9(18) COMP-3.
             09 IO-RTN-USED-KEY-NUMERIC          PIC S9(9) COMP.
             09 IO-RTN-USED-KEY-NUMERIC-PREV     PIC S9(9) COMP.
             09 IO-RTN-USED-KEY-NUMERIC-NEXT     PIC S9(9) COMP.
             09 IO-RTN-USED-KEY-PARENT-ALPHA     PIC X(256).
             09 IO-RTN-USED-KEY-PARENT-NUMERIC   PIC S9(9) COMP.
      *
      *   SYSTEM FIELDS STORAGE
      *
             09 FILLER OCCURS 32.
               11 IO-RTN-USED-KEY-LAST-SX        PIC S9(9) COMP.
             09 IO-RTN-USED-LAST-SEGMENT         PIC X(8).
             09 IO-RTN-LAST-OPEN-CURSOR          PIC 9(8).
             09 FILLER                           PIC X(272).
      *  16K AVAILABLE
         05 IO-RTN-COMM-DATA                     PIC X(16000).
         05 FILLER REDEFINES IO-RTN-COMM-DATA.
           07 LAST-IMS-FUNCTION                  PIC X(4).
             88 LAST-IMS-FUNCTION-READ           VALUE 'GU  '
                                                       'GN  '
                                                       'GNP '.
             88 LAST-IMS-FUNCTION-READHLD        VALUE 'GHU '
                                                       'GHN '
                                                       'GHNP'.
           07 LAST-IMS-SEGMENT-NAME              PIC X(8).
           07 LAST-IMS-SEGMENT-LEVEL             PIC 9(9) COMP.
           07 LAST-SSA.
             09 LAST-SSA-SEGMENT                 PIC X(8).
             09 LAST-SSA-DATA                    PIC X(248).
           07 FILLER REDEFINES LAST-SSA          PIC X(256).
             88 LAST-SSA-EMPTY                   VALUE SPACE
                                                       LOW-VALUE
                                                       HIGH-VALUE.
           07 SEGMENTS-LAST-AREA.
             09 A1111111-LAST-AREA               PIC X(40).
           07 LAST-IMS-CCODE                     PIC X(240).
      *
      *    I-O USER PROGRAM AREA
       01 LK-IO-AREA                    PIC X(32000).
       01 LK-LOAD-DICTIONARY-AREA REDEFINES LK-IO-AREA.
         03 LK-LOAD-DICTIONARY-PTR      POINTER.
      *
      *    POSSIBLE SSAs
      *
       01 LK-SSA-01                     PIC X(256).
       01 LK-SSA-02                     PIC X(256).
       01 LK-SSA-03                     PIC X(256).
       01 LK-SSA-04                     PIC X(256).
       01 LK-SSA-05                     PIC X(256).
       01 LK-SSA-06                     PIC X(256).
       01 LK-SSA-07                     PIC X(256).
       01 LK-SSA-08                     PIC X(256).
       01 LK-SSA-09                     PIC X(256).
       01 LK-SSA-10                     PIC X(256).
       01 LK-SSA-11                     PIC X(256).
       01 LK-SSA-12                     PIC X(256).
       01 LK-SSA-13                     PIC X(256).
       01 LK-SSA-14                     PIC X(256).
       01 LK-SSA-15                     PIC X(256).
       01 LK-SSA-16                     PIC X(256).
      *
       01 LK-DUAL-PCB.
         03 DUAL-PCB-FIXED-PART.
           05 DUAL-DBD-NAME             PIC X(8).
           05 DUAL-SEGMENT-LEVEL        PIC 9(2).
           05 DUAL-STATUS-CODE          PIC X(2).
             88 DUAL-STATUS-CODE-OK     VALUE SPACE.
           05 DUAL-PROC-OPTS            PIC X(4).
           05 DUAL-INTERNAL-INDEX       PIC S9(9)  COMP.
           05 DUAL-SEGMENT-NAME         PIC X(8).
           05 DUAL-FB-KEY-LENGTH        PIC S9(9)  COMP.
           05 DUAL-NUM-SENSEGS          PIC S9(9)  COMP.
         03 DUAL-KEY-FB                 PIC X.
      *
       01 LK-AIB-SAVE-AREA              PIC X.
      *
      * THE NEXT AREAS ARE NECESSARY TO EXTRACT PCB NAME
      * FOR AERTDLI COMMANDS
       01 IRIS-LK-CELLS.
         03 FILLER                      OCCURS 100.
           05 IRIS-LK-POINTER           POINTER.
      *
      *    IRIS AIB STRUCTURE
      *
           COPY IRISAIB.
      *
      *    DIB BLOCK FOR EXEC DLI
      *
       01 LK-DIB-BLOCK                  PIC X(32).
      *
      *    TO EXTRACT THE PCB NAME
      *
       01 IRIS-PCB-AIB.
         03 IRIS-PCB-AIB-HEADER         PIC X(36).
         03 IRIS-PCB-AIB-FEEDBACK       PIC X(1587).
           COPY IRISISEG REPLACING ==01 MEMORY-PCB-AREA.==
                                BY ==03 MEMORY-PCB-AIB-AREA.==.
         03 IRIS-RNT-CNTL-AIB-AREA      PIC X(33000).
      *
       01 LK-IO-AREA-BACKUP             PIC X(32000).
      *
       PROCEDURE DIVISION USING IRIS-WORK-AREA
                                IRIS-DB-PCB
                                LK-IO-AREA
                                LK-SSA-01
                                LK-SSA-02
                                LK-SSA-03
                                LK-SSA-04
                                LK-SSA-05
                                LK-SSA-06
                                LK-SSA-07
                                LK-SSA-08
                                LK-SSA-09
                                LK-SSA-10
                                LK-SSA-11
                                LK-SSA-12
                                LK-SSA-13
                                LK-SSA-14
                                LK-SSA-15
                                LK-SSA-16.
      *
       MAIN-PROGRAM.
      *
           IF IRIS-AERTDLI
             PERFORM EXTRACT-PCB-FROM-AIB
           END-IF
      *
           IF IRIS-EXECDLI
             PERFORM EXTRACT-PCB-EXEC
           END-IF
      *
           IF IRIS-TRACE-FULL
           OR IRIS-TRACE-PERFORMANCE
             MOVE 1 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IVPDB2IO:START' NL
                    ' DBD        =(IVPDB2) ' NL
                    ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                    ' CALLER ID  =(' IRIS-CALL-ID ') ' NL
             DELIMITED BY SIZE INTO IRIS-MSG-TXT
             POINTER IRIS-MSG-LEN
             IF DB-PCB-IRIS-EYE
               STRING ' PCB INDEX  =(' RUN-PCB-INDEX ') ' NL
               DELIMITED BY SIZE INTO IRIS-MSG-TXT
               POINTER IRIS-MSG-LEN
             END-IF
             STRING ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                    ' SEGMENT    =(' IRIS-SEGMENT ') ' NL
                    ' FUNCTION   =(' IRIS-FUNCTION ') ' NL
                    ' CUSTOM ID  =(' IRIS-CUSTOM-FUNC-ID ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             POINTER IRIS-MSG-LEN
             MOVE 0 TO IRIS-MSG-LEN
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             PERFORM PRINT-SSAS
           END-IF
      *
           IF NOT DB-PCB-IRIS-EYE
             PERFORM DLI-ACCESS
             IF IRIS-TRACE-FULL
               MOVE 0 TO IRIS-MSG-LEN
               IF IRIS-AERTDLI
                 SET ADDRESS OF IRIS-AIB TO ADDRESS OF IRIS-DB-PCB
                 SET ADDRESS OF LK-DUAL-PCB TO IRIS-AIBRESA1
                 STRING '<IRISTRACE> - IVPDB2IO:END' NL
                        ' DBD        =(IVPDB2) ' NL
                        ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                        ' CALLER ID  =(' IRIS-CALL-ID ') ' NL
                        ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                        ' CUSTOM ID  =(' IRIS-CUSTOM-FUNC-ID ') ' NL
                        ' SECTION    =(' WS-LAST-IORTN-SECTION ') ' NL
                        ' PCB AREA   =(' LK-DUAL-PCB ') ' NL
                        ' I-O AREA   =(' LK-IO-AREA(1:20)') '
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               ELSE
                 STRING '<IRISTRACE> - IVPDB2IO:END' NL
                        ' DBD        =(IVPDB2) ' NL
                        ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                        ' CALLER ID  =(' IRIS-CALL-ID ') ' NL
                        ' PCB INDEX  =(' RUN-PCB-INDEX ') ' NL
                        ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                        ' CUSTOM ID  =(' IRIS-CUSTOM-FUNC-ID ') ' NL
                        ' SEGMENT    =(' WS-SEGMENT-NAME ') ' NL
                        ' SECTION    =(' WS-LAST-IORTN-SECTION ') ' NL
                        ' PCB AREA   =(' DB-PCB-FIXED-PART ') ' NL
                        ' I-O AREA   =(' LK-IO-AREA(1:20)') '
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               END-IF
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
             IF IRIS-EXECDLI
               PERFORM SET-DIB-BLOCK
             END-IF
             GOBACK
           END-IF
      *
           PERFORM INIT-VARIABLES
      *
           IF SQL-DYNAMIC-SSA
             COMPUTE IRIS-SSAS-NUM = IRIS-PARAM-NUM - 3
             MOVE WS-DBD-NAME TO IRIS-SSA-PROCSEQ
             SET IRIS-DYNSSA-EXECUTE TO TRUE
             SET IRISPSSA-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                    IRIS-DB-PCB
                                    IRIS-SSA-AREA
                                    MEM-SSA-KEYS
                                    LK-SSA-01 LK-SSA-02 LK-SSA-03
                                    LK-SSA-04 LK-SSA-05 LK-SSA-06
                                    LK-SSA-07 LK-SSA-08 LK-SSA-09
                                    LK-SSA-10 LK-SSA-11 LK-SSA-12
                                    LK-SSA-13 LK-SSA-14 LK-SSA-15
                                    LK-SSA-16
      * SPECIAL TRACE FOR IRIS TRANSFORMATION FROM DYNAMIC TO CUSTOM
             IF IRIS-TRACE-STANDARD
               MOVE 0 TO IRIS-MSG-LEN
               STRING '<IRISDYN2CUS> IVPDB2IO:' IRIS-PROGRAM-NAME
                      ':' IRIS-CALL-ID ':' RUN-PCB-INDEX
                      ':' IRIS-IMS-FUNCTION ':' IRIS-SEGMENT
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               PERFORM VARYING WS-IDX FROM 1 BY 1
               UNTIL WS-IDX > IRIS-SSA-COUNT
                 MOVE 0 TO IRIS-MSG-LEN
                 MOVE IRIS-SSA-LEN(WS-IDX) TO WS-LEN
                 STRING '<IRISDYN2CUS> SSA-' WS-IDX
                        ':' IRIS-SSA-TXT(WS-IDX)(1:WS-LEN)
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                 SET IRISTRAC-RTN TO TRUE
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               END-PERFORM
             END-IF
             IF IRIS-TRACE-FULL
               MOVE 0 TO IRIS-MSG-LEN
               MOVE IRIS-ERR-MESSAGE-ID TO WS-ZONED-FIELD-2
               STRING '<IRISTRACE> - IVPDB2IO:DYNAMIC-SSA' NL
                      ' DBD        =(IVPDB2) ' NL
                      ' CMDS COUNT =(' IRIS-SSA-CMDS-COUNT ') ' NL
                      ' SSA SEGM   =(' IRIS-SSA-SEGMENT ') ' NL
                      ' SSA FUNC   =(' IRIS-SSA-FUNCTION(1) ') ' NL
                      ' SSA FUNC ID=(' IRIS-SSA-CUSTOM-FUNC-ID ') ' NL
                      ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                      ' IMS CODE   =(' DB-PCB-STATUS-CODE ') ' NL
                      ' ERROR ID   =(' WS-ZONED-FIELD-2 ') ' NL
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               PERFORM VARYING WS-IDX FROM 1 BY 1
               UNTIL WS-IDX > IRIS-SSA-COUNT
                 MOVE 0 TO IRIS-MSG-LEN
                 MOVE WS-IDX TO WS-ZONED-FIELD-2
                 MOVE IRIS-SSA-LEN(WS-IDX) TO WS-LEN
                 STRING '<IRISTRACE> - IVPDB2IO:DYNAMIC-SSA' NL
                   ' PGM SSA NUM=(' WS-ZONED-FIELD-2 ') ' NL
                   ' PGM SSA VAL=(' IRIS-SSA-TXT(WS-IDX)(1:WS-LEN) ') '
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                 SET IRISTRAC-RTN TO TRUE
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               END-PERFORM
               PERFORM VARYING WS-IDX FROM 1 BY 1
               UNTIL WS-IDX > IRIS-SSA-CMDS-COUNT
                 MOVE 0 TO IRIS-MSG-LEN
                 MOVE WS-IDX TO WS-ZONED-FIELD-2
                 MOVE IRIS-SSA-SQL-LEN(WS-IDX) TO WS-LEN
                 STRING '<IRISTRACE> - IVPDB2IO:DYNAMIC-SSA' NL
                    ' DYN SSA NUM=(' WS-ZONED-FIELD-2 ') ' NL
                    ' DYN SSA FUN=(' IRIS-SSA-FUNCTION(WS-IDX) ') ' NL
                    ' DYN-SSA-SQL=('
                                IRIS-SSA-SQL-TXT(WS-IDX)(1:WS-LEN) ')'
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                 SET IRISTRAC-RTN TO TRUE
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               END-PERFORM
             END-IF
             IF IRIS-NO-ERROR
               EVALUATE TRUE
               WHEN IRIS-SSA-CMDS-COUNT > ZERO
                 OR SSA-SELECT-NEXT(1)
                 MOVE IRIS-SSA-SEGMENT TO WS-SEGMENT-NAME
                 MOVE IRIS-SSA-FUNCTION(1) TO IRIS-FUNCTION
                 MOVE IRIS-SSA-SQL-LEN(1)
                             TO SQL-CONDITION-CLAUSE-LENGTH
                 MOVE IRIS-SSA-SQL-TXT(1)
                             TO SQL-CONDITION-CLAUSE-TEXT
                 ADD 1 TO SQL-CONDITION-CLAUSE-LENGTH
               WHEN SSA-USER-CUSTOM(1)
                 MOVE IRIS-SSA-SEGMENT TO WS-SEGMENT-NAME
                 MOVE IRIS-SSA-CUSTOM-FUNC-ID TO IRIS-CUSTOM-FUNC-ID
                 MOVE IRIS-SSA-FUNCTION(1) TO IRIS-FUNCTION
               WHEN OTHER
                 SET IRIS-ERR-DYNAMIC-SSA-FAILURE TO TRUE
               END-EVALUATE
             END-IF
           END-IF
      *
           EVALUATE TRUE
      *
           WHEN LOAD-DATA-DICTIONARY-SEG
      *
             SET IRISADDR-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                    MEM-DBD-SEGMS-IVPDB2
                                    LK-LOAD-DICTIONARY-PTR
             SET SKIP-SEGMENT-ACCESS TO TRUE
             SET SKIP-DLI-ACCESS TO TRUE
      *
           WHEN LOAD-DATA-DICTIONARY-FLD
      *
             SET IRISADDR-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                    MEM-DBD-FLDS-IVPDB2
                                    LK-LOAD-DICTIONARY-PTR
             SET SKIP-SEGMENT-ACCESS TO TRUE
             SET SKIP-DLI-ACCESS TO TRUE
           WHEN SQL-USER-CUSTOM
      *
           IF IRIS-TRACE-PERFORMANCE
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - BEFORE SQL ACCESS'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           EVALUATE IRIS-CUSTOM-FUNC-ID
           WHEN 0
             SET IRIS-ERR-FUNCTION-NOT-FOUND TO TRUE
      * IRISDB - Segm: A1111111, Op: ISRT, RevId: 1
           WHEN 1
             SET SQL-INSERT TO TRUE
      * IRISDB - Segm: A1111111, Op: GU, RevId: 2
           WHEN 2
      * IRISDB - Segm: A1111111, Op: GHU, RevId: 3
           WHEN 3
             IF IRIS-FUNC-GU OR IRIS-FUNC-GHU
             OR WS-SEGMENT-NAME NOT =
               IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
               MOVE IRIS-KEYVALUE(1, 1)(1:10) TO
                      IO-RTN-USED-KEY-ALPHA(1)(1:10)
               SET SQL-SELECT-PRIMARY TO TRUE
             ELSE
               SET SKIP-SEGMENT-ACCESS TO TRUE
               SET IRIS-SQL-NOT-FOUND TO TRUE
               MOVE IRIS-DB-SQLCODE TO IRIS-SQLCODE
               GO TO SKIP-FUNCTION
             END-IF
      * IRISDB - Segm: A1111111, Op: REPL, RevId: 4
           WHEN 4
             IF IRIS-FUNC-GU OR IRIS-FUNC-GHU
             OR WS-SEGMENT-NAME NOT =
               IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
               SET SQL-SELECT-SEEK TO TRUE
             ELSE
               SET SQL-SELECT-NEXT TO TRUE
             END-IF
             SET SQL-UPDATE TO TRUE
      * IRISDB - Segm: A1111111, Op: DLET, RevId: 5
           WHEN 5
             SET SQL-DELETE TO TRUE
           WHEN OTHER
             SET IRIS-ERR-FUNCTION-NOT-FOUND TO TRUE
           END-EVALUATE
      *
           WHEN SQL-SELECT-ALL-UNQ
             IF WS-SEGMENT-NAME = SPACE
               MOVE 'A1111111' TO WS-SEGMENT-NAME
               MOVE SPACE TO IO-RTN-USED-KEY-GNUNQ-NEXT-SEG
                             IO-RTN-USED-KEY-GNUNQ-SAVE-POS
             END-IF
           END-EVALUATE.
      *
       SKIP-FUNCTION.
      *
           IF IRIS-NO-ERROR
           AND NOT SKIP-SEGMENT-ACCESS
             EVALUATE WS-SEGMENT-NAME
             WHEN 'A1111111'
      *
               PERFORM HANDLE-SEGMENT-A1111111
      *
             WHEN OTHER
               SET IRIS-ERR-RTN-SEGMENT-NOT-FOUND TO TRUE
             END-EVALUATE
      *
           END-IF
           IF IRIS-TRACE-PERFORMANCE
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - AFTER  SQL ACCESS'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           PERFORM FINALIZE-VARIABLES
      *
      *      HANDLE DUAL ACCESS
      *
           IF NOT DB-STATUS-INTERNAL-NOT-HANDLED
           AND NOT IRIS-ERR-UNHANDLED-SQLCODE
           AND NOT SKIP-DLI-ACCESS
           AND (DUAL-BOTH OR DUAL-UPDATE-ONLY)
             PERFORM DLI-ACCESS
           END-IF
      *
           IF IRIS-TRACE-STANDARD
             MOVE 80 TO WS-LEN
             IF WS-SEGMENT-LEN < WS-LEN
               MOVE WS-SEGMENT-LEN TO WS-LEN
             ELSE
               IF IRIS-TRACE-FULL
                 MOVE WS-SEGMENT-LEN TO WS-LEN
               END-IF
             END-IF
             IF WS-LEN = ZERO
               MOVE 1 TO WS-LEN
             END-IF
             MOVE IRIS-SQLCODE TO IRIS-DB-SQLCODE
             IF NOT IRIS-SQL-OK AND NOT IRIS-SQL-NOT-FOUND
               MOVE 0 TO IRIS-MSG-LEN
               MOVE IRIS-SQLCODE TO WS-SQLCODE-N
               MOVE WS-SQLCODE-N TO WS-SQLCODE-E
               STRING '<IRISTRACE> - IVPDB2IO:SQL_RC' NL
                    ' SQLCODE    =(' WS-SQLCODE-E ')' NL
                    ' SQLERRM    =(' IRIS-SQLERRMC(1:IRIS-SQLERRML) ')'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
             MOVE 0 TO IRIS-MSG-LEN
             IF IRIS-NO-ERROR AND IRIS-TRACE-FULL
               STRING '<IRISTRACE> - IVPDB2IO:END' NL
                      ' DBD        =(IVPDB2) ' NL
                      ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                      ' CALLER ID  =(' IRIS-CALL-ID ') ' NL
                      ' PCB INDEX  =(' RUN-PCB-INDEX ') ' NL
                      ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                      ' CUSTOM ID  =(' IRIS-CUSTOM-FUNC-ID ') ' NL
                      ' SEGMENT    =(' WS-SEGMENT-NAME ') ' NL
                      ' SECTION    =(' WS-LAST-IORTN-SECTION ') ' NL
                      ' PCB AREA   =(' DB-PCB-FIXED-PART ') ' NL
                      ' KFB AREA   =(' DB-PCB-KEY-FB
                                   (1:DB-PCB-FB-KEY-LENGTH) ') ' NL
                      ' I-O AREA   =(' LK-IO-AREA(1:WS-LEN)') '
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             ELSE
               IF NOT IRIS-NO-ERROR
                 MOVE 1 TO WS-ERROR-DESCRIPTION-LEN
                 STRING IRIS-ERROR-DESCRIPTION(IRIS-ERR-MESSAGE-ID)
                 DELIMITED BY '_'
                 INTO WS-ERROR-DESCRIPTION
                 POINTER WS-ERROR-DESCRIPTION-LEN
                 SUBTRACT 1 FROM WS-ERROR-DESCRIPTION-LEN
                 MOVE IRIS-ERR-MESSAGE-ID TO WS-MESSAGE-ID-EDITED
                 STRING '<IRISTRACE> - IVPDB2IO:END' NL
                        ' DBD        =(IVPDB2) ' NL
                        ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                        ' CALLER ID  =(' IRIS-CALL-ID ') ' NL
                        ' PCB INDEX  =(' RUN-PCB-INDEX ') ' NL
                        ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                        ' CUSTOM ID  =(' IRIS-CUSTOM-FUNC-ID ') ' NL
                        ' SEGMENT    =(' WS-SEGMENT-NAME ') ' NL
                        ' SECTION    =(' WS-LAST-IORTN-SECTION ') ' NL
                        ' ERROR ID   =(' WS-MESSAGE-ID-EDITED ') ' NL
                        ' ERROR DESCR=('
                 WS-ERROR-DESCRIPTION(1:WS-ERROR-DESCRIPTION-LEN)') ' NL
                        ' PCB AREA   =(' DB-PCB-FIXED-PART ') ' NL
                        ' I-O AREA   =(' LK-IO-AREA(1:WS-LEN) ') '
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                 SET IRISTRAC-RTN TO TRUE
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               END-IF
             END-IF
           ELSE
      *    IN CASE OF INTERNAL ERROR EMIT ALWAYS A MESSAGE TO CONSOLE
             IF DB-STATUS-INTERNAL-NOT-HANDLED
             OR IRIS-ERR-UNHANDLED-SQLCODE
               MOVE 1 TO WS-ERROR-DESCRIPTION-LEN
               STRING IRIS-ERROR-DESCRIPTION(IRIS-ERR-MESSAGE-ID)
               DELIMITED BY '_'
               INTO WS-ERROR-DESCRIPTION
               POINTER WS-ERROR-DESCRIPTION-LEN
               SUBTRACT 1 FROM WS-ERROR-DESCRIPTION-LEN
               MOVE 0 TO IRIS-MSG-LEN
               MOVE IRIS-SQLCODE TO WS-SQLCODE-N
               MOVE WS-SQLCODE-N TO WS-SQLCODE-E
               MOVE IRIS-ERR-MESSAGE-ID TO WS-MESSAGE-ID-EDITED
               STRING '<IRISTRACE> - IVPDB2IO:END' NL
                      ' DBD        =(IVPDB2) ' NL
                      ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                      ' CALLER ID  =(' IRIS-CALL-ID ') ' NL
                      ' PCB INDEX  =(' RUN-PCB-INDEX ') ' NL
                      ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                      ' CUSTOM ID  =(' IRIS-CUSTOM-FUNC-ID ') ' NL
                      ' SEGMENT    =(' WS-SEGMENT-NAME ') ' NL
                      ' SECTION    =(' WS-LAST-IORTN-SECTION ') ' NL
                      ' ERROR ID   =(' WS-MESSAGE-ID-EDITED ') ' NL
                      ' ERROR DESCR=('
               WS-ERROR-DESCRIPTION(1:WS-ERROR-DESCRIPTION-LEN) ') ' NL
                    ' SQLCODE    =(' WS-SQLCODE-E ') ' NL
                    ' SQLERRM    =(' IRIS-SQLERRMC(1:IRIS-SQLERRML)') '
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
           END-IF
           MOVE IRIS-IMS-FUNCTION TO LAST-IMS-FUNCTION
           MOVE WS-LAST-SEGMENT-NAME TO LAST-IMS-SEGMENT-NAME
           MOVE WS-LAST-SEGMENT-LEVEL TO LAST-IMS-SEGMENT-LEVEL
           MOVE IRIS-KEYVALUE-TAB(1:240) TO LAST-IMS-CCODE
      *
           IF DUAL-UPDATE-ONLY
             IF IRIS-IMS-FUNCTION = 'GU  ' OR 'GHU '
               MOVE SPACE TO LAST-SSA
             END-IF
             PERFORM STORE-SSA
           END-IF
      *
           IF IRIS-AERTDLI
      *      SET THE ADDRESS OF IRIS PCB TO THE AIB FIELD
             SET IRIS-AIBRESA1 TO ADDRESS OF IRIS-DB-PCB
      *      RESTORE THE AIB INTO THE SECOND LINKAGE PARM
             SET ADDRESS OF IRIS-DB-PCB TO ADDRESS OF IRIS-AIB
           END-IF
      *
           IF IRIS-EXECDLI
             PERFORM SET-DIB-BLOCK
           END-IF.
      *
       MAIN-PROGRAM-EX.
      *
           GOBACK.
      *
      ******************************************************************
      *                                                                *
      *                    P R O G R A M    E N D                      *
      *                                                                *
      ******************************************************************
      *
      ******************************************************************
      *    ACCESS FOR DLI
      ******************************************************************
      *
       DLI-ACCESS SECTION.
      *
           MOVE 'DLI-ACCESS' TO WS-LAST-IORTN-SECTION
      *
           IF NOT DB-PCB-IRIS-EYE
             IF IRIS-AERTDLI
      *        RESTORE THE AIB INTO THE SECOND LINKAGE PARM
               SET ADDRESS OF IRIS-DB-PCB TO ADDRESS OF IRIS-AIB
             END-IF
             IF IRIS-TRACE-PERFORMANCE
               MOVE 0 TO IRIS-MSG-LEN
               STRING '<IRISTRACE> - BEFORE DLI ACCESS'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
             MOVE IRIS-IMS-API TO WS-IMS-API
             IF IRIS-EXECDLI
               SET IRIS-CBLTDLI TO TRUE
             END-IF
             CALL IRIS-IMS-API USING IRIS-PARAM-NUM
                                     IRIS-IMS-FUNCTION
                                     IRIS-DB-PCB
                                     LK-IO-AREA
                                     LK-SSA-01
                                     LK-SSA-02
                                     LK-SSA-03
                                     LK-SSA-04
                                     LK-SSA-05
                                     LK-SSA-06
                                     LK-SSA-07
                                     LK-SSA-08
                                     LK-SSA-09
                                     LK-SSA-10
                                     LK-SSA-11
                                     LK-SSA-12
                                     LK-SSA-13
                                     LK-SSA-14
                                     LK-SSA-15
                                     LK-SSA-16
             MOVE WS-IMS-API TO IRIS-IMS-API
             IF IRIS-TRACE-PERFORMANCE
               MOVE 0 TO IRIS-MSG-LEN
               STRING '<IRISTRACE> - AFTER  DLI ACCESS'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
             GO TO DLI-ACCESS-EX
      *
           END-IF
      *
           IF IRIS-AERTDLI
             SET ADDRESS OF LK-DUAL-PCB TO ADDRESS OF IRIS-AIB
           ELSE
             SET ADDRESS OF LK-DUAL-PCB TO RUN-IMS-DUAL-POINTER
           END-IF
           IF DUAL-UPDATE-ONLY
             EVALUATE TRUE
             WHEN IRIS-FUNC-GHNP
             WHEN IRIS-FUNC-GHN
               IF WS-LAST-SEGMENT-NAME = LAST-IMS-SEGMENT-NAME
                 PERFORM FORCE-LEVEL-POSITION
               ELSE
                 IF LAST-IMS-SEGMENT-LEVEL < WS-LAST-SEGMENT-LEVEL
                   PERFORM FORCE-PARENT-POSITION
                 ELSE
                   PERFORM FORCE-LEVEL-POSITION
                 END-IF
               END-IF
             WHEN IRIS-FUNC-ISRT
               PERFORM FORCE-LEVEL-POSITION
             END-EVALUATE
           END-IF
           MOVE LK-IO-AREA(1:WS-SEGMENT-LEN) TO WS-IO-AREA
      *
           IF IRIS-TRACE-PERFORMANCE
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - BEFORE DLI ACCESS'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           MOVE IRIS-IMS-API TO WS-IMS-API
           IF IRIS-EXECDLI
             SET IRIS-CBLTDLI TO TRUE
           END-IF
           CALL IRIS-IMS-API USING IRIS-PARAM-NUM
                                   IRIS-IMS-FUNCTION
                                   LK-DUAL-PCB
                                   WS-IO-AREA
                                   LK-SSA-01
                                   LK-SSA-02
                                   LK-SSA-03
                                   LK-SSA-04
                                   LK-SSA-05
                                   LK-SSA-06
                                   LK-SSA-07
                                   LK-SSA-08
                                   LK-SSA-09
                                   LK-SSA-10
                                   LK-SSA-11
                                   LK-SSA-12
                                   LK-SSA-13
                                   LK-SSA-14
                                   LK-SSA-15
                                   LK-SSA-16
           MOVE WS-IMS-API TO IRIS-IMS-API
           IF IRIS-TRACE-PERFORMANCE
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - AFTER  DLI ACCESS'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           IF IRIS-AERTDLI
             SET ADDRESS OF LK-AIB-SAVE-AREA TO ADDRESS OF IRIS-AIB
             SET ADDRESS OF IRIS-AIB TO ADDRESS OF LK-DUAL-PCB
             SET ADDRESS OF LK-DUAL-PCB TO IRIS-AIBRESA1
           END-IF
           IF ((DB-PCB-STATUS-CODE NOT = DUAL-STATUS-CODE
           OR WS-IO-AREA(1:WS-SEGMENT-LEN) NOT =
                                          LK-IO-AREA(1:WS-SEGMENT-LEN))
      * 'GB' AND 'GE' ARE CONSIDERED THE SAME
                   AND (DB-PCB-STATUS-CODE NOT = 'GE' AND 'GB'
                     OR DUAL-STATUS-CODE NOT = 'GE' AND 'GB'))
           AND ( DB-PCB-FIXED-PART(1:12) NOT = DUAL-PCB-FIXED-PART(1:12)
           AND  DB-PCB-FIXED-PART(21:8) NOT =
                                              DUAL-PCB-FIXED-PART(21:8))
                 MOVE 1 TO IRIS-MSG-LEN
                 STRING '<IRISTRACE> - IVPDB2IO:WARNING' NL
                      ' WARNING    =(DLI PCB MISMATCH) ' NL
                      ' SQL RC     =(' DB-PCB-STATUS-CODE ') ' NL
                      ' DLI RC     =(' DUAL-STATUS-CODE ') ' NL
                      ' SQL PCB    =(' DB-PCB-FIXED-PART ') ' NL
                      ' DLI PCB    =(' DUAL-PCB-FIXED-PART ') ' NL
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                 POINTER IRIS-MSG-LEN
                 SET IRISTRAC-RTN TO TRUE
                 MOVE 0 TO IRIS-MSG-LEN
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           IF ((DB-PCB-STATUS-CODE NOT = DUAL-STATUS-CODE
           OR WS-IO-AREA(1:WS-SEGMENT-LEN) NOT =
                                          LK-IO-AREA(1:WS-SEGMENT-LEN))
      * 'GB' AND 'GE' ARE CONSIDERED THE SAME
                   AND (DB-PCB-STATUS-CODE NOT = 'GE' AND 'GB'
                     OR DUAL-STATUS-CODE NOT = 'GE' AND 'GB'))
      * REQUESTED CHECK OF AREA FOR GE/GB
           OR (WS-IO-AREA(1:WS-SEGMENT-LEN) NOT =
                                           LK-IO-AREA(1:WS-SEGMENT-LEN)
           AND EXEC-CHECK-MISMATCH  )
             MOVE 1 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IVPDB2IO:DLI-ACCESS' NL
             DELIMITED BY SIZE INTO IRIS-MSG-TXT
             POINTER IRIS-MSG-LEN
             IF DB-PCB-STATUS-CODE NOT = DUAL-STATUS-CODE
               STRING ' ERROR      =(DLI STATUS CODES MISMATCH) ' NL
               DELIMITED BY SIZE INTO IRIS-MSG-TXT
               POINTER IRIS-MSG-LEN
             ELSE
               STRING ' ERROR      =(IO AREAS MISMATCH) ' NL
               DELIMITED BY SIZE INTO IRIS-MSG-TXT
               POINTER IRIS-MSG-LEN
             END-IF
             STRING ' DBD        =(IVPDB2) ' NL
                  ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                  ' CALLER ID  =(' IRIS-CALL-ID ') ' NL
                  ' PCB INDEX  =(' RUN-PCB-INDEX ') ' NL
                  ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                  ' SEGMENT    =(' WS-SEGMENT-NAME ') ' NL
                  ' FUNCTION   =(' IRIS-FUNCTION ') ' NL
                  ' CUSTOM ID  =(' IRIS-CUSTOM-FUNC-ID ') '
                  ' SECTION    =(' WS-LAST-IORTN-SECTION ') ' NL
                  ' SQL RC     =(' DB-PCB-STATUS-CODE ') ' NL
                  ' DLI RC     =(' DUAL-STATUS-CODE ') ' NL
                  ' SQL I/O    =(' LK-IO-AREA(1:WS-SEGMENT-LEN) ')' NL
                  ' DLI I/O    =(' WS-IO-AREA(1:WS-SEGMENT-LEN) ')' NL
                  ' SQL PCB    =(' DB-PCB-FIXED-PART ') ' NL
                  ' DLI PCB    =(' DUAL-PCB-FIXED-PART ') ' NL
                  ' SQL KFB    =(' DB-PCB-KEY-FB
                                 (1:WS-FB-KEY-LENGTH) ') ' NL
                  ' DLI KFB    =(' DUAL-KEY-FB
                                 (1:WS-FB-KEY-LENGTH) ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             POINTER IRIS-MSG-LEN
             MOVE 0 TO IRIS-MSG-LEN
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             IF SQL-SELECT-DYNAMIC
               MOVE 0 TO IRIS-MSG-LEN
               STRING ' CONDITION  =(' WS-WHERE(1:WS-WHERE-LEN) ')' NL
                    ' ORDER BY   =(' WS-ORDERBY(1:WS-ORDERBY-LEN) ')'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
             IF DB-PCB-STATUS-CODE NOT = DUAL-STATUS-CODE
               SET DB-STATUS-DUAL-PCB-MISMATCH TO TRUE
             ELSE
               SET DB-STATUS-DUAL-IOAREA-MISMATCH TO TRUE
             END-IF
           ELSE
             IF IRIS-TRACE-FULL
               MOVE 0 TO IRIS-MSG-LEN
               STRING '<IRISTRACE> - IVPDB2IO:DLI-ACCESS'
                  ' PERFECT MATCH!'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
           END-IF
      *
           IF WS-REVERT-TO-SQL-ONLY
             SET DUAL-SQL-ONLY TO TRUE
           END-IF.
      *
       DLI-ACCESS-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    PRINT SSAS CONTENT
      ******************************************************************
      *
       PRINT-SSAS SECTION.
      *
           MOVE 'PRINT-SSAS' TO WS-LAST-IORTN-SECTION
      *
           IF IRIS-PARAM-NUM > 3
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-01(WS-IDX:1) = '('
             OR LK-SSA-01(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-01(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-01(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA01=(' LK-SSA-01(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 4
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-02(WS-IDX:1) = '('
             OR LK-SSA-02(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-02(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-02(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA02=(' LK-SSA-02(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 5
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-03(WS-IDX:1) = '('
             OR LK-SSA-03(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-03(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-03(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA03=(' LK-SSA-03(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 6
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-04(WS-IDX:1) = '('
             OR LK-SSA-04(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-04(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-04(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA04=(' LK-SSA-04(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 7
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-05(WS-IDX:1) = '('
             OR LK-SSA-05(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-05(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-05(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA05=(' LK-SSA-05(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 8
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-06(WS-IDX:1) = '('
             OR LK-SSA-06(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-06(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-06(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA06=(' LK-SSA-06(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 9
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-07(WS-IDX:1) = '('
             OR LK-SSA-07(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-07(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-07(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA07=(' LK-SSA-07(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 10
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-08(WS-IDX:1) = '('
             OR LK-SSA-08(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-08(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-08(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA08=(' LK-SSA-08(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 11
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-09(WS-IDX:1) = '('
             OR LK-SSA-09(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-09(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-09(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA09=(' LK-SSA-09(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 12
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-10(WS-IDX:1) = '('
             OR LK-SSA-10(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-10(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-10(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA10=(' LK-SSA-10(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 13
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-11(WS-IDX:1) = '('
             OR LK-SSA-11(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-11(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-11(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA11=(' LK-SSA-11(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 14
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-12(WS-IDX:1) = '('
             OR LK-SSA-12(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-12(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-12(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA12=(' LK-SSA-12(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 15
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-13(WS-IDX:1) = '('
             OR LK-SSA-13(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-13(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-13(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA13=(' LK-SSA-13(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 16
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-14(WS-IDX:1) = '('
             OR LK-SSA-14(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-14(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-14(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA14=(' LK-SSA-14(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 17
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-15(WS-IDX:1) = '('
             OR LK-SSA-15(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-15(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-15(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA15=(' LK-SSA-15(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           IF IRIS-PARAM-NUM > 18
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             PERFORM VARYING WS-IDX FROM 9 BY 1 UNTIL WS-IDX > 540
             OR LK-SSA-16(WS-IDX:1) = '('
             OR LK-SSA-16(WS-IDX:1) = ' '
               CONTINUE
             END-PERFORM
             IF LK-SSA-16(WS-IDX:1) = '('
               PERFORM VARYING WS-IDX FROM WS-IDX BY 1
               UNTIL WS-IDX > 540
               OR LK-SSA-16(WS-IDX:1) = ')'
                 CONTINUE
               END-PERFORM
             END-IF
             STRING '<IRISTRACE> - IVPDB2IO'
                                   ':SSA16=(' LK-SSA-16(1:WS-IDX) ')'
             DELIMITED BY SIZE INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           .
      *
       PRINT-SSAS-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    EXTRACT PCB FROM AIB STACK
      ******************************************************************
      *
       EXTRACT-PCB-FROM-AIB SECTION.
      *
           MOVE 'EXTRACT-PCB-FROM-AIB' TO WS-LAST-IORTN-SECTION
      *
           SET WS-NOT-FOUND TO TRUE
           SET ADDRESS OF IRIS-AIB TO ADDRESS OF IRIS-DB-PCB
           SET ADDRESS OF IRIS-LK-CELLS TO IRIS-AIB-POINTER
           PERFORM VARYING WS-INDEX FROM 1 BY 1
                   UNTIL WS-INDEX > IRIS-AIB-PCBS-COUNT
             SET ADDRESS OF IRIS-PCB-AIB TO IRIS-LK-POINTER(WS-INDEX)
             IF DT-PCB-NAME = IRIS-AIBRSNM1
               IF IRIS-TRACE-STANDARD
                 DISPLAY 'AERTDLI: PCB ' IRIS-AIBRSNM1 ' USED FOR DB2'
               END-IF
      *        OVERRIDE THE ADDRESS OF THE AIB WITH THE PCB
               SET ADDRESS OF IRIS-DB-PCB TO IRIS-LK-POINTER(WS-INDEX)
               SET WS-FOUND TO TRUE
               MOVE IRIS-PARAM-NUM TO WS-INDEX
             END-IF
           END-PERFORM
           IF WS-NOT-FOUND
             IF IRIS-TRACE-STANDARD
               DISPLAY 'AERTDLI: PCB ' IRIS-AIBRSNM1 ' USED FOR IMS'
             END-IF
           END-IF
           .
      *
       EXTRACT-PCB-FROM-AIB-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    EXTRACT PCB WHEN EXEC DLI CASE
      ******************************************************************
      *
       EXTRACT-PCB-EXEC SECTION.
      *
           MOVE 'EXTRACT-PCB-EXEC' TO WS-LAST-IORTN-SECTION
      *
           SET WS-NOT-FOUND TO TRUE
           SET ADDRESS OF IRIS-LK-CELLS TO IRIS-EXEC-DLI-PTR
           SET ADDRESS OF LK-DIB-BLOCK TO ADDRESS OF IRIS-DB-PCB
           MOVE LK-DIB-BLOCK TO IRIS-DIB-BLOCK
CHECK      SET ADDRESS OF IRIS-DB-PCB TO
CHECK          IRIS-LK-POINTER(IRIS-PCB-NUM + 1)
CHECK *         IRIS-LK-POINTER(IRIS-PCB-NUM)
           PERFORM BUILD-SSAS
           .
      *
       EXTRACT-PCB-EXEC-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    INITIALIZE VARIABLES
      ******************************************************************
      *
       INIT-VARIABLES SECTION.
      *
           MOVE 'INIT-VARIABLES' TO WS-LAST-IORTN-SECTION
      *
           MOVE IRIS-SEGMENT TO WS-SEGMENT-NAME
           IF IRIS-FUNC-REPL
             MOVE LAST-IMS-CCODE TO IRIS-KEYVALUE-TAB(1:240)
           END-IF
           IF IRIS-FUNC-ISRT
           OR IRIS-FUNC-REPL
             MOVE 1 TO WS-CREATION-USER-LEN
                       WS-LAST-UPDATE-USER-LEN
                       WS-CREATION-TRAN-LEN
                       WS-LAST-UPDATE-TRAN-LEN
             MOVE SPACE TO WS-CREATION-USER-TXT
                           WS-LAST-UPDATE-USER-TXT
                           WS-CREATION-TRAN-TXT
                           WS-LAST-UPDATE-TRAN-TXT
             IF IRIS-CREATION-USER = SPACE
               MOVE 'IVPDB2IO' TO WS-AUDIT-DUMMY
             ELSE
               MOVE IRIS-CREATION-USER TO WS-AUDIT-DUMMY
             END-IF
             STRING WS-AUDIT-DUMMY DELIMITED BY SPACE
             INTO WS-CREATION-USER-TXT POINTER WS-CREATION-USER-LEN
             SUBTRACT 1 FROM WS-CREATION-USER-LEN
             IF IRIS-LAST-UPDATE-USER = SPACE
               MOVE 'IVPDB2IO' TO WS-AUDIT-DUMMY
             ELSE
               MOVE IRIS-LAST-UPDATE-USER TO WS-AUDIT-DUMMY
             END-IF
             STRING WS-AUDIT-DUMMY DELIMITED BY SPACE
             INTO WS-LAST-UPDATE-USER-TXT
             POINTER WS-LAST-UPDATE-USER-LEN
             SUBTRACT 1 FROM WS-LAST-UPDATE-USER-LEN
             IF IRIS-CREATION-TRAN = SPACE
               MOVE IRIS-PROGRAM-NAME TO WS-AUDIT-DUMMY
             ELSE
               MOVE IRIS-CREATION-TRAN TO WS-AUDIT-DUMMY
             END-IF
             STRING WS-AUDIT-DUMMY DELIMITED BY SPACE
             INTO WS-CREATION-TRAN-TXT POINTER WS-CREATION-TRAN-LEN
             SUBTRACT 1 FROM WS-CREATION-TRAN-LEN
             IF IRIS-LAST-UPDATE-TRAN = SPACE
               MOVE IRIS-PROGRAM-NAME TO WS-AUDIT-DUMMY
             ELSE
               MOVE IRIS-LAST-UPDATE-TRAN TO WS-AUDIT-DUMMY
             END-IF
             STRING WS-AUDIT-DUMMY DELIMITED BY SPACE
             INTO WS-LAST-UPDATE-TRAN-TXT
             POINTER WS-LAST-UPDATE-TRAN-LEN
             SUBTRACT 1 FROM WS-LAST-UPDATE-TRAN-LEN
           END-IF
           SET DB-STATUS-OK TO TRUE
           SET IRIS-SQL-OK TO TRUE
           SET IRIS-NO-ERROR TO TRUE
           SET COMMAND-WITHOUT-HOLD TO TRUE
           SET HAS-NOT-PATHCALLS TO TRUE
           SET HAS-PATHCALLS-NO-ERROR TO TRUE
           SET IS-NOT-PATHCALL-REVERSE TO TRUE
           MOVE ZERO TO WS-PATHCALL-LEVEL
           SET DEFAULT-DYNAMIC-ACCESS TO TRUE
           SET CUSTOM-STATIC-ACCESS TO TRUE
           SET EXEC-SEGMENT-ACCESS TO TRUE
           SET EXEC-DLI-ACCESS TO TRUE
           SET SKIP-SAVE-AREA TO TRUE
           IF EXEC-GE-PATH-CALL
             SET EXEC-CHECK-MISMATCH TO TRUE
           ELSE
             SET SKIP-CHECK-MISMATCH TO TRUE
           END-IF
           IF IRIS-FUNC-GHU
           OR IRIS-FUNC-GHN
           OR IRIS-FUNC-GHNP
             SET COMMAND-WITH-HOLD TO TRUE
           END-IF
           SET WS-KEEP-AS-IS TO TRUE
           IF IRIS-FUNC-GHU
           OR IRIS-FUNC-GHN
           OR IRIS-FUNC-GHNP
           OR IRIS-FUNC-ISRT
           OR IRIS-FUNC-REPL
           OR IRIS-FUNC-DLET
             IF DUAL-SQL-ONLY
               IF INIT-DUAL-BOTH
                 SET DUAL-BOTH TO TRUE
               END-IF
               IF INIT-DUAL-UPDATE-ONLY
                 SET DUAL-UPDATE-ONLY TO TRUE
               END-IF
               SET WS-REVERT-TO-SQL-ONLY TO TRUE
             END-IF
           ELSE
             IF DUAL-UPDATE-ONLY
               SET SKIP-DLI-ACCESS TO TRUE
             END-IF
           END-IF
           MOVE ZERO  TO IRIS-SQLCODE
                         IRIS-SQLERRML
                         WS-FB-KEY-LENGTH
                         WS-SEGMENT-LEVEL
                         WS-SEGMENT-LEN
                         WS-LAST-SEGMENT-LEVEL
                         WS-PATHCALL-LEN
                         WS-INIT-PATHCALL-LEN
                         SQL-CONDITION-CLAUSE-LENGTH
                         SQL-JOIN-CLAUSE-LENGTH
                         SQL-ORDERBY-CLAUSE-LENGTH
           MOVE SPACE TO IRIS-SQLERRMC
                         WS-LAST-SEGMENT-NAME
                         WS-LAST-IORTN-SECTION
           IF DUAL-UPDATE-ONLY
             IF IRIS-IMS-FUNCTION = 'GU  ' OR 'GHU '
               MOVE SPACE TO SEGMENTS-LAST-AREA
             END-IF
           END-IF
           .
      *
       INIT-VARIABLES-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    FINALIZE VARIABLES
      ******************************************************************
      *
       FINALIZE-VARIABLES SECTION.
      *
           MOVE 'FINALIZE-VARIABLES' TO WS-LAST-IORTN-SECTION
      *
           IF IO-RTN-FB-KEY-MAX-LENGTH > 0
           AND IO-RTN-FB-KEY-MAX-LENGTH < WS-FB-KEY-LENGTH
             MOVE IO-RTN-FB-KEY-MAX-LENGTH TO WS-FB-KEY-LENGTH
           END-IF
      *
      *    SETUP PCB INFO
      *
           IF IRIS-NO-ERROR
             MOVE WS-FB-KEY-LENGTH TO DB-PCB-FB-KEY-LENGTH
             MOVE WS-FB-KEY-AREA(1:WS-FB-KEY-LENGTH)
                                   TO DB-PCB-KEY-FB (1:WS-FB-KEY-LENGTH)
             MOVE WS-LAST-SEGMENT-NAME TO DB-PCB-SEGMENT-NAME
             MOVE WS-LAST-SEGMENT-LEVEL TO DB-PCB-SEGMENT-LEVEL
             ADD 1 TO WS-LAST-SEGMENT-LEVEL GIVING WS-IDX
             PERFORM VARYING WS-IDX FROM WS-IDX BY 1
                     UNTIL WS-IDX > WS-SEGMENTS-MAX-LVL
               INITIALIZE IO-RTN-USED-KEY-STATUS(WS-IDX)
               INITIALIZE IO-RTN-USED-SSA-KEYS(WS-IDX)
             END-PERFORM
             IF IRIS-FUNC-GU OR IRIS-FUNC-GHU
             OR IRIS-FUNC-ISRT
               MOVE ZERO TO
                 IO-RTN-LAST-OPEN-CURSOR(WS-LAST-SEGMENT-LEVEL)
             END-IF
      *
      *    SETUP STATUS
      *
             MOVE IRIS-SQLCODE TO IRIS-DB-SQLCODE
             EVALUATE TRUE
             WHEN IRIS-SQL-OK
               SET DB-STATUS-OK TO TRUE
             WHEN IRIS-SQL-UNIQ-CONSTR-VIOLATED
               SET DB-STATUS-DUPLICATED-KEY TO TRUE
             WHEN IRIS-SQL-DEADLOCK
               SET DB-STATUS-RESOURCE-DEADLOCK TO TRUE
             WHEN IRIS-SQL-INTE-CONSTR-VIOLATED
               SET DB-STATUS-ISRT-RULE-VIOLATION TO TRUE
             WHEN IRIS-SQL-NOT-FOUND
               SET DB-STATUS-SEGMENT-NOT-FOUND TO TRUE
             WHEN IRIS-SQL-END-DB-REACHED
               SET DB-STATUS-END-DB-REACHED TO TRUE
             WHEN IRIS-SQL-CHG-SEG
               SET DB-STATUS-CHANGED-SEGMENT-TYPE TO TRUE
             WHEN IRIS-SQL-CHG-LEV
               SET DB-STATUS-HIGHER-LEVEL-CROSSED TO TRUE
             WHEN IRIS-SQL-DUAL-PCB-MISMATCH
               SET DB-STATUS-DUAL-PCB-MISMATCH TO TRUE
             WHEN IRIS-SQL-DUAL-IOAREA-MISMATCH
               SET DB-STATUS-DUAL-IOAREA-MISMATCH TO TRUE
             WHEN OTHER
               SET DB-STATUS-INTERNAL-NOT-HANDLED TO TRUE
               SET IRIS-ERR-UNHANDLED-SQLCODE TO TRUE
             END-EVALUATE
             IF IRIS-SQL-CHG-SEG
             OR IRIS-SQL-CHG-LEV
               SET IRIS-SQL-OK TO TRUE
               MOVE IRIS-DB-SQLCODE TO IRIS-SQLCODE
             END-IF
           ELSE
             SET DB-STATUS-INTERNAL-NOT-HANDLED TO TRUE
           END-IF
           IF EXEC-SAVE-AREA
             SET ADDRESS OF LK-IO-AREA-BACKUP TO RUN-IO-AREA-PTR
             MOVE LK-IO-AREA(1:WS-SEGMENT-LEN) TO
                  LK-IO-AREA-BACKUP(1:WS-SEGMENT-LEN)
           END-IF
           SET SKIP-GE-PATH-CALL TO TRUE
      *
           .
      *
       FINALIZE-VARIABLES-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    PROCESS FOR UNQUALIFIED GN
      ******************************************************************
      *
       HANDLE-SELECT-ALL SECTION.
      *
           MOVE 'HANDLE-SELECT-ALL' TO WS-LAST-IORTN-SECTION
      *
           SET SQL-SELECT-SEEK TO TRUE
           SET IRIS-SQL-NOT-FOUND TO TRUE
           PERFORM UNTIL IRIS-SQL-OK
                      OR IRIS-SQL-END-DB-REACHED
                      OR (NOT IRIS-SQL-OK
                          AND NOT IRIS-SQL-NOT-FOUND)
             EVALUATE IO-RTN-USED-KEY-GNUNQ-NEXT-SEG
             WHEN 'A1111111'
             WHEN SPACE
               EVALUATE TRUE
               WHEN NOT A1111111-GNUNQ-VSTD
                 SET A1111111-GNUNQ-VSTD TO TRUE
                 IF NOT A1111111-GNUNQ-FIRST-VSTD
                   SET SQL-SELECT-SEEK TO TRUE
                   SET A1111111-GNUNQ-FIRST-VSTD TO TRUE
                 ELSE
                   SET SQL-SELECT-NEXT TO TRUE
                 END-IF
                 PERFORM HANDLE-SEGMENT-A1111111
                 MOVE IRIS-SQLCODE TO IRIS-DB-SQLCODE
                 IF IRIS-SQL-NOT-FOUND
                   SET IRIS-SQL-END-DB-REACHED TO TRUE
                   MOVE IRIS-DB-SQLCODE TO IRIS-SQLCODE
                 END-IF
               WHEN OTHER
                 MOVE SPACE TO A1111111-GNUNQ-SESS-AREA
               END-EVALUATE
             WHEN OTHER
               IF IRIS-NO-ERROR
                 SET IRIS-ERR-RTN-SEGMENT-NOT-FOUND TO TRUE
                 SET DB-STATUS-INTERNAL-NOT-HANDLED TO TRUE
               END-IF
             END-EVALUATE
           END-PERFORM.
      *
       HANDLE-SELECT-ALL-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    HANDLE SEGMENT A1111111
      ******************************************************************
      *
       HANDLE-SEGMENT-A1111111 SECTION.
      *
           MOVE A1111111-LVL TO WS-SEGMENT-LEVEL
           MOVE A1111111-LEN TO WS-SEGMENT-LEN
      *
           MOVE 'HANDLE-SEGMENT-A1111111' TO WS-LAST-IORTN-SECTION
      *
           EVALUATE TRUE
           WHEN SQL-SELECT-PRIMARY
             PERFORM A1111111-SELECT-PRIMARY-KEY
           WHEN SQL-SELECT-SEEK
             SET COMMAND-CODE-FIRST TO TRUE
             EVALUATE TRUE
             WHEN  IRIS-CODE-FIRST(1)
               SET COMMAND-CODE-FIRST TO TRUE
             WHEN  IRIS-CODE-LAST(1)
               SET COMMAND-CODE-LAST TO TRUE
             END-EVALUATE
      *
             PERFORM A1111111-SELECT-FIRST
           WHEN SQL-SELECT-NEXT
             SET COMMAND-CODE-FIRST TO TRUE
             EVALUATE TRUE
             WHEN  IRIS-CODE-FIRST(1)
               SET COMMAND-CODE-FIRST TO TRUE
             WHEN  IRIS-CODE-LAST(1)
               SET COMMAND-CODE-LAST TO TRUE
             END-EVALUATE
      *
             PERFORM A1111111-SELECT-NEXT
           WHEN SQL-SELECT-DYNAMIC
             SET COMMAND-CODE-FIRST TO TRUE
             EVALUATE TRUE
             WHEN  IRIS-CODE-FIRST(1)
               SET COMMAND-CODE-FIRST TO TRUE
             WHEN  IRIS-CODE-LAST(1)
               SET COMMAND-CODE-LAST TO TRUE
             END-EVALUATE
      *
             PERFORM A1111111-SELECT-DYNAMIC
           WHEN SQL-SELECT-PATH
             SET COMMAND-CODE-FIRST TO TRUE
             EVALUATE TRUE
             WHEN  IRIS-CODE-FIRST(1)
               SET COMMAND-CODE-FIRST TO TRUE
             WHEN  IRIS-CODE-LAST(1)
               SET COMMAND-CODE-LAST TO TRUE
             END-EVALUATE
      *
             PERFORM A1111111-SELECT-DYNAMIC
           WHEN SQL-INSERT
             SET COMMAND-CODE-LAST TO TRUE
             EVALUATE TRUE
             WHEN  IRIS-CODE-FIRST(1)
               SET COMMAND-CODE-FIRST TO TRUE
             WHEN  IRIS-CODE-LAST(1)
               SET COMMAND-CODE-LAST TO TRUE
             END-EVALUATE
      *
             PERFORM A1111111-INSERT
           WHEN SQL-UPDATE
             PERFORM A1111111-UPDATE
           WHEN SQL-DELETE
             PERFORM A1111111-DELETE
           WHEN SQL-SELECT-ALL-UNQ
             PERFORM HANDLE-SELECT-ALL
           WHEN SQL-SELECT-ALL-SEEK
             MOVE SPACE TO IO-RTN-USED-KEY-GNUNQ-NEXT-SEG
                           IO-RTN-USED-KEY-GNUNQ-SAVE-POS
             SET A1111111-GNUNQ-VSTD TO TRUE
             MOVE 1 TO SQL-CONDITION-CLAUSE-LENGTH
             STRING 'KEY_A1111111 = '''
             IO-RTN-USED-KEY-ALPHA(1)(1:10)
             ''''
             DELIMITED BY SIZE INTO SQL-CONDITION-CLAUSE-TEXT
             WITH POINTER SQL-CONDITION-CLAUSE-LENGTH
             SET SQL-SELECT-DYNAMIC TO TRUE
             PERFORM A1111111-SELECT-DYNAMIC
           WHEN SQL-SELECT-ALL-NEXT
             PERFORM HANDLE-SELECT-ALL
           WHEN OTHER
             SET IRIS-ERR-FUNCTION-NOT-FOUND TO TRUE
           END-EVALUATE
      *
           MOVE A1111111-LVL TO WS-LAST-SEGMENT-LEVEL
           MOVE 'A1111111' TO WS-LAST-SEGMENT-NAME.
      *
       HANDLE-SEGMENT-A1111111-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    SELECT ON PRIMARY KEY FOR SEGMENT A1111111
      ******************************************************************
      *
       A1111111-SELECT-PRIMARY-KEY SECTION.
      *
           MOVE 'A1111111-SELECT-PRIMARY-KEY' TO WS-LAST-IORTN-SECTION
      *
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
           MOVE IO-RTN-USED-KEY-ALPHA(1)(1:10) TO
                                   KEY-A1111111 OF IVPDB2-A1111111(1:10)
           IF COMMAND-WITH-HOLD
             EXEC SQL
             SELECT
               KEY_A1111111,
               FIRST_NAME,
               EXTENSION,
               ZIP_CODE,
               A1111111_FILLER_1,
               CREATION_TS,
               LAST_UPDATE_TS,
               CREATION_USER_I,
               LAST_UPDATE_USER_I,
               CREATION_TRAN_I,
               LAST_UPDATE_TRAN_I
             INTO
               :IVPDB2-A1111111.KEY-A1111111,
               :IVPDB2-A1111111.FIRST-NAME,
               :IVPDB2-A1111111.EXTENSION,
               :IVPDB2-A1111111.ZIP-CODE,
               :IVPDB2-A1111111.A1111111-FILLER-1,
               :IVPDB2-A1111111.CREATION-TS,
               :IVPDB2-A1111111.LAST-UPDATE-TS,
               :IVPDB2-A1111111.CREATION-USER-I,
               :IVPDB2-A1111111.LAST-UPDATE-USER-I,
               :IVPDB2-A1111111.CREATION-TRAN-I,
               :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
             FROM
             IVPDB2_A1111111
               WHERE
                 KEY_A1111111 =
               :IVPDB2-A1111111.KEY-A1111111
               WITH RS USE AND KEEP UPDATE LOCKS
             END-EXEC
           ELSE
             EXEC SQL
             SELECT
               KEY_A1111111,
               FIRST_NAME,
               EXTENSION,
               ZIP_CODE,
               A1111111_FILLER_1,
               CREATION_TS,
               LAST_UPDATE_TS,
               CREATION_USER_I,
               LAST_UPDATE_USER_I,
               CREATION_TRAN_I,
               LAST_UPDATE_TRAN_I
             INTO
               :IVPDB2-A1111111.KEY-A1111111,
               :IVPDB2-A1111111.FIRST-NAME,
               :IVPDB2-A1111111.EXTENSION,
               :IVPDB2-A1111111.ZIP-CODE,
               :IVPDB2-A1111111.A1111111-FILLER-1,
               :IVPDB2-A1111111.CREATION-TS,
               :IVPDB2-A1111111.LAST-UPDATE-TS,
               :IVPDB2-A1111111.CREATION-USER-I,
               :IVPDB2-A1111111.LAST-UPDATE-USER-I,
               :IVPDB2-A1111111.CREATION-TRAN-I,
               :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
             FROM
             IVPDB2_A1111111
               WHERE
                 KEY_A1111111 = 
               :IVPDB2-A1111111.KEY-A1111111
             END-EXEC
           END-IF
      *
           SET IO-RTN-USED-KEY-NOT-CHANGED(A1111111-LVL) TO TRUE
           MOVE SQLCODE TO IRIS-DB-SQLCODE
           IF SQLCODE = ZERO
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10) TO
                                          IO-RTN-USED-KEY-ALPHA(1)(1:10)
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                                                 TO WS-FB-KEY-AREA(1:10)
             MOVE 10 TO WS-FB-KEY-LENGTH
             MOVE A1111111-LEN TO WS-SEGMENT-LEN
             PERFORM A1111111-SET-IO-AREA
             SET IO-RTN-USED-KEY-CHANGED(A1111111-LVL) TO TRUE
             MOVE 'A1111111' TO WS-LAST-SEGMENT-NAME
                                IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
           END-IF
      *
           MOVE SQLCODE TO IRIS-SQLCODE
           MOVE SQLERRM TO IRIS-SQLERRM.
      *
       A1111111-SELECT-PRIMARY-KEY-EX.
           EXIT.
      *
      ******************************************************************
      *    SELECT FIRST TIME FOR SEGMENT A1111111
      ******************************************************************
      *
       A1111111-SELECT-FIRST SECTION.
      *
           MOVE 'A1111111-SELECT-FIRST' TO WS-LAST-IORTN-SECTION
      *
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
           IF COMMAND-WITH-HOLD
             IF COMMAND-CODE-LAST
               EXEC SQL
               SELECT
                 KEY_A1111111,
                 FIRST_NAME,
                 EXTENSION,
                 ZIP_CODE,
                 A1111111_FILLER_1,
                 CREATION_TS,
                 LAST_UPDATE_TS,
                 CREATION_USER_I,
                 LAST_UPDATE_USER_I,
                 CREATION_TRAN_I,
                 LAST_UPDATE_TRAN_I
               INTO
                 :IVPDB2-A1111111.KEY-A1111111,
                 :IVPDB2-A1111111.FIRST-NAME,
                 :IVPDB2-A1111111.EXTENSION,
                 :IVPDB2-A1111111.ZIP-CODE,
                 :IVPDB2-A1111111.A1111111-FILLER-1,
                 :IVPDB2-A1111111.CREATION-TS,
                 :IVPDB2-A1111111.LAST-UPDATE-TS,
                 :IVPDB2-A1111111.CREATION-USER-I,
                 :IVPDB2-A1111111.LAST-UPDATE-USER-I,
                 :IVPDB2-A1111111.CREATION-TRAN-I,
                 :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
               FROM
               IVPDB2_A1111111
                 WHERE
                   KEY_A1111111 = (
                   SELECT
                     MAX(KEY_A1111111)
                   FROM
                     IVPDB2_A1111111
                   )
                 WITH RS USE AND KEEP UPDATE LOCKS
               END-EXEC
             ELSE
               EXEC SQL
               SELECT
                 KEY_A1111111,
                 FIRST_NAME,
                 EXTENSION,
                 ZIP_CODE,
                 A1111111_FILLER_1,
                 CREATION_TS,
                 LAST_UPDATE_TS,
                 CREATION_USER_I,
                 LAST_UPDATE_USER_I,
                 CREATION_TRAN_I,
                 LAST_UPDATE_TRAN_I
               INTO
                 :IVPDB2-A1111111.KEY-A1111111,
                 :IVPDB2-A1111111.FIRST-NAME,
                 :IVPDB2-A1111111.EXTENSION,
                 :IVPDB2-A1111111.ZIP-CODE,
                 :IVPDB2-A1111111.A1111111-FILLER-1,
                 :IVPDB2-A1111111.CREATION-TS,
                 :IVPDB2-A1111111.LAST-UPDATE-TS,
                 :IVPDB2-A1111111.CREATION-USER-I,
                 :IVPDB2-A1111111.LAST-UPDATE-USER-I,
                 :IVPDB2-A1111111.CREATION-TRAN-I,
                 :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
               FROM
               IVPDB2_A1111111
                 WHERE
                   KEY_A1111111 = (
                   SELECT
                     MIN(KEY_A1111111)
                   FROM
                     IVPDB2_A1111111
                   )
                 WITH RS USE AND KEEP UPDATE LOCKS
               END-EXEC
             END-IF
           ELSE
             IF COMMAND-CODE-LAST
               EXEC SQL
               SELECT
                 KEY_A1111111,
                 FIRST_NAME,
                 EXTENSION,
                 ZIP_CODE,
                 A1111111_FILLER_1,
                 CREATION_TS,
                 LAST_UPDATE_TS,
                 CREATION_USER_I,
                 LAST_UPDATE_USER_I,
                 CREATION_TRAN_I,
                 LAST_UPDATE_TRAN_I
               INTO
                 :IVPDB2-A1111111.KEY-A1111111,
                 :IVPDB2-A1111111.FIRST-NAME,
                 :IVPDB2-A1111111.EXTENSION,
                 :IVPDB2-A1111111.ZIP-CODE,
                 :IVPDB2-A1111111.A1111111-FILLER-1,
                 :IVPDB2-A1111111.CREATION-TS,
                 :IVPDB2-A1111111.LAST-UPDATE-TS,
                 :IVPDB2-A1111111.CREATION-USER-I,
                 :IVPDB2-A1111111.LAST-UPDATE-USER-I,
                 :IVPDB2-A1111111.CREATION-TRAN-I,
                 :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
               FROM
               IVPDB2_A1111111
                 WHERE
                   KEY_A1111111 = (
                   SELECT
                     MAX(KEY_A1111111)
                   FROM
                     IVPDB2_A1111111
                   )
               END-EXEC
             ELSE
               EXEC SQL
               SELECT
                 KEY_A1111111,
                 FIRST_NAME,
                 EXTENSION,
                 ZIP_CODE,
                 A1111111_FILLER_1,
                 CREATION_TS,
                 LAST_UPDATE_TS,
                 CREATION_USER_I,
                 LAST_UPDATE_USER_I,
                 CREATION_TRAN_I,
                 LAST_UPDATE_TRAN_I
               INTO
                 :IVPDB2-A1111111.KEY-A1111111,
                 :IVPDB2-A1111111.FIRST-NAME,
                 :IVPDB2-A1111111.EXTENSION,
                 :IVPDB2-A1111111.ZIP-CODE,
                 :IVPDB2-A1111111.A1111111-FILLER-1,
                 :IVPDB2-A1111111.CREATION-TS,
                 :IVPDB2-A1111111.LAST-UPDATE-TS,
                 :IVPDB2-A1111111.CREATION-USER-I,
                 :IVPDB2-A1111111.LAST-UPDATE-USER-I,
                 :IVPDB2-A1111111.CREATION-TRAN-I,
                 :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
               FROM
               IVPDB2_A1111111
                 WHERE
                   KEY_A1111111 = (
                   SELECT
                     MIN(KEY_A1111111)
                   FROM
                     IVPDB2_A1111111
                   )
               END-EXEC
             END-IF
           END-IF
      *
           SET IO-RTN-USED-KEY-NOT-CHANGED(A1111111-LVL) TO TRUE
      *
           MOVE SQLCODE TO IRIS-DB-SQLCODE
           EVALUATE TRUE
           WHEN IRIS-SQL-OK
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10) TO
                                          IO-RTN-USED-KEY-ALPHA(1)(1:10)
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                                                 TO WS-FB-KEY-AREA(1:10)
             MOVE 10 TO WS-FB-KEY-LENGTH
             MOVE A1111111-LEN TO WS-SEGMENT-LEN
             PERFORM A1111111-SET-IO-AREA
             SET IO-RTN-USED-KEY-CHANGED(A1111111-LVL) TO TRUE
             MOVE 'A1111111' TO WS-LAST-SEGMENT-NAME
                                IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
           END-EVALUATE
      *
           MOVE SQLCODE TO IRIS-SQLCODE
           MOVE SQLERRM TO IRIS-SQLERRM.
      *
       A1111111-SELECT-FIRST-EX.
           EXIT.
      *
      ******************************************************************
      *    SELECT NEXT FOR SEGMENT A1111111
      ******************************************************************
      *
       A1111111-SELECT-NEXT SECTION.
      *
           MOVE 'A1111111-SELECT-NEXT' TO WS-LAST-IORTN-SECTION
      *
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
           MOVE IO-RTN-USED-KEY-ALPHA(1)(1:10) TO
                                   KEY-A1111111 OF IVPDB2-A1111111(1:10)
           IF COMMAND-WITH-HOLD
             IF COMMAND-CODE-LAST
               EXEC SQL
               SELECT
                 KEY_A1111111,
                 FIRST_NAME,
                 EXTENSION,
                 ZIP_CODE,
                 A1111111_FILLER_1,
                 CREATION_TS,
                 LAST_UPDATE_TS,
                 CREATION_USER_I,
                 LAST_UPDATE_USER_I,
                 CREATION_TRAN_I,
                 LAST_UPDATE_TRAN_I
               INTO
                 :IVPDB2-A1111111.KEY-A1111111,
                 :IVPDB2-A1111111.FIRST-NAME,
                 :IVPDB2-A1111111.EXTENSION,
                 :IVPDB2-A1111111.ZIP-CODE,
                 :IVPDB2-A1111111.A1111111-FILLER-1,
                 :IVPDB2-A1111111.CREATION-TS,
                 :IVPDB2-A1111111.LAST-UPDATE-TS,
                 :IVPDB2-A1111111.CREATION-USER-I,
                 :IVPDB2-A1111111.LAST-UPDATE-USER-I,
                 :IVPDB2-A1111111.CREATION-TRAN-I,
                 :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
               FROM
               IVPDB2_A1111111
                 WHERE
                   KEY_A1111111 = (
                   SELECT
                     MAX(KEY_A1111111)
                   FROM
                     IVPDB2_A1111111
                   WHERE
                     KEY_A1111111 >
                     :IVPDB2-A1111111.KEY-A1111111
                   )
                 WITH RS USE AND KEEP UPDATE LOCKS
               END-EXEC
             ELSE
               EXEC SQL
               SELECT
                 KEY_A1111111,
                 FIRST_NAME,
                 EXTENSION,
                 ZIP_CODE,
                 A1111111_FILLER_1,
                 CREATION_TS,
                 LAST_UPDATE_TS,
                 CREATION_USER_I,
                 LAST_UPDATE_USER_I,
                 CREATION_TRAN_I,
                 LAST_UPDATE_TRAN_I
               INTO
                 :IVPDB2-A1111111.KEY-A1111111,
                 :IVPDB2-A1111111.FIRST-NAME,
                 :IVPDB2-A1111111.EXTENSION,
                 :IVPDB2-A1111111.ZIP-CODE,
                 :IVPDB2-A1111111.A1111111-FILLER-1,
                 :IVPDB2-A1111111.CREATION-TS,
                 :IVPDB2-A1111111.LAST-UPDATE-TS,
                 :IVPDB2-A1111111.CREATION-USER-I,
                 :IVPDB2-A1111111.LAST-UPDATE-USER-I,
                 :IVPDB2-A1111111.CREATION-TRAN-I,
                 :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
               FROM
               IVPDB2_A1111111
                 WHERE
                   KEY_A1111111 = (
                   SELECT
                     MIN(KEY_A1111111)
                   FROM
                     IVPDB2_A1111111
                   WHERE
                     KEY_A1111111 >
                     :IVPDB2-A1111111.KEY-A1111111
                   )
                 WITH RS USE AND KEEP UPDATE LOCKS
               END-EXEC
             END-IF
           ELSE
             IF COMMAND-CODE-LAST
               EXEC SQL
               SELECT
                 KEY_A1111111,
                 FIRST_NAME,
                 EXTENSION,
                 ZIP_CODE,
                 A1111111_FILLER_1,
                 CREATION_TS,
                 LAST_UPDATE_TS,
                 CREATION_USER_I,
                 LAST_UPDATE_USER_I,
                 CREATION_TRAN_I,
                 LAST_UPDATE_TRAN_I
               INTO
                 :IVPDB2-A1111111.KEY-A1111111,
                 :IVPDB2-A1111111.FIRST-NAME,
                 :IVPDB2-A1111111.EXTENSION,
                 :IVPDB2-A1111111.ZIP-CODE,
                 :IVPDB2-A1111111.A1111111-FILLER-1,
                 :IVPDB2-A1111111.CREATION-TS,
                 :IVPDB2-A1111111.LAST-UPDATE-TS,
                 :IVPDB2-A1111111.CREATION-USER-I,
                 :IVPDB2-A1111111.LAST-UPDATE-USER-I,
                 :IVPDB2-A1111111.CREATION-TRAN-I,
                 :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
               FROM
               IVPDB2_A1111111
                 WHERE
                   KEY_A1111111 = (
                   SELECT
                     MAX(KEY_A1111111)
                   FROM
                     IVPDB2_A1111111
                   WHERE
                     KEY_A1111111 >
                     :IVPDB2-A1111111.KEY-A1111111
                   )
               END-EXEC
             ELSE
               EXEC SQL
               SELECT
                 KEY_A1111111,
                 FIRST_NAME,
                 EXTENSION,
                 ZIP_CODE,
                 A1111111_FILLER_1,
                 CREATION_TS,
                 LAST_UPDATE_TS,
                 CREATION_USER_I,
                 LAST_UPDATE_USER_I,
                 CREATION_TRAN_I,
                 LAST_UPDATE_TRAN_I
               INTO
                 :IVPDB2-A1111111.KEY-A1111111,
                 :IVPDB2-A1111111.FIRST-NAME,
                 :IVPDB2-A1111111.EXTENSION,
                 :IVPDB2-A1111111.ZIP-CODE,
                 :IVPDB2-A1111111.A1111111-FILLER-1,
                 :IVPDB2-A1111111.CREATION-TS,
                 :IVPDB2-A1111111.LAST-UPDATE-TS,
                 :IVPDB2-A1111111.CREATION-USER-I,
                 :IVPDB2-A1111111.LAST-UPDATE-USER-I,
                 :IVPDB2-A1111111.CREATION-TRAN-I,
                 :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
               FROM
               IVPDB2_A1111111
                 WHERE
                   KEY_A1111111 = (
                   SELECT
                     MIN(KEY_A1111111)
                   FROM
                     IVPDB2_A1111111
                   WHERE
                     KEY_A1111111 >
                     :IVPDB2-A1111111.KEY-A1111111
                   )
               END-EXEC
             END-IF
           END-IF
      *
           SET IO-RTN-USED-KEY-NOT-CHANGED(A1111111-LVL) TO TRUE
           MOVE SQLCODE TO IRIS-DB-SQLCODE
           EVALUATE TRUE
           WHEN IRIS-SQL-OK
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10) TO
                                          IO-RTN-USED-KEY-ALPHA(1)(1:10)
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                                                 TO WS-FB-KEY-AREA(1:10)
             MOVE 10 TO WS-FB-KEY-LENGTH
             MOVE A1111111-LEN TO WS-SEGMENT-LEN
             PERFORM A1111111-SET-IO-AREA
             SET IO-RTN-USED-KEY-CHANGED(A1111111-LVL) TO TRUE
             MOVE 'A1111111' TO WS-LAST-SEGMENT-NAME
                                IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
           END-EVALUATE
      *
           MOVE SQLCODE TO IRIS-SQLCODE
           MOVE SQLERRM TO IRIS-SQLERRM.
      *
       A1111111-SELECT-NEXT-EX.
           EXIT.
      *
      *    DYNAMIC SELECT FOR SEGMENT A1111111
      ******************************************************************
      *
       A1111111-SELECT-DYNAMIC SECTION.
      *
           MOVE 'A1111111-SELECT-DYNAMIC' TO WS-LAST-IORTN-SECTION
      *
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
      *    PREPARE THE WHERE CONDITION IF ANY
      *
           MOVE ZERO TO WS-WHERE-LEN WS-ORDERBY-LEN
           IF SQL-CONDITION-CLAUSE-LENGTH > 0
             SUBTRACT 1 FROM SQL-CONDITION-CLAUSE-LENGTH
             STRING 'WHERE '
             SQL-CONDITION-CLAUSE-TEXT(1:SQL-CONDITION-CLAUSE-LENGTH)
             ' ' DELIMITED BY SIZE INTO WS-WHERE
             COMPUTE WS-WHERE-LEN = SQL-CONDITION-CLAUSE-LENGTH + 7
           END-IF
      *
           IF SQL-ORDERBY-CLAUSE-LENGTH > 0
             SUBTRACT 1 FROM SQL-ORDERBY-CLAUSE-LENGTH
             MOVE SQL-ORDERBY-CLAUSE-TEXT(1:SQL-ORDERBY-CLAUSE-LENGTH)
                            TO WS-ORDERBY(1:SQL-ORDERBY-CLAUSE-LENGTH)
             MOVE SQL-ORDERBY-CLAUSE-LENGTH TO WS-ORDERBY-LEN
           ELSE
             COMPUTE WS-ORDERBY-LEN = 1
             STRING ' ORDER BY '
                   ' KEY_A1111111 '
             DELIMITED BY SIZE INTO WS-ORDERBY POINTER WS-ORDERBY-LEN
             SUBTRACT 1 FROM WS-ORDERBY-LEN
           END-IF
      *
           IF COMMAND-CODE-LAST
             STRING WS-ORDERBY(1:WS-ORDERBY-LEN)
             ' DESC ' DELIMITED BY SIZE
             INTO WS-ORDERBY
             ADD 6 TO WS-ORDERBY-LEN
           END-IF
      *
      *
      *    PREPARE THE DYNAMIC QUERY
      *
           MOVE 1 TO WS-SQL-STM-LEN
           STRING
           'SELECT '
             'IVPDB2_A1111111.KEY_A1111111, '
             'IVPDB2_A1111111.FIRST_NAME, '
             'IVPDB2_A1111111.EXTENSION, '
             'IVPDB2_A1111111.ZIP_CODE, '
             'IVPDB2_A1111111.A1111111_FILLER_1, '
             'IVPDB2_A1111111.CREATION_TS, '
             'IVPDB2_A1111111.LAST_UPDATE_TS, '
             'IVPDB2_A1111111.CREATION_USER_I, '
             'IVPDB2_A1111111.LAST_UPDATE_USER_I, '
             'IVPDB2_A1111111.CREATION_TRAN_I, '
             'IVPDB2_A1111111.LAST_UPDATE_TRAN_I '
           'FROM '
           'IVPDB2_A1111111 '
           DELIMITED BY SIZE
           INTO WS-SQL-STM-TXT POINTER WS-SQL-STM-LEN
           IF SQL-JOIN-CLAUSE-LENGTH > 0
             SUBTRACT 1 FROM SQL-JOIN-CLAUSE-LENGTH
             STRING SQL-JOIN-CLAUSE-TEXT(1:SQL-JOIN-CLAUSE-LENGTH) ' '
             DELIMITED BY SIZE
             INTO WS-SQL-STM-TXT POINTER WS-SQL-STM-LEN
           END-IF
           IF WS-WHERE-LEN > 0
             STRING
             WS-WHERE(1:WS-WHERE-LEN)
             DELIMITED BY SIZE
             INTO WS-SQL-STM-TXT POINTER WS-SQL-STM-LEN
             IF IRIS-TRACE-FULL
               MOVE 0 TO IRIS-MSG-LEN
               STRING '<IRISTRACE> - IVPDB2IO:SELECT-DYNAMIC' NL
                    ' SECTION    =(A1111111-SELECT-DYNAMIC) ' NL
                    ' CONDITION  =(' WS-WHERE(1:WS-WHERE-LEN) ')' NL
                    ' ORDER BY   =(' WS-ORDERBY(1:WS-ORDERBY-LEN) ')'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
           END-IF
           IF WS-ORDERBY-LEN > 0
             STRING WS-ORDERBY(1:WS-ORDERBY-LEN) DELIMITED BY SIZE
             INTO WS-SQL-STM-TXT POINTER WS-SQL-STM-LEN
           END-IF
           STRING ' FETCH FIRST 1 ROW ONLY '
           DELIMITED BY SIZE
           INTO WS-SQL-STM-TXT POINTER WS-SQL-STM-LEN
           IF COMMAND-WITH-HOLD
             STRING ' WITH RS USE AND KEEP UPDATE LOCKS '
             DELIMITED BY SIZE
             INTO WS-SQL-STM-TXT POINTER WS-SQL-STM-LEN
           END-IF
           SUBTRACT 1 FROM WS-SQL-STM-LEN
      *
      *    DECLARE THE DYNAMIC CURSOR
      *
           EXEC SQL
             DECLARE A1111111_CRS CURSOR
             FOR A1111111_STATEMENT
           END-EXEC
      *
      *    DECLARE THE SQL STATEMENT
      *
           EXEC SQL
             DECLARE A1111111_STATEMENT STATEMENT
           END-EXEC
      *
      *    PREPARE THE STATEMENT
      *
           EXEC SQL
             PREPARE A1111111_STATEMENT
             INTO :SQLDA
             FROM :WS-SQL-STM
           END-EXEC
      *
      *    TEST THE RETURN CODE
      *
           IF SQLCODE NOT = ZERO
             MOVE SQLCODE TO IRIS-SQLCODE
             MOVE SQLERRM TO IRIS-SQLERRM
             GO TO A1111111-SELECT-DYNAMIC-EX
           END-IF
      *
      *    OPEN THE DYNAMIC CURSOR
      *
           EXEC SQL
             OPEN A1111111_CRS
           END-EXEC
      *
      *    TEST THE RETURN CODE
      *
           IF SQLCODE NOT = ZERO
             MOVE SQLCODE TO IRIS-SQLCODE
             MOVE SQLERRM TO IRIS-SQLERRM
             GO TO A1111111-SELECT-DYNAMIC-EX
           END-IF
      *
      *    FETCH THE DYNAMIC CURSOR
      *
           EXEC SQL
             FETCH A1111111_CRS
             INTO
               :IVPDB2-A1111111.KEY-A1111111,
               :IVPDB2-A1111111.FIRST-NAME,
               :IVPDB2-A1111111.EXTENSION,
               :IVPDB2-A1111111.ZIP-CODE,
               :IVPDB2-A1111111.A1111111-FILLER-1,
               :IVPDB2-A1111111.CREATION-TS,
               :IVPDB2-A1111111.LAST-UPDATE-TS,
               :IVPDB2-A1111111.CREATION-USER-I,
               :IVPDB2-A1111111.LAST-UPDATE-USER-I,
               :IVPDB2-A1111111.CREATION-TRAN-I,
               :IVPDB2-A1111111.LAST-UPDATE-TRAN-I
           END-EXEC
      *
           IF SQLCODE NOT = ZERO
             MOVE SQLCODE TO IRIS-SQLCODE
             MOVE SQLERRM TO IRIS-SQLERRM
      *
      *      CLOSE THE DYNAMIC CURSOR
      *
             EXEC SQL
               CLOSE A1111111_CRS
             END-EXEC
             GO TO A1111111-SELECT-DYNAMIC-EX
           END-IF
      *
           MOVE SQLCODE TO IRIS-SQLCODE
           MOVE SQLERRM TO IRIS-SQLERRM
      *    The following CLOSE would release the record if FOR UPDATE is
      *    being used
           IF SQLCODE = ZERO
           AND COMMAND-WITH-HOLD
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10) TO
                                          IO-RTN-USED-KEY-ALPHA(1)(1:10)
             PERFORM A1111111-SELECT-PRIMARY-KEY
           END-IF
      *
      *      CLOSE THE DYNAMIC CURSOR
      *
           EXEC SQL
             CLOSE A1111111_CRS
           END-EXEC
      *
           SET IO-RTN-USED-KEY-NOT-CHANGED(A1111111-LVL) TO TRUE
      *
           MOVE IRIS-SQLCODE TO IRIS-DB-SQLCODE
           EVALUATE TRUE
           WHEN IRIS-SQL-OK
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10) TO
                                          IO-RTN-USED-KEY-ALPHA(1)(1:10)
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                                                 TO WS-FB-KEY-AREA(1:10)
             MOVE 10 TO WS-FB-KEY-LENGTH
             IF SQL-SELECT-DYNAMIC
               MOVE A1111111-LEN TO WS-SEGMENT-LEN
               PERFORM A1111111-SET-IO-AREA
             END-IF
             SET IO-RTN-USED-KEY-CHANGED(A1111111-LVL) TO TRUE
             MOVE 'A1111111' TO WS-LAST-SEGMENT-NAME
                                IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
           END-EVALUATE.
      *
       A1111111-SELECT-DYNAMIC-EX.
           EXIT.
      *
      ******************************************************************
      *    INSERT FOR SEGMENT A1111111
      ******************************************************************
      *
       A1111111-INSERT SECTION.
      *
           MOVE 'A1111111-INSERT' TO WS-LAST-IORTN-SECTION
      *
      *
           INITIALIZE IVPDB2-A1111111
      *
           MOVE LK-IO-AREA(1:A1111111-LEN)
           TO IVPDB2-A1111111(1:A1111111-LEN)
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
      *
      *
           EXEC SQL
             INSERT INTO
               IVPDB2_A1111111
               (
               KEY_A1111111,
               FIRST_NAME,
               EXTENSION,
               ZIP_CODE,
               A1111111_FILLER_1,
               CREATION_TS,
               LAST_UPDATE_TS,
               CREATION_USER_I,
               LAST_UPDATE_USER_I,
               CREATION_TRAN_I,
               LAST_UPDATE_TRAN_I
               )
             VALUES
             (
               :IVPDB2-A1111111.KEY-A1111111,
               :IVPDB2-A1111111.FIRST-NAME,
               :IVPDB2-A1111111.EXTENSION,
               :IVPDB2-A1111111.ZIP-CODE,
               :IVPDB2-A1111111.A1111111-FILLER-1,
               CURRENT TIMESTAMP,
               CURRENT TIMESTAMP,
               :WS-CREATION-USER,
               :WS-LAST-UPDATE-USER,
               :WS-CREATION-TRAN,
               :WS-LAST-UPDATE-TRAN
             )
           END-EXEC
      *
           SET IO-RTN-USED-KEY-NOT-CHANGED(A1111111-LVL) TO TRUE
           MOVE SQLCODE TO IRIS-DB-SQLCODE
           EVALUATE TRUE
           WHEN IRIS-SQL-OK
           WHEN IRIS-SQL-UNIQ-CONSTR-VIOLATED
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10) TO
                                          IO-RTN-USED-KEY-ALPHA(1)(1:10)
             MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                                                 TO WS-FB-KEY-AREA(1:10)
             MOVE 10 TO WS-FB-KEY-LENGTH
             SET IO-RTN-USED-KEY-CHANGED(A1111111-LVL) TO TRUE
             MOVE 'A1111111' TO WS-LAST-SEGMENT-NAME
                                IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
           END-EVALUATE
      *
           MOVE SQLCODE TO IRIS-SQLCODE
           MOVE SQLERRM TO IRIS-SQLERRM.
      *
       A1111111-INSERT-EX.
           EXIT.
      *
      ******************************************************************
      *    UPDATE FOR SEGMENT A1111111
      ******************************************************************
      *
       A1111111-UPDATE SECTION.
      *
           MOVE 'A1111111-UPDATE' TO WS-LAST-IORTN-SECTION
      *
      *
           INITIALIZE IVPDB2-A1111111
      *
           MOVE LK-IO-AREA(1:A1111111-LEN)
           TO IVPDB2-A1111111(1:A1111111-LEN)
      *
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
           MOVE IO-RTN-USED-KEY-ALPHA(1)(1:10) TO
                                   KEY-A1111111 OF IVPDB2-A1111111(1:10)
      *
      *
           EXEC SQL
             UPDATE IVPDB2_A1111111 SET
               FIRST_NAME = :IVPDB2-A1111111.FIRST-NAME,
               EXTENSION = :IVPDB2-A1111111.EXTENSION,
               ZIP_CODE = :IVPDB2-A1111111.ZIP-CODE,
               A1111111_FILLER_1 = :IVPDB2-A1111111.A1111111-FILLER-1,
               LAST_UPDATE_TS = CURRENT TIMESTAMP,
               LAST_UPDATE_USER_I = :WS-LAST-UPDATE-USER,
               LAST_UPDATE_TRAN_I = :WS-LAST-UPDATE-TRAN
             WHERE
               KEY_A1111111 =
             :IVPDB2-A1111111.KEY-A1111111
           END-EXEC
      *
           IF SQLCODE = ZERO
             MOVE LK-IO-AREA(1:A1111111-LEN) TO A1111111-LAST-AREA
           END-IF
           MOVE SQLCODE TO IRIS-SQLCODE
           MOVE SQLERRM TO IRIS-SQLERRM.
      *
       A1111111-UPDATE-EX.
           EXIT.
      *
      ******************************************************************
      *    DELETE FOR SEGMENT A1111111
      ******************************************************************
      *
       A1111111-DELETE SECTION.
      *
           MOVE 'A1111111-DELETE' TO WS-LAST-IORTN-SECTION
      *
      *
           IF HAS-PATHCALLS
             SET IRIS-ERR-RTN-UNHANDLED-ACCESS TO TRUE
             GO TO A1111111-DELETE-EX
           END-IF
      *
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
           MOVE IO-RTN-USED-KEY-ALPHA(1)(1:10) TO
                                   KEY-A1111111 OF IVPDB2-A1111111(1:10)
      *
           EXEC SQL
             DELETE FROM IVPDB2_A1111111
             WHERE
               KEY_A1111111 =
             :IVPDB2-A1111111.KEY-A1111111
           END-EXEC
      *
           MOVE SQLCODE TO IRIS-SQLCODE
           MOVE SQLERRM TO IRIS-SQLERRM.
      *
       A1111111-DELETE-EX.
           EXIT.
      *
      ******************************************************************
      *    SET I/O AREA A1111111
      ******************************************************************
      *
       A1111111-SET-IO-AREA SECTION.
      *
           MOVE 'A1111111-SET-IO-AREA' TO WS-LAST-IORTN-SECTION
      *
           MOVE IVPDB2-A1111111(1:WS-SEGMENT-LEN)
                          TO LK-IO-AREA(1:WS-SEGMENT-LEN)
                             A1111111-LAST-AREA.
      *
       A1111111-SET-IO-AREA-EX.
           EXIT.
      *
      *
       STORE-SSA SECTION.
      *
           EVALUATE WS-SEGMENT-NAME
           WHEN 'A1111111'
             CONTINUE
           END-EVALUATE.
      *
       STORE-SSA-EX.
           EXIT.
      *
       FORCE-PARENT-POSITION SECTION.
      *
           IF LAST-IMS-FUNCTION-READHLD
             GO TO FORCE-PARENT-POSITION-EX
           END-IF
           .
      *
       FORCE-PARENT-POSITION-EX.
           EXIT.
      *
       FORCE-LEVEL-POSITION SECTION.
      *
           .
      *
       FORCE-LEVEL-POSITION-EX.
           EXIT.
      *
      ******************************************************************
      *    SET DIB BLOCK
      ******************************************************************
      *
       SET-DIB-BLOCK SECTION.
      *
           MOVE 'SET-DIB-BLOCK' TO WS-LAST-IORTN-SECTION
      *
           MOVE 'IR'                   TO IRIS-DIBVER
           MOVE DB-PCB-STATUS-CODE     TO IRIS-DIBSTAT
           MOVE DB-PCB-SEGMENT-NAME    TO IRIS-DIBSEGM
           MOVE DB-PCB-SEGMENT-LEVEL   TO IRIS-DIBSEGLV
           MOVE DB-PCB-FB-KEY-LENGTH   TO IRIS-DIBKFBL
           MOVE DB-PCB-DBD-NAME        TO IRIS-DIBDBDNM
           MOVE 'IRIS'                 TO IRIS-DIBDBORG
           MOVE IRIS-DIB-BLOCK         TO LK-DIB-BLOCK
           SET ADDRESS OF IRIS-DB-PCB  TO ADDRESS OF LK-DIB-BLOCK.
      *
       SET-DIB-BLOCK-EX.
      *
           EXIT.
      *
      *
      ******************************************************************
      *    CHECK FUNCTION ALIAS
      ******************************************************************
      *
       CHECK-FUNCTION-ALIAS SECTION.
      *
           EVALUATE IRIS-CUSTOM-FUNC-ID
           WHEN 0
             SET IS-NOT-ALIAS TO TRUE
           WHEN 2
             EVALUATE WS-FUNC-ALIAS
             WHEN 3
               SET IS-ALIAS TO TRUE
             END-EVALUATE
           WHEN 3
             EVALUATE WS-FUNC-ALIAS
             WHEN 2
               SET IS-ALIAS TO TRUE
             END-EVALUATE
           WHEN OTHER
             SET IS-NOT-ALIAS TO TRUE
           END-EVALUATE
           .
      *
       CHECK-FUNCTION-ALIAS-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    BUILD SSAS FROM EXEC DLI
      ******************************************************************
      *
       BUILD-SSAS SECTION.
      *
           MOVE 'BUILD-SSAS' TO WS-LAST-IORTN-SECTION
      *
           EVALUATE IRIS-CUSTOM-FUNC-ID
           WHEN 0
             SET IRIS-ERR-FUNCTION-NOT-FOUND TO TRUE
           WHEN OTHER
             SET IRIS-ERR-FUNCTION-NOT-FOUND TO TRUE
           END-EVALUATE
           .
      *
       BUILD-SSAS-EX.
      *
           EXIT.
