      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: PCB RE-ORGANIZATION
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IRISPOCP.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
           EXEC SQL INCLUDE SQLCA END-EXEC.
       01 IRIS-CURRENT-TIMESTAMP PIC X(26) VALUE SPACES.
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
      *
      *
      * WORKING STORAGE COMMON VARIABLES
      *
      * COMMON INDEXES
       01 WS-INDEX                        PIC S9(9) COMP.
       01 WS-INDEX-2                      PIC S9(9) COMP.
      * WORKING SEGMENT NAME
       01 WS-SEGMENT-NAME                 PIC X(8).
       01 WS-PARENT-NAME                  PIC X(8).
      * COMMMON AREA 
       01 WS-PARENT-INDEX-ZONED           PIC 99.
      * PERFORM EXIT VARIABLES
       01 FILLER                          PIC 9.
         88  CONTINUE-PERFORM             VALUE 0.
         88  EXIT-PERFORM                 VALUE 1.
      *
       LINKAGE SECTION.
      *
      *    CONTROL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IRISPOPT'==.
      *
      *  STRUCTURES TO REPRESENT TP AND DB PCB
      * 
           COPY IRISISEG.
      *
      *  PCB SENSEGS REPRESENTATION
      * 
           COPY IRISSSEG.
      *
      *  SEGMENT REPRESENTATION
      * 
           COPY IRISSEGM.
      *
      *  DBD FIELD REPRESENTATION
      * 
           COPY IRISFLD.
      *
      * INFO STATISTICS
      *
           COPY IRISRNTM.
      
       PROCEDURE DIVISION USING IRIS-WORK-AREA
                                MEMORY-PCB-AREA.
      *
       MAIN-PROGRAM.
      *
      *    ALLOCATE THE WORKING TABLES
      * 
           IF IRIS-TRACE-DEBUG
             EXEC SQL
              SET :IRIS-CURRENT-TIMESTAMP = CURRENT_TIMESTAMP
             END-EXEC
           END-IF
           PERFORM ALLOCATE-TABLES THRU ALLOCATE-TABLES-EX
      *
      *    IF NO SEGMENT IS PRESENT GO BACK
      * 
           IF DT-NUM-SENSEG = 0
             GOBACK
           END-IF
      *
      *    INITIALIZE WORKING FIELDS
      * 
           PERFORM INITIALIZE-FIELDS THRU INITIALIZE-FIELDS-EX
      *
      *    ORGANIZE THE WORKING TABLES
      * 
           PERFORM ORG-TABLES THRU ORG-TABLES-EX
      *
      *    ORGANIZE THE KEYS
      * 
           PERFORM ORG-KEYS THRU ORG-KEYS-EX
      *
      *    INITIALIZE RUN-FUNCTIONS-OCCURENCES
      *      
           IF IRIS-TRACE-DEBUG
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPOPT) -- SENSEG TRACE -- ' 
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT        
             SET IRIS-MSG-LEVEL-DEBUG TO TRUE
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             PERFORM VARYING WS-INDEX FROM 1 BY 1 
             UNTIL WS-INDEX > DT-PHYS-NUM-SENSEG
               MOVE RUN-PARENT-SEGMENT-INDEX(WS-INDEX) 
                                            TO WS-PARENT-INDEX-ZONED
               MOVE ZERO TO IRIS-MSG-LEN
               STRING ' ' DS-SEGMENT-NAME (WS-INDEX) ' ' 
               DS-PARENT-SEGMENT-NAME (WS-INDEX)
               ' PARENT-IND=' WS-PARENT-INDEX-ZONED ' ' 
               RUN-SEQUENCE-TYPE(WS-INDEX)
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT        
               SET IRIS-MSG-LEVEL-DEBUG TO TRUE
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-PERFORM
           END-IF.
      *
       MAIN-PROGRAM-EX.
      *
           GOBACK.
      *
      ******************************************************************
      *    ALLOCATE THE ININITIAL TABLES
      ******************************************************************
      *
       ALLOCATE-TABLES SECTION.
      *
           SET ADDRESS OF RUN-CONTROL-AREA TO DT-INFO-PTR           
           SET ADDRESS OF MEMORY-DBD-SEGMENTS TO DT-DBD-SEGM-PTR
           SET ADDRESS OF MEMORY-DBD-FIELDS TO DT-DBD-FIELD-PTR
           SET ADDRESS OF MEMORY-PCB-SENSEG TO DT-PSB-SENSEG-PTR.
      *
       ALLOCATE-TABLES-EX.
           EXIT.
      *
      ******************************************************************
      *    INITIALIZE THE WORKING FIELDS
      ******************************************************************
      *
       INITIALIZE-FIELDS SECTION.
      *
           PERFORM VARYING WS-INDEX FROM 1 BY 1 
           UNTIL WS-INDEX > DT-PHYS-NUM-SENSEG
             MOVE ZERO TO RUN-PHYSICAL-INDEX(WS-INDEX)
                          RUN-PARENT-SEGMENT-INDEX(WS-INDEX)
                          RUN-PHYSICAL-PARENT-INDEX(WS-INDEX)
           END-PERFORM.
       INITIALIZE-FIELDS-EX.
           EXIT.
      *
      ******************************************************************
      *    ORGANIZE THE WORKING TABLES
      ******************************************************************
      *
       ORG-TABLES SECTION.
      *
           MOVE 1 TO DT-ROOT-SEGM DT-CURR-SEGM DT-SAVE-LEVEL
           MOVE 0 TO DT-PHYS-ROOT-SEGM
      *
           PERFORM VARYING WS-INDEX FROM 1 BY 1                         
           UNTIL WS-INDEX > DT-PHYS-NUM-SENSEG
      *
             MOVE PS-SEGMENT-NAME(WS-INDEX) TO WS-SEGMENT-NAME
             MOVE PS-PARENT-SEGMENT-NAME(WS-INDEX) TO WS-PARENT-NAME    
      *
             SET CONTINUE-PERFORM TO TRUE
             PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
             UNTIL WS-INDEX-2 > DS-SEGMENTS-COUNT 
             OR EXIT-PERFORM
               IF DS-PARENT-SEGMENT-NAME(WS-INDEX-2) = SPACE  
                 MOVE WS-INDEX-2 TO DT-PHYS-ROOT-SEGM
               END-IF
               IF DS-SEGMENT-NAME(WS-INDEX-2) = WS-SEGMENT-NAME         
                 MOVE WS-INDEX-2 TO RUN-PHYSICAL-INDEX(WS-INDEX)            
                 SET EXIT-PERFORM TO TRUE 
               END-IF
             END-PERFORM
      *
             IF WS-PARENT-NAME NOT = SPACE
               SET CONTINUE-PERFORM TO TRUE
               PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
               UNTIL WS-INDEX-2 > DS-SEGMENTS-COUNT 
               OR EXIT-PERFORM
                 IF DS-SEGMENT-NAME(WS-INDEX-2) = WS-PARENT-NAME        
                   MOVE WS-INDEX-2 
                           TO RUN-PHYSICAL-PARENT-INDEX(WS-INDEX)       
                   SET EXIT-PERFORM TO TRUE 
                 END-IF
               END-PERFORM
      *
               SET CONTINUE-PERFORM TO TRUE
               PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
               UNTIL WS-INDEX-2 > DT-PHYS-NUM-SENSEG 
               OR EXIT-PERFORM
                 IF PS-SEGMENT-NAME(WS-INDEX-2) = WS-PARENT-NAME        
                   MOVE WS-INDEX-2 
                           TO RUN-PARENT-SEGMENT-INDEX(WS-INDEX)        
                   SET EXIT-PERFORM TO TRUE 
                 END-IF
               END-PERFORM
             END-IF
      *     
           END-PERFORM.
      *     
       ORG-TABLES-EX.
           EXIT.
      *
      ******************************************************************
      *    ORG THE KEYS
      ******************************************************************
      *
       ORG-KEYS SECTION.
      *
           PERFORM VARYING WS-INDEX FROM 1 BY 1 
           UNTIL WS-INDEX > DT-PHYS-NUM-SENSEG
             SET RUN-SEQUENCE-NONE(WS-INDEX) TO TRUE
             PERFORM VARYING WS-INDEX-2 FROM 1 BY 1 
             UNTIL WS-INDEX-2 > DBD-FIELDS-LENGTH 
             OR (DF-SEGMENT-NAME(WS-INDEX-2) = DS-SEGMENT-NAME(WS-INDEX) 
             AND (DF-FIELD-SEQUENCE-UNIQUE(WS-INDEX-2) 
                                OR DF-FIELD-SEQUENCE-MULTI(WS-INDEX-2)))
               CONTINUE
             END-PERFORM
             IF WS-INDEX-2 <= DBD-FIELDS-LENGTH THEN
               MOVE DF-FIELD-DATA-TYPE(WS-INDEX-2) 
                                     TO RUN-SEQUENCE-DATA-TYPE(WS-INDEX)
               MOVE DF-FIELD-SEQUENCE(WS-INDEX-2) 
                                          TO RUN-SEQUENCE-TYPE(WS-INDEX)
               MOVE DF-FIELD-NAME (WS-INDEX-2) 
                                          TO RUN-SEQUENCE-NAME(WS-INDEX)
               MOVE DF-FIELD-BYTES (WS-INDEX-2) 
                                        TO RUN-SEQUENCE-LENGTH(WS-INDEX)
             END-IF
           END-PERFORM.
      *
       ORG-KEYS-EX.
           EXIT.
