      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: ABEND MODULE
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
       PROGRAM-ID. IRISABCP.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           EXEC SQL INCLUDE SQLCA END-EXEC.
       01 IRIS-CURRENT-TIMESTAMP PIC X(26) VALUE SPACES.

       LINKAGE SECTION.
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IRISTRAC'==.
      *
       PROCEDURE DIVISION USING IRIS-WORK-AREA.
       MAIN.
      *    FIXME: CUSTOMIZE WITH CUSTOMER ABEND PROCESS 
           DISPLAY 'BLOCKING ERROR, ABEND IN PROGRESS'
           IF IRIS-TRACE-DEBUG
             EXEC SQL
              SET :IRIS-CURRENT-TIMESTAMP = CURRENT_TIMESTAMP
             END-EXEC
           END-IF
           GOBACK.
