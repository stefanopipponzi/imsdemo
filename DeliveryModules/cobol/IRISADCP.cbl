      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: AREA ALLOCATION API
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IRISADCP.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
           EXEC SQL INCLUDE SQLCA END-EXEC.
       01 IRIS-CURRENT-TIMESTAMP PIC X(26) VALUE SPACES.
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
      *
       01 WS-ADDR-9 PIC 9(9).
       LINKAGE SECTION.
      *
      *    IRIS GLOBAL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IRISADDR'==.
      *
       01  LK-AREA   PIC X.
      *
       01  LK-PTR    POINTER.
       01  LK-ADDR   REDEFINES LK-PTR PIC S9(9) COMP.
      *
       PROCEDURE DIVISION USING IRIS-WORK-AREA
                                LK-AREA
                                LK-PTR.
       MAIN.
           IF IRIS-TRACE-DEBUG
             EXEC SQL
              SET :IRIS-CURRENT-TIMESTAMP = CURRENT_TIMESTAMP
             END-EXEC
           END-IF
           IF IRIS-TRACE-DEBUG
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISADCP: START' NL
                    ' LK-AREA =(' LK-AREA ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           SET LK-PTR TO ADDRESS OF LK-AREA
           IF IRIS-TRACE-DEBUG
             MOVE 0 TO IRIS-MSG-LEN
             MOVE LK-ADDR TO WS-ADDR-9
             STRING '<IRISTRACE> - IRISADCP: END' NL
                    ' ADDRESS =(' WS-ADDR-9 ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
      *
       MAIN-EX.
           GOBACK.
