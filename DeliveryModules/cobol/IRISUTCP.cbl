CBL DYNAM,TRUNC(BIN),NOSQLCCSID
      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: DATABASE I/O ROUTINE FOR DL/I UTIILITY FUNCTIONS
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IRISUTCP.
      *
       ENVIRONMENT DIVISION.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
           
           COPY IRISDCPT.
      *
      *    DATABASE COMMON SQLCODES
      *
           COPY IRISSQLC.
      *
      *    STANDARD SQL INCLUDE
      *
           EXEC SQL INCLUDE SQLCA END-EXEC.
      *
      *    IRIS LOOKUP TABLE DCLGEN
      *
           EXEC SQL INCLUDE IRISDLVW END-EXEC.
      *
      *    CHECKPOINT HANDLING
      *
           EXEC SQL INCLUDE IRISC1VW END-EXEC.
      *
      *    WORKING STORAGE VARIABLES
      *
      *01 WS-BLL-CELL-POINTER           POINTER.
      *01 WS-BLL-CELL-NUM REDEFINES WS-BLL-CELL-POINTER
      *                                 PIC S9(9) COMP-5.
       01 WS-TRACE-LEVEL                PIC X.
       01 WS-SQLCODE-N                  PIC S9(5).
       01 WS-SQLCODE-E                  PIC -ZZZZ9.
       01 WS-CLEAN-UP                   PIC S9(8) COMP VALUE +1.
       01 WS-ABEND-CODE                 PIC S9(8) COMP VALUE +0.
       01 WS-PARAM-NUM                  PIC S9(9) COMP.
       01 WS-SCHED-PARAM-NUM            PIC S9(9) COMP.
       01 WS-MESSAGE-ID-EDITED          PIC ZZZZ9.
       01 WS-MESSAGE                    PIC X(32000).
       01 WS-ERROR-DESCRIPTION          PIC X(80).
       01 WS-ERROR-DESCRIPTION-LEN      PIC S9(4)   COMP.
       01 WS-INDEX                      PIC S9(4)   COMP.
       01 WS-INDEX-C                    PIC S9(4)   COMP.
       01 WS-INDEX-2                    PIC S9(4)   COMP.
       01 WS-INDEX-Z                    PIC ZZ9.
       01 WS-ADDR-9                     PIC 9(9).
       01 WS-LAST-IORTN-SECTION         PIC X(30).
       01 WS-PTR                        POINTER.
       01 WS-PSB-PCBS-PTR               POINTER.
       01 WS-PSB-SENSEGS-PTR            POINTER.
       01 WS-IO-RTN.
         03 WS-IO-RTN-DBDNAME           PIC X(8).
         03 FILLER REDEFINES WS-IO-RTN-DBDNAME.
           05 FILLER                    PIC X(6).
           88 RTN-IS-PCBIO                             VALUE 'PCB_IO',
                                                             'ALT_IO'.
           88 RTN-IS-GSAM                              VALUE 'GSAMIO'.
           05 FILLER                    PIC X(2).
         03 FILLER REDEFINES WS-IO-RTN-DBDNAME.
           05 FILLER                    PIC X(3).
           05 FILLER                    PIC X(2).
             88 RTN-IS-GSAMDBD                         VALUE 'GS'.
           05 FILLER                    PIC X(3).
       01 WS-DUMMY-PCB.
         03 WS-DUMMY-PCB-HEADER         PIC X(36).
         03 FILLER REDEFINES WS-DUMMY-PCB-HEADER.
           05 WS-DUMMY-DBD-NAME         PIC X(8).
           05 WS-DUMMY-SEG-LVL          PIC 9(2).
           05 WS-DUMMY-STS-CODE         PIC X(2).
           05 WS-DUMMY-PROC-OPT         PIC X(4).
           05 WS-DUMMY-PCB-RESERVED     PIC S9(9)  COMP.
           05 FILLER REDEFINES WS-DUMMY-PCB-RESERVED PIC X(4).
             88 WS-DUMMY-PCB-IRIS-EYE   VALUE 'IRIS'.
         03 WS-DUMMY-FB-KEY             PIC X(1587).
         03 WS-DUMMY-FB-RNTM            PIC X(377).
         03 WS-DUMMY-CNTL-FIXED         PIC X(1000).
         03 WS-DUMMY-CNTL-VAR           PIC X(32000).
      *
       01 IRIS-DLIUIB.
         03 IRIS-UIBPCBAL               POINTER.
         03 IRIS-UIBRCODE.
              05 IRIS-UIBFCTR           PIC X.
              05 IRIS-UIBDLTR           PIC X.
      *
       01 FILLER                        PIC S9(4)   COMP.
         88 WS-PSB-NOT-ALLOCATED        VALUE 0.
         88 WS-PSB-ALLOCATED            VALUE 1.
       01 FILLER                        PIC S9(4)   COMP.
         88 WS-IRISDLVW-OK              VALUE 0.
         88 WS-IRISDLVW-NOK             VALUE 1.
       01 WS-RUN-DBD-NAME               PIC X(8).
       01 WS-RUN-DBD-STATUS             PIC S9(4)   COMP.
         88 WS-DUAL-IMS-ONLY            VALUE 0.
         88 WS-DUAL-SQL-ONLY            VALUE 1.
         88 WS-DUAL-BOTH                VALUE 2.
         88 WS-DUAL-UPDATE-ONLY         VALUE 3.

       01 WS-DBD-LOOKUP-TABLE-COUNT     PIC S9(4)   COMP.
       01 WS-DBD-LOOKUP-AREA.
         03 WS-DBD-LOOKUP-TABLE         OCCURS 255.
           05 WS-LOOKUP-DBD             PIC X(3).
           05 WS-LOOKUP-STATUS          PIC S9(4)   COMP.
       01 WS-CMPAT-IDX                  PIC S9(4)   COMP.
       01 WS-CHKP-ID                    PIC X(14).
      *
      * MAX PCBS, IF THE VALUE HAS TO CHANGE ALSO THE ARRAYS BELOW
      * IRIS-BLL-CELLS, IRIS-PCBS AND THE LINKAGE ARRAY IRIS-LK-CELLS
      * MUST BE CHANGED ACCORDINGLY
      *
       01 IRIS-MAX-PCBS                 PIC S9(4)   COMP  VALUE 100.
       01 WS-DBD-NAME                   PIC X(3) VALUE SPACE.
      *
       01 WS-CHECKPOINT-TIME    PIC X(16).
       01 WS-SQLCODE            PIC S9(9) COMP.
      * For saving IO AREAS in Path Calls
      * Size limited to max path call for II6 DBD
       01 IRIS-IO-AREAS.
         03 FILLER                      OCCURS 100.
           05 IRIS-IO-AREA              PIC X(500).
      *
       01 IRIS-BLL-CELLS.
         03 FILLER                      OCCURS 100.
           05 IRIS-BLL-POINTER          POINTER.
      *
       01 IRIS-PCBS-AREA.
         03 IRIS-PCBS-TAB               OCCURS 100.
      * 36 BYTES
           05 IRIS-PCB-HEADER.
             07 IRIS-PCB-DBD-NAME       PIC X(8).
             07 IRIS-PCB-SEGMENT-LEVEL  PIC 9(2).
             07 IRIS-PCB-STATUS-CODE    PIC X(2).
             07 IRIS-PCB-PROC-OPTS      PIC X(4).
             07 IRIS-PCB-RESERVED       PIC S9(9)  COMP.
             07 IRIS-PCB-SEGMENT-NAME   PIC X(8).
             07 IRIS-PCB-FB-KEY-LENGTH  PIC S9(9)  COMP.
             07 IRIS-PCB-NUM-SENSEGS    PIC S9(9)  COMP.
      * 1587 BYTES
           05  IRIS-PCB-FEEDBACK        PIC X(1587).
      * 377 Bytes
           COPY IRISISEG REPLACING ==01 MEMORY-PCB-AREA.==
                                BY ==05 MEMORY-PCB-AREA.==.
      * 33000 BYTES
           COPY IRISRNTM REPLACING ==01 RUN-CONTROL-AREA.==
                                BY ==05 IRIS-RNT-CNTL-AREA.==.
      *
       LINKAGE SECTION.
      *
      *    IRIS GLOBAL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IRISUTIL'==.
      *
      *    IRIS PCB POINTERS
      *
           COPY IRISPTRS.
      *
      *    IRIS PCB STRUCTURE
      *
           COPY IRISPCB.
      *
       01  IRIS-LK-CELLS.
           03 FILLER                    OCCURS 100.
              05 IRIS-LK-POINTER        POINTER.
      *
       01  IRIS-LK-PCBS-ADDR            PIC S9(9)   COMP-5.
       01  IRIS-LK-PCBS-PTR REDEFINES IRIS-LK-PCBS-ADDR POINTER.
      *
       01 LK-PSB-MEMORY-PCB-AREA.
         03 TAB-PSB-MEM-PCBS-COUNT      PIC S9(4)   COMP.
         03 FILLER                      PIC 9.
           88 TAB-PSB-MEM-IS-CMPAT      VALUE 1.
         03 TAB-PSB-MEM-PCB-AREA        OCCURS 1 TO 255
                         DEPENDING ON TAB-PSB-MEM-PCBS-COUNT.
           05 TAB-PSB-MEM-PCB-DATA      PIC X(377).
      *
       01 LK-PSB-MEMORY-SENSEG-AREA.
         03 TAB-PSB-MEM-SENSEGS-COUNT   PIC S9(4) COMP.
         03 TAB-PSB-MEM-SENSESG-AREA    OCCURS 1 TO 4096
                         DEPENDING ON TAB-PSB-MEM-SENSEGS-COUNT.
           05 TAB-PSB-MEM-SENSEG-DATA   PIC X(16).
      *
      *    IRIS AIB STRUCTURE
      *
           COPY IRISAIB.
      *
      *    DIB BLOCK FOR EXEC DLI
      *
       01 LK-DIB-BLOCK                  PIC X(32).
      *
       PROCEDURE DIVISION USING IRIS-WORK-AREA
                                COPY IRISPTRU.
       MAIN-PROGRAM.
      *
MSYSTS     IF IRIS-TRACE-LEVEL = LOW-VALUE
MSYSTS       DISPLAY 'IRISUTCP: OVERRIDE TRACE MODE TO STANDARD'
MSYSTS       SET IRIS-TRACE-STANDARD TO TRUE
MSYSTS     END-IF
           IF IRIS-TRACE-STANDARD
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISUTCP:START' NL
                    ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                    ' CALLER ID  =(' IRIS-CALL-ID ') ' NL
                    ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           PERFORM INIT-VARIABLES THRU INIT-VARIABLES-EX
      *
           EVALUATE TRUE
           WHEN IRIS-FUNC-DLIT
      * INITIALIZE PCBS FOR THE BATCH PROGRAMS
             PERFORM HANDLE-BATCH-PCB THRU HANDLE-BATCH-PCB-EX
           WHEN IRIS-FUNC-SCHD
           WHEN IRIS-FUNC-SCHB
      * MANAGE THE EXEC DLI SCHED FOR FITTING IN THE CBLTDLI SCHED
             PERFORM HANDLE-SCHED THRU HANDLE-SCHED-EX
           WHEN IRIS-FUNC-PCB
      * INITIALIZE PCBS FOR THE ONLINE PROGRAMS
             PERFORM HANDLE-ONLINE-PCB THRU HANDLE-ONLINE-PCB-EX
           WHEN IRIS-FUNC-TERM
      * PERFORM TERM
             PERFORM HANDLE-TERM THRU HANDLE-TERM-EX
           WHEN IRIS-FUNC-CHKP
      * PERFORM CHKP
             PERFORM HANDLE-CHKP THRU HANDLE-CHKP-EX
           WHEN IRIS-FUNC-XRST
      * PERFORM XRST
             PERFORM HANDLE-XRST THRU HANDLE-XRST-EX
           WHEN IRIS-FUNC-ROLB
      * PERFORM ROLB
             PERFORM HANDLE-ROLB THRU HANDLE-ROLB-EX
           WHEN IRIS-FUNC-APSB
      * PERFORM APSB
             PERFORM HANDLE-APSB THRU HANDLE-APSB-EX
           WHEN IRIS-FUNC-DPSB
      * PERFORM DPSB
             PERFORM HANDLE-DPSB THRU HANDLE-DPSB-EX
           WHEN OTHER
      * ABEND OR RETURN - MANAGED FROM THE CALLER
             CONTINUE
           END-EVALUATE
      *
           PERFORM FINALIZE-VARIABLES THRU FINALIZE-VARIABLES-EX
      *
           IF IRIS-TRACE-STANDARD
             MOVE 0 TO IRIS-MSG-LEN
             IF IRIS-NO-ERROR
               STRING '<IRISTRACE> - IRISUTCP:END' NL
                      ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                      ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                      ' SECTION    =(' WS-LAST-IORTN-SECTION ') '
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             ELSE
               MOVE 1 TO WS-ERROR-DESCRIPTION-LEN
               STRING IRIS-ERROR-DESCRIPTION(IRIS-ERR-MESSAGE-ID)
               DELIMITED BY '_'
               INTO WS-ERROR-DESCRIPTION
               POINTER WS-ERROR-DESCRIPTION-LEN
               SUBTRACT 1 FROM WS-ERROR-DESCRIPTION-LEN
               MOVE IRIS-ERR-MESSAGE-ID TO WS-MESSAGE-ID-EDITED
               STRING '<IRISTRACE> - IRISUTCP:END' NL
                      ' CALLER PGM =(' IRIS-PROGRAM-NAME ') ' NL
                      ' IMS FUNC   =(' IRIS-IMS-FUNCTION ') ' NL
                      ' SECTION    =(' WS-LAST-IORTN-SECTION ') ' NL
                      ' ERROR ID   =(' WS-MESSAGE-ID-EDITED ') ' NL
                      ' ERROR DESCR=('
               WS-ERROR-DESCRIPTION(1:WS-ERROR-DESCRIPTION-LEN)') '
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             END-IF
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF.
      *
       MAIN-PROGRAM-EX.
      *
           GOBACK.
      *
      ******************************************************************
      *    INITIALIZE VARIABLES
      ******************************************************************
      *
       INIT-VARIABLES SECTION.
      *
           MOVE ZERO TO WS-DBD-LOOKUP-TABLE-COUNT
                        WS-CMPAT-IDX
           SET WS-PSB-NOT-ALLOCATED TO TRUE.
      *
       INIT-VARIABLES-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    FINALIZE VARIABLES
      ******************************************************************
      *
       FINALIZE-VARIABLES SECTION.
      *
           CONTINUE.
      *
       FINALIZE-VARIABLES-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    HANDLE EXEC DLI SCHEDULE
      ******************************************************************
      *
       HANDLE-SCHED SECTION.
      *
           MOVE 'HANDLE-SCHED' TO WS-LAST-IORTN-SECTION
      *
           SET ADDRESS OF LK-DIB-BLOCK TO ADDRESS OF IRIS-LK-DIB-BLOCK
           MOVE LK-DIB-BLOCK TO IRIS-DIB-BLOCK
           IF IRIS-FUNC-SCHD
             PERFORM HANDLE-ONLINE-PCB THRU HANDLE-ONLINE-PCB-EX
ALTERN*      SET IRIS-EXEC-DLI-PTR TO IRIS-LK-BLL-POINTER
             SET IRISADDR-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA IRIS-BLL-CELLS
                                    IRIS-EXEC-DLI-PTR
           ELSE
             PERFORM SET-LINKAGE-PCBS THRU SET-LINKAGE-PCBS-EX
             PERFORM HANDLE-BATCH-PCB THRU HANDLE-BATCH-PCB-EX
           END-IF
           PERFORM SET-DIB-BLOCK THRU SET-DIB-BLOCK-EX.
     *
       HANDLE-SCHED-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    HANDLE PCB SCHEDULE FOR ONLINE
      ******************************************************************
      *
       HANDLE-ONLINE-PCB SECTION.
      *
           MOVE 'HANDLE-ONLINE-PCB' TO WS-LAST-IORTN-SECTION
      *
           MOVE SPACE TO IRIS-AUDIT-FIELDS
           MOVE LOW-VALUE TO IRIS-PCBS-AREA
           MOVE IRIS-PARAM-NUM TO WS-SCHED-PARAM-NUM
      *
           MOVE IRIS-LK-PSB-NAME TO IRIS-PSB-NAME
           IF IRIS-INTERMEDIATE
             PERFORM INTERMEDIATE-SCHED THRU INTERMEDIATE-SCHED-EX
           ELSE
             PERFORM FINAL-SCHED THRU FINAL-SCHED-EX
           END-IF.
      *
       HANDLE-ONLINE-PCB-EX.
      *
           EXIT.
      ******************************************************************
      *    HANDLE TERM
      ******************************************************************
      *
       HANDLE-TERM SECTION.
      *
           MOVE 'HANDLE-TERM' TO WS-LAST-IORTN-SECTION
      *
      *    INITIALIZE IRIS-USED-KEYS-PSB.
           MOVE LOW-VALUE TO IRIS-PCBS-AREA
           IF IRIS-FINAL
             EXEC SQL COMMIT END-EXEC
           END-IF
      *
           IF IRIS-INTERMEDIATE
             CALL IRIS-IMS-API USING IRIS-IMS-FUNCTION
           END-IF.
      *
       HANDLE-TERM-EX.
      *
           EXIT.
      ******************************************************************
      *    HANDLE ROLB
      ******************************************************************
      *
       HANDLE-ROLB SECTION.
      *
           MOVE 'HANDLE-ROLB' TO WS-LAST-IORTN-SECTION
      *
      *    INITIALIZE IRIS-USED-KEYS-PSB
           MOVE LOW-VALUE TO IRIS-PCBS-AREA
           EXEC SQL ROLLBACK END-EXEC
      *
           IF IRIS-INTERMEDIATE
             CALL IRIS-IMS-API USING IRIS-IMS-FUNCTION
                                     IRIS-PCB-CHKP
           END-IF.
      *
       HANDLE-ROLB-EX.
      *
           EXIT.
      ******************************************************************
      *    HANDLE CHKP
      ******************************************************************
      *
       HANDLE-CHKP SECTION.
      *
           MOVE 'HANDLE-CHKP' TO WS-LAST-IORTN-SECTION
      *
MSYSTS**** UNCOMMENT NEXT INSTRUCTIONS IN CASE A
MSYSTS**** DATABASE POINTER RESET IS REQUIRED WHEN DOING A CHKP
MSYSTS*    DISPLAY 'RESETTING DATABASE POSITION...'
MSYSTS*    PERFORM VARYING WS-INDEX FROM 1 BY 1
MSYSTS*            UNTIL WS-INDEX > IRIS-MAX-PCBS
MSYSTS*      INITIALIZE RUN-USED-KEY-SECONDARY(WS-INDEX)
MSYSTS*      INITIALIZE RUN-USED-KEY-GNUNQ-NEXT-SEG(WS-INDEX)
MSYSTS*      INITIALIZE RUN-USED-KEY-GNUNQ-SAVE-POS(WS-INDEX)
MSYSTS*      PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
MSYSTS*              UNTIL WS-INDEX-2 > 16
MSYSTS*        INITIALIZE RUN-USED-KEYS-PSB(WS-INDEX, WS-INDEX-2)
MSYSTS*      END-PERFORM
MSYSTS*    END-PERFORM
      *
           ACCEPT WS-CHECKPOINT-TIME(1:8) FROM DATE YYYYMMDD
           ACCEPT WS-CHECKPOINT-TIME(9:8) FROM TIME
           IF IRIS-CHKP-ID(1:8) = SPACES OR LOW-VALUES
             MOVE WS-CHECKPOINT-TIME(3:) TO WS-CHKP-ID
             STRING '<IRISTRACE> - IRISUTCP: CHKP' NL
                    ' INPUT CHKP-ID SPACES OR LOW-VALUES ' NL
                    ' USED TIMESTAMP: ' WS-CHKP-ID
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           ELSE
             MOVE IRIS-CHKP-ID TO WS-CHKP-ID
           END-IF
      *
      *    CHECK IF THERE IS ALREADY A PREVIOUS CHECKPOINT
      *
           MOVE 'REGION' TO REGION-ID OF IRISC1VW
           MOVE DT-PSB-NAME(1) TO PSB-NAME OF IRISC1VW
           MOVE WS-CHKP-ID TO CHECKPOINT-ID OF IRISC1VW
           EXEC SQL
             DECLARE CHECKPOINT_CRS CURSOR FOR
             SELECT
               CHECKPOINT_ID
             FROM
               IRISC1VW
             WHERE
               REGION_ID = :IRISC1VW.REGION-ID
             AND
               PSB_NAME =  :IRISC1VW.PSB-NAME
             AND
               CHECKPOINT_ID = :IRISC1VW.CHECKPOINT-ID
             FOR UPDATE
           END-EXEC
      *
           PERFORM VERIFY-SQLCODE
      *
           EXEC SQL
             OPEN CHECKPOINT_CRS
           END-EXEC
      *
           PERFORM VERIFY-SQLCODE
      *
           EXEC SQL
             FETCH CHECKPOINT_CRS
             INTO
               :IRISC1VW.CHECKPOINT-ID
           END-EXEC
      *
           PERFORM VERIFY-SQLCODE
      *
           MOVE 1 TO VARS-COUNT OF IRISC1VW
           MOVE IRIS-CHKP-AREA-LEN TO VARS-AREA-LEN OF IRISC1VW
           MOVE IRIS-CHKP-AREA(1:IRIS-CHKP-AREA-LEN) TO
                VARS-AREA-TEXT OF IRISC1VW
           MOVE WS-CHKP-ID TO CHECKPOINT-ID OF IRISC1VW
           MOVE SQLCODE TO IRIS-DB-SQLCODE
           IF IRIS-SQL-NOT-FOUND
             EXEC SQL INSERT
               INTO
                 IRISC1VW(
                   REGION_ID,
                   PSB_NAME,
                   CHECKPOINT_ID,
                   CHECKPOINT_TIME,
                   VARS_COUNT,
                   VARS_AREA
                 )
               VALUES(
                 :IRISC1VW.REGION-ID,
                 :IRISC1VW.PSB-NAME,
                 :IRISC1VW.CHECKPOINT-ID,
                 :WS-CHECKPOINT-TIME,
                 :IRISC1VW.VARS-COUNT,
                 :IRISC1VW.VARS-AREA
               )
             END-EXEC
           ELSE
             EXEC SQL
               UPDATE IRISC1VW
               SET
                 CHECKPOINT_TIME = :WS-CHECKPOINT-TIME,
                 VARS_COUNT =      :IRISC1VW.VARS-COUNT,
                 VARS_AREA =       :IRISC1VW.VARS-AREA
               WHERE
                 REGION_ID = :IRISC1VW.REGION-ID
               AND
                 PSB_NAME = :IRISC1VW.PSB-NAME
               AND
                 CHECKPOINT_ID = :IRISC1VW.CHECKPOINT-ID
             END-EXEC
           END-IF
      *
      *  PRESERVE INSERT/UPDATE SQLCODE
           MOVE SQLCODE TO WS-SQLCODE
           EXEC SQL
             CLOSE CHECKPOINT_CRS
           END-EXEC
           MOVE WS-SQLCODE TO SQLCODE
      *
           PERFORM VERIFY-SQLCODE
      *
           MOVE VARS-AREA-LEN  OF IRISC1VW TO IRIS-CHKP-AREA-LEN
           MOVE VARS-AREA-TEXT OF IRISC1VW TO
                IRIS-CHKP-AREA(1:IRIS-CHKP-AREA-LEN)
      *
           PERFORM VARYING WS-INDEX FROM 1 BY 1
                   UNTIL WS-INDEX > WS-SCHED-PARAM-NUM
             MOVE IRIS-PCB-DBD-NAME(WS-INDEX) TO WS-IO-RTN-DBDNAME
             IF RTN-IS-GSAMDBD
               MOVE IRIS-PARAM-NUM TO WS-PARAM-NUM
               MOVE 2 TO IRIS-PARAM-NUM
               SET IRIS-CBLTDLI TO TRUE
      *        MOVE IRIS-TRACE-LEVEL TO WS-TRACE-LEVEL
      *        SET IRIS-TRACE-NONE TO TRUE
               SET WS-DUMMY-PCB-IRIS-EYE TO TRUE
               SET CHKP-CHKP-SYMBOLIC TO TRUE
               SET IRIS-FUNC-CHKP TO TRUE
               MOVE 'REGION' TO IRIS-GSAM-REGION-ID
               MOVE DT-PSB-NAME(WS-INDEX) TO IRIS-GSAM-PSB-NAME
               MOVE WS-INDEX TO IRIS-GSAM-PCB-NUM
               MOVE WS-CHKP-ID TO IRIS-GSAM-CHECKPOINT-ID
               MOVE WS-CHECKPOINT-TIME TO IRIS-GSAM-CHECKPOINT-TIME
               CALL WS-IO-RTN USING IRIS-WORK-AREA
                                    IRIS-PCBS-TAB(WS-INDEX)
               MOVE WS-PARAM-NUM TO IRIS-PARAM-NUM
      *        MOVE WS-TRACE-LEVEL TO IRIS-TRACE-LEVEL
               IF IRIS-PCB-STATUS-CODE(WS-INDEX) NOT = SPACES
                 SET ADDRESS OF IRIS-DB-PCB TO
                     ADDRESS OF IRIS-PCB-CHKP
                 SET DB-STATUS-INTERNAL-NOT-HANDLED TO TRUE
                 GOBACK
               END-IF
             END-IF
           END-PERFORM
      *
           IF IRIS-FINAL
             EXEC SQL COMMIT END-EXEC
           ELSE
             CALL IRIS-IMS-API USING IRIS-IMS-FUNCTION
                                     IRIS-PCB-CHKP
                                     IRIS-CHKP-LENGTH
                                     IRIS-CHKP-ID
                                     IRIS-CHKP-AREA-LEN
                                     IRIS-CHKP-AREA
           END-IF.
      *
       HANDLE-CHKP-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    HANDLE XRST
      ******************************************************************
      *
       HANDLE-XRST SECTION.
      *
           MOVE 'HANDLE-XRST' TO WS-LAST-IORTN-SECTION
      *
           MOVE IRIS-CHKP-ID TO WS-CHKP-ID
           IF IRIS-INTERMEDIATE
             CALL IRIS-IMS-API USING IRIS-IMS-FUNCTION
                                     IRIS-PCB-CHKP
                                     IRIS-CHKP-LENGTH
                                     IRIS-CHKP-ID
                                     IRIS-CHKP-AREA-LEN
                                     IRIS-CHKP-AREA
           END-IF.
      *
           IF WS-CHKP-ID(1:8) = SPACES OR LOW-VALUES
             IF IRIS-TRACE-STANDARD
               STRING '<IRISTRACE> - IRISUTCP: XRST' NL
                      ' GETTING CHKP-ID FROM COLD PARAMETER'
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
      *
             MOVE WS-CHKP-ID TO IRIS-GSAM-CHECKPOINT-ID
             SET IRISCHKP-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
      *
             IF IRIS-GSAM-CHECKPOINT-ID = SPACES OR LOW-VALUES
               IF IRIS-TRACE-STANDARD
                 STRING ' CHKP-ID NOT FOUND: NO RESTART'
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                 SET IRISTRAC-RTN TO TRUE
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               END-IF
               GO TO HANDLE-XRST-EX
             END-IF
             IF IRIS-TRACE-STANDARD
               STRING ' CHKP-ID FOUND: ' IRIS-GSAM-CHECKPOINT-ID
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               MOVE IRIS-GSAM-CHECKPOINT-ID TO WS-CHKP-ID
             END-IF
           END-IF
           MOVE 'REGION' TO REGION-ID OF IRISC1VW
           MOVE DT-PSB-NAME(1) TO PSB-NAME OF IRISC1VW
           MOVE WS-CHKP-ID TO CHECKPOINT-ID OF IRISC1VW
           IF WS-CHKP-ID = 'LAST'
             EXEC SQL
               DECLARE RESTART_LAST_CRS CURSOR FOR
               SELECT
                 CHECKPOINT_ID,
                 VARS_COUNT,
                 VARS_AREA
               FROM
                 IRISC1VW
               WHERE
                 REGION_ID = :IRISC1VW.REGION-ID
               AND
                 PSB_NAME =  :IRISC1VW.PSB-NAME
               ORDER BY
                 CHECKPOINT_TIME DESC
             END-EXEC
      *
             PERFORM VERIFY-SQLCODE
      *
             EXEC SQL
               OPEN RESTART_LAST_CRS
             END-EXEC
      *
             PERFORM VERIFY-SQLCODE
      *
             EXEC SQL
               FETCH RESTART_LAST_CRS
               INTO
                 :IRISC1VW.CHECKPOINT-ID,
                 :IRISC1VW.VARS-COUNT,
                 :IRISC1VW.VARS-AREA
             END-EXEC
      *
             PERFORM VERIFY-SQLCODE
      *
             MOVE CHECKPOINT-ID OF IRISC1VW TO WS-CHKP-ID
      *
             EXEC SQL
               CLOSE RESTART_LAST_CRS
             END-EXEC
      *
           ELSE
             EXEC SQL
               SELECT
                 VARS_COUNT,
                 VARS_AREA
               INTO
                 :IRISC1VW.VARS-COUNT,
                 :IRISC1VW.VARS-AREA
               FROM
                 IRISC1VW
               WHERE
                 REGION_ID = :IRISC1VW.REGION-ID
               AND
                 PSB_NAME =  :IRISC1VW.PSB-NAME
               AND
                 CHECKPOINT_ID = :IRISC1VW.CHECKPOINT-ID
             END-EXEC
      *
             PERFORM VERIFY-SQLCODE
      *
           END-IF
      *
           MOVE SQLCODE TO IRIS-DB-SQLCODE
           IF IRIS-SQL-NOT-FOUND
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISUTCP: XRST' NL
                    ' PSB-NAME=(' PSB-NAME OF IRISC1VW ') ' NL
                    ' CHKP-ID =(' WS-CHKP-ID  ') ' NL
             'PREVIOUS CHKP NOT FOUND: NO RESTART '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             GO TO HANDLE-XRST-EX
           END-IF.
      *
           MOVE VARS-AREA-LEN OF IRISC1VW TO IRIS-CHKP-AREA-LEN
           MOVE VARS-AREA-TEXT OF IRISC1VW TO
               IRIS-CHKP-AREA(1:IRIS-CHKP-AREA-LEN)
      *
           PERFORM VARYING WS-INDEX FROM 1 BY 1
                   UNTIL WS-INDEX > WS-SCHED-PARAM-NUM
             MOVE IRIS-PCB-DBD-NAME(WS-INDEX) TO WS-IO-RTN-DBDNAME
             IF RTN-IS-GSAMDBD
               MOVE IRIS-PARAM-NUM TO WS-PARAM-NUM
               MOVE 2 TO IRIS-PARAM-NUM
               SET IRIS-CBLTDLI TO TRUE
      *        MOVE IRIS-TRACE-LEVEL TO WS-TRACE-LEVEL
      *        SET IRIS-TRACE-NONE TO TRUE
               SET WS-DUMMY-PCB-IRIS-EYE TO TRUE
               SET CHKP-X-RESTART TO TRUE
               SET IRIS-FUNC-XRST TO TRUE
               MOVE 'REGION' TO IRIS-GSAM-REGION-ID
               MOVE DT-PSB-NAME(WS-INDEX) TO IRIS-GSAM-PSB-NAME
               MOVE WS-INDEX TO IRIS-GSAM-PCB-NUM
               MOVE WS-CHKP-ID TO IRIS-GSAM-CHECKPOINT-ID
               CALL WS-IO-RTN USING IRIS-WORK-AREA
                                    IRIS-PCBS-TAB(WS-INDEX)
               MOVE WS-PARAM-NUM TO IRIS-PARAM-NUM
      *        MOVE WS-TRACE-LEVEL TO IRIS-TRACE-LEVEL
               IF IRIS-PCB-STATUS-CODE(WS-INDEX) NOT = SPACES
                 SET ADDRESS OF IRIS-DB-PCB TO
                     ADDRESS OF IRIS-PCB-CHKP
                 SET DB-STATUS-INTERNAL-NOT-HANDLED TO TRUE
                 GOBACK
               END-IF
             END-IF
           END-PERFORM.
      *
       HANDLE-XRST-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    SCHED FOR INTERMEDIATE VERSION
      ******************************************************************
      *
       INTERMEDIATE-SCHED SECTION.
      *
           MOVE 'INTERMEDIATE-SCHED' TO WS-LAST-IORTN-SECTION
      *
           SET IRIS-FUNC-PCB TO TRUE
           IF IRIS-TRACE-STANDARD
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISUTCP: SCHED' NL
                    ' PSB NAME   =(' IRIS-LK-PSB-NAME ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           CALL IRIS-IMS-API USING IRIS-IMS-FUNCTION
                                   IRIS-LK-PSB-NAME
                                   IRIS-LK-BLL-POINTER

           IF IRIS-TRACE-DEBUG
             MOVE 0 TO IRIS-MSG-LEN
             MOVE IRIS-LK-BLL-ADDR TO WS-ADDR-9
             STRING '<IRISTRACE> - IRISUTCP: IMS UIB' NL
                    ' IMS UIB ADDRESS =(' WS-ADDR-9 ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           SET ADDRESS OF IRIS-LK-PCBS-ADDR TO IRIS-LK-BLL-POINTER
           SET ADDRESS OF IRIS-LK-CELLS TO IRIS-LK-PCBS-PTR
           SET IRISADDR-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA IRIS-DLIUIB
                                  IRIS-LK-BLL-POINTER
           IF IRIS-TRACE-DEBUG
             MOVE 0 TO IRIS-MSG-LEN
             MOVE IRIS-LK-BLL-ADDR TO WS-ADDR-9
             STRING '<IRISTRACE> - IRISUTCP: IRIS UIB' NL
                    ' IRIS UIB ADDRESS =(' WS-ADDR-9 ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           SET IRISADDR-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA IRIS-BLL-CELLS
                                  IRIS-UIBPCBAL
           PERFORM VARYING WS-INDEX FROM 1 BY 1
                   UNTIL WS-INDEX > IRIS-PARAM-NUM
             SET ADDRESS OF IRIS-DB-PCB TO IRIS-LK-POINTER(WS-INDEX)
             MOVE DB-PCB-DBD-NAME TO WS-RUN-DBD-NAME
             PERFORM DUAL-LOOKUP THRU DUAL-LOOKUP-EX
             IF WS-IRISDLVW-NOK
               MOVE HIGH-VALUE TO IRIS-UIBRCODE
               GO TO INTERMEDIATE-SCHED-EX
             END-IF
      *
             IF WS-DUAL-IMS-ONLY
               IF IRIS-TRACE-STANDARD
                 MOVE 0 TO IRIS-MSG-LEN
                 MOVE WS-INDEX TO WS-INDEX-Z
                 STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                        ' DBD ' DB-PCB-DBD-NAME ' IMS ONLY'
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                 SET IRISTRAC-RTN TO TRUE
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               END-IF
               SET IRIS-BLL-POINTER(WS-INDEX) TO
                   IRIS-LK-POINTER(WS-INDEX)
             ELSE
      *
               IF IRIS-TRACE-STANDARD
                 EVALUATE TRUE
                 WHEN WS-DUAL-SQL-ONLY
                   MOVE 0 TO IRIS-MSG-LEN
                   MOVE WS-INDEX TO WS-INDEX-Z
                   STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                          ' DBD ' DB-PCB-DBD-NAME ' DB2 ONLY'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                 WHEN WS-DUAL-BOTH
                   MOVE 0 TO IRIS-MSG-LEN
                   MOVE WS-INDEX TO WS-INDEX-Z
                   STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                          ' DBD ' DB-PCB-DBD-NAME ' DUAL'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                 WHEN WS-DUAL-UPDATE-ONLY
                   MOVE 0 TO IRIS-MSG-LEN
                   MOVE WS-INDEX TO WS-INDEX-Z
                   STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                          ' DBD ' DB-PCB-DBD-NAME ' DUAL UPDATE ONLY'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                 WHEN OTHER
                   MOVE 0 TO IRIS-MSG-LEN
                   MOVE WS-INDEX TO WS-INDEX-Z
                   STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                          ' DBD ' DB-PCB-DBD-NAME ' NOT MANAGED FLAG'
                          ' IN IRISDLDW TABLE!!!!'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                 END-EVALUATE
               END-IF
               PERFORM ALLOCATE-PSB THRU ALLOCATE-PSB-EX
               IF TAB-PSB-MEM-IS-CMPAT
                 MOVE 1 TO WS-CMPAT-IDX
               END-IF
               IF NOT IRIS-NO-ERROR
                 DISPLAY 'IRISUTIL - ALLOCATE PSB ERROR'
                 GO TO INTERMEDIATE-SCHED-EX
               END-IF
               PERFORM ALLOCATE-PCB THRU ALLOCATE-PCB-EX
      *
               INITIALIZE IRIS-PCB-HEADER(WS-INDEX)
               SET IRISADDR-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                      IRIS-PCBS-TAB(WS-INDEX) WS-PTR
               SET IRIS-BLL-POINTER(WS-INDEX) TO WS-PTR
               SET ADDRESS OF IRIS-DB-PCB TO IRIS-BLL-POINTER(WS-INDEX)
               SET DB-PCB-IRIS-EYE TO TRUE
               MOVE WS-RUN-DBD-NAME TO DB-PCB-DBD-NAME
               MOVE DB-PCB-FIXED-PART TO IRIS-PCB-HEADER(WS-INDEX)
               SET RUN-IMS-DUAL-POINTER(WS-INDEX)
                                          TO IRIS-LK-POINTER(WS-INDEX)
               MOVE WS-RUN-DBD-STATUS TO RUN-DBD-STATUS(WS-INDEX)
             END-IF
           END-PERFORM.
      *
       INTERMEDIATE-SCHED-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    SCHED FOR FINAL VERSION
      ******************************************************************
      *
       FINAL-SCHED SECTION.
      *
           MOVE 'FINAL-SCHED' TO WS-LAST-IORTN-SECTION
      *
           IF IRIS-TRACE-STANDARD
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISUTCP: SCHED' NL
                    ' PSB NAME   =(' IRIS-LK-PSB-NAME ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           SET IRISADDR-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA IRIS-DLIUIB
                                  IRIS-LK-BLL-POINTER
           IF IRIS-TRACE-DEBUG
             MOVE 0 TO IRIS-MSG-LEN
             MOVE IRIS-LK-BLL-ADDR TO WS-ADDR-9
             STRING '<IRISTRACE> - IRISUTCP: IRIS UIB' NL
                    ' IRIS UIB ADDRESS =(' WS-ADDR-9 ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           SET IRISADDR-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA IRIS-BLL-CELLS
                                  IRIS-UIBPCBAL
           PERFORM VARYING WS-INDEX FROM 1 BY 1
                   UNTIL WS-INDEX > IRIS-PARAM-NUM
      *
             PERFORM ALLOCATE-PSB THRU ALLOCATE-PSB-EX
      *
             IF TAB-PSB-MEM-IS-CMPAT
               MOVE 1 TO WS-CMPAT-IDX
             END-IF
      *
             IF NOT IRIS-NO-ERROR
               DISPLAY 'IRISUTIL - ALLOCATE PSB ERROR'
               GO TO FINAL-SCHED-EX
             END-IF
      *
             PERFORM ALLOCATE-PCB THRU ALLOCATE-PCB-EX
      *
             INITIALIZE IRIS-PCB-HEADER(WS-INDEX)
             SET IRISADDR-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                    IRIS-PCBS-TAB(WS-INDEX) WS-PTR
             SET IRIS-BLL-POINTER(WS-INDEX) TO WS-PTR
             SET ADDRESS OF IRIS-DB-PCB TO IRIS-BLL-POINTER(WS-INDEX)
             SET DB-PCB-IRIS-EYE TO TRUE
             MOVE WS-IO-RTN-DBDNAME TO DB-PCB-DBD-NAME
             MOVE DB-PCB-FIXED-PART TO IRIS-PCB-HEADER(WS-INDEX)
             SET RUN-IMS-DUAL-POINTER(WS-INDEX) TO NULL
             SET WS-DUAL-SQL-ONLY TO TRUE
             MOVE WS-RUN-DBD-STATUS TO RUN-DBD-STATUS(WS-INDEX)
           END-PERFORM.
      *
       FINAL-SCHED-EX.
      *
           EXIT.
      ******************************************************************
      *    HANDLE APSB
      ******************************************************************
      *
       HANDLE-APSB SECTION.
      *
           MOVE 'HANDLE-APSB' TO WS-LAST-IORTN-SECTION
      *
      *    ALLOCATE PSB FOR STORED PROCEDURES
      *
      *    INITIALIZE IRIS-USED-KEYS-PSB.
           MOVE LOW-VALUE TO IRIS-PCBS-AREA
           MOVE IRIS-PARAM-NUM TO WS-SCHED-PARAM-NUM
      *
           MOVE SPACE TO IRIS-AUDIT-FIELDS
      *
           SET ADDRESS OF IRIS-AIB TO ADDRESS OF IRIS-PCB-AIB
           IF IRIS-TRACE-STANDARD
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISUTCP: SCHED' NL
                    ' PSB NAME   =(' IRIS-AIBRSNM1 ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           MOVE IRIS-AIBRSNM1 TO IRIS-PSB-NAME
           IF IRIS-INTERMEDIATE
             CALL IRIS-IMS-API USING IRIS-IMS-FUNCTION
                                     IRIS-AIB
             IF IRIS-AIBRETRN NOT = ZEROES
               SET ADDRESS OF IRIS-PCB-AIB TO ADDRESS OF IRIS-AIB
               DISPLAY 'IRISUTIL - APSB FAILED -'
                       ' RETURN CODE: ' IRIS-AIBRETRN
                       ' REASON CODE: ' IRIS-AIBREASN
                       ' ERRCOD EXT : ' IRIS-AIBERRXT
               GO TO HANDLE-APSB-EX
             END-IF
             IF IRIS-TRACE-DEBUG
               MOVE 0 TO IRIS-MSG-LEN
               MOVE IRIS-AIBRESA1-ADDR TO WS-ADDR-9
               STRING '<IRISTRACE> - IRISUTCP: IMS PCBS' NL
                      ' IMS PCBS ADDRESS =(' WS-ADDR-9 ') '
               MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
               SET IRISTRAC-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             END-IF
           END-IF
      *
           SET ADDRESS OF IRIS-LK-CELLS TO IRIS-AIBRESA1
           SET IRISADDR-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA IRIS-BLL-CELLS
                                  IRIS-AIB-POINTER
           IF IRIS-TRACE-DEBUG
             MOVE 0 TO IRIS-MSG-LEN
             MOVE IRIS-AIB-ADDRESS TO WS-ADDR-9
             STRING '<IRISTRACE> - IRISUTCP: IRIS PCBS' NL
                    ' IMS PCBS ADDRESS =(' WS-ADDR-9 ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
           PERFORM VARYING WS-INDEX FROM 1 BY 1
                   UNTIL WS-INDEX > IRIS-PARAM-NUM
             IF IRIS-INTERMEDIATE
               SET ADDRESS OF IRIS-DB-PCB TO IRIS-LK-POINTER(WS-INDEX)
               MOVE DB-PCB-DBD-NAME TO WS-RUN-DBD-NAME
               PERFORM DUAL-LOOKUP THRU DUAL-LOOKUP-EX
               IF WS-IRISDLVW-NOK
                 MOVE -1 TO IRIS-AIBRETRN
                 GO TO HANDLE-APSB-EX
               END-IF
             ELSE
               SET WS-DUAL-SQL-ONLY TO TRUE
             END-IF
      *
             IF WS-DUAL-IMS-ONLY
               IF IRIS-TRACE-STANDARD
                 MOVE 0 TO IRIS-MSG-LEN
                 MOVE WS-INDEX TO WS-INDEX-Z
                 STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                        ' DBD ' DB-PCB-DBD-NAME ' IMS ONLY'
                 MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                 SET IRISTRAC-RTN TO TRUE
                 CALL IRIS-WS-RTN USING IRIS-WORK-AREA
               END-IF
               SET IRIS-BLL-POINTER(WS-INDEX) TO
                   IRIS-LK-POINTER(WS-INDEX)
             ELSE
      *
               IF IRIS-INTERMEDIATE AND IRIS-TRACE-STANDARD
                 EVALUATE TRUE
                 WHEN WS-DUAL-SQL-ONLY
                   MOVE 0 TO IRIS-MSG-LEN
                   MOVE WS-INDEX TO WS-INDEX-Z
                   STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                          ' DBD ' DB-PCB-DBD-NAME ' DB2 ONLY'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                 WHEN WS-DUAL-BOTH
                   MOVE 0 TO IRIS-MSG-LEN
                   MOVE WS-INDEX TO WS-INDEX-Z
                   STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                          ' DBD ' DB-PCB-DBD-NAME ' DUAL'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                 WHEN WS-DUAL-UPDATE-ONLY
                   MOVE 0 TO IRIS-MSG-LEN
                   MOVE WS-INDEX TO WS-INDEX-Z
                   STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                          ' DBD ' DB-PCB-DBD-NAME ' DUAL UPDATE ONLY'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                 WHEN OTHER
                   MOVE 0 TO IRIS-MSG-LEN
                   MOVE WS-INDEX TO WS-INDEX-Z
                   STRING '<IRISTRACE> - IRISUTCP: PCB #' WS-INDEX-Z
                          ' DBD ' DB-PCB-DBD-NAME ' NOT MANAGED FLAG'
                          ' IN IRISDLDW TABLE!!!!'
                   MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
                   SET IRISTRAC-RTN TO TRUE
                   CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                 END-EVALUATE
               END-IF
               PERFORM ALLOCATE-PSB THRU ALLOCATE-PSB-EX
               IF NOT IRIS-NO-ERROR
                 DISPLAY 'IRISUTIL - ALLOCATE PSB ERROR'
                 GO TO HANDLE-APSB-EX
               END-IF
               PERFORM ALLOCATE-PCB THRU ALLOCATE-PCB-EX
      *
               IF IRIS-FINAL
                 MOVE WS-IO-RTN-DBDNAME TO WS-RUN-DBD-NAME
               END-IF
      *
               INITIALIZE IRIS-PCB-HEADER(WS-INDEX)
               SET IRISADDR-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                      IRIS-PCBS-TAB(WS-INDEX) WS-PTR
               SET IRIS-BLL-POINTER(WS-INDEX) TO WS-PTR
               SET ADDRESS OF IRIS-DB-PCB TO IRIS-BLL-POINTER(WS-INDEX)
               SET DB-PCB-IRIS-EYE TO TRUE
               MOVE WS-RUN-DBD-NAME TO DB-PCB-DBD-NAME
               MOVE DB-PCB-FIXED-PART TO IRIS-PCB-HEADER(WS-INDEX)
               SET RUN-IMS-DUAL-POINTER(WS-INDEX)
                                          TO IRIS-LK-POINTER(WS-INDEX)
               MOVE WS-RUN-DBD-STATUS TO RUN-DBD-STATUS(WS-INDEX)
             END-IF
           END-PERFORM.
      * RESET THE AIB POINTER
           SET ADDRESS OF IRIS-PCB-AIB TO ADDRESS OF IRIS-AIB
           .
       HANDLE-APSB-EX.
      *
           EXIT.
      ******************************************************************
      *    HANDLE DPSB
      ******************************************************************
      *
       HANDLE-DPSB SECTION.
      *
           MOVE 'HANDLE-DPSB' TO WS-LAST-IORTN-SECTION
      *
      *    DE-ALLOCATE PSB FOR STORED PROCEDURES
      *
      *    INITIALIZE IRIS-USED-KEYS-PSB.
           MOVE LOW-VALUE TO IRIS-PCBS-AREA
      *
           SET ADDRESS OF IRIS-AIB TO ADDRESS OF IRIS-PCB-AIB
           IF IRIS-INTERMEDIATE
             CALL IRIS-IMS-API USING IRIS-IMS-FUNCTION
                                     IRIS-AIB
           END-IF
      * RESET THE AIB POINTER
           SET ADDRESS OF IRIS-PCB-AIB TO ADDRESS OF IRIS-AIB
           .
      *
       HANDLE-DPSB-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    HANDLE PCB SCHEDULE FOR BATCH
      ******************************************************************
      *
       HANDLE-BATCH-PCB SECTION.
      *
           MOVE 'HANDLE-BATCH-PCB' TO WS-LAST-IORTN-SECTION
      *
           IF IRIS-TRACE-STANDARD
             MOVE 0 TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - IRISUTCP: SCHED' NL
                    ' PSB NAME   =(' IRIS-PSB-NAME ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
           END-IF
      *
           MOVE SPACE TO IRIS-AUDIT-FIELDS
           MOVE LOW-VALUE TO IRIS-PCBS-AREA
           MOVE IRIS-PARAM-NUM TO WS-SCHED-PARAM-NUM
           MOVE IRIS-PSB-NAME TO DT-PSB-NAME(1)
      *
           IF IRIS-FINAL
             PERFORM ALLOCATE-PSB THRU ALLOCATE-PSB-EX
             IF NOT IRIS-NO-ERROR
               GO TO HANDLE-BATCH-PCB-EX
             END-IF
           END-IF
      *
           MOVE ZERO TO WS-INDEX
      *
      *    IRIS PCB ALLOCATION
      *
           SET IRISADDR-RTN TO TRUE
      *
           PERFORM HANDLE-BATCH-PCB-A THRU HANDLE-BATCH-PCB-A-EX
           PERFORM HANDLE-BATCH-PCB-B THRU HANDLE-BATCH-PCB-B-EX
      *
           .
      *
       HANDLE-BATCH-PCB-EX.
      *
           EXIT.
      *
       HANDLE-BATCH-PCB-A SECTION.
      *
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==1==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==2==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==3==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==4==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==5==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==6==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==7==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==8==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==9==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==10==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==11==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==12==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==13==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==14==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==15==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==16==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==17==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==18==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==19==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==20==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==21==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==22==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==23==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==24==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==25==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==26==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==27==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==28==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==29==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==30==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==31==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==32==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==33==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==34==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==35==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==36==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==37==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==38==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==39==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==40==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==41==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==42==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==43==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==44==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==45==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==46==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==47==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==48==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==49==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==50==.
      *
       HANDLE-BATCH-PCB-A-EX.
      *
           EXIT.
      *
       HANDLE-BATCH-PCB-B SECTION.
      *
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==51==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==52==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==53==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==54==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==55==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==56==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==57==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==58==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==59==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==60==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==61==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==62==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==63==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==64==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==65==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==66==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==67==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==68==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==69==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==70==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==71==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==72==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==73==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==74==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==75==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==76==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==77==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==78==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==79==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==80==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==81==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==82==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==83==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==84==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==85==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==86==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==87==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==88==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==89==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==90==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==91==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==92==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==93==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==94==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==95==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==96==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==97==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==98==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==99==.
           COPY IRISPCBA REPLACING ==:NPCB:== BY ==100==.
      *
       HANDLE-BATCH-PCB-B-EX.
      *
           EXIT.
      ******************************************************************
      *    ALLOCATE PSB
      ******************************************************************
      *
       ALLOCATE-PSB SECTION.
      *
           MOVE 'ALLOCATE-PSB' TO WS-LAST-IORTN-SECTION
      *
           IF WS-PSB-ALLOCATED
             GO TO ALLOCATE-PSB-EX
           END-IF
      *
           SET IRISPSB-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                  IRIS-PSB-NAME
                                  WS-PSB-PCBS-PTR
                                  WS-PSB-SENSEGS-PTR
           IF NOT IRIS-NO-ERROR
             GO TO ALLOCATE-PSB-EX
           END-IF
           SET ADDRESS OF LK-PSB-MEMORY-PCB-AREA TO WS-PSB-PCBS-PTR
           SET ADDRESS OF LK-PSB-MEMORY-SENSEG-AREA
                                              TO WS-PSB-SENSEGS-PTR
           SET WS-PSB-ALLOCATED TO TRUE
      *
           .
      *
       ALLOCATE-PSB-EX.
      *
           EXIT.
      ******************************************************************
      *    ALLOCATE PCB
      ******************************************************************
      *
       ALLOCATE-PCB SECTION.
      *
           MOVE 'ALLOCATE-PCB' TO WS-LAST-IORTN-SECTION
      *
           COMPUTE WS-INDEX-C = WS-INDEX + WS-CMPAT-IDX
           MOVE TAB-PSB-MEM-PCB-AREA(WS-INDEX-C)
                                          TO MEMORY-PCB-AREA(WS-INDEX)
           MOVE DT-DBD-NAME OF MEMORY-PCB-AREA(WS-INDEX)
                                          TO WS-IO-RTN-DBDNAME
      *
           IF RTN-IS-PCBIO OR RTN-IS-GSAM
             SET DT-DBD-SEGM-PTR OF MEMORY-PCB-AREA(WS-INDEX) TO NULL
             SET WS-DUMMY-PCB-IRIS-EYE TO TRUE
             SET DT-DBD-FIELD-PTR OF MEMORY-PCB-AREA(WS-INDEX) TO NULL
             MOVE ZERO TO RUN-FB-KEY-MAX-LENGTH(WS-INDEX)
             MOVE WS-INDEX TO RUN-PCB-INDEX(WS-INDEX)
             SET DT-PSB-SENSEG-PTR OF MEMORY-PCB-AREA(WS-INDEX) TO NULL
             IF RTN-IS-PCBIO
               SET IRISADDR-RTN TO TRUE
               CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                 IRIS-DC-TASK-INFO
                                 DT-DC-PTR OF MEMORY-PCB-AREA(WS-INDEX)
               SET IRIS-DFH-PTR TO IRIS-DFH-POINTER
             END-IF
           ELSE
             MOVE IRIS-PARAM-NUM TO WS-PARAM-NUM
             MOVE 3 TO IRIS-PARAM-NUM
             SET IRIS-CBLTDLI TO TRUE
             MOVE IRIS-TRACE-LEVEL TO WS-TRACE-LEVEL
             SET IRIS-TRACE-NONE TO TRUE
             SET WS-DUMMY-PCB-IRIS-EYE TO TRUE
             SET LOAD-DATA-DICTIONARY-SEG TO TRUE
             CALL WS-IO-RTN USING IRIS-WORK-AREA WS-DUMMY-PCB WS-PTR
             SET DT-DBD-SEGM-PTR OF MEMORY-PCB-AREA(WS-INDEX) TO WS-PTR
             SET WS-DUMMY-PCB-IRIS-EYE TO TRUE
             SET LOAD-DATA-DICTIONARY-FLD TO TRUE
             CALL WS-IO-RTN USING IRIS-WORK-AREA WS-DUMMY-PCB WS-PTR
             MOVE WS-PARAM-NUM TO IRIS-PARAM-NUM
             MOVE WS-TRACE-LEVEL TO IRIS-TRACE-LEVEL
             SET DT-DBD-FIELD-PTR OF MEMORY-PCB-AREA(WS-INDEX) TO WS-PTR
             SET IRISADDR-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                    IRIS-RNT-CNTL-AREA(WS-INDEX) WS-PTR
             SET DT-INFO-PTR OF MEMORY-PCB-AREA(WS-INDEX) TO WS-PTR
             MOVE DT-ADD-NUM-SENSEG OF MEMORY-PCB-AREA(WS-INDEX)
                                                        TO WS-INDEX-2
      *      FEEDBACK KEY LENGTH
             MOVE DT-PCB-KEY-LENGTH OF MEMORY-PCB-AREA(WS-INDEX)
                                      TO RUN-FB-KEY-MAX-LENGTH(WS-INDEX)
             MOVE WS-INDEX TO RUN-PCB-INDEX(WS-INDEX)
             IF RUN-FB-KEY-MAX-LENGTH(WS-INDEX) >
                                   LENGTH OF IRIS-PCB-FEEDBACK(WS-INDEX)
               COMPUTE RUN-FB-KEY-MAX-LENGTH(WS-INDEX) =
                                   LENGTH OF IRIS-PCB-FEEDBACK(WS-INDEX)
             END-IF
             SET IRISADDR-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                          TAB-PSB-MEM-SENSESG-AREA(WS-INDEX-2) WS-PTR
             SET DT-PSB-SENSEG-PTR OF MEMORY-PCB-AREA(WS-INDEX)
                                                            TO WS-PTR
             SET IRISPOPT-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                               MEMORY-PCB-AREA(WS-INDEX)
      * IO Area backup
             SET IRISADDR-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                          IRIS-IO-AREA(WS-INDEX) WS-PTR
             SET RUN-IO-AREA-PTR (WS-INDEX) TO WS-PTR
           END-IF
      *
           .
      *
       ALLOCATE-PCB-EX.
      *
           EXIT.
      ******************************************************************
      *    DUAL LOOKUP
      ******************************************************************
      *
       DUAL-LOOKUP SECTION.
      *
           MOVE 'DUAL-LOOKUP' TO WS-LAST-IORTN-SECTION
      *
           SET WS-DUAL-IMS-ONLY TO TRUE

           MOVE WS-RUN-DBD-NAME(1:3) TO WS-DBD-NAME
           PERFORM CHECK-CLONE-NAME THRU CHECK-CLONE-NAME-EX
           PERFORM VARYING WS-INDEX-2 FROM 1 BY 1
           UNTIL WS-INDEX-2 > WS-DBD-LOOKUP-TABLE-COUNT
      *      OR WS-RUN-DBD-NAME = WS-LOOKUP-DBD(WS-INDEX-2)
             OR WS-DBD-NAME = WS-LOOKUP-DBD(WS-INDEX-2)
             CONTINUE
           END-PERFORM
           IF WS-INDEX-2 <= WS-DBD-LOOKUP-TABLE-COUNT
             MOVE WS-LOOKUP-STATUS(WS-INDEX-2) TO WS-RUN-DBD-STATUS
           ELSE
      *    IF WS-RUN-DBD-NAME NOT = SPACE AND LOW-VALUE
           IF WS-DBD-NAME NOT = SPACE AND LOW-VALUE
               SET WS-IRISDLVW-OK TO TRUE
      *        MOVE WS-RUN-DBD-NAME TO DBD-NAME
DUALTB         MOVE WS-DBD-NAME TO DBD-NAME
DUALTB         EXEC SQL
DUALTB           SELECT DBD_STATUS
DUALTB           INTO :DBD-STATUS
DUALTB           FROM IRISDLVW
DUALTB           WHERE DBD_NAME = :DBD-NAME
DUALTB         END-EXEC
DUALTB         MOVE SQLCODE TO IRIS-DB-SQLCODE
DUALTB         IF IRIS-SQL-OK
DUALTB           EVALUATE DBD-STATUS
DUALTB           WHEN 'B'
DUALTB             SET WS-DUAL-BOTH TO TRUE
DUALTB           WHEN 'D'
DUALTB             SET WS-DUAL-SQL-ONLY TO TRUE
DUALTB           WHEN 'I'
DUALTB             SET WS-DUAL-IMS-ONLY TO TRUE
DUALTB           WHEN 'U'
DUALTB             SET WS-DUAL-UPDATE-ONLY TO TRUE
DUALTB           WHEN OTHER
DUALTB             SET WS-DUAL-IMS-ONLY TO TRUE
DUALTB           END-EVALUATE
DUALTB         ELSE
DUALTB           MOVE 1 TO IRIS-MSG-LEN
DUALTB           MOVE SPACE TO IRIS-MSG-TXT
DUALTB           IF IRIS-SQL-NOT-FOUND
DUALTB             SET WS-DUAL-IMS-ONLY TO TRUE
DUALTB             STRING '(IRISUTIL:DRIVER TABLE-IRISDLVW '
DUALTB             '- DBD ENTRY NOT FOUND, '
DUALTB             'DEFAULTING TO IMS-ONLY)'
DUALTB             DELIMITED BY SIZE
DUALTB             INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
DUALTB           ELSE
DUALTB             SET WS-IRISDLVW-NOK TO TRUE
DUALTB             MOVE SQLCODE TO WS-SQLCODE-E
DUALTB             STRING '(IRISUTIL:IRISDLDW ACCESS ERROR) '
DUALTB             'DBD-NAME=(' DBD-NAME ') '
DUALTB             'SQLCODE =(' WS-SQLCODE-E  ') ' NL
DUALTB             'SQLERRM =(' SQLERRMC(1:SQLERRML)  ') '
DUALTB             DELIMITED BY SIZE
DUALTB             INTO IRIS-MSG-TXT POINTER IRIS-MSG-LEN
DUALTB           END-IF
DUALTB           SET IRISTRAC-RTN TO TRUE
DUALTB           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
DUALTB         END-IF
             ELSE
               SET WS-DUAL-IMS-ONLY TO TRUE
             END-IF
             ADD 1 TO WS-DBD-LOOKUP-TABLE-COUNT
      *      MOVE WS-RUN-DBD-NAME
             MOVE WS-DBD-NAME
                        TO WS-LOOKUP-DBD(WS-DBD-LOOKUP-TABLE-COUNT)
             MOVE WS-RUN-DBD-STATUS
                        TO WS-LOOKUP-STATUS(WS-DBD-LOOKUP-TABLE-COUNT)
           END-IF
      *
           .
      *
       DUAL-LOOKUP-EX.
      *
           EXIT.

       CHECK-CLONE-NAME.
      *
            IF WS-DBD-NAME EQUAL 'XXE' OR 'XXF' OR 'XXG'
              MOVE 'CCA' TO WS-DBD-NAME
            END-IF
            IF WS-DBD-NAME EQUAL 'XXB' OR 'XXC' OR 'XXD'
              MOVE 'CC9' TO WS-DBD-NAME
            END-IF
            IF WS-DBD-NAME EQUAL 'XXA' OR 'XX8' OR 'XX9'
              MOVE 'CC5' TO WS-DBD-NAME
            END-IF
            .
      *
       CHECK-CLONE-NAME-EX.
      *
             EXIT.

      *
      ******************************************************************
      * VERIFY THE SQL OPERATION
      ******************************************************************
      *
       VERIFY-SQLCODE SECTION.
           MOVE SQLCODE TO IRIS-DB-SQLCODE
           IF IRIS-SQL-OK
           OR IRIS-SQL-NOT-FOUND
             CONTINUE
           ELSE
             MOVE 1 TO IRIS-MSG-LEN
             MOVE SPACE TO IRIS-MSG-TXT
             MOVE SQLCODE TO WS-SQLCODE-E
             MOVE 0 TO IRIS-MSG-LEN
             STRING '(IRISUTIL:IRIS CHKP/XRST ACCESS ERROR) '
             'PSB-NAME=(' PSB-NAME OF IRISC1VW ') '
             'SQLCODE =(' WS-SQLCODE-E  ') ' NL
             'SQLERRM =(' SQLERRMC(1:SQLERRML)  ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             SET ADDRESS OF IRIS-DB-PCB TO ADDRESS OF IRIS-PCB-CHKP
             SET DB-STATUS-INTERNAL-NOT-HANDLED TO TRUE
             GOBACK
           END-IF.
       VERIFY-SQLCODE-EX.
           EXIT.
      *
      ******************************************************************
      *    SET DIB BLOCK
      ******************************************************************
      *
       SET-DIB-BLOCK SECTION.
      *
           MOVE 'SET-DIB-BLOCK' TO WS-LAST-IORTN-SECTION
      *
           MOVE 'IR'                   TO IRIS-DIBVER
           MOVE DB-PCB-STATUS-CODE     TO IRIS-DIBSTAT
           MOVE DB-PCB-SEGMENT-NAME    TO IRIS-DIBSEGM
           MOVE DB-PCB-SEGMENT-LEVEL   TO IRIS-DIBSEGLV
           MOVE DB-PCB-FB-KEY-LENGTH   TO IRIS-DIBKFBL
           MOVE DB-PCB-DBD-NAME        TO IRIS-DIBDBDNM
           MOVE 'IRIS'                 TO IRIS-DIBDBORG
           MOVE IRIS-DIB-BLOCK         TO LK-DIB-BLOCK
           SET ADDRESS OF IRIS-LK-DIB-BLOCK TO ADDRESS OF LK-DIB-BLOCK.
      *
       SET-DIB-BLOCK-EX.
      *
           EXIT.
      *
      ******************************************************************
      *    SET LINKAGE PCBS FOR CHALLENGER TOOL USAGE
      ******************************************************************
      *
       SET-LINKAGE-PCBS SECTION.
      *
           MOVE 'SET-LINKAGE-PCBS' TO WS-LAST-IORTN-SECTION
      *
           SET ADDRESS OF IRIS-LK-CELLS TO IRIS-EXEC-DLI-PTR
           SET ADDRESS OF IRIS-PCB-PTR-1 TO
               ADDRESS OF IRIS-LK-POINTER(1)
           SET ADDRESS OF IRIS-PCB-PTR-2   TO
               ADDRESS OF IRIS-LK-POINTER(2)
           SET ADDRESS OF IRIS-PCB-PTR-3   TO
               ADDRESS OF IRIS-LK-POINTER(3)
           SET ADDRESS OF IRIS-PCB-PTR-4   TO
               ADDRESS OF IRIS-LK-POINTER(4)
           SET ADDRESS OF IRIS-PCB-PTR-5   TO
               ADDRESS OF IRIS-LK-POINTER(5)
           SET ADDRESS OF IRIS-PCB-PTR-6   TO
               ADDRESS OF IRIS-LK-POINTER(6)
           SET ADDRESS OF IRIS-PCB-PTR-7   TO
               ADDRESS OF IRIS-LK-POINTER(7)
           SET ADDRESS OF IRIS-PCB-PTR-8   TO
               ADDRESS OF IRIS-LK-POINTER(8)
           SET ADDRESS OF IRIS-PCB-PTR-9   TO
               ADDRESS OF IRIS-LK-POINTER(9)
           SET ADDRESS OF IRIS-PCB-PTR-10  TO
               ADDRESS OF IRIS-LK-POINTER(10)
           SET ADDRESS OF IRIS-PCB-PTR-11  TO
               ADDRESS OF IRIS-LK-POINTER(11)
           SET ADDRESS OF IRIS-PCB-PTR-12  TO
               ADDRESS OF IRIS-LK-POINTER(12)
           SET ADDRESS OF IRIS-PCB-PTR-13  TO
               ADDRESS OF IRIS-LK-POINTER(13)
           SET ADDRESS OF IRIS-PCB-PTR-14  TO
               ADDRESS OF IRIS-LK-POINTER(14)
           SET ADDRESS OF IRIS-PCB-PTR-15  TO
               ADDRESS OF IRIS-LK-POINTER(15)
           SET ADDRESS OF IRIS-PCB-PTR-16  TO
               ADDRESS OF IRIS-LK-POINTER(16)
           SET ADDRESS OF IRIS-PCB-PTR-17  TO
               ADDRESS OF IRIS-LK-POINTER(17)
           SET ADDRESS OF IRIS-PCB-PTR-18  TO
               ADDRESS OF IRIS-LK-POINTER(18)
           SET ADDRESS OF IRIS-PCB-PTR-19  TO
               ADDRESS OF IRIS-LK-POINTER(19)
           SET ADDRESS OF IRIS-PCB-PTR-20  TO
               ADDRESS OF IRIS-LK-POINTER(10)
           SET ADDRESS OF IRIS-PCB-PTR-21  TO
               ADDRESS OF IRIS-LK-POINTER(21)
           SET ADDRESS OF IRIS-PCB-PTR-22  TO
               ADDRESS OF IRIS-LK-POINTER(22)
           SET ADDRESS OF IRIS-PCB-PTR-23  TO
               ADDRESS OF IRIS-LK-POINTER(23)
           SET ADDRESS OF IRIS-PCB-PTR-24  TO
               ADDRESS OF IRIS-LK-POINTER(24)
           SET ADDRESS OF IRIS-PCB-PTR-25  TO
               ADDRESS OF IRIS-LK-POINTER(25)
           SET ADDRESS OF IRIS-PCB-PTR-26  TO
               ADDRESS OF IRIS-LK-POINTER(26)
           SET ADDRESS OF IRIS-PCB-PTR-27  TO
               ADDRESS OF IRIS-LK-POINTER(27)
           SET ADDRESS OF IRIS-PCB-PTR-28  TO
               ADDRESS OF IRIS-LK-POINTER(28)
           SET ADDRESS OF IRIS-PCB-PTR-29  TO
               ADDRESS OF IRIS-LK-POINTER(29)
           SET ADDRESS OF IRIS-PCB-PTR-30  TO
               ADDRESS OF IRIS-LK-POINTER(30)
           SET ADDRESS OF IRIS-PCB-PTR-31  TO
               ADDRESS OF IRIS-LK-POINTER(31)
           SET ADDRESS OF IRIS-PCB-PTR-32  TO
               ADDRESS OF IRIS-LK-POINTER(32)
           SET ADDRESS OF IRIS-PCB-PTR-33  TO
               ADDRESS OF IRIS-LK-POINTER(33)
           SET ADDRESS OF IRIS-PCB-PTR-34  TO
               ADDRESS OF IRIS-LK-POINTER(34)
           SET ADDRESS OF IRIS-PCB-PTR-35  TO
               ADDRESS OF IRIS-LK-POINTER(35)
           SET ADDRESS OF IRIS-PCB-PTR-36  TO
               ADDRESS OF IRIS-LK-POINTER(36)
           SET ADDRESS OF IRIS-PCB-PTR-37  TO
               ADDRESS OF IRIS-LK-POINTER(37)
           SET ADDRESS OF IRIS-PCB-PTR-38  TO
               ADDRESS OF IRIS-LK-POINTER(38)
           SET ADDRESS OF IRIS-PCB-PTR-39  TO
               ADDRESS OF IRIS-LK-POINTER(39)
           SET ADDRESS OF IRIS-PCB-PTR-40  TO
               ADDRESS OF IRIS-LK-POINTER(40)
           SET ADDRESS OF IRIS-PCB-PTR-41  TO
               ADDRESS OF IRIS-LK-POINTER(41)
           SET ADDRESS OF IRIS-PCB-PTR-42  TO
               ADDRESS OF IRIS-LK-POINTER(42)
           SET ADDRESS OF IRIS-PCB-PTR-43  TO
               ADDRESS OF IRIS-LK-POINTER(43)
           SET ADDRESS OF IRIS-PCB-PTR-44  TO
               ADDRESS OF IRIS-LK-POINTER(44)
           SET ADDRESS OF IRIS-PCB-PTR-45  TO
               ADDRESS OF IRIS-LK-POINTER(45)
           SET ADDRESS OF IRIS-PCB-PTR-46  TO
               ADDRESS OF IRIS-LK-POINTER(46)
           SET ADDRESS OF IRIS-PCB-PTR-47  TO
               ADDRESS OF IRIS-LK-POINTER(47)
           SET ADDRESS OF IRIS-PCB-PTR-48  TO
               ADDRESS OF IRIS-LK-POINTER(48)
           SET ADDRESS OF IRIS-PCB-PTR-49  TO
               ADDRESS OF IRIS-LK-POINTER(49)
           SET ADDRESS OF IRIS-PCB-PTR-50  TO
               ADDRESS OF IRIS-LK-POINTER(50)
           SET ADDRESS OF IRIS-PCB-PTR-51  TO
               ADDRESS OF IRIS-LK-POINTER(51)
           SET ADDRESS OF IRIS-PCB-PTR-52  TO
               ADDRESS OF IRIS-LK-POINTER(52)
           SET ADDRESS OF IRIS-PCB-PTR-53  TO
               ADDRESS OF IRIS-LK-POINTER(53)
           SET ADDRESS OF IRIS-PCB-PTR-54  TO
               ADDRESS OF IRIS-LK-POINTER(54)
           SET ADDRESS OF IRIS-PCB-PTR-55  TO
               ADDRESS OF IRIS-LK-POINTER(55)
           SET ADDRESS OF IRIS-PCB-PTR-56  TO
               ADDRESS OF IRIS-LK-POINTER(56)
           SET ADDRESS OF IRIS-PCB-PTR-57  TO
               ADDRESS OF IRIS-LK-POINTER(57)
           SET ADDRESS OF IRIS-PCB-PTR-58  TO
               ADDRESS OF IRIS-LK-POINTER(58)
           SET ADDRESS OF IRIS-PCB-PTR-59  TO
               ADDRESS OF IRIS-LK-POINTER(59)
           SET ADDRESS OF IRIS-PCB-PTR-60  TO
               ADDRESS OF IRIS-LK-POINTER(60)
           SET ADDRESS OF IRIS-PCB-PTR-61  TO
               ADDRESS OF IRIS-LK-POINTER(61)
           SET ADDRESS OF IRIS-PCB-PTR-62  TO
               ADDRESS OF IRIS-LK-POINTER(62)
           SET ADDRESS OF IRIS-PCB-PTR-63  TO
               ADDRESS OF IRIS-LK-POINTER(63)
           SET ADDRESS OF IRIS-PCB-PTR-64  TO
               ADDRESS OF IRIS-LK-POINTER(64)
           SET ADDRESS OF IRIS-PCB-PTR-65  TO
               ADDRESS OF IRIS-LK-POINTER(65)
           SET ADDRESS OF IRIS-PCB-PTR-66  TO
               ADDRESS OF IRIS-LK-POINTER(66)
           SET ADDRESS OF IRIS-PCB-PTR-67  TO
               ADDRESS OF IRIS-LK-POINTER(67)
           SET ADDRESS OF IRIS-PCB-PTR-68  TO
               ADDRESS OF IRIS-LK-POINTER(68)
           SET ADDRESS OF IRIS-PCB-PTR-69  TO
               ADDRESS OF IRIS-LK-POINTER(69)
           SET ADDRESS OF IRIS-PCB-PTR-70  TO
               ADDRESS OF IRIS-LK-POINTER(70)
           SET ADDRESS OF IRIS-PCB-PTR-71  TO
               ADDRESS OF IRIS-LK-POINTER(71)
           SET ADDRESS OF IRIS-PCB-PTR-72  TO
               ADDRESS OF IRIS-LK-POINTER(72)
           SET ADDRESS OF IRIS-PCB-PTR-73  TO
               ADDRESS OF IRIS-LK-POINTER(73)
           SET ADDRESS OF IRIS-PCB-PTR-74  TO
               ADDRESS OF IRIS-LK-POINTER(74)
           SET ADDRESS OF IRIS-PCB-PTR-75  TO
               ADDRESS OF IRIS-LK-POINTER(75)
           SET ADDRESS OF IRIS-PCB-PTR-76  TO
               ADDRESS OF IRIS-LK-POINTER(76)
           SET ADDRESS OF IRIS-PCB-PTR-77  TO
               ADDRESS OF IRIS-LK-POINTER(77)
           SET ADDRESS OF IRIS-PCB-PTR-78  TO
               ADDRESS OF IRIS-LK-POINTER(78)
           SET ADDRESS OF IRIS-PCB-PTR-79  TO
               ADDRESS OF IRIS-LK-POINTER(79)
           SET ADDRESS OF IRIS-PCB-PTR-80  TO
               ADDRESS OF IRIS-LK-POINTER(80)
           SET ADDRESS OF IRIS-PCB-PTR-81  TO
               ADDRESS OF IRIS-LK-POINTER(81)
           SET ADDRESS OF IRIS-PCB-PTR-82  TO
               ADDRESS OF IRIS-LK-POINTER(82)
           SET ADDRESS OF IRIS-PCB-PTR-83  TO
               ADDRESS OF IRIS-LK-POINTER(83)
           SET ADDRESS OF IRIS-PCB-PTR-84  TO
               ADDRESS OF IRIS-LK-POINTER(84)
           SET ADDRESS OF IRIS-PCB-PTR-85  TO
               ADDRESS OF IRIS-LK-POINTER(85)
           SET ADDRESS OF IRIS-PCB-PTR-86  TO
               ADDRESS OF IRIS-LK-POINTER(86)
           SET ADDRESS OF IRIS-PCB-PTR-87  TO
               ADDRESS OF IRIS-LK-POINTER(87)
           SET ADDRESS OF IRIS-PCB-PTR-88  TO
               ADDRESS OF IRIS-LK-POINTER(88)
           SET ADDRESS OF IRIS-PCB-PTR-89  TO
               ADDRESS OF IRIS-LK-POINTER(89)
           SET ADDRESS OF IRIS-PCB-PTR-90  TO
               ADDRESS OF IRIS-LK-POINTER(90)
           SET ADDRESS OF IRIS-PCB-PTR-91  TO
               ADDRESS OF IRIS-LK-POINTER(91)
           SET ADDRESS OF IRIS-PCB-PTR-92  TO
               ADDRESS OF IRIS-LK-POINTER(92)
           SET ADDRESS OF IRIS-PCB-PTR-93  TO
               ADDRESS OF IRIS-LK-POINTER(93)
           SET ADDRESS OF IRIS-PCB-PTR-94  TO
               ADDRESS OF IRIS-LK-POINTER(94)
           SET ADDRESS OF IRIS-PCB-PTR-95  TO
               ADDRESS OF IRIS-LK-POINTER(95)
           SET ADDRESS OF IRIS-PCB-PTR-96  TO
               ADDRESS OF IRIS-LK-POINTER(96)
           SET ADDRESS OF IRIS-PCB-PTR-97  TO
               ADDRESS OF IRIS-LK-POINTER(97)
           SET ADDRESS OF IRIS-PCB-PTR-98  TO
               ADDRESS OF IRIS-LK-POINTER(98)
           SET ADDRESS OF IRIS-PCB-PTR-99  TO
               ADDRESS OF IRIS-LK-POINTER(99)
           SET ADDRESS OF IRIS-PCB-PTR-100 TO
               ADDRESS OF IRIS-LK-POINTER(100)
           .
      *
       SET-LINKAGE-PCBS-EX.
      *
           EXIT.

