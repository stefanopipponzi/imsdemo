      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: RETRIEVES THE INFO OF THE FIELD STORED INTO
      *              MEMORY-DBD-FIELDS
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IRISGFCP.
      *
       ENVIRONMENT DIVISION.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
           EXEC SQL INCLUDE SQLCA END-EXEC.
       01 IRIS-CURRENT-TIMESTAMP PIC X(26) VALUE SPACES.
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
      *
      * WORKING STORAGE COMMON VARIABLES
      *
      * COMMON PERFORM INDEX
       01 WS-INDEX                        PIC S9(9) COMP.
      *
       LINKAGE SECTION.
      *
      *    CONTROL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IRISPSSA'==.
      *
      *  STRUCTURES TO REPRESENT TP AND DB PCB
      * 
           COPY IRISISEG.
      *
      *  INPUT PARAMETERS
      * 
       01 LK-SEGMENT-NAME                 PIC X(8).
       01 LK-FIELD-NAME                   PIC X(8).
      *
      *  OUTPUT PARAMETERS
      * 
       01 LK-FIELD-START                  PIC S9(9) COMP.
       01 LK-FIELD-LENGTH                 PIC S9(9) COMP.
       01 LK-FIELD-TYPE                   PIC S9(9) COMP.
       01 LK-FIELD-BRIDGE-TYPE            PIC S9(9) COMP.
       01 LK-FIELD-BRIDGE-NAME.
         49 LK-FIELD-BRIDGE-NAME-LEN      PIC S9(4) COMP.
         49 LK-FIELD-BRIDGE-NAME-TXT      PIC X(32).
       01 LK-FIELD-BRIDGE-DECIMALS        PIC S9(9) COMP.
      *
      *  FIELDS REPRESENTATION
      * 
           COPY IRISFLD.
      *
       PROCEDURE DIVISION USING IRIS-WORK-AREA
                                MEMORY-PCB-AREA
                                LK-SEGMENT-NAME
                                LK-FIELD-NAME
                                LK-FIELD-START
                                LK-FIELD-LENGTH
                                LK-FIELD-TYPE
                                LK-FIELD-BRIDGE-TYPE
                                LK-FIELD-BRIDGE-NAME
                                LK-FIELD-BRIDGE-DECIMALS.
      *
       MAIN-PROGRAM.
      *
           SET ADDRESS OF MEMORY-DBD-FIELDS TO DT-DBD-FIELD-PTR
      *    
           IF IRIS-TRACE-DEBUG
             EXEC SQL
              SET :IRIS-CURRENT-TIMESTAMP = CURRENT_TIMESTAMP
             END-EXEC
           END-IF
           PERFORM VARYING WS-INDEX FROM 1 BY 1
           UNTIL WS-INDEX > DBD-FIELDS-LENGTH OR 
           (DF-SEGMENT-NAME(WS-INDEX) = LK-SEGMENT-NAME 
            AND DF-FIELD-NAME(WS-INDEX) = LK-FIELD-NAME)
             CONTINUE
           END-PERFORM
      *
           IF WS-INDEX <= DBD-FIELDS-LENGTH
             MOVE DF-FIELD-START(WS-INDEX) TO LK-FIELD-START
             MOVE DF-FIELD-BYTES(WS-INDEX) TO LK-FIELD-LENGTH
             MOVE DF-FIELD-DATA-TYPE(WS-INDEX) TO LK-FIELD-TYPE
             MOVE DF-FIELD-BRIDGE-TYPE(WS-INDEX) TO LK-FIELD-BRIDGE-TYPE
             MOVE DF-FIELD-BRIDGE-NAME(WS-INDEX) TO LK-FIELD-BRIDGE-NAME
             MOVE DF-FIELD-BRIDGE-DECIMALS(WS-INDEX) 
                                             TO LK-FIELD-BRIDGE-DECIMALS
           ELSE
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISGFLD) ' NL                      
                    ' ERROR      =(FIELD NOT FOUND) ' NL
                    ' DBD        =(' DT-DBD-NAME ') ' NL
                    ' SEGMENT    =(' LK-SEGMENT-NAME ') ' NL
                    ' FIELD      =(' LK-FIELD-NAME ') '
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT       
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             SET IRIS-ERR-FIELD-NOT-FOUND TO TRUE
             SET IRIS-MSG-LEVEL-ERROR TO TRUE 
           END-IF.
      *
       MAIN-PROGRAM-EX.
      *
           GOBACK.
