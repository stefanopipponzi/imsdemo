      ******************************************************************
      *                                                                *
      * Copyright (c) 2018 by Modern Systems, Inc.                     *
      * All rights reserved.                                           *
      *                                                                *
      ******************************************************************
      * Product: IRIS-DB - v. 5.5.0
      ******************************************************************
      *
      * DESCRIPTION: PSB DEFINITION
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IRISPSCP.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
           EXEC SQL INCLUDE SQLCA END-EXEC.
       01 IRIS-CURRENT-TIMESTAMP PIC X(26) VALUE SPACES.
      *
      *    COMMON VARIABLES
      *
           COPY IRISCOMM.
      *
      *    DEFINITION OF ALL THE PSBS
      *
           COPY IRISPSBD.
      *
      * WORKING STORAGE COMMON VARIABLES
      *
      * INDEXES
       01 WS-INDEX              PIC S9(9) COMP.
      * POINTERS
       01 WS-PTR                POINTER.
      * WORK PSB
       01 WS-PSBNAME.
         03 WS-PSBNAME-PREF     PIC X(7).
         03 WS-PSBENV           PIC X     VALUE 'P'.
      *
       LINKAGE SECTION.
      *
      *    CONTROL AREA
      *
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IRISPSSA'==.
      *
       01 LK-PSBNAME.
         03 LK-PSBNAME-PREF     PIC X(7).
         03 LK-PSBENV           PIC X.
           88 IS-TEST-PSB       VALUE 'T'.
       01 LK-PCBS-PTR           POINTER.
       01 LK-SENSEGS-PTR        POINTER.
      *
       PROCEDURE DIVISION USING IRIS-WORK-AREA
                                LK-PSBNAME
                                LK-PCBS-PTR
                                LK-SENSEGS-PTR.
      *
       MAIN-PROGRAM.
      *
      *    SETUP OF ALL THE PSBS
      *
           COPY IRISPSBP.
      *
      *    SEARCH FOR INPUT PSB
      *
           IF IRIS-TRACE-DEBUG
             EXEC SQL
              SET :IRIS-CURRENT-TIMESTAMP = CURRENT_TIMESTAMP
             END-EXEC
           END-IF
           PERFORM VARYING WS-INDEX FROM 1 BY 1
           UNTIL WS-INDEX > PSBS-COUNT
           OR LK-PSBNAME = TAB-PSB-NAME(WS-INDEX)
             CONTINUE
           END-PERFORM
      *
           IF WS-INDEX > PSBS-COUNT
           AND IS-TEST-PSB
             MOVE LK-PSBNAME-PREF TO WS-PSBNAME-PREF
             PERFORM VARYING WS-INDEX FROM 1 BY 1
             UNTIL WS-INDEX > PSBS-COUNT
             OR WS-PSBNAME = TAB-PSB-NAME(WS-INDEX)
               CONTINUE
             END-PERFORM
           END-IF
      *
           IF WS-INDEX <= PSBS-COUNT
             SET LK-PCBS-PTR TO TAB-PSB-PCBS-PTR(WS-INDEX)
             SET LK-SENSEGS-PTR TO TAB-PSB-SENSEGS-PTR(WS-INDEX)
           ELSE
             MOVE ZERO TO IRIS-MSG-LEN
             STRING '<IRISTRACE> - (IRISPSB)' NL
                    ' ERROR      =(PSB NOT FOUND IN IRIS DICTIONARY)' NL
                    ' PSB        =(' LK-PSBNAME ')'
             MESSAGE-END DELIMITED BY SIZE INTO IRIS-MSG-TXT
             SET IRISTRAC-RTN TO TRUE
             CALL IRIS-WS-RTN USING IRIS-WORK-AREA
             SET IRIS-ERR-PSB-NOT-FOUND TO TRUE
             SET IRIS-MSG-LEVEL-ERROR TO TRUE
           END-IF.
      *
       MAIN-PROGRAM-EX.
      *
           GOBACK.
