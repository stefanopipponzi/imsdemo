//IVPDB2UN JOB (C000000100),'UNLOAD IVPDB2',                                    
//       CLASS=N,MSGCLASS=X,MSGLEVEL=(1,1),NOTIFY=&SYSUID                       
/*JOBPARM  ROOM=25,S=TSO2                                                       
/*XEQ      TSO2                                                                 
/*ROUTE PRINT MEM0                                                              
//*/*JOBPARM R=06,S=TSO2                                                        
//*                                                                             
//*  Licensed Materials - Property of Modern Systems, INC.                      
//*                                                                             
//*  (C) Copyright Modern Systems 2018                                          
//*                                                                             
//*                                                                             
//*  IRIS-DB - v. 5.6.2                                                         
//*                                                                             
//*  Date: 2018/2020                                                            
//*                                                                             
//*  Description: Unload IMS data for DBD: IVPDB2                               
//*                                                                             
//*                                                                             
//* -------------------------------------------------------------------         
//*                                                                             
//* THIS JOB CONTAINS JCL TO UNLOAD:  IVPDB2                                    
//*                                                                             
//* -------------------------------------------------------------------         
//* THIS STEP SCRATCHES THE UNLOAD/WORK DATASETS FOR DBD IVPDB2                 
//* -------------------------------------------------------------------         
//*                                                                             
//SCRATCH  EXEC PGM=IDCAMS,REGION=2048K                                         
//SYSPRINT DD SYSOUT=*                                                          
//SYSIN    DD *                                                                 
 DELETE (  +                                                                    
    'LZTEST.DFSIVD2.SEGDATA' +                                                  
   );                                                                           
 SET MAXCC=0;                                                                   
/*                                                                              
//*                                                                             
//* -------------------------------------------------------------------         
//* THIS STEP UNLOADS DATA BASE IVPDB2 USING BMC UNLOADPLUS                     
//* -------------------------------------------------------------------         
//*                                                                             
//IVPDB2   EXEC PGM=DFSRRC00,REGION=4096K,                                      
//            PARM=(ULU,DFSURGU0,IVPDB2,,,,0,,,,T,UTDG,,N,,)                    
//*                                                     DBRC(NO)                
//STEPLIB  DD DISP=SHR,DSN=IMSDEV.UTDG.PPLOAD                                   
//         DD DISP=SHR,DSN=IMSDEV.UTDG.SDFSRESL                                 
//DFSRESLB DD DISP=SHR,DSN=IMSDEV.UTDG.SDFSRESL                                 
//IMS      DD DISP=SHR,DSN=IMSDEV.UTDG.DBDLIB                                   
//PLUSLIST DD SYSOUT=*                                                          
//PLUSOUT  DD SYSOUT=*                                                          
//SYSPRINT DD SYSOUT=*                                                          
//SYSUDUMP DD SYSOUT=*                                                          
//SYSOUT   DD SYSOUT=*                                                          
//ABNLIGNR DD DUMMY                                                             
//PDX      DD DUMMY                                                             
//DFSURGU1 DD DSN=LZTEST.DFSIVD2.SEGDATA,                                       
//            DISP=(,CATLG,DELETE),                                             
//            UNIT=DISK,                                                        
//            SPACE=(CYL,(5,5),RLSE),                                           
//            DCB=(RECFM=VB,LRECL=60,BLKSIZE=600)                               
//PLUSIN   DD *                                                                 
  UNLOAD -                                                                      
      SEQERROR(ACCEPT) MONITOR(YES,500) DFSURGU1(XSHORT) -                      
      PTRERROR(ACCEPT) EXPAND(NO)                                               
/*                                                                              
//IVPDB2   DD  DSN=UTDGO.FE.DFSIVD2,DISP=SHR,DCB=BUFNO=44                       