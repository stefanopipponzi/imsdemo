-- -----------------------------------------------------------------------
-- 
--      Licensed Materials - Property of Modern Systems, INC.
--                                                           
--      (C) Copyright Modern Systems 2018
-- 
-- -----------------------------------------------------------------------
-- IRIS-DB - v. 5.6.2
-- -----------------------------------------------------------------------
-- Date: 2018/2020
-- -----------------------------------------------------------------------
-- 
-- Description: Create SQL contraints for DBD: IVPDB2
-- 
-- -----------------------------------------------------------------------
 
-- -----------------------------------------------------------------------
-- CREATE UNIQUE INDEX FOR SEGMENT: A1111111
-- -----------------------------------------------------------------------
 
CREATE UNIQUE INDEX PROD.IVPDB2A1111111PK1 ON PROD.IVPDB2_A1111111(
  KEY_A1111111
)
  USING STOGROUP SG7IS000
  PRIQTY 720
  SECQTY -1
  ERASE NO
  FREEPAGE 0
  PCTFREE 10
  GBPCACHE CHANGED
  CLUSTER
  BUFFERPOOL BP31
  CLOSE NO
  COPY NO
  PIECESIZE 2 G
  COMPRESS NO
;
ALTER TABLE PROD.IVPDB2_A1111111
ADD CONSTRAINT IVPDB2A1111111PK1 PRIMARY KEY(
  KEY_A1111111
);

