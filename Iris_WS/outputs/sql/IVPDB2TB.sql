-- -----------------------------------------------------------------------
-- 
--      Licensed Materials - Property of Modern Systems, INC.
--                                                           
--      (C) Copyright Modern Systems 2018
-- 
-- -----------------------------------------------------------------------
-- IRIS-DB - v. 5.6.2
-- -----------------------------------------------------------------------
-- Date: 2018/2020
-- -----------------------------------------------------------------------
-- 
-- Description: Create SQL structure for DBD: IVPDB2
-- 
-- -----------------------------------------------------------------------
-- DROP    DATABASE IVPDB2DBP; COMMIT;
CREATE  DATABASE IVPDB2DBP
    BUFFERPOOL BP30
    INDEXBP BP31
    STOGROUP SG7TS000
    CCSID EBCDIC
;      
-- -----------------------------------------------------------------------
-- CREATE TABLESPACES
-- -----------------------------------------------------------------------
    
CREATE                       
  TABLESPACE IVPDB2A1111111TS         
     IN IVPDB2DBP                
     USING STOGROUP SG7TS000  
     PRIQTY 720               
     SECQTY -1                
     ERASE NO                 
     FREEPAGE 0               
     PCTFREE 10               
     GBPCACHE CHANGED         
     COMPRESS YES             
     TRACKMOD YES             
     LOGGED                   
     DSSIZE 2 G               
     SEGSIZE 64               
     MAXPARTITIONS 8          
     BUFFERPOOL BP30
     LOCKSIZE ANY   
     LOCKMAX SYSTEM 
     CLOSE NO       
     CCSID EBCDIC   
     MAXROWS 255    
 ;                  
 
-- -----------------------------------------------------------------------
-- CREATE TABLE FOR SEGMENT: A1111111
-- -----------------------------------------------------------------------
 
CREATE TABLE PROD.IVPDB2_A1111111 (
-- -----------------------------------------------------------------------
-- DATA SECTION
-- -----------------------------------------------------------------------
  KEY_A1111111 CHAR (10) NOT NULL,
  FIRST_NAME CHAR (10) NOT NULL,
  EXTENSION CHAR (10) NOT NULL,
  ZIP_CODE CHAR (7) NOT NULL,
  A1111111_FILLER_1 CHAR (3) NOT NULL,
-- -----------------------------------------------------------------------
-- IRIS-DB RESERVED SECTION
-- -----------------------------------------------------------------------
  CREATION_TS TIMESTAMP NOT NULL WITH DEFAULT,
  LAST_UPDATE_TS TIMESTAMP NOT NULL WITH DEFAULT,
  CREATION_USER_I VARCHAR(25) NOT NULL WITH DEFAULT,
  LAST_UPDATE_USER_I VARCHAR(25) NOT NULL WITH DEFAULT,
  CREATION_TRAN_I VARCHAR(25) NOT NULL WITH DEFAULT,
  LAST_UPDATE_TRAN_I VARCHAR(25) NOT NULL WITH DEFAULT
)
  IN IVPDB2DBP.IVPDB2A1111111TS
  CCSID EBCDIC
  NOT VOLATILE
  APPEND NO
;

