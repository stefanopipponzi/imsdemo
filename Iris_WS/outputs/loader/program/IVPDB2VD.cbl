      *CBL TRUNC(BIN)
      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: IMS DATA VALIDATION PROGRAM FOR DBD: IVPDB2
      *
      ******************************************************************
      *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IVPDB2VD.
      *
       ENVIRONMENT DIVISION.
      *
       INPUT-OUTPUT SECTION.
      *
       FILE-CONTROL.
      *
           SELECT FILEIN ASSIGN TO FILEIN
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE  IS SEQUENTIAL
           FILE STATUS IS FILEIN-STATUS.
      *
           SELECT FILEODB2 ASSIGN TO FILEODB2
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE  IS SEQUENTIAL
           FILE STATUS IS FILEODB2-STATUS.
      *
           SELECT FILEOIMS ASSIGN TO FILEOIMS
           ORGANIZATION IS SEQUENTIAL
           ACCESS MODE  IS SEQUENTIAL
           FILE STATUS IS FILEOIMS-STATUS.
      *
       DATA DIVISION.
      *
       FILE SECTION.
      *
       FD  FILEIN
           RECORDING MODE IS V
           RECORD IS VARYING
           FROM 1 TO 32752
           LABEL RECORD IS STANDARD
           DATA RECORD IS REC-FILEIN.
       01  REC-FILEIN.
         03  FILLER                           PIC X.
           88 HEAD-TRAIL-RECORD               VALUE LOW-VALUE.
         03  FILLER                           PIC X(5).
         03  REC-SEGMNAME                     PIC X(8).
           88  REC-SEGMNAME-ROOT              VALUE 'A1111111'.
         03  FILLER                           PIC X(21).
         03  REC-DATA                         PIC X(32717).
         03  FILLER REDEFINES REC-DATA.
           05  REC-SEGM-ROOT-KEY              PIC X(10).
      *
       FD  FILEODB2
           BLOCK CONTAINS 0 RECORDS
           RECORD IS VARYING IN SIZE FROM 18 TO 58
           DEPENDING ON FILEODB2-REC-LEN.
       01  FD-FILEODB2.
         03  FD-FILEODB2-SEGMNAME             PIC X(8).
         03  FD-FILEODB2-DATA                 PIC X(40).
         03  FD-FILEODB2-CONCAT-KEY           PIC X(10).
      *
       FD  FILEOIMS
           BLOCK CONTAINS 0 RECORDS
           RECORD IS VARYING IN SIZE FROM 18 TO 58
           DEPENDING ON FILEOIMS-REC-LEN.
       01  FD-FILEOIMS.
         03  FD-FILEOIMS-SEGMNAME             PIC X(8).
         03  FD-FILEOIMS-DATA                 PIC X(40).
         03  FD-FILEOIMS-CONCAT-KEY           PIC X(10).
      *
       WORKING-STORAGE SECTION.
      *
      ******************************************************************
      * IRIS-DB COPYBOOKS
      ******************************************************************
           COPY IRISCOMM.
           COPY IRISGLOB REPLACING ==:PROGNM:== BY =='IVPDB2VD'==.
      ******************************************************************
      * STANDARD SQL INCLUDE
      ******************************************************************
           EXEC SQL INCLUDE SQLCA END-EXEC.
      ******************************************************************
      * SYSIN CARD
      ******************************************************************
       01  SYSIN-CARD.
         03  SYSIN-COMMAND                       PIC X(4).
         03  FILLER REDEFINES SYSIN-COMMAND      PIC X(4).
           88  SYSIN-COMMAND-VALID               VALUE 'EDB2'
                                                       'EIMS'
                                                       'VALD'
                                                       'EALL'.
           88  SYSIN-COMMAND-PULL-DB2            VALUE 'EDB2'.
           88  SYSIN-COMMAND-PULL-IMS            VALUE 'EIMS'.
           88  SYSIN-COMMAND-PULL-ALL            VALUE 'EALL'.
           88  SYSIN-COMMAND-VALIDATE            VALUE 'VALD'.
         03  FILLER                              PIC X(76).
      ******************************************************************
      * FILE I/O WORKING AREA
      ******************************************************************
       01  IO-REC-FILE.
         03  IO-REC-SEGMNAME                     PIC X(8).
         03  IO-REC-DATA                         PIC X(40).
         03  IO-REC-CONCAT-KEY                   PIC X(10).
      *  SEGMENT KEY: A1111111
         03  FILLER REDEFINES IO-REC-CONCAT-KEY.
           05  IO-REC-CONCAT-A1111111-KEY        PIC X(10).
       01  IO-REC-FILEODB2                       PIC X(58).
       01  IO-REC-FILEOIMS                       PIC X(58).
       01  FILEODB2-REC-LEN                      PIC 9(9) COMP.
       01  FILEOIMS-REC-LEN                      PIC 9(9) COMP.
      ******************************************************************
      * WS VARIABLES
      ******************************************************************
       01  DB-REC-DATA            PIC X(40).
       01  FILEIN-STATUS          PIC X(2).
         88  FILEIN-STATUS-OK                     VALUE '00'.
       01  FILEODB2-STATUS        PIC X(2).
         88  FILEODB2-STATUS-OK                   VALUE '00'.
       01  FILEOIMS-STATUS        PIC X(2).
         88  FILEOIMS-STATUS-OK                   VALUE '00'.
       01  FILLER                 PIC X(01)       VALUE SPACE.
         88  EOF-DB                               VALUE 'X'.
       01  SEGMENT-LEN            PIC S9(9)  COMP VALUE ZERO.
       01  SEGMENT-NAME           PIC S9(9)  COMP VALUE ZERO.
       01  A1111111-LEN           PIC S9(9)  COMP VALUE 40.
      * MAX CONCATENATED KEY
       01  IMS-CONCATENATED-KEY   PIC X(10).
       01  COUNTERS.
         03  A1111111-CNTL-IMS    PIC S9(9)  COMP VALUE ZERO.
         03  A1111111-CNTL-SQL    PIC S9(11) COMP-3 VALUE ZERO.
       01  PERCENTAGE             PIC S9(9)  COMP.
       01  PERCENTAGE-OLD         PIC S9(9)  COMP.
       01  PERCENTAGE-VALUE       PIC S9(9)  COMP.
       01  PERCENTAGE-EDIT        PIC ZZZ.
       01  SEGM-COUNTER           PIC S9(9)  COMP VALUE ZERO.
       01  SEGM-ERR-COUNTER       PIC S9(9)  COMP VALUE ZERO.
       01  SQL-SEGM-COUNTER       PIC S9(9)  COMP VALUE ZERO.
       01  SEGM-COUNTER-Z         PIC ZZ,ZZZ,ZZZ,ZZ9.
       01  SEGM-ERR-COUNTER-Z     PIC ZZ,ZZZ,ZZZ,ZZ9.
       01  TOTAL-DISP             PIC ZZ,ZZZ,ZZZ,ZZ9.
       01  SQLCODE-N              PIC S9(9)       VALUE ZERO.
       01  SQLCODE-Z              PIC -ZZZZZ9.
       01  DIFF-POS               PIC S9(9)  COMP.
       01  DIFF-LEN               PIC S9(9)  COMP.
       01  IDX                    PIC S9(9)  COMP.
      *
       01  CURRENT-DATE-VAL.
         03  CURRENT-YEAR         PIC 9(4).
         03  CURRENT-MONTH        PIC 9(2).
         03  CURRENT-DAY          PIC 9(2).
         03  CURRENT-HOURS        PIC 9(2).
         03  CURRENT-MINUTE       PIC 9(2).
         03  CURRENT-SECOND       PIC 9(2).
         03  CURRENT-MILLISECONDS PIC 9(2).
      *
       01  CURRENT-DATE-DISP.
         03  CURRENT-YEAR         PIC 9(4).
         03  FILLER               PIC X        VALUE '/'.
         03  CURRENT-MONTH        PIC 9(2).
         03  FILLER               PIC X        VALUE '/'.
         03  CURRENT-DAY          PIC 9(2).
         03  FILLER               PIC X        VALUE ' '.
         03  CURRENT-HOURS        PIC 9(2).
         03  FILLER               PIC X        VALUE ':'.
         03  CURRENT-MINUTE       PIC 9(2).
         03  FILLER               PIC X        VALUE ':'.
         03  CURRENT-SECOND       PIC 9(2).
         03  FILLER               PIC X        VALUE '.'.
         03  CURRENT-MILLISECONDS PIC 9(2).
      *
       01 WS-COMMON-FLD-PACKED             PIC X(2560).
       01 WS-COMMON-FLD-PACKED             PIC X(2560).
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-18-00 OCCURS 256 PIC S9(18)         COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-17-01 OCCURS 256 PIC S9(17)V9       COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-16-02 OCCURS 256 PIC S9(16)V9(02)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-15-03 OCCURS 256 PIC S9(15)V9(03)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-14-04 OCCURS 256 PIC S9(14)V9(04)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-13-05 OCCURS 256 PIC S9(13)V9(05)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-12-06 OCCURS 256 PIC S9(12)V9(06)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-11-07 OCCURS 256 PIC S9(11)V9(07)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-10-08 OCCURS 256 PIC S9(10)V9(08)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-09-09 OCCURS 256 PIC S9(09)V9(09)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-08-10 OCCURS 256 PIC S9(08)V9(10)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-07-11 OCCURS 256 PIC S9(07)V9(11)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-06-12 OCCURS 256 PIC S9(06)V9(12)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-05-13 OCCURS 256 PIC S9(05)V9(13)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-04-14 OCCURS 256 PIC S9(04)V9(14)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-03-15 OCCURS 256 PIC S9(03)V9(15)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-02-16 OCCURS 256 PIC S9(02)V9(16)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-01-17 OCCURS 256 PIC S9(01)V9(17)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-00-18 OCCURS 256 PIC SV9(18)        COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-18-00 OCCURS 256 PIC 9(18)       COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-17-01 OCCURS 256 PIC 9(17)V9     COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-16-02 OCCURS 256 PIC 9(16)V9(02) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-15-03 OCCURS 256 PIC 9(15)V9(03) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-14-04 OCCURS 256 PIC 9(14)V9(04) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-13-05 OCCURS 256 PIC 9(13)V9(05) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-12-06 OCCURS 256 PIC 9(12)V9(06) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-11-07 OCCURS 256 PIC 9(11)V9(07) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-10-08 OCCURS 256 PIC 9(10)V9(08) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-09-09 OCCURS 256 PIC 9(09)V9(09) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-08-10 OCCURS 256 PIC 9(08)V9(10) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-07-11 OCCURS 256 PIC 9(07)V9(11) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-06-12 OCCURS 256 PIC 9(06)V9(12) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-05-13 OCCURS 256 PIC 9(05)V9(13) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-04-14 OCCURS 256 PIC 9(04)V9(14) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-03-15 OCCURS 256 PIC 9(03)V9(15) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-02-16 OCCURS 256 PIC 9(02)V9(16) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-01-17 OCCURS 256 PIC 9(01)V9(17) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-00-18 OCCURS 256 PIC V9(18)      COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-CHR   OCCURS 256 PIC X(10).
      *
       01 WS-XZONED-18                  PIC X(18).
       01 WS-NUMZONED-18-00 REDEFINES WS-XZONED-18    PIC S9(18).
       01 WS-NUMZONED-17-01 REDEFINES WS-XZONED-18    PIC S9(17)V9.
       01 WS-NUMZONED-16-02 REDEFINES WS-XZONED-18    PIC S9(16)V9(02).
       01 WS-NUMZONED-15-03 REDEFINES WS-XZONED-18    PIC S9(15)V9(03).
       01 WS-NUMZONED-14-04 REDEFINES WS-XZONED-18    PIC S9(14)V9(04).
       01 WS-NUMZONED-13-05 REDEFINES WS-XZONED-18    PIC S9(13)V9(05).
       01 WS-NUMZONED-12-06 REDEFINES WS-XZONED-18    PIC S9(12)V9(06).
       01 WS-NUMZONED-11-07 REDEFINES WS-XZONED-18    PIC S9(11)V9(07).
       01 WS-NUMZONED-10-08 REDEFINES WS-XZONED-18    PIC S9(10)V9(08).
       01 WS-NUMZONED-09-09 REDEFINES WS-XZONED-18    PIC S9(09)V9(09).
       01 WS-NUMZONED-08-10 REDEFINES WS-XZONED-18    PIC S9(08)V9(10).
       01 WS-NUMZONED-07-11 REDEFINES WS-XZONED-18    PIC S9(07)V9(11).
       01 WS-NUMZONED-06-12 REDEFINES WS-XZONED-18    PIC S9(06)V9(12).
       01 WS-NUMZONED-05-13 REDEFINES WS-XZONED-18    PIC S9(05)V9(13).
       01 WS-NUMZONED-04-14 REDEFINES WS-XZONED-18    PIC S9(04)V9(14).
       01 WS-NUMZONED-03-15 REDEFINES WS-XZONED-18    PIC S9(03)V9(15).
       01 WS-NUMZONED-02-16 REDEFINES WS-XZONED-18    PIC S9(02)V9(16).
       01 WS-NUMZONED-01-17 REDEFINES WS-XZONED-18    PIC S9(01)V9(17).
       01 WS-NUMZONED-00-18 REDEFINES WS-XZONED-18    PIC SV9(18).
       01 WS-NUMZONED-NS-18-00 REDEFINES WS-XZONED-18 PIC 9(18).
       01 WS-NUMZONED-NS-17-01 REDEFINES WS-XZONED-18 PIC 9(17)V9.
       01 WS-NUMZONED-NS-16-02 REDEFINES WS-XZONED-18 PIC 9(16)V9(02).
       01 WS-NUMZONED-NS-15-03 REDEFINES WS-XZONED-18 PIC 9(15)V9(03).
       01 WS-NUMZONED-NS-14-04 REDEFINES WS-XZONED-18 PIC 9(14)V9(04).
       01 WS-NUMZONED-NS-13-05 REDEFINES WS-XZONED-18 PIC 9(13)V9(05).
       01 WS-NUMZONED-NS-12-06 REDEFINES WS-XZONED-18 PIC 9(12)V9(06).
       01 WS-NUMZONED-NS-11-07 REDEFINES WS-XZONED-18 PIC 9(11)V9(07).
       01 WS-NUMZONED-NS-10-08 REDEFINES WS-XZONED-18 PIC 9(10)V9(08).
       01 WS-NUMZONED-NS-09-09 REDEFINES WS-XZONED-18 PIC 9(09)V9(09).
       01 WS-NUMZONED-NS-08-10 REDEFINES WS-XZONED-18 PIC 9(08)V9(10).
       01 WS-NUMZONED-NS-07-11 REDEFINES WS-XZONED-18 PIC 9(07)V9(11).
       01 WS-NUMZONED-NS-06-12 REDEFINES WS-XZONED-18 PIC 9(06)V9(12).
       01 WS-NUMZONED-NS-05-13 REDEFINES WS-XZONED-18 PIC 9(05)V9(13).
       01 WS-NUMZONED-NS-04-14 REDEFINES WS-XZONED-18 PIC 9(04)V9(14).
       01 WS-NUMZONED-NS-03-15 REDEFINES WS-XZONED-18 PIC 9(03)V9(15).
       01 WS-NUMZONED-NS-02-16 REDEFINES WS-XZONED-18 PIC 9(02)V9(16).
       01 WS-NUMZONED-NS-01-17 REDEFINES WS-XZONED-18 PIC 9(01)V9(17).
       01 WS-NUMZONED-NS-00-18 REDEFINES WS-XZONED-18 PIC V9(18).
      *
      *    SEGMENTS DCLGENS INCLUSION
      *
           EXEC SQL INCLUDE A11111TB END-EXEC.
      *
       PROCEDURE DIVISION.
      *
       MAIN-PROGRAM.
      *
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'IVPDB2VD - IMS DATA VALIDATION STARTED AT: '
                                                       CURRENT-DATE-DISP
      *
           PERFORM ACCEPT-SYSIN THRU ACCEPT-SYSIN
           IF NOT SYSIN-COMMAND-VALID
             DISPLAY 'INVALID SYSIN CARD : ' SYSIN-CARD
             DISPLAY 'POSSIBLE VALUES ARE: EALL, EDB2, EIMS'
             GO TO HND-ABEND
           END-IF
      *
           PERFORM PROCESS THRU PROCESS-EX
      *
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'IVPDB2VD - IMS DATA VALIDATION ENDED AT: '
                                                       CURRENT-DATE-DISP
      *
           .
      *
       MAIN-PROGRAM-EX.
           STOP RUN.
      *
       PROCESS SECTION.
      *
           IF SYSIN-COMMAND-PULL-ALL OR SYSIN-COMMAND-PULL-DB2
             PERFORM HND-PULL-DB2 THRU HND-PULL-DB2-EX
           END-IF
           IF SYSIN-COMMAND-PULL-ALL OR SYSIN-COMMAND-PULL-IMS
             PERFORM HND-PULL-IMS THRU HND-PULL-IMS-EX
           END-IF
           IF SYSIN-COMMAND-VALIDATE
             PERFORM HND-VALIDATE THRU HND-VALIDATE-EX
           END-IF
      *
           .
      *
       PROCESS-EX.
           EXIT.
      *
       ACCEPT-SYSIN SECTION.
      *
           MOVE LOW-VALUE TO SYSIN-CARD
           ACCEPT SYSIN-CARD.
      *
       ACCEPT-SYSIN-EX.
           EXIT.
      *
       HND-VALIDATE SECTION.
      *
           OPEN INPUT FILEODB2
           IF NOT FILEODB2-STATUS-OK
             GO TO HND-ABEND
           END-IF
      *
           OPEN INPUT FILEOIMS
           IF NOT FILEOIMS-STATUS-OK
             GO TO HND-ABEND
           END-IF
      *
           PERFORM HND-COUNT-DB2 THRU HND-COUNT-DB2-EX
      *
           MOVE SQL-SEGM-COUNTER TO SEGM-COUNTER-Z
           DISPLAY '----------------------------------'
           DISPLAY '>STARTING DATA VALIDATION...'
           DISPLAY '>SEGMENTS TO BE COMPARED: ' SEGM-COUNTER-Z
      *
           MOVE ZERO TO SEGM-COUNTER
      *
           PERFORM UNTIL NOT FILEODB2-STATUS-OK
                      OR NOT FILEOIMS-STATUS-OK
      *
             READ FILEODB2
             MOVE FD-FILEODB2(1:FILEODB2-REC-LEN)
                                  TO IO-REC-FILEODB2(1:FILEODB2-REC-LEN)
             READ FILEOIMS
             MOVE FD-FILEOIMS(1:FILEOIMS-REC-LEN)
                                  TO IO-REC-FILEOIMS(1:FILEOIMS-REC-LEN)
             IF FILEODB2-STATUS-OK
             AND FILEOIMS-STATUS-OK
               ADD 1 TO SEGM-COUNTER
      *
      *        EVERY 5% PRINT A STATUS MESSAGE
      *
               COMPUTE PERCENTAGE = (SEGM-COUNTER * 100)
                                              / SQL-SEGM-COUNTER
               COMPUTE PERCENTAGE-VALUE = PERCENTAGE
                                             - PERCENTAGE-OLD
               IF PERCENTAGE-VALUE >= 5
                 MOVE PERCENTAGE TO PERCENTAGE-OLD
                                    PERCENTAGE-EDIT
                 MOVE SEGM-COUNTER TO SEGM-COUNTER-Z
                 MOVE SQL-SEGM-COUNTER TO TOTAL-DISP
                 MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
                 MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
                 DISPLAY
                    '>-- SEGMENTS COMPARED: ' PERCENTAGE-EDIT '%, '
                    SEGM-COUNTER-Z ' OF ' TOTAL-DISP ' - '
                    CURRENT-DATE-DISP
               END-IF
               IF IO-REC-FILEODB2 NOT = IO-REC-FILEOIMS
                 PERFORM HND-DIFFERENCE THRU HND-DIFFERENCE-EX
               END-IF
             END-IF
      *
           END-PERFORM.
      *
           CLOSE FILEODB2
           CLOSE FILEOIMS
      *
           PERFORM HND-STATISTICS THRU HND-STATISTICS-EX.
      *
       HND-VALIDATE-EX.
           EXIT.
      *
       HND-PULL-IMS SECTION.
      *
           DISPLAY '- '
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'HND-PULL-IMS START: '  CURRENT-DATE-DISP
      *
           OPEN INPUT FILEIN
           IF NOT FILEIN-STATUS-OK
             GO TO HND-ABEND
           END-IF
           OPEN OUTPUT FILEOIMS
           IF NOT FILEOIMS-STATUS-OK
             GO TO HND-ABEND
           END-IF
      *
      *    READ THE FIRST RECORD
      *
           READ FILEIN
      *
           IF NOT FILEIN-STATUS-OK
             DISPLAY 'ERROR: EMPTY INPUT FILE'
             GO TO HND-ABEND
           END-IF
      *
           IF NOT HEAD-TRAIL-RECORD
             DISPLAY 'ERROR: MISSING HEADER RECORD'
             DISPLAY 'FIRST RECORD: ' REC-FILEIN
             GO TO HND-ABEND
           ELSE
             DISPLAY 'INFO: HEADER RECORD IGNORED'
           END-IF
      *
           MOVE ZERO TO SEGM-COUNTER
      *
           PERFORM UNTIL NOT FILEIN-STATUS-OK
      *
             READ FILEIN
             IF FILEIN-STATUS-OK
             AND NOT HEAD-TRAIL-RECORD
               ADD 1 TO SEGM-COUNTER
               EVALUATE REC-SEGMNAME
               WHEN 'A1111111'
                 MOVE A1111111-LEN TO SEGMENT-LEN
                 ADD 1 TO A1111111-CNTL-IMS
                 MOVE REC-DATA(1:10) TO IMS-CONCATENATED-KEY(1:10)
               END-EVALUATE
      *
               MOVE REC-SEGMNAME TO IO-REC-SEGMNAME
               MOVE REC-DATA(1:SEGMENT-LEN) TO IO-REC-DATA
               MOVE IMS-CONCATENATED-KEY TO IO-REC-CONCAT-KEY
               COMPUTE FILEOIMS-REC-LEN = LENGTH OF FD-FILEOIMS
               WRITE FD-FILEOIMS FROM IO-REC-FILE
             ELSE
               IF FILEIN-STATUS-OK
                 DISPLAY 'INFO: TRAILER RECORD IGNORED'
               END-IF
             END-IF
      *
           END-PERFORM
      *
           CLOSE FILEOIMS
     *
           PERFORM HND-STATISTICS THRU HND-STATISTICS-EX.
      *
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'HND-PULL-IMS END: '  CURRENT-DATE-DISP.
      *
       HND-PULL-IMS-EX.
           EXIT.
      *
       HND-PULL-DB2 SECTION.
      *
           DISPLAY '- '
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'HND-PULL-DB2 START: '  CURRENT-DATE-DISP
      *
           OPEN OUTPUT FILEODB2
           IF NOT FILEODB2-STATUS-OK
             GO TO HND-ABEND
           END-IF
      *
           EXEC SQL
             DECLARE IVPDB2_A1111111_CRS CURSOR FOR
             SELECT
               KEY_A1111111,
               FIRST_NAME,
               EXTENSION,
               ZIP_CODE,
               A1111111_FILLER_1
             FROM IVPDB2_A1111111
           END-EXEC
      *
           EXEC SQL
             OPEN IVPDB2_A1111111_CRS
           END-EXEC
      *
           IF SQLCODE NOT = ZERO
             DISPLAY 'OPEN IVPDB2_A1111111_CRS'
             MOVE SQLCODE TO IRIS-SQLCODE
             MOVE SQLERRM TO IRIS-SQLERRM
             GO TO HND-ABEND
           END-IF
      *
           MOVE ZERO TO A1111111-CNTL-SQL
           MOVE LOW-VALUE TO IO-REC-FILE
           MOVE 'A1111111' TO IO-REC-SEGMNAME
           MOVE SPACE TO IO-REC-CONCAT-KEY
           PERFORM UNTIL SQLCODE NOT = ZERO
             EXEC SQL
               FETCH IVPDB2_A1111111_CRS INTO
               :IVPDB2-A1111111.KEY-A1111111,
               :IVPDB2-A1111111.FIRST-NAME,
               :IVPDB2-A1111111.EXTENSION,
               :IVPDB2-A1111111.ZIP-CODE,
               :IVPDB2-A1111111.A1111111-FILLER-1
             END-EXEC
             IF SQLCODE = ZERO
               ADD 1 TO A1111111-CNTL-SQL
               MOVE IVPDB2-A1111111(1:A1111111-LEN)
                                         TO IO-REC-DATA
               MOVE SPACE TO IO-REC-CONCAT-A1111111-KEY
               MOVE KEY-A1111111 OF IVPDB2-A1111111 TO
               IO-REC-CONCAT-A1111111-KEY(1:10)
               COMPUTE FILEODB2-REC-LEN = LENGTH OF FD-FILEODB2
               WRITE FD-FILEODB2 FROM IO-REC-FILE
             END-IF
           END-PERFORM
      *
           IF SQLCODE NOT = ZERO AND NOT = 100
             DISPLAY 'FETCH IVPDB2_A1111111_CRS'
             MOVE SQLCODE TO IRIS-SQLCODE
             MOVE SQLERRM TO IRIS-SQLERRM
             GO TO HND-ABEND
           END-IF
      *
           EXEC SQL
             CLOSE IVPDB2_A1111111_CRS
           END-EXEC
      *
           IF SQLCODE NOT = ZERO
             DISPLAY 'CLOSE IVPDB2_A1111111_CRS'
             MOVE SQLCODE TO IRIS-SQLCODE
             MOVE SQLERRM TO IRIS-SQLERRM
             GO TO HND-ABEND
           END-IF
      *
           ADD A1111111-CNTL-SQL TO SQL-SEGM-COUNTER
      *
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'HND-PULL-DB2 FETCH A1111111: '  CURRENT-DATE-DISP
      *
           CLOSE FILEODB2
      *
           PERFORM HND-STATISTICS-SQL THRU HND-STATISTICS-SQL-EX.
      *
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'HND-PULL-DB2 END: '  CURRENT-DATE-DISP.
      *
       HND-PULL-DB2-EX.
           EXIT.
      *
       HND-COUNT-DB2 SECTION.
      *
           DISPLAY '- '
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'HND-COUNT-DB2 START: ' CURRENT-DATE-DISP
      *
           EXEC SQL
             SELECT COUNT(*) INTO :A1111111-CNTL-SQL
             FROM IVPDB2_A1111111
           END-EXEC
      *
           IF SQLCODE NOT = ZERO
             DISPLAY 'COUNT(*) IVPDB2_A1111111'
             MOVE SQLCODE TO IRIS-SQLCODE
             MOVE SQLERRM TO IRIS-SQLERRM
             GO TO HND-ABEND
           END-IF
      *
           ADD A1111111-CNTL-SQL TO SQL-SEGM-COUNTER
      *
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'HND-COUNT-DB2 END: ' CURRENT-DATE-DISP.
      *
       HND-COUNT-DB2-EX.
           EXIT.
      *
       HND-DIFFERENCE SECTION.
      *
           MOVE ZERO TO DIFF-POS
           MOVE LENGTH OF IO-REC-FILEODB2 TO DIFF-LEN
           PERFORM VARYING IDX FROM 1 BY 1
           UNTIL IDX > DIFF-LEN
              OR DIFF-POS > ZERO
              IF IO-REC-FILEODB2(IDX:1) NOT = IO-REC-FILEOIMS(IDX:1)
                MOVE IDX TO DIFF-POS
              END-IF
           END-PERFORM
           MOVE 50 TO DIFF-LEN
           IF DIFF-LEN + DIFF-POS > LENGTH OF IO-REC-FILEODB2
             COMPUTE DIFF-LEN =
                     LENGTH OF IO-REC-FILEODB2 - DIFF-POS + 1
           END-IF
           ADD 1 TO SEGM-ERR-COUNTER
           MOVE SEGM-COUNTER TO SEGM-COUNTER-Z
           MOVE SEGM-ERR-COUNTER TO SEGM-ERR-COUNTER-Z
           DISPLAY '-'
           DISPLAY 'RECORD MISMATCH (' SEGM-COUNTER-Z ') - '
                   'OFFSET: ' DIFF-POS
           DISPLAY 'DB2 (' IO-REC-FILEODB2(1:8) ')>'
                                      IO-REC-FILEODB2(DIFF-POS:DIFF-LEN)
           DISPLAY 'IMS (' IO-REC-FILEOIMS(1:8) ')>'
                                      IO-REC-FILEOIMS(DIFF-POS:DIFF-LEN)
           DISPLAY 'DB2 (' IO-REC-FILEODB2(1:8) ')>'
                                      IO-REC-FILEODB2
           DISPLAY 'IMS (' IO-REC-FILEOIMS(1:8) ')>'
                                      IO-REC-FILEOIMS
           DISPLAY '-'
      *
           GO TO HND-ABEND
      *
           .
      *
       HND-DIFFERENCE-EX.
           EXIT.
      *
       HND-STATISTICS SECTION.
      *
           MOVE SEGM-COUNTER TO SEGM-COUNTER-Z
           DISPLAY '------------- IMS DATA -------------'
           IF SYSIN-COMMAND-PULL-ALL
             IF A1111111-CNTL-SQL = A1111111-CNTL-IMS
               DISPLAY '>A1111111 OK'
             ELSE
               DISPLAY '>A1111111 NOK, IMS: ' A1111111-CNTL-IMS
             END-IF
           ELSE
             DISPLAY '>A1111111:    ' A1111111-CNTL-IMS
             MOVE SEGM-COUNTER TO SEGM-COUNTER-Z
             DISPLAY '>TOTAL   : ' SEGM-COUNTER-Z
           END-IF.
      *
       HND-STATISTICS-EX.
           EXIT.
      *
       HND-STATISTICS-SQL SECTION.
      *
           DISPLAY '------------- SQL DATA -------------'
           DISPLAY '>A1111111:    ' A1111111-CNTL-SQL
           MOVE SQL-SEGM-COUNTER TO SEGM-COUNTER-Z
           DISPLAY '>TOTAL   : ' SEGM-COUNTER-Z
      *
           .
      *
       HND-STATISTICS-SQL-EX.
           EXIT.
      *
       HND-ABEND.
      *
           MOVE SEGM-COUNTER TO SEGM-COUNTER-Z
           MOVE SEGM-ERR-COUNTER TO SEGM-ERR-COUNTER-Z
           MOVE IRIS-SQLCODE TO SQLCODE-N
           MOVE SQLCODE-N TO SQLCODE-Z
           DISPLAY '----------------------------------'
           DISPLAY 'OPERATION TERMINATED WITH ERRORS: '
           DISPLAY ' FILEIN STATUS   = ' FILEIN-STATUS
           DISPLAY ' FILEODB2 STATUS = ' FILEODB2-STATUS
           DISPLAY ' FILEOIMS STATUS = ' FILEOIMS-STATUS
           DISPLAY ' ROWS READ       = ' SEGM-COUNTER-Z
           DISPLAY ' MISMATCHES      = ' SEGM-ERR-COUNTER-Z
           DISPLAY ' SQLCODE         = ' SQLCODE-Z
           IF SQLERRML > 0
             DISPLAY ' SQLERRM         = ' SQLERRMC(1:SQLERRML)
           END-IF
           DISPLAY '----------------------------------'
           MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-VAL
           MOVE CORR CURRENT-DATE-VAL TO CURRENT-DATE-DISP
           DISPLAY 'IVPDB2VD - IMS DATA VALIDATION ENDED AT: '
                                                       CURRENT-DATE-DISP
           MOVE 99 TO RETURN-CODE
           STOP RUN.
