      *CBL TRUNC(BIN),VLR(COMPAT)
      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: DATA CONVERTER PROGRAM FOR DBD: IVPDB2
      *
      ******************************************************************
      *                                                                *
       IDENTIFICATION DIVISION.
      *
       PROGRAM-ID. IVPDB2CV.
      *
       ENVIRONMENT DIVISION.
      *
       INPUT-OUTPUT SECTION.
      *
       FILE-CONTROL.
      *
           SELECT INPUT-FILE ASSIGN TO IVPDB2
                         ORGANIZATION IS SEQUENTIAL
                         ACCESS MODE IS SEQUENTIAL
                         STATUS FS-INPUT-FILE-STATUS.
      *
           SELECT A1111111-FILE ASSIGN TO A1111111
                         ORGANIZATION IS SEQUENTIAL
                         ACCESS MODE IS SEQUENTIAL
                         STATUS FS-OUTPUT-FILE-STATUS.
      *
       DATA DIVISION.
      *
       FILE SECTION.
      *
       FD INPUT-FILE
           RECORDING MODE IS V
           RECORD IS VARYING
           FROM 1 TO 32752
           LABEL RECORD IS STANDARD
           DATA RECORD IS INPUT-RECORD.
       01 INPUT-RECORD.
         03 FILLER                      PIC X.
           88 HEAD-TRAIL-RECORD         VALUE LOW-VALUE.
         03 FILLER                      PIC X(3).
         03 INPUT-SEGMENT-LEN           PIC S9(4) COMP.
         03 INPUT-SEGMENT               PIC X(8).
         03 FILLER                      PIC X(17).
         03 INPUT-RBA                   PIC 9(9)  COMP.
         03 INPUT-DATA                  PIC X(32717).
      *
       FD A1111111-FILE
           RECORDING MODE IS F.
       01 A1111111-RECORD PIC X(200).
      *
       WORKING-STORAGE SECTION.
      *
      *    FILE-STATUS HANDLING
      *
       01 FS-INPUT-FILE-STATUS          PIC X(02) VALUE SPACE.
         88 FILEIN-STATUS-OK            VALUE '00'.
         88 FILEIN-EOF                  VALUE '10'.
         88 FILEIN-INVALID-KEY          VALUE '20'.
         88 FILEIN-DUP-KEY              VALUE '22'.
         88 FILEIN-NOT-FOUND            VALUE '23'.
         88 FILEIN-BOUNDARY-ERROR       VALUE '24'.
         88 FILEIN-IO-ERROR             VALUE '30'.
         88 FILEIN-LOGIC-ERROR          VALUE '92'.
         88 FILEIN-NOT-AVAIL            VALUE '93'.
       01 FS-OUTPUT-FILE-STATUS         PIC X(02) VALUE SPACE.
         88 FILEOUT-STATUS-OK           VALUE '00'.
         88 FILEOUT-EOF                 VALUE '10'.
         88 FILEOUT-INVALID-KEY         VALUE '20'.
         88 FILEOUT-DUP-KEY             VALUE '22'.
         88 FILEOUT-NOT-FOUND           VALUE '23'.
         88 FILEOUT-BOUNDARY-ERROR      VALUE '24'.
         88 FILEOUT-IO-ERROR            VALUE '30'.
         88 FILEOUT-LOGIC-ERROR         VALUE '92'.
         88 FILEOUT-NOT-AVAIL           VALUE '93'.
      *
       01 A1111111-FILE-LEN             PIC S9(9)  COMP.
       01 A1111111-REC-COUNT            PIC S9(9)  COMP VALUE ZERO.
      *
      *    SEGMENTS CONCATENATED KEYS
      *
       01 WS-CK-POS                     PIC S9(4) COMP.
       01 WS-CK-LEN                     PIC S9(4) COMP.
       01 A1111111-CONCATENATED-KEY     PIC X(4096).
      *
       01 INPUT-RECORD-EBCDIC.
         03 FILLER                      PIC X(35).
         03 INPUT-RECORD-EBCDIC-DATA    PIC X(40).
      *
      *    WORKING STORAGE VARIABLES
      *
       01 WS-PROGRAM-NAME               PIC X(10)
                                        VALUE '*IVPDB2  *'.
       01 WS-RC                         PIC S9(9)  COMP.
       01 WS-IND-1                      PIC S9(9)  COMP.
       01 WS-IND-2                      PIC S9(9)  COMP.
       01 WS-IND-SEG                    PIC S9(9)  COMP.
       01 WS-IND-FLD                    PIC S9(9)  COMP.
       01 WS-RECORD-COUNT               PIC S9(9)  COMP.
       01 WS-RECORD-COUNT-EDIT          PIC ZZZ,ZZZ,ZZZ,ZZ9.
       01 WS-DIVIDE                     PIC S9(9)  COMP.
       01 WS-REMAINDER                  PIC S9(9)  COMP.
       01 WS-FIELD-NAME                 PIC X(30).
       01 WS-FIELD-VALUE                PIC X(60).
       01 WS-FIELD-ADDRESS              POINTER.
       01 WS-FIELD-LENGTH               PIC S9(4)  COMP.
       01 WS-INPUT-SEGMENT              PIC X(8).
       01 FILLER.
         03 WS-CACHED-DATA              OCCURS 32000.
           05 WS-CACHED-SEGMENT         PIC X(8).
           05 WS-CACHED-SEGMENT-LVL     PIC S9(4)  COMP.
           05 WS-CACHED-SEGMENT-DATA    PIC X(200).
           05 WS-CACHED-SEGMENT-CKEY    PIC X(10).
       01 WS-CACHED-DATA-IDX            PIC S9(9)  COMP VALUE 0.
       01 WS-CACHED-DATA-MAX            PIC S9(9)  COMP VALUE 32000.
       01 WS-CACHING-SEGMENT            PIC X(8).
       01 WS-FILESIZE-NUM               PIC S9(18) COMP.
      *
      *    IF A SEGMENT CONTAINS ALL X'DD' IT MEANS THAT ALL THE SEGMENTS
      *    HAVING A GREATER LEVEL HAVE TO BE DISCARTED, THE NEXT TO BE
      *    LOADED IS A LEVEL EQUAL OR LESS TO THE ONE WHICH HAS
      *    STARTED.
      *
       01 WS-ALLOWED-LEVEL                 PIC S9(9) COMP.
         88 WS-ALLOWED-LEVEL-ALL           VALUE +999.
      *
       01 FILLER.
         03 WS-USED-KEY-VALUE OCCURS 16.
           05 WS-USED-KEY-ALPHA            PIC X(256).
           05 WS-USED-KEY-PACKED           PIC S9(18) COMP-3.
           05 WS-USED-KEY-NUMERIC          PIC S9(18) COMP-3.
           05 WS-USED-KEY-NUMERIC-NEXT     PIC S9(18) COMP-3.
           05 WS-USED-KEY-NUMERIC-PREV     PIC S9(18) COMP-3.
       01 WS-COMMON-FLD                    PIC X(4608).
       01 FILLER REDEFINES WS-COMMON-FLD.
         03 WS-ZONED-FLD     OCCURS 256    PIC S9(18).
       01 FILLER REDEFINES WS-COMMON-FLD.
         03 WS-ZONED-FLD-CHR OCCURS 256    PIC X(18).
       01 FILLER REDEFINES WS-COMMON-FLD.
         03 WS-COMP-FLD-2    OCCURS 256    PIC S9(4) COMP.
       01 FILLER REDEFINES WS-COMMON-FLD.
         03 WS-COMP-FLD-4    OCCURS 256    PIC S9(9) COMP.
      *
       01 WS-COMMON-FLD-PACKED             PIC X(2560).
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-18-00 OCCURS 256 PIC S9(18)         COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-17-01 OCCURS 256 PIC S9(17)V9       COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-16-02 OCCURS 256 PIC S9(16)V9(02)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-15-03 OCCURS 256 PIC S9(15)V9(03)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-14-04 OCCURS 256 PIC S9(14)V9(04)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-13-05 OCCURS 256 PIC S9(13)V9(05)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-12-06 OCCURS 256 PIC S9(12)V9(06)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-11-07 OCCURS 256 PIC S9(11)V9(07)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-10-08 OCCURS 256 PIC S9(10)V9(08)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-09-09 OCCURS 256 PIC S9(09)V9(09)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-08-10 OCCURS 256 PIC S9(08)V9(10)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-07-11 OCCURS 256 PIC S9(07)V9(11)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-06-12 OCCURS 256 PIC S9(06)V9(12)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-05-13 OCCURS 256 PIC S9(05)V9(13)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-04-14 OCCURS 256 PIC S9(04)V9(14)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-03-15 OCCURS 256 PIC S9(03)V9(15)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-02-16 OCCURS 256 PIC S9(02)V9(16)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-01-17 OCCURS 256 PIC S9(01)V9(17)   COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-00-18 OCCURS 256 PIC SV9(18)        COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-18-00 OCCURS 256 PIC 9(18)       COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-17-01 OCCURS 256 PIC 9(17)V9     COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-16-02 OCCURS 256 PIC 9(16)V9(02) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-15-03 OCCURS 256 PIC 9(15)V9(03) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-14-04 OCCURS 256 PIC 9(14)V9(04) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-13-05 OCCURS 256 PIC 9(13)V9(05) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-12-06 OCCURS 256 PIC 9(12)V9(06) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-11-07 OCCURS 256 PIC 9(11)V9(07) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-10-08 OCCURS 256 PIC 9(10)V9(08) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-09-09 OCCURS 256 PIC 9(09)V9(09) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-08-10 OCCURS 256 PIC 9(08)V9(10) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-07-11 OCCURS 256 PIC 9(07)V9(11) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-06-12 OCCURS 256 PIC 9(06)V9(12) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-05-13 OCCURS 256 PIC 9(05)V9(13) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-04-14 OCCURS 256 PIC 9(04)V9(14) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-03-15 OCCURS 256 PIC 9(03)V9(15) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-02-16 OCCURS 256 PIC 9(02)V9(16) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-01-17 OCCURS 256 PIC 9(01)V9(17) COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-NS-FLD-00-18 OCCURS 256 PIC V9(18)      COMP-3.
       01 FILLER REDEFINES WS-COMMON-FLD-PACKED.
         03 WS-PACKED-FLD-CHR   OCCURS 256 PIC X(10).
      *
       01 WS-XZONED-18                  PIC X(18).
       01 WS-NUMZONED-18-00 REDEFINES WS-XZONED-18    PIC S9(18).
       01 WS-NUMZONED-17-01 REDEFINES WS-XZONED-18    PIC S9(17)V9.
       01 WS-NUMZONED-16-02 REDEFINES WS-XZONED-18    PIC S9(16)V9(02).
       01 WS-NUMZONED-15-03 REDEFINES WS-XZONED-18    PIC S9(15)V9(03).
       01 WS-NUMZONED-14-04 REDEFINES WS-XZONED-18    PIC S9(14)V9(04).
       01 WS-NUMZONED-13-05 REDEFINES WS-XZONED-18    PIC S9(13)V9(05).
       01 WS-NUMZONED-12-06 REDEFINES WS-XZONED-18    PIC S9(12)V9(06).
       01 WS-NUMZONED-11-07 REDEFINES WS-XZONED-18    PIC S9(11)V9(07).
       01 WS-NUMZONED-10-08 REDEFINES WS-XZONED-18    PIC S9(10)V9(08).
       01 WS-NUMZONED-09-09 REDEFINES WS-XZONED-18    PIC S9(09)V9(09).
       01 WS-NUMZONED-08-10 REDEFINES WS-XZONED-18    PIC S9(08)V9(10).
       01 WS-NUMZONED-07-11 REDEFINES WS-XZONED-18    PIC S9(07)V9(11).
       01 WS-NUMZONED-06-12 REDEFINES WS-XZONED-18    PIC S9(06)V9(12).
       01 WS-NUMZONED-05-13 REDEFINES WS-XZONED-18    PIC S9(05)V9(13).
       01 WS-NUMZONED-04-14 REDEFINES WS-XZONED-18    PIC S9(04)V9(14).
       01 WS-NUMZONED-03-15 REDEFINES WS-XZONED-18    PIC S9(03)V9(15).
       01 WS-NUMZONED-02-16 REDEFINES WS-XZONED-18    PIC S9(02)V9(16).
       01 WS-NUMZONED-01-17 REDEFINES WS-XZONED-18    PIC S9(01)V9(17).
       01 WS-NUMZONED-00-18 REDEFINES WS-XZONED-18    PIC SV9(18).
       01 WS-NUMZONED-NS-18-00 REDEFINES WS-XZONED-18 PIC 9(18).
       01 WS-NUMZONED-NS-17-01 REDEFINES WS-XZONED-18 PIC 9(17)V9.
       01 WS-NUMZONED-NS-16-02 REDEFINES WS-XZONED-18 PIC 9(16)V9(02).
       01 WS-NUMZONED-NS-15-03 REDEFINES WS-XZONED-18 PIC 9(15)V9(03).
       01 WS-NUMZONED-NS-14-04 REDEFINES WS-XZONED-18 PIC 9(14)V9(04).
       01 WS-NUMZONED-NS-13-05 REDEFINES WS-XZONED-18 PIC 9(13)V9(05).
       01 WS-NUMZONED-NS-12-06 REDEFINES WS-XZONED-18 PIC 9(12)V9(06).
       01 WS-NUMZONED-NS-11-07 REDEFINES WS-XZONED-18 PIC 9(11)V9(07).
       01 WS-NUMZONED-NS-10-08 REDEFINES WS-XZONED-18 PIC 9(10)V9(08).
       01 WS-NUMZONED-NS-09-09 REDEFINES WS-XZONED-18 PIC 9(09)V9(09).
       01 WS-NUMZONED-NS-08-10 REDEFINES WS-XZONED-18 PIC 9(08)V9(10).
       01 WS-NUMZONED-NS-07-11 REDEFINES WS-XZONED-18 PIC 9(07)V9(11).
       01 WS-NUMZONED-NS-06-12 REDEFINES WS-XZONED-18 PIC 9(06)V9(12).
       01 WS-NUMZONED-NS-05-13 REDEFINES WS-XZONED-18 PIC 9(05)V9(13).
       01 WS-NUMZONED-NS-04-14 REDEFINES WS-XZONED-18 PIC 9(04)V9(14).
       01 WS-NUMZONED-NS-03-15 REDEFINES WS-XZONED-18 PIC 9(03)V9(15).
       01 WS-NUMZONED-NS-02-16 REDEFINES WS-XZONED-18 PIC 9(02)V9(16).
       01 WS-NUMZONED-NS-01-17 REDEFINES WS-XZONED-18 PIC 9(01)V9(17).
       01 WS-NUMZONED-NS-00-18 REDEFINES WS-XZONED-18 PIC V9(18).
      *
       01 WS-COMMON-FLD-EDIT               PIC X(4864).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-18-00 OCCURS 256   PIC -9(18).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-17-01 OCCURS 256   PIC -9(17).9.
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-16-02 OCCURS 256   PIC -9(16).9(02).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-15-03 OCCURS 256   PIC -9(15).9(03).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-14-04 OCCURS 256   PIC -9(14).9(04).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-13-05 OCCURS 256   PIC -9(13).9(05).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-12-06 OCCURS 256   PIC -9(12).9(06).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-11-07 OCCURS 256   PIC -9(11).9(07).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-10-08 OCCURS 256   PIC -9(10).9(08).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-09-09 OCCURS 256   PIC -9(09).9(09).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-08-10 OCCURS 256   PIC -9(08).9(10).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-07-11 OCCURS 256   PIC -9(07).9(11).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-06-12 OCCURS 256   PIC -9(06).9(12).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-05-13 OCCURS 256   PIC -9(05).9(13).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-04-14 OCCURS 256   PIC -9(04).9(14).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-03-15 OCCURS 256   PIC -9(03).9(15).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-02-16 OCCURS 256   PIC -9(02).9(16).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-01-17 OCCURS 256   PIC -9(01).9(17).
       01 FILLER REDEFINES WS-COMMON-FLD-EDIT.
         03 WS-EDIT-FLD-00-18 OCCURS 256   PIC -.9(18).
      *
       01 FILLER                           PIC 9     VALUE 0.
         88 SHOW-RECORD-DUMP                      VALUE 1.
       01 FILLER                           PIC 9     VALUE 1.
         88 SHOW-WARNING-DETAILS                  VALUE 1.
       01 FILLER                           PIC 9     VALUE 0.
         88 EVAL-DUMMY-COND                       VALUE 1.
       01 WS-WARNINGS                      PIC 9(9)  VALUE 0.
       01 FILLER.
         03 WS-WARNINGS-SUMMARY.
           05 FILLER                       PIC X(8)    VALUE 'A1111111'.
           05 FILLER                       PIC X(1000) VALUE LOW-VALUE.
         03 WS-WARNINGS-TAB REDEFINES WS-WARNINGS-SUMMARY OCCURS 1.
           05 WS-WARNINGS-SEGNAME          PIC X(8).
           05 WS-WARNINGS-DATA             PIC X(1000).
           05 FILLER REDEFINES WS-WARNINGS-DATA OCCURS 10.
             07 WS-WARNINGS-OCCURS         PIC 9(10).
             07 WS-WARNINGS-FLDNAME        PIC X(30).
             07 WS-WARNINGS-VALUE          PIC X(60).
       01 WS-WARNINGS-HEADER-L             PIC X(99) VALUE ALL '-'.
       01 WS-WARNINGS-HEADER.
         03 FILLER                         PIC X(2)  VALUE '| '.
         03 FILLER                         PIC X(8)  VALUE 'SEGMENT'.
         03 FILLER                         PIC X(3)  VALUE ' | '.
         03 FILLER                         PIC X(10) VALUE '  OCCURS  '.
         03 FILLER                         PIC X(3)  VALUE ' | '.
         03 FILLER                         PIC X(7)  VALUE SPACE.
         03 FILLER                         PIC X(5)  VALUE 'FIELD'.
         03 FILLER                         PIC X(7)  VALUE SPACE.
         03 FILLER                         PIC X(3)  VALUE ' | '.
         03 FILLER                         PIC X(22) VALUE SPACE.
         03 FILLER                         PIC X(5)  VALUE 'VALUE'.
         03 FILLER                         PIC X(22) VALUE SPACE.
         03 FILLER                         PIC X(2)  VALUE ' |'.
       01 WS-WARNINGS-LINE.
         03 FILLER                         PIC X(2)  VALUE '| '.
         03 WS-WARN-SEGNAME-EDIT           PIC X(8).
         03 FILLER                         PIC X(3)  VALUE ' | '.
         03 WS-WARN-OCCURS-EDIT            PIC ZZ,ZZZ,ZZ9.
         03 FILLER                         PIC X(3)  VALUE ' | '.
         03 WS-WARN-FLDNAME-EDIT           PIC X(19).
         03 FILLER                         PIC X(3)  VALUE ' | '.
         03 WS-WARN-VALUE-EDIT             PIC X(49).
         03 FILLER                         PIC X(2)  VALUE ' |'.
       01 WS-INT-CODE-ZONED                PIC 9(5)  VALUE 0.
      *
      *    COPYBOOKSs INCLUSION
      *
      *    COPY A11111TB.
      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: DCLGEN FOR TABLE: IVPDB2_A1111111
      *
      ******************************************************************

      ******************************************************************
      * COBOL DCLGEN FOR SEGMENT: A1111111
      ******************************************************************

       01 IVPDB2-A1111111.
0001     03 KEY-A1111111                      PIC X(10).
0011     03 FIRST-NAME                        PIC X(10).
0021     03 EXTENSION                         PIC X(10).
0031     03 ZIP-CODE                          PIC X(7).
0038     03 A1111111-FILLER-1                 PIC X(3).
0041     03 CREATION-TS                       PIC X(26).
0067     03 LAST-UPDATE-TS                    PIC X(26).
0093     03 CREATION-USER-I.
0093       49 CREATION-USER-I-LEN             PIC S9(4) COMP.
0095       49 CREATION-USER-I-TXT             PIC X(25).
0120     03 LAST-UPDATE-USER-I.
0120       49 LAST-UPDATE-USER-I-LEN          PIC S9(4) COMP.
0122       49 LAST-UPDATE-USER-I-TXT          PIC X(25).
0147     03 CREATION-TRAN-I.
0147       49 CREATION-TRAN-I-LEN             PIC S9(4) COMP.
0149       49 CREATION-TRAN-I-TXT             PIC X(25).
0174     03 LAST-UPDATE-TRAN-I.
0174       49 LAST-UPDATE-TRAN-I-LEN          PIC S9(4) COMP.
0176       49 LAST-UPDATE-TRAN-I-TXT          PIC X(25).
      *    COPY A11111CO.
      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: DCLGEN FOR TABLE: IVPDB2_A1111111
      *
      ******************************************************************

      ******************************************************************
      * COBOL DCLGEN FOR SEGMENT: A1111111
      ******************************************************************

       01 O-IVPDB2-A1111111.
0001     03 O-KEY-A1111111                      PIC X(10).
0011     03 O-FIRST-NAME                        PIC X(10).
0021     03 O-EXTENSION                         PIC X(10).
0031     03 O-ZIP-CODE                          PIC X(7).
0038     03 O-A1111111-FILLER-1                 PIC X(3).
0041     03 O-CREATION-TS                       PIC X(26).
0067     03 O-LAST-UPDATE-TS                    PIC X(26).
0093     03 O-CREATION-USER-I.
0093       49 O-CREATION-USER-I-LEN             PIC S9(4) COMP.
0095       49 O-CREATION-USER-I-TXT             PIC X(25).
0120     03 O-LAST-UPDATE-USER-I.
0120       49 O-LAST-UPDATE-USER-I-LEN          PIC S9(4) COMP.
0122       49 O-LAST-UPDATE-USER-I-TXT          PIC X(25).
0147     03 O-CREATION-TRAN-I.
0147       49 O-CREATION-TRAN-I-LEN             PIC S9(4) COMP.
0149       49 O-CREATION-TRAN-I-TXT             PIC X(25).
0174     03 O-LAST-UPDATE-TRAN-I.
0174       49 O-LAST-UPDATE-TRAN-I-LEN          PIC S9(4) COMP.
0176       49 O-LAST-UPDATE-TRAN-I-TXT          PIC X(25).
      *    COPY A11111CC.
      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: DCLGEN FOR TABLE: IVPDB2_A1111111
      *
      ******************************************************************

      ******************************************************************
      * COBOL DCLGEN FOR SEGMENT: A1111111
      ******************************************************************

      *
       01 C-IVPDB2-A1111111.
0001     03 C-KEY-A1111111                      PIC X(10).
0011     03 C-FIRST-NAME                        PIC X(10).
0021     03 C-EXTENSION                         PIC X(10).
0031     03 C-ZIP-CODE                          PIC X(7).
0038     03 C-A1111111-FILLER-1                 PIC X(3).
0041     03 C-CREATION-TS                       PIC X(26).
0067     03 C-LAST-UPDATE-TS                    PIC X(26).
0093     03 C-CREATION-USER-I.
0093       49 C-CREATION-USER-I-LEN             PIC S9(4) COMP.
0095       49 C-CREATION-USER-I-TXT             PIC X(25).
0120     03 C-LAST-UPDATE-USER-I.
0120       49 C-LAST-UPDATE-USER-I-LEN          PIC S9(4) COMP.
0122       49 C-LAST-UPDATE-USER-I-TXT          PIC X(25).
0147     03 C-CREATION-TRAN-I.
0147       49 C-CREATION-TRAN-I-LEN             PIC S9(4) COMP.
0149       49 C-CREATION-TRAN-I-TXT             PIC X(25).
0174     03 C-LAST-UPDATE-TRAN-I.
0174       49 C-LAST-UPDATE-TRAN-I-LEN          PIC S9(4) COMP.
0176       49 C-LAST-UPDATE-TRAN-I-TXT          PIC X(25).
      *
      *
       LINKAGE SECTION.
      *
       01 LK-FIELD-AREA            PIC X(32000).
      *
      *
      *
       PROCEDURE DIVISION.
      *
       MAIN-PROGRAM SECTION.
      *
      *    INITIALIZE THE VARIABLES
      *
           PERFORM INITIALIZE-VARIABLES
           THRU INITIALIZE-VARIABLES-EX
      *
      *    OPEN INPUT FILE
      *
           PERFORM OPEN-INPUT-FILE
           THRU OPEN-INPUT-FILE-EX
      *
      *    OPEN OUTPUT FILE
      *
           PERFORM OPEN-OUTPUT-FILES
           THRU OPEN-OUTPUT-FILES-EX
      *
      *    READ THE FIRST RECORD
      *
           PERFORM READ-INPUT-FILE
           THRU READ-INPUT-FILE-EX
      *
           IF FILEIN-EOF
             DISPLAY WS-PROGRAM-NAME ' EMPTY INPUT FILE'
             PERFORM STOP-PROGRAM
             THRU STOP-PROGRAM-EX
           END-IF
      *
           IF NOT HEAD-TRAIL-RECORD
             DISPLAY WS-PROGRAM-NAME ' MISSING HEADER RECORD'
             DISPLAY ' FIRST RECORD: ' INPUT-RECORD
             PERFORM STOP-PROGRAM
             THRU STOP-PROGRAM-EX
           ELSE
             DISPLAY WS-PROGRAM-NAME ' HEADER RECORD IGNORED'
           END-IF
      *
      *    READ ALL THE RECORDS TO BE LOADED
      *
           PERFORM UNTIL NOT FILEIN-STATUS-OK
      *
      *      READ THE NEXT RECORD
      *
             PERFORM READ-INPUT-FILE
             THRU READ-INPUT-FILE-EX
      *
             IF FILEIN-STATUS-OK
             AND NOT HEAD-TRAIL-RECORD
      *
               ADD 1 TO WS-RECORD-COUNT
      *
      *        EVERY 10000 RECORDS PRINT A STATUS MESSAGE
      *
               DIVIDE WS-RECORD-COUNT BY 10000 GIVING WS-DIVIDE
                                               REMAINDER WS-REMAINDER
      *
               IF WS-REMAINDER = 0
                 MOVE WS-RECORD-COUNT TO WS-RECORD-COUNT-EDIT
                 DISPLAY ' '
                 DISPLAY WS-PROGRAM-NAME
                      ' ********************************************'
                      '*************'
                 DISPLAY WS-PROGRAM-NAME
                      ' RECORDS CONVERTED: ' WS-RECORD-COUNT-EDIT
                 DISPLAY WS-PROGRAM-NAME
                      ' ********************************************'
                      '*************'
                 DISPLAY ' '
               END-IF
      *
      *        SAVE THE INPUT SEGMENT DATA
      *
               MOVE INPUT-SEGMENT TO WS-INPUT-SEGMENT
               MOVE INPUT-RECORD TO INPUT-RECORD-EBCDIC
      *
      *        CONVERT AND LOAD THE SEGMENT
      *
               EVALUATE WS-INPUT-SEGMENT
               WHEN 'A1111111'
      *
                 ADD 1 TO A1111111-REC-COUNT
      *
                 PERFORM A1111111-CONVERT
                 THRU A1111111-CONVERT-EX
                 SET WS-ALLOWED-LEVEL-ALL TO TRUE
      *
                 PERFORM A1111111-CACHE
                 THRU A1111111-CACHE-EX
               WHEN OTHER
                 DISPLAY WS-PROGRAM-NAME ' UNKNOWN SEGMENT: '
                                                     WS-INPUT-SEGMENT
                 PERFORM DUMP-RECORD
                 THRU DUMP-RECORD-EX
                 MOVE 69 TO RETURN-CODE
                 PERFORM STOP-PROGRAM
                 THRU STOP-PROGRAM-EX
               END-EVALUATE
      *
             ELSE
               IF FILEIN-STATUS-OK
                 DISPLAY WS-PROGRAM-NAME ' TRAILER RECORD IGNORED'
               END-IF
             END-IF
      *
           END-PERFORM
      *
      *    CLOSE THE INPUT FILE
      *
           PERFORM CLOSE-INPUT-FILE
           THRU CLOSE-INPUT-FILE-EX
      *
           MOVE 'A1111111' TO WS-CACHING-SEGMENT
           PERFORM WRITE-CACHED-SEGMENT
           THRU WRITE-CACHED-SEGMENT-EX
      *
      *    CLOSE THE OUTPUT FILES
      *
           PERFORM CLOSE-OUTPUT-FILES
           THRU CLOSE-OUTPUT-FILES-EX
      *
           DISPLAY ' '
           DISPLAY WS-PROGRAM-NAME
                      ' ********************************************'
                      '*************'
           MOVE WS-RECORD-COUNT TO WS-RECORD-COUNT-EDIT
           DISPLAY WS-PROGRAM-NAME
                      ' RECORDS CONVERTED: ' WS-RECORD-COUNT-EDIT
           MOVE A1111111-REC-COUNT TO WS-RECORD-COUNT-EDIT
           DISPLAY WS-PROGRAM-NAME
                      ' SEGMENT A1111111 : ' WS-RECORD-COUNT-EDIT
           DISPLAY WS-PROGRAM-NAME
                      ' ********************************************'
                      '*************'
           DISPLAY ' '.
      *
       MAIN-PROGRAM-EX.
      *
      *    SEND A WARNING MESSAGE IF ANY
      *
           IF WS-WARNINGS > ZERO
      *
             MOVE WS-WARNINGS TO WS-RECORD-COUNT-EDIT
             DISPLAY ' '
             DISPLAY WS-PROGRAM-NAME WS-WARNINGS-HEADER-L
             DISPLAY WS-PROGRAM-NAME
                        ' ATTENTION!!! WARNINGS DURING THE LOAD:    '
                                                   WS-RECORD-COUNT-EDIT
             DISPLAY WS-PROGRAM-NAME WS-WARNINGS-HEADER-L
             DISPLAY WS-PROGRAM-NAME WS-WARNINGS-HEADER
             DISPLAY WS-PROGRAM-NAME WS-WARNINGS-HEADER-L
             PERFORM VARYING WS-IND-SEG FROM 1 BY 1
             UNTIL WS-IND-SEG > 1
               PERFORM VARYING WS-IND-FLD FROM 1 BY 1
               UNTIL WS-IND-FLD > 10
                 IF WS-WARNINGS-FLDNAME(WS-IND-SEG, WS-IND-FLD) NOT =
                                                               LOW-VALUE
                   MOVE WS-WARNINGS-SEGNAME(WS-IND-SEG) TO
                                                    WS-WARN-SEGNAME-EDIT
                   MOVE WS-WARNINGS-OCCURS(WS-IND-SEG, WS-IND-FLD) TO
                                                     WS-WARN-OCCURS-EDIT
                   MOVE WS-WARNINGS-FLDNAME(WS-IND-SEG, WS-IND-FLD) TO
                                                    WS-WARN-FLDNAME-EDIT
                   MOVE WS-WARNINGS-VALUE(WS-IND-SEG, WS-IND-FLD) TO
                                                      WS-WARN-VALUE-EDIT
                   DISPLAY WS-PROGRAM-NAME WS-WARNINGS-LINE
                 END-IF
               END-PERFORM
             END-PERFORM
             DISPLAY WS-PROGRAM-NAME WS-WARNINGS-HEADER-L
             DISPLAY ' '
      *
             PERFORM STOP-PROGRAM
             THRU STOP-PROGRAM-EX
      *
           END-IF
      *
           DISPLAY WS-PROGRAM-NAME ' RETURN-CODE=(OK )'
      *
           GOBACK.
      *
      ******************************************************************
      *                                                                *
      *                    P R O G R A M    E N D                      *
      *                                                                *
      ******************************************************************
      *
      ******************************************************************
      *    INITIALIZE VARIABLES
      ******************************************************************
      *
       INITIALIZE-VARIABLES SECTION.
           MOVE ZERO TO WS-RECORD-COUNT
           SET WS-ALLOWED-LEVEL-ALL TO TRUE
      *
           COMPUTE A1111111-FILE-LEN = LENGTH OF C-IVPDB2-A1111111
      *
           MOVE ZERO TO RETURN-CODE.
       INITIALIZE-VARIABLES-EX.
           EXIT.
      *
      ******************************************************************
      *    OPEN THE INPUT FILE
      ******************************************************************
      *
       OPEN-INPUT-FILE SECTION.
      *
           OPEN INPUT INPUT-FILE
           IF NOT FILEIN-STATUS-OK
             DISPLAY WS-PROGRAM-NAME ' CANNOT OPEN INPUT FILE STATUS: '
                                                   FS-INPUT-FILE-STATUS
             PERFORM STOP-PROGRAM
             THRU STOP-PROGRAM-EX
           END-IF.
      *
       OPEN-INPUT-FILE-EX.
           EXIT.
      *
      *
      ******************************************************************
      *    OPEN THE OUTPUT FILES
      ******************************************************************
      *
       OPEN-OUTPUT-FILES SECTION.
      *
           OPEN OUTPUT A1111111-FILE
           IF NOT FILEOUT-STATUS-OK
             DISPLAY WS-PROGRAM-NAME ' CANNOT OPEN OUTPUT FILE: '
             'A1111111-FILE, STATUS: ' FS-OUTPUT-FILE-STATUS
             PERFORM STOP-PROGRAM
             THRU STOP-PROGRAM-EX
           END-IF.
      *
       OPEN-OUTPUT-FILES-EX.
           EXIT.
      *
      ******************************************************************
      *    READ THE INPUT FILE
      ******************************************************************
      *
       READ-INPUT-FILE SECTION.
      *
           READ INPUT-FILE
      *
           IF NOT FILEIN-STATUS-OK
           AND NOT FILEIN-EOF
             DISPLAY WS-PROGRAM-NAME ' CANNOT READ INPUT FILE STATUS: '
                                                   FS-INPUT-FILE-STATUS
             PERFORM STOP-PROGRAM
             THRU STOP-PROGRAM-EX
           END-IF.
      *
       READ-INPUT-FILE-EX.
           EXIT.
      *
      ******************************************************************
      *    CLOSE INPUT FILE
      ******************************************************************
      *
       CLOSE-INPUT-FILE SECTION.
           CLOSE INPUT-FILE.
       CLOSE-INPUT-FILE-EX.
           EXIT.
      *
      *
      ******************************************************************
      *    CLOSE OUTPUT FILES
      ******************************************************************
      *
       CLOSE-OUTPUT-FILES SECTION.
      *
           CLOSE A1111111-FILE.
      *
       CLOSE-OUTPUT-FILES-EX.
           EXIT.
      *
      *
      ******************************************************************
      *    SEND THE ERROR MESSAGE
      ******************************************************************
      *
       STOP-PROGRAM SECTION.
      *
             MOVE 99 TO RETURN-CODE
           DISPLAY WS-PROGRAM-NAME ' RETURN-CODE=(NOK)'
      *
           GOBACK.
      *
       STOP-PROGRAM-EX.
           EXIT.
      *
      ******************************************************************
      *    SEND A CONVERSION WARNING AND DUMP THE RECORD
      ******************************************************************
      *
       WARNING-CONVERSION SECTION.
      *
           ADD 1 TO WS-WARNINGS
      *
           IF SHOW-WARNING-DETAILS
      *
             MOVE WS-RECORD-COUNT TO WS-RECORD-COUNT-EDIT
             SET ADDRESS OF LK-FIELD-AREA TO WS-FIELD-ADDRESS
             EVALUATE TRUE
             WHEN LK-FIELD-AREA(1:WS-FIELD-LENGTH) = LOW-VALUE
               MOVE '<LOW-VALUE>' TO WS-FIELD-VALUE
             WHEN LK-FIELD-AREA(1:WS-FIELD-LENGTH) = HIGH-VALUE
               MOVE '<HIGH-VALUE>' TO WS-FIELD-VALUE
             WHEN LK-FIELD-AREA(1:WS-FIELD-LENGTH) = SPACE
               MOVE '<SPACE>' TO WS-FIELD-VALUE
             WHEN OTHER
               MOVE LK-FIELD-AREA(1:WS-FIELD-LENGTH) TO WS-FIELD-VALUE
             END-EVALUATE
      *
             DISPLAY WS-PROGRAM-NAME ' RECORD: ' WS-RECORD-COUNT-EDIT
             ', SEGMENT: ' WS-INPUT-SEGMENT ', FIELD: ' WS-FIELD-NAME
             ', VALUE=' WS-FIELD-VALUE
      *
             PERFORM VARYING WS-IND-SEG FROM 1 BY 1
             UNTIL WS-IND-SEG > 1
               IF WS-WARNINGS-SEGNAME(WS-IND-SEG) = WS-INPUT-SEGMENT
                 PERFORM VARYING WS-IND-FLD FROM 1 BY 1
                 UNTIL WS-IND-FLD > 10
                   IF WS-WARNINGS-FLDNAME(WS-IND-SEG, WS-IND-FLD) =
                                                           WS-FIELD-NAME
                   AND WS-WARNINGS-VALUE(WS-IND-SEG, WS-IND-FLD) =
                                                          WS-FIELD-VALUE
                     ADD 1 TO WS-WARNINGS-OCCURS(WS-IND-SEG, WS-IND-FLD)
                     ADD 10 TO WS-IND-FLD
                   ELSE
                     IF WS-WARNINGS-FLDNAME(WS-IND-SEG, WS-IND-FLD) =
                                                               LOW-VALUE
                       MOVE WS-FIELD-NAME TO
                             WS-WARNINGS-FLDNAME(WS-IND-SEG, WS-IND-FLD)
                       MOVE WS-FIELD-VALUE TO
                               WS-WARNINGS-VALUE(WS-IND-SEG, WS-IND-FLD)
                       MOVE 1 TO
                              WS-WARNINGS-OCCURS(WS-IND-SEG, WS-IND-FLD)
                       ADD 10 TO WS-IND-FLD
                     END-IF
                   END-IF
                 END-PERFORM
                 ADD 1 TO WS-IND-SEG
               END-IF
             END-PERFORM
           END-IF
      *
           IF SHOW-RECORD-DUMP
             PERFORM DUMP-RECORD
             THRU DUMP-RECORD-EX
           END-IF.
      *
       WARNING-CONVERSION-EX.
           EXIT.
      *
      ******************************************************************
      *    DUMP THE RECORD AREA
      ******************************************************************
      *
       DUMP-RECORD SECTION.
           MOVE WS-RECORD-COUNT TO WS-RECORD-COUNT-EDIT
           DISPLAY WS-PROGRAM-NAME ' DUMP RECORD NUMBER: '
                                                   WS-RECORD-COUNT-EDIT
           DISPLAY 'RECORD=' INPUT-RECORD.
       DUMP-RECORD-EX.
           EXIT.
      *
      ******************************************************************
      *    WRITE LAST CACHED SEGMENT
      ******************************************************************
      *
       WRITE-CACHED-SEGMENT SECTION.
      *
      *    IF IT IS A ROOT SEGMENT
           IF WS-CACHING-SEGMENT = 'A1111111'
             PERFORM VARYING WS-IND-1 FROM 1 BY 1
                     UNTIL WS-IND-1 > WS-CACHED-DATA-IDX
      *
      *        WRITE THE PREVIOUS RECORDS
      *
               EVALUATE WS-CACHED-SEGMENT(WS-IND-1)
               WHEN 'A1111111'
                 MOVE WS-CACHED-SEGMENT-DATA(WS-IND-1)
                      (1:LENGTH OF C-IVPDB2-A1111111) TO C-IVPDB2-A1111111
                 MOVE WS-CACHED-SEGMENT-CKEY(WS-IND-1)
                                        TO A1111111-CONCATENATED-KEY
                 PERFORM A1111111-WRITE
                 THRU A1111111-WRITE-EX
               END-EVALUATE
      *
               IF NOT FILEOUT-STATUS-OK
                 DISPLAY WS-PROGRAM-NAME ' FILE STATUS ERROR: '
                                                FS-OUTPUT-FILE-STATUS
                 DISPLAY WS-PROGRAM-NAME ' SEGMENT: ' 
                                          WS-CACHED-SEGMENT(WS-IND-1)
                 PERFORM DUMP-RECORD
                 THRU DUMP-RECORD-EX
                 MOVE 79 TO RETURN-CODE
                 PERFORM STOP-PROGRAM
                 THRU STOP-PROGRAM-EX
               END-IF
      *
             END-PERFORM
      *
             MOVE ZERO TO WS-CACHED-DATA-IDX
      *
           END-IF.
      *
       WRITE-CACHED-SEGMENT-EX.
           EXIT.
      *
      ******************************************************************
      *    CONVERT SEGMENT A1111111
      ******************************************************************
       A1111111-CONVERT SECTION.
      *
           INITIALIZE IVPDB2-A1111111
           MOVE INPUT-DATA(1:40) TO IVPDB2-A1111111(1:40)
      *
           IF IVPDB2-A1111111 = ALL X'DD'
             MOVE 1 TO WS-ALLOWED-LEVEL
             GO TO A1111111-CONVERT-EX
           END-IF
      *
           MOVE INPUT-RECORD-EBCDIC-DATA(1:40) TO
                            O-IVPDB2-A1111111(1:40)
      *
      *
           MOVE IVPDB2-A1111111(1:40) TO INPUT-DATA(1:40)
      *
           .
       A1111111-CONVERT-EX.
           EXIT.
      *
      ******************************************************************
      *    CACHE SEGMENT A1111111
      ******************************************************************
      *
       A1111111-CACHE SECTION.
      *
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
      *
           MOVE 'A1111111' TO WS-CACHING-SEGMENT
      *
           PERFORM WRITE-CACHED-SEGMENT
           THRU WRITE-CACHED-SEGMENT-EX
      *
           MOVE 1 TO WS-CK-POS
           MOVE ZERO TO WS-CK-LEN
           COMPUTE WS-CK-LEN = 10
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10)
                    TO A1111111-CONCATENATED-KEY(WS-CK-POS:WS-CK-LEN)
           ADD WS-CK-LEN TO WS-CK-POS
      *
           MOVE LOW-VALUE TO C-IVPDB2-A1111111
      *
           MOVE KEY-A1111111 OF IVPDB2-A1111111
               TO C-KEY-A1111111 OF C-IVPDB2-A1111111
           MOVE FIRST-NAME OF IVPDB2-A1111111
               TO C-FIRST-NAME OF C-IVPDB2-A1111111
           MOVE EXTENSION OF IVPDB2-A1111111
               TO C-EXTENSION OF C-IVPDB2-A1111111
           MOVE ZIP-CODE OF IVPDB2-A1111111
               TO C-ZIP-CODE OF C-IVPDB2-A1111111
           MOVE A1111111-FILLER-1 OF IVPDB2-A1111111
               TO C-A1111111-FILLER-1 OF C-IVPDB2-A1111111
      *
      *                                                                         
      * $IRIS_WS\profile\template_CONVERT_PROGRAM_AUDIT.txt                     
      *                                                                         
      * No default values have been setup for AUDIT fields                      
      *
           ADD 1 TO WS-CACHED-DATA-IDX
           IF WS-CACHED-DATA-IDX > WS-CACHED-DATA-MAX
             DISPLAY WS-PROGRAM-NAME ' CACHE LIMIT REACHED.'
             PERFORM STOP-PROGRAM
             THRU STOP-PROGRAM-EX
           END-IF
           MOVE 'A1111111' TO WS-CACHED-SEGMENT(WS-CACHED-DATA-IDX)
           MOVE 1 TO WS-CACHED-SEGMENT-LVL(WS-CACHED-DATA-IDX)
           MOVE C-IVPDB2-A1111111 TO
                      WS-CACHED-SEGMENT-DATA(WS-CACHED-DATA-IDX)
           MOVE A1111111-CONCATENATED-KEY TO
                      WS-CACHED-SEGMENT-CKEY(WS-CACHED-DATA-IDX)
      *
           MOVE KEY-A1111111 OF IVPDB2-A1111111(1:10) TO
                                              WS-USED-KEY-ALPHA(1)(1:10)
           .
      *
       A1111111-CACHE-EX.
           EXIT.
      *
      ******************************************************************
      *    WRITE SEGMENT A1111111
      ******************************************************************
      *
       A1111111-WRITE SECTION.
      *
      *
           WRITE A1111111-RECORD FROM C-IVPDB2-A1111111.
      *
       A1111111-WRITE-EX.
           EXIT.
