//IVPDB2L1 JOB (CSIO-CCP-CICSBAT),'DB2 LOAD IVPDB2',MSGCLASS=U,CLASS=A,         
//         NOTIFY=&SYSUID,REGION=0M                                             
/*JOBPARM SYSAFF=SYSD                                                           
//*                                                                             
//*  Licensed Materials - Property of Modern Systems, INC.                      
//*                                                                             
//**********************************************************************        
//* STEP 1: LOAD DB2 DATA FOR SEGMENT A1111111                                  
//**********************************************************************        
//DELDSN1  EXEC PGM=IEFBR14                                                     
//DD01    DD DISP=(MOD,DELETE),DSN=DPR0.A1111111.SYSDISC,                       
//         UNIT=SYSDA,SPACE=(TRK,1)                                             
//*                                                                             
//STEP01 EXEC PGM=AMUUMAIN,                                                     
//   PARM=(DPR0,'A1111111',,,MSGLEVEL(1)),                                      
//   REGION=0M                                                                  
//STEPLIB  DD DISP=SHR,DSN='DB2.DPR0.BMCLOAD'                                   
//         DD DISP=SHR,DSN='DB2.DPR0.DSNEXIT'                                   
//         DD DISP=SHR,DSN='DB2.DPR0.DSNLOAD'                                   
//ABNLIGNR DD DUMMY                                                             
//DSSPRINT DD SYSOUT=*                                                          
//SYSPRINT DD SYSOUT=*                                                          
//UTPRINT  DD SYSOUT=*                                                          
//SYSREC   DD DISP=OLD,DSN=DB2P.PRD.IRISDB.DB2DATA.IVPDB2.A1111111              
//SYSIN    DD  DISP=SHR,DSN=DB2P.MDRNSYS.CNTL.PRD(A1111111)                     
//SYSDISC  DD DSN=DPR0.A1111111.SYSDISC,UNIT=(SYSDA,5),                         
//         DISP=(NEW,CATLG,CATLG),SPACE=(CYL,(10,5),RLSE)                       
//*                                                                             
//*                                                                             
//*  Licensed Materials - Property of Modern Systems, INC.                      
//*                                                                             
//*  (C) Copyright Modern Systems 2018                                          
//*                                                                             
//*                                                                             
//*  IRIS-DB - v. 5.6.2                                                         
//*                                                                             
//*  Date: 2018/2020                                                            
//*                                                                             
//*  Description: Load IMS data for DBD: IVPDB2                                 
