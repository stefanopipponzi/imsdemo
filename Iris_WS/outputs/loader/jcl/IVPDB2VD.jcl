//IVPDB2VD JOB (CSIO-CCP-CICSBAT),'VALIDATE IVPDB2',MSGCLASS=U,CLASS=A,         
//         NOTIFY=&SYSUID                                                       
//*                                                                             
/*JOBPARM SYSAFF=SYSD                                                           
//*                                                                             
//*  Licensed Materials - Property of Modern Systems, INC.                      
//*                                                                             
//*  (C) Copyright Modern Systems 2018                                          
//*                                                                             
//*                                                                             
//*  IRIS-DB - v. 5.6.2                                                         
//*                                                                             
//*  Date: 2018/2020                                                            
//*                                                                             
//*  Description: Data validation for DBD: IVPDB2                               
//*                                                                             
//DELETE EXEC PGM=IEFBR14                                                       
//DD1 DD DSN=DB2P.PRD.DB2DATA.IVPDB2.VALIDATE,DISP=(MOD,DELETE,DELETE),         
// UNIT=SYSDA,SPACE=(CYL,(0))                                                   
//DD2 DD DSN=DB2P.PRD.DB2DATA.IVPDB2.VALIDATS,DISP=(MOD,DELETE,DELETE),         
// UNIT=SYSDA,SPACE=(CYL,(0))                                                   
//DD3 DD DSN=DB2P.PRD.IMSDATA.IVPDB2.VALIDATE,DISP=(MOD,DELETE,DELETE),         
// UNIT=SYSDA,SPACE=(CYL,(0))                                                   
//DD4 DD DSN=DB2P.PRD.IMSDATA.IVPDB2.VALIDATS,DISP=(MOD,DELETE,DELETE),         
// UNIT=SYSDA,SPACE=(CYL,(0))                                                   
//DD5 DD DSN=DB2P.PRD.IVPDB2.DIFFS,DISP=(MOD,DELETE,DELETE),                    
// UNIT=SYSDA,SPACE=(CYL,(0))                                                   
//*                                                                             
//EXPDATA   EXEC PGM=IKJEFT01                                                   
//STEPLIB   DD DSN=DB2.DPR0.DSNEXIT,DISP=SHR                                    
//          DD DSN=DB2.DPR0.DSNLOAD,DISP=SHR                                    
//          DD DSN=CSIOCHP.PGMLIB,DISP=SHR                                      
//SYSOUT    DD SYSOUT=*                                                         
//SYSPRINT  DD SYSOUT=*                                                         
//SYSUDUMP  DD SYSOUT=*                                                         
//SYSTSPRT  DD SYSOUT=*                                                         
//SYSTSIN   DD *                                                                
DSN SYSTEM(DPR0)                                                                
RUN PROGRAM(IVPDB2VD) PLAN(IVPDB2VD) -                                          
 LIB('CSIOCHP.PGMLIB')                                                          
END                                                                             
/*                                                                              
//FILEIN   DD DISP=SHR,DSN=MDST.IMSTODB2.PRD.IVPDB2                             
//FILEODB2 DD DSN=DB2P.PRD.DB2DATA.IVPDB2.VALIDATE,                             
//         DISP=(NEW,CATLG),                                                    
//         DCB=(RECFM=VB,LRECL=62,BLKSIZE=620,DSORG=PS),                        
//         UNIT=CTAPE,LABEL=(1,SL,RETPD=10)                                     
//FILEOIMS DD DSN=DB2P.PRD.IMSDATA.IVPDB2.VALIDATE,                             
//         DISP=(NEW,CATLG),                                                    
//         DCB=(RECFM=VB,LRECL=62,BLKSIZE=620,DSORG=PS),                        
//         UNIT=CTAPE,LABEL=(1,SL,RETPD=10)                                     
//SYSIN    DD *                                                                 
EALL                                                                            
/*                                                                              
//SORTDB2  EXEC PGM=SORT,COND=(0,NE,EXPDATA)                                    
//SORTIN   DD DSN=DB2P.PRD.DB2DATA.IVPDB2.VALIDATE,DISP=SHR                     
//SORTOUT  DD DSN=DB2P.PRD.DB2DATA.IVPDB2.VALIDATS,                             
//         DISP=(NEW,CATLG),                                                    
//         UNIT=CTAPE,LABEL=(1,SL,RETPD=10)                                     
//SYSOUT   DD SYSOUT=*                                                          
//SYSIN    DD *                                                                 
  SORT FIELDS=(5,58,CH,A)                                                       
  OPTION VLSHRT                                                                 
/*                                                                              
//SORTIMS  EXEC PGM=SORT,COND=(0,NE,EXPDATA)                                    
//SORTIN   DD DSN=DB2P.PRD.IMSDATA.IVPDB2.VALIDATE,DISP=SHR                     
//SORTOUT  DD DSN=DB2P.PRD.IMSDATA.IVPDB2.VALIDATS,                             
//         DISP=(NEW,CATLG),                                                    
//         UNIT=CTAPE,LABEL=(1,SL,RETPD=10)                                     
//SYSOUT   DD SYSOUT=*                                                          
//SYSIN    DD *                                                                 
  SORT FIELDS=(5,58,CH,A)                                                       
  OPTION VLSHRT                                                                 
/*                                                                              
//CMPFILES EXEC PGM=SORT,COND=(0,NE,EXPDATA)                                    
//SORTJNF1 DD DSN=DB2P.PRD.DB2DATA.IVPDB2.VALIDATS,DISP=SHR                     
//SORTJNF2 DD DSN=DB2P.PRD.IMSDATA.IVPDB2.VALIDATS,DISP=SHR                     
//SORTOUT  DD DSN=DB2P.PRD.IVPDB2.DIFFS,                                        
//         DISP=(NEW,CATLG),                                                    
//         UNIT=SYSDA,SPACE=(CYL,1)                                             
//SYSOUT   DD SYSOUT=*                                                          
//SYSIN    DD *                                                                 
  JOINKEYS FILE=F1,FIELDS=(5,58,A),SORTED                                       
  JOINKEYS FILE=F2,FIELDS=(5,58,A),SORTED                                       
  JOIN UNPAIRED,ONLY,F1,F2                                                      
  SORT FIELDS=COPY                                                              
  END                                                                           
/*                                                                              
//ANYDIFF  EXEC PGM=IDCAMS                                                      
//SYSPRINT DD SYSOUT=*                                                          
//IN       DD DISP=SHR,DSN=DB2P.PRD.IVPDB2.DIFFS                                
//SYSIN    DD *                                                                 
  PRINT INFILE(IN) DUMP COUNT(1)                                                
  IF LASTCC = 0 THEN SET MAXCC = 12                                             
  IF LASTCC = 4 THEN SET MAXCC = 0                                              
/*                                                                              
