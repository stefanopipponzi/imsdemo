//IVPDB2CV JOB (C000000100),'UNLOAD IVPDB2',                                    
//       CLASS=N,MSGCLASS=X,MSGLEVEL=(1,1),NOTIFY=&SYSUID                       
/*JOBPARM  ROOM=25,S=TSO2                                                       
/*XEQ      TSO2                                                                 
/*ROUTE PRINT MEM0                                                              
//*/*JOBPARM R=06,S=TSO2                                                        
//*                                                                             
//*  Licensed Materials - Property of Modern Systems, INC.                      
//*                                                                             
//*  (C) Copyright Modern Systems 2018                                          
//*                                                                             
//*                                                                             
//*  IRIS-DB - v. 5.6.2                                                         
//*                                                                             
//*  Date: 2018/2020                                                            
//*                                                                             
//*  Description: Convert IMS data for DBD: IVPDB2                              
//*                                                                             
//**********************************************************************        
//*  STEP 1: DELETE PREVIOUS FILES                                              
//**********************************************************************        
//*---------------------------------------------------------------------        
//*  DELETE PREVIOUS FILES                                                      
//*---------------------------------------------------------------------        
//DELCONV  EXEC PGM=IEFBR14                                                     
//SYSUT1   DD  DSN=LZTEST.IVPDB2.SEGDATA.A1111111,                              
//             DISP=(MOD,DELETE,DELETE),SPACE=(1,0)                             
//SYSPRINT DD  SYSOUT=*                                                         
//*                                                                             
//*---------------------------------------------------------------------        
//*  STEP 2: CONVERT IMS DATA FOR DB2 LOAD                                      
//*---------------------------------------------------------------------        
//CONVERT  EXEC  PGM=IVPDB2CV,REGION=256M                                       
//STEPLIB  DD DSN=LZTEST.PGMLIB,DISP=SHR                                        
//SYSOUT   DD SYSOUT=*                                                          
//SYSPRINT DD SYSOUT=*                                                          
//SYSUDUMP DD SYSOUT=X,HOLD=YES                                                 
//SYSDBOUT DD SYSOUT=X,HOLD=YES                                                 
//IVPDB2   DD DISP=SHR,DSN=LZTEST.IVPDB2.SEGDATA                                
//A1111111  DD DSN=LZTEST.IVPDB2.SEGDATA.A1111111,                              
//            DCB=(LRECL=200,RECFM=FB,BLKSIZE=2000),                            
//            DISP=(NEW,CATLG),UNIT=DISK,SPACE=(CYL,(2,1),RLSE)                 
//*                                                                             
