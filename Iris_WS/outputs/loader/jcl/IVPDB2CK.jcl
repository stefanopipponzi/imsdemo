//IVPDB2CK JOB (CSIO-CCP-CICSBAT),'CHECK IVPDB2',                               
//   CLASS=A,MSGCLASS=U,NOTIFY=&SYSUID,MSGLEVEL=(1,1)                           
//*                                                                             
//*  Licensed Materials - Property of Modern Systems, INC.                      
//*                                                                             
//*  (C) Copyright Modern Systems 2018                                          
//*                                                                             
//*                                                                             
//*  IRIS-DB - v. 5.6.2                                                         
//*                                                                             
//*  Date: 2018/2020                                                            
//*                                                                             
//*  Description: Create SQL check indexes for DBD: IVPDB2                      
//*                                                                             
/*JOBPARM SYSAFF=SYSD                                                           
//*                                                                             
//* * * * * * * * * * * * * * * * * * * * * * * * * *                           
//STEP1 EXEC PGM=DSNUTILB,                                                      
//   PARM=(DPR0,B),                                                             
//*                                            ***RESTART***                    
//*  PARM=(DPR0,B,RESTART)                                                      
//   REGION=0M                                                                  
//STEPLIB  DD DISP=SHR,DSN='DB2.DPR0.BMCLOAD'                                   
//         DD DISP=SHR,DSN='DB2.DPR0.DSNEXIT'                                   
//         DD DISP=SHR,DSN='DB2.DPR0.DSNLOAD'                                   
//*--------------------------------------------------------------------         
//*  UTILITY WORK DD STATEMENTS                                                 
//*--------------------------------------------------------------------         
//SYSERR DD UNIT=SYSDA,DISP=(,PASS),SPACE=(CYL,(50,50))                         
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,(250,200))                                  
//SORTWK02 DD UNIT=SYSDA,SPACE=(CYL,(250,200))                                  
//SORTWK03 DD UNIT=SYSDA,SPACE=(CYL,(250,200))                                  
//SORTWK04 DD UNIT=SYSDA,SPACE=(CYL,(250,200))                                  
//SYSUT1 DD UNIT=SYSDA,DISP=(,PASS),SPACE=(CYL,(750,250))                       
//SORTOUT DD UNIT=SYSDA,DISP=(,PASS),SPACE=(CYL,(750,250))                      
//*                                                                             
//SYSPRINT DD SYSOUT=*                                                          
//SYSUDUMP DD SYSOUT=*                                                          
//UTPRINT  DD SYSOUT=*                                                          
//SYSOUT   DD SYSOUT=*                                                          
//*                                                                             
//SYSIN    DD DISP=SHR,DSN=DB2P.MDRNSYS.CNTL.PRD(IVPDB2CK)                      
//*                                                                             
