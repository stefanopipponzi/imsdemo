//IVPDB2TB JOB (CSIO-CCP-CICSBAT),'MDSY',                                       
//       CLASS=4,MSGCLASS=U,MSGLEVEL=(1,1),                                     
//       NOTIFY=&SYSUID                                                         
//*                                                                             
//*  Licensed Materials - Property of Modern Systems, INC.                      
//*                                                                             
//*  (C) Copyright Modern Systems 2018                                          
//*                                                                             
//*                                                                             
//*  IRIS-DB - v. 5.6.2                                                         
//*                                                                             
//*  Date: 2018/2020                                                            
//*                                                                             
//*  Description: Create SQL tables for DBD: IVPDB2                             
//*                                                                             
/*JOBPARM SYSAFF=SYSD                                                           
//*                                                                             
//*                                                                             
//IVPDB2TB EXEC PGM=IKJEFT01                                                    
//STEPLIB DD DISP=SHR,DSN=DB2.DPR0.DSNEXIT                                      
//        DD DISP=SHR,DSN=DB2.DPR0.DSNLOAD                                      
//ABNLIGNR DD DUMMY                                                             
//SYSTERM  DD SYSOUT=*                                                          
//SYSPRINT DD SYSOUT=*                                                          
//SYSTSPRT DD SYSOUT=*                                                          
//SYSUDUMP DD SYSOUT=*                                                          
//SYSTSIN  DD *                                                                 
 DSN SYSTEM(DPR0)                                                               
 RUN  PROGRAM(DSNTEP2) PLAN(DSNTEP2) -                                          
      LIB('DB2.DPR0.RUNLIB.LOAD') PARMS('/ALIGN(LHS)')                          
 END                                                                            
//*                                                                             
//SYSIN    DD DISP=SHR,DSN=DB2P.MDRNSYS.CNTL.PRD(IVPDB2TB)                      
//         DD DISP=SHR,DSN=DB2P.MDRNSYS.CNTL.PRD(IVPDB2IX)                      
//*                                                                             
//**                                                                            
