//IVPDB2IX JOB (CSIO-CCP-CICSBAT),'CHECK IVPDB2',                               
//   CLASS=A,MSGCLASS=U,NOTIFY=&SYSUID,MSGLEVEL=(1,1)                           
//*                                                                             
//*  Licensed Materials - Property of Modern Systems, INC.                      
//*                                                                             
//*  (C) Copyright Modern Systems 2018                                          
//*                                                                             
//*                                                                             
//*  IRIS-DB - v. 5.6.2                                                         
//*                                                                             
//*  Date: 2018/2020                                                            
//*                                                                             
//*  Description: Create SQL indexes for DBD: IVPDB2                            
//*                                                                             
/*JOBPARM SYSAFF=SYSD                                                           
//*                                                                             
//* * * * * * * * * * * * * * * * * * * * * * * * * *                           
//STEP1 EXEC PGM=DSNUTILB,                                                      
//   PARM=(DPR0,B),                                                             
//*                                            ***RESTART***                    
//*  PARM=(DPR0,B,RESTART)                                                      
//   REGION=0M                                                                  
//*                                                                             
//*                                                                             
//IVPDB2IX EXEC PGM=IKJEFT01                                                    
//STEPLIB DD DISP=SHR,DSN=DB2.DPR0.DSNEXIT                                      
//        DD DISP=SHR,DSN=DB2.DPR0.DSNLOAD                                      
//ABNLIGNR DD DUMMY                                                             
//SYSTERM  DD SYSOUT=*                                                          
//SYSPRINT DD SYSOUT=*                                                          
//SYSTSPRT DD SYSOUT=*                                                          
//SYSUDUMP DD SYSOUT=*                                                          
//SYSTSIN  DD *                                                                 
 DSN SYSTEM(DPR0)                                                               
 RUN  PROGRAM(DSNTEP2) PLAN(DSNTEP2) -                                          
      LIB('DB2.DPR0.RUNLIB.LOAD') PARMS('/ALIGN(LHS)')                          
 END                                                                            
//*                                                                             
//SYSIN    DD DISP=SHR,DSN=DB2P.MDRNSYS.CNTL.PRD(IVPDB2IX)                      
//*                                                                             
