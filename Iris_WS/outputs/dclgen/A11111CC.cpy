      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: DCLGEN FOR TABLE: IVPDB2_A1111111
      *
      ******************************************************************

      ******************************************************************
      * COBOL DCLGEN FOR SEGMENT: A1111111
      ******************************************************************

      *
       01 C-IVPDB2-A1111111.
0001     03 C-KEY-A1111111                      PIC X(10).
0011     03 C-FIRST-NAME                        PIC X(10).
0021     03 C-EXTENSION                         PIC X(10).
0031     03 C-ZIP-CODE                          PIC X(7).
0038     03 C-A1111111-FILLER-1                 PIC X(3).
0041     03 C-CREATION-TS                       PIC X(26).
0067     03 C-LAST-UPDATE-TS                    PIC X(26).
0093     03 C-CREATION-USER-I.
0093       49 C-CREATION-USER-I-LEN             PIC S9(4) COMP.
0095       49 C-CREATION-USER-I-TXT             PIC X(25).
0120     03 C-LAST-UPDATE-USER-I.
0120       49 C-LAST-UPDATE-USER-I-LEN          PIC S9(4) COMP.
0122       49 C-LAST-UPDATE-USER-I-TXT          PIC X(25).
0147     03 C-CREATION-TRAN-I.
0147       49 C-CREATION-TRAN-I-LEN             PIC S9(4) COMP.
0149       49 C-CREATION-TRAN-I-TXT             PIC X(25).
0174     03 C-LAST-UPDATE-TRAN-I.
0174       49 C-LAST-UPDATE-TRAN-I-LEN          PIC S9(4) COMP.
0176       49 C-LAST-UPDATE-TRAN-I-TXT          PIC X(25).
      *
