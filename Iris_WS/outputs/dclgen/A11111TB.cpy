      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: DCLGEN FOR TABLE: IVPDB2_A1111111
      *
      ******************************************************************

      ******************************************************************
      * COBOL DCLGEN FOR SEGMENT: A1111111
      ******************************************************************

       01 IVPDB2-A1111111.
0001     03 KEY-A1111111                      PIC X(10).
0011     03 FIRST-NAME                        PIC X(10).
0021     03 EXTENSION                         PIC X(10).
0031     03 ZIP-CODE                          PIC X(7).
0038     03 A1111111-FILLER-1                 PIC X(3).
0041     03 CREATION-TS                       PIC X(26).
0067     03 LAST-UPDATE-TS                    PIC X(26).
0093     03 CREATION-USER-I.
0093       49 CREATION-USER-I-LEN             PIC S9(4) COMP.
0095       49 CREATION-USER-I-TXT             PIC X(25).
0120     03 LAST-UPDATE-USER-I.
0120       49 LAST-UPDATE-USER-I-LEN          PIC S9(4) COMP.
0122       49 LAST-UPDATE-USER-I-TXT          PIC X(25).
0147     03 CREATION-TRAN-I.
0147       49 CREATION-TRAN-I-LEN             PIC S9(4) COMP.
0149       49 CREATION-TRAN-I-TXT             PIC X(25).
0174     03 LAST-UPDATE-TRAN-I.
0174       49 LAST-UPDATE-TRAN-I-LEN          PIC S9(4) COMP.
0176       49 LAST-UPDATE-TRAN-I-TXT          PIC X(25).
