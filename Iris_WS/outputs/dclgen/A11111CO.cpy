      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: DCLGEN FOR TABLE: IVPDB2_A1111111
      *
      ******************************************************************

      ******************************************************************
      * COBOL DCLGEN FOR SEGMENT: A1111111
      ******************************************************************

       01 O-IVPDB2-A1111111.
0001     03 O-KEY-A1111111                      PIC X(10).
0011     03 O-FIRST-NAME                        PIC X(10).
0021     03 O-EXTENSION                         PIC X(10).
0031     03 O-ZIP-CODE                          PIC X(7).
0038     03 O-A1111111-FILLER-1                 PIC X(3).
0041     03 O-CREATION-TS                       PIC X(26).
0067     03 O-LAST-UPDATE-TS                    PIC X(26).
0093     03 O-CREATION-USER-I.
0093       49 O-CREATION-USER-I-LEN             PIC S9(4) COMP.
0095       49 O-CREATION-USER-I-TXT             PIC X(25).
0120     03 O-LAST-UPDATE-USER-I.
0120       49 O-LAST-UPDATE-USER-I-LEN          PIC S9(4) COMP.
0122       49 O-LAST-UPDATE-USER-I-TXT          PIC X(25).
0147     03 O-CREATION-TRAN-I.
0147       49 O-CREATION-TRAN-I-LEN             PIC S9(4) COMP.
0149       49 O-CREATION-TRAN-I-TXT             PIC X(25).
0174     03 O-LAST-UPDATE-TRAN-I.
0174       49 O-LAST-UPDATE-TRAN-I-LEN          PIC S9(4) COMP.
0176       49 O-LAST-UPDATE-TRAN-I-TXT          PIC X(25).
