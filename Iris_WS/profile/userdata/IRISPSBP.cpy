      ******************************************************************        
      *                                                                         
      * Copyright (c) 2018 by Modern Systems, Inc.                              
      * All rights reserved.                                                    
      *                                                                         
      ******************************************************************        
      * IRIS-DB - v. 5.6.2
      ******************************************************************        
      * Year: 2020
      ******************************************************************        
      *                                                                         
      * Description: STATIC PSBs DEFINITION 
      *                                                                         
      ******************************************************************        
      * 
      *   PSB POINTERS SETUP
      * 
           SET IRISADDR-RTN TO TRUE
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                  PSB-DFSIVA34-PCBS-AREA WS-PTR
           SET TAB-PSB-PCBS-PTR(1) TO WS-PTR
           CALL IRIS-WS-RTN USING IRIS-WORK-AREA
                                  PSB-DFSIVA34-SENSEGS-AREA WS-PTR
           SET TAB-PSB-SENSEGS-PTR(1) TO WS-PTR
