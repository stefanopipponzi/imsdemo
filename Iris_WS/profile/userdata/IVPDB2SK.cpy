      ******************************************************************        
      *                                                                         
      * Copyright (c) 2018 by Modern Systems, Inc.                              
      * All rights reserved.                                                    
      *                                                                         
      ******************************************************************        
      * IRIS-DB - v. 5.6.2 - rv. 5.6.2
      ******************************************************************        
      * Year: 2020
      ******************************************************************        
      *                                                                         
      * Description: CUSTOM SQL RULES FOR DBD IVPDB2 
      *                                                                         
      ******************************************************************        
       01 MEM-SSA-KEYS.
         03 FILLER                      PIC S9(4) COMP VALUE 4.
         03 FILLER                      PIC S9(9) COMP VALUE 5.
         03 FILLER                      PIC S9(4) COMP VALUE 11.
         03 FILLER                      PIC X(120)     VALUE
            'D:A1111111:'.
         03 FILLER                      PIC X(480)     VALUE SPACE.
         03 FILLER                      PIC S9(9) COMP VALUE 4.
         03 FILLER                      PIC S9(4) COMP VALUE 11.
         03 FILLER                      PIC X(120)     VALUE
            'R:A1111111:'.
         03 FILLER                      PIC X(480)     VALUE SPACE.
         03 FILLER                      PIC S9(9) COMP VALUE 1.
         03 FILLER                      PIC S9(4) COMP VALUE 11.
         03 FILLER                      PIC X(120)     VALUE
            'I:A1111111:'.
         03 FILLER                      PIC X(480)     VALUE SPACE.
         03 FILLER                      PIC S9(9) COMP VALUE 2.
         03 FILLER                      PIC S9(4) COMP VALUE 22.
         03 FILLER                      PIC X(120)     VALUE
            'G:A1111111:A1111111:=:'.
         03 FILLER                      PIC X(480)     VALUE SPACE.
       01 TAB-MEM-SSA-KEYS REDEFINES MEM-SSA-KEYS.
         03 TAB-MEM-SSA-COUNT           PIC S9(4) COMP.
         03 FILLER                      OCCURS 4.
           05 TAB-MEM-SSA-FUNCID        PIC S9(9) COMP.
           05 TAB-MEM-SSA-LEN           PIC S9(4) COMP.
           05 TAB-MEM-SSA-TXT           PIC X(600).
