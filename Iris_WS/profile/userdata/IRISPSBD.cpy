      ******************************************************************        
      *                                                                         
      * Copyright (c) 2018 by Modern Systems, Inc.                              
      * All rights reserved.                                                    
      *                                                                         
      ******************************************************************        
      * IRIS-DB - v. 5.6.2
      ******************************************************************        
      * Year: 2020
      ******************************************************************        
      *                                                                         
      * Description: STATIC PSBs DEFINITION 
      *                                                                         
      ******************************************************************        
      * 
      *   PSB PCBs FOR : DFSIVA34
      * 
       01 PSB-DFSIVA34-PCBS-AREA.
         03 PSB-DFSIVA34-PCBS-COUNT PIC S9(4) COMP    VALUE 2.
         03 PSB-DFSIVA34-IS-CMPAT   PIC 9             VALUE 1.
         03 PSB-DFSIVA34-PCBS-TAB.
           05 MEMORY-PCB-AREA-01.
      *      07 ROOT-SEGM           PIC S9(9) COMP    VALUE ZERO.
      *      07 PHYS-ROOT-SEGM      PIC S9(9) COMP    VALUE ZERO.
      *      07 CURR-SEGM           PIC S9(9) COMP    VALUE ZERO.
      *      07 PARENTAGE           PIC S9(9) COMP    VALUE ZERO.
             07 FILLER              PIC X(16)         VALUE LOW-VALUE.
             07 POSITION-STATUS     PIC 9             VALUE ZERO.
             07 PSB-PCB.
               09 PSB-NAME          PIC X(8)          VALUE 'DFSIVA34'.
               09 PSB-PROGRESSIVE   PIC S9(4) COMP    VALUE 2.
               09 PCB-NAME          PIC X(8)          VALUE SPACE.
               09 PCB-TYPE          PIC S9(4) COMP    VALUE 1050.
               09 DBD-NAME          PIC X(8)          VALUE 'PCB_IO  '.
               09 PCB-KEY-LENGTH    PIC S9(4) COMP    VALUE 0.
               09 COPIES            PIC S9(4) COMP    VALUE 0.
               09 PCB-PROC-OPTS     PIC X(4)          VALUE SPACE.
               09 PCB-PROC-SEQ      PIC X(8)          VALUE SPACE.
               09 PCB-POS           PIC S9(4) COMP    VALUE 0.
      *        09 PCB-ALTRESP       PIC S9(4) COMP    VALUE 0.
      *        09 PCB-SAMETRM       PIC S9(4) COMP    VALUE 0.
      *        09 PCB-MODIFY        PIC S9(4) COMP    VALUE 0.
      *        09 PCB-EXPRESS       PIC S9(4) COMP    VALUE 0.
      *        09 PCB-LIST          PIC S9(4) COMP    VALUE 0.
             07 FILLER              PIC X(10)         VALUE LOW-VALUE.
             07 LAST-KEY-SEC        PIC X(256).
             07 SAVE-LEVEL          PIC S9(9) COMP    VALUE ZERO.
             07 NUM-SENSEG          PIC S9(9) COMP    VALUE 0.
             07 PHYS-NUM-SENSEG     PIC S9(9) COMP    VALUE 0.
             07 ADD-NUM-SENSEG      PIC S9(9) COMP    VALUE 1.
      *      07 PSB-SENSEG-PTR      POINTER.
      *      07 DBD-SEGM-PTR        POINTER.
      *      07 INFO-PTR            POINTER.
             07 FILLER              PIC X(12)         VALUE LOW-VALUE.
             07 NUM-FIELDS          PIC S9(9) COMP    VALUE 0.
             07 DBD-FIELD-PTR       POINTER.
             07 SECONDARY-KEY       PIC S9(9) COMP    VALUE 0.
             07 LAST-PRESENTED      PIC X(8)          VALUE SPACE.
           05 MEMORY-PCB-AREA-02.
      *      07 ROOT-SEGM           PIC S9(9) COMP    VALUE ZERO.
      *      07 PHYS-ROOT-SEGM      PIC S9(9) COMP    VALUE ZERO.
      *      07 CURR-SEGM           PIC S9(9) COMP    VALUE ZERO.
      *      07 PARENTAGE           PIC S9(9) COMP    VALUE ZERO.
             07 FILLER              PIC X(16)         VALUE LOW-VALUE.
             07 POSITION-STATUS     PIC 9             VALUE ZERO.
             07 PSB-PCB.
               09 PSB-NAME          PIC X(8)          VALUE 'DFSIVA34'.
               09 PSB-PROGRESSIVE   PIC S9(4) COMP    VALUE 2.
               09 PCB-NAME          PIC X(8)          VALUE SPACE.
               09 PCB-TYPE          PIC S9(4) COMP    VALUE 1052.
               09 DBD-NAME          PIC X(8)          VALUE 'IVPDB2  '.
               09 PCB-KEY-LENGTH    PIC S9(4) COMP    VALUE 10.
               09 COPIES            PIC S9(4) COMP    VALUE 0.
               09 PCB-PROC-OPTS     PIC X(4)          VALUE 'A   '.
               09 PCB-PROC-SEQ      PIC X(8)          VALUE SPACE.
               09 PCB-POS           PIC S9(4) COMP    VALUE 1054.
      *        09 PCB-ALTRESP       PIC S9(4) COMP    VALUE 0.
      *        09 PCB-SAMETRM       PIC S9(4) COMP    VALUE 0.
      *        09 PCB-MODIFY        PIC S9(4) COMP    VALUE 0.
      *        09 PCB-EXPRESS       PIC S9(4) COMP    VALUE 0.
      *        09 PCB-LIST          PIC S9(4) COMP    VALUE 0.
             07 FILLER              PIC X(10)         VALUE LOW-VALUE.
             07 LAST-KEY-SEC        PIC X(256).
             07 SAVE-LEVEL          PIC S9(9) COMP    VALUE ZERO.
             07 NUM-SENSEG          PIC S9(9) COMP    VALUE 1.
             07 PHYS-NUM-SENSEG     PIC S9(9) COMP    VALUE 1.
             07 ADD-NUM-SENSEG      PIC S9(9) COMP    VALUE 1.
      *      07 PSB-SENSEG-PTR      POINTER.
      *      07 DBD-SEGM-PTR        POINTER.
      *      07 INFO-PTR            POINTER.
             07 FILLER              PIC X(12)         VALUE LOW-VALUE.
             07 NUM-FIELDS          PIC S9(9) COMP    VALUE 1.
             07 DBD-FIELD-PTR       POINTER.
             07 SECONDARY-KEY       PIC S9(9) COMP    VALUE 0.
             07 LAST-PRESENTED      PIC X(8)          VALUE SPACE.
      *
      *   PSB SENSEGSs FOR : DFSIVA34
      *
       01 PSB-DFSIVA34-SENSEGS-AREA.
         03 PSB-DFSIVA34-SENSEGS-COUNT PIC S9(4) COMP VALUE 1.
         03 PSB-DFSIVA34-TAB.
      * POS: 1
           05 MEMORY-PCB-02-SENSEG-01.
             07 SEGMENT-NAME        PIC X(8)          VALUE 'A1111111'.
             07 PARENT-SEGMENT-NAME PIC X(8)          VALUE SPACE.
      * 
      *   PSB POINTERS DECLARATION
      * 
       01 PSBS-COUNT                PIC S9(4) COMP    VALUE 1.
       01 PSBS-POINTERS.
         03 TAB-PSBS-POINTERS.
           05 FILLER                PIC X(8)          VALUE 'DFSIVA34'.
      *    05 FILLER POINTER.
      *    05 FILLER POINTER.
           05 FILLER                PIC X(8).
         03 FILLER REDEFINES TAB-PSBS-POINTERS OCCURS 1.
           05 TAB-PSB-NAME          PIC X(8).
           05 TAB-PSB-PCBS-PTR      POINTER.
           05 TAB-PSB-SENSEGS-PTR   POINTER.
