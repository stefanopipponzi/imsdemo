      ******************************************************************
      *
      * Copyright (c) 2018 by Modern Systems, Inc.
      * All rights reserved.
      *
      ******************************************************************
      * IRIS-DB - v. 5.6.2
      ******************************************************************
      * Date: 2018/2020
      ******************************************************************
      *
      * Description: USER CUSTOM COPYBOOK FOR DBD: IVPDB2
      *
      ******************************************************************
           EVALUATE IRIS-CUSTOM-FUNC-ID
           WHEN 0
             SET IRIS-ERR-FUNCTION-NOT-FOUND TO TRUE
           COPY IVPDB2SM.
      * IRISDB - Segm: A1111111, Op: ISRT, RevId: 1
           WHEN 1
             SET SQL-INSERT TO TRUE
      * IRISDB - Segm: A1111111, Op: GU, RevId: 2
           WHEN 2
      * IRISDB - Segm: A1111111, Op: GHU, RevId: 3
           WHEN 3
             IF IRIS-FUNC-GU OR IRIS-FUNC-GHU
             OR WS-SEGMENT-NAME NOT =
               IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
               MOVE IRIS-KEYVALUE(1, 1)(1:10) TO
                      IO-RTN-USED-KEY-ALPHA(1)(1:10)
               SET SQL-SELECT-PRIMARY TO TRUE
             ELSE
               SET SKIP-SEGMENT-ACCESS TO TRUE
               SET IRIS-SQL-NOT-FOUND TO TRUE
               MOVE IRIS-DB-SQLCODE TO IRIS-SQLCODE
               GO TO SKIP-FUNCTION
             END-IF
      * IRISDB - Segm: A1111111, Op: REPL, RevId: 4
           WHEN 4
             IF IRIS-FUNC-GU OR IRIS-FUNC-GHU
             OR WS-SEGMENT-NAME NOT =
               IO-RTN-USED-LAST-SEGMENT(A1111111-LVL)
               SET SQL-SELECT-SEEK TO TRUE
             ELSE
               SET SQL-SELECT-NEXT TO TRUE
             END-IF
             SET SQL-UPDATE TO TRUE
      * IRISDB - Segm: A1111111, Op: DLET, RevId: 5
           WHEN 5
             SET SQL-DELETE TO TRUE
           WHEN OTHER
             SET IRIS-ERR-FUNCTION-NOT-FOUND TO TRUE
           END-EVALUATE
