//[JOBNAME] JOB (C000000100),'UNLOAD [DBDNAME]',
//       CLASS=N,MSGCLASS=X,MSGLEVEL=(1,1),NOTIFY=&SYSUID
/*JOBPARM  ROOM=25,S=TSO2
/*XEQ      TSO2
/*ROUTE PRINT MEM0
//*/*JOBPARM R=06,S=TSO2
//*
//*  Licensed Materials - Property of Modern Systems, INC.
//*
//*  (C) Copyright Modern Systems 2018
//*
//*
//*  [PRODUCT_NAME] - v. [PRODUCT_VERSION]
//*
//*  Date: 2018/2020
//*
//*  Description: [DESCRIPTION]
//*